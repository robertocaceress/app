package com.example.rcksuporte05.rcksistemas.activity;

import android.content.Intent;
import android.os.Handler;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.ProdutoDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ImagemProdutoAdapter;
import com.example.rcksuporte05.rcksistemas.model.Produto;
import com.example.rcksuporte05.rcksistemas.model.ProdutoFoto;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.Utilitaria;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class ActivityProdutoImagem extends AppCompatActivity {
    @BindView(R.id.toolbar_pager_produto_img)
    Toolbar toolbar;

    private DBHelper db = new DBHelper(this);
    private ViewPager mViewPager;
    private ImagemProdutoAdapter mViewPagerAdapter;
    private ArrayList<ProdutoFoto>  mContents;
    private ProdutoDAO produtoDAO = new ProdutoDAO(db);

    private static  int currentPage = 0;
    private static  int numPage = 0;
    private ActionMode actionMode;
    String id_produto;
    String nome_produto;
    float saldo_estoque;
    Utilitaria util = new Utilitaria();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_produto_image);
        ButterKnife.bind(this);
        id_produto     = getIntent().getExtras().getString("id_produto");
        nome_produto   = getIntent().getExtras().getString("nome_produto");
        saldo_estoque  = getIntent().getExtras().getFloat("saldo_estoque");
        int acao       = getIntent().getExtras().getInt("acao");
        int position   = getIntent().getExtras().getInt("position");
        mViewPager = (ViewPager) findViewById(R.id.view_pager_produto_img);

        mContents = new ArrayList<>();
        List<ProdutoFoto> listaProdutoFoto;

        listaProdutoFoto = produtoDAO.getListaFotos("SELECT FOTO_MINIATURA, FOTO_ARQUIVO, ID_FOTO FROM TBL_PRODUTO_FOTO WHERE ID_PRODUTO = '" + id_produto + "' AND ATIVO = 'S' ORDER BY FOTO_PRINCIPAL DESC");
        for ( int i = 0 ; i < listaProdutoFoto.size(); i++)
            mContents.add(new ProdutoFoto(listaProdutoFoto.get(i).getId_foto(), listaProdutoFoto.get(i).getImages()));


        mViewPagerAdapter = new ImagemProdutoAdapter( mContents, this);
        mViewPager.setPageTransformer(true, new ViewPagerStack());

        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(mViewPagerAdapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.circle_view_produto_img);
        indicator.setViewPager(mViewPager);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){

            @Override
            public void onPageScrolled(int i, float v, int i1) {
                int PageScrolled = i1;
            }

            @Override
            public void onPageSelected(int i) {
                int PageSelec = i;
                mViewPagerAdapter.setImagemListada(i);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                int PageScroll = state;
                if ( state == ViewPager.SCROLL_STATE_IDLE) {
                    int pageCount = mContents.size();//;images.length;
                }
            }
        });

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Voltar");

        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setTitle("Imagem(s) do produto");
        if ( (acao) == 1 || (acao) == 2 ) {
            getSupportActionBar().hide();
            enableActionMode(position);
        }
        final Handler handler = new Handler();
    }

    private class ViewPagerStack implements ViewPager.PageTransformer {
        @Override
        public void transformPage(@NonNull View page, float position) {
            if ( position >= 0) {
                page.setScaleX( 0.7f - 0.05f * position);
                page.setScaleY(0.7f);
                page.setTranslationX( -page.getWidth() * position);
                page.setTranslationY(-48 * position);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                System.gc();
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public void enableActionMode(final int position) {
        if (actionMode == null) {
            actionMode = startActionMode(new ActionMode.Callback() {
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    mode.getMenuInflater().inflate(R.menu.menu_action_mode_produtos_pedido_imagem, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case android.R.id.home:
                            System.gc();
                            //Intent intent = new Intent(ActivityViewProdutoImage.this, ActivityProduto.class);
                            startActivity(new Intent(ActivityProdutoImagem.this, ActivityProdutoDrawer.class));
                            finish();
                        case R.id.action_continua_pedido_imagem:
                            Boolean produtoRepetido = false;
                            Boolean controlaEstoque = false;

                            if (PedidoHelper.getListaWebPedidoItens() != null) {
                                for (final WebPedidoItens webPedidoItens : PedidoHelper.getListaWebPedidoItens())
                                    if (webPedidoItens.getId_produto().trim().equals(id_produto.trim())) {
                                        produtoRepetido = true;
                                        break;
                                    }

                            }

                            if ( !produtoRepetido ) {
                                String SQL = "";
                                if ( ClienteHelper.getCliente() != null)
                                    SQL = "SELECT P.*,  TI.VALOR_UNITARIO AS PRECO_TABELA_CLIENTE, TC.NOME_TABELA AS NOME_TABELA FROM TBL_PRODUTO AS P " +
                                            " LEFT JOIN  TBL_TABELA_PRECO_ITENS AS TI ON TI.ID_PRODUTO = P.ID_PRODUTO  AND TI.ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco() +
                                            " LEFT JOIN TBL_TABELA_PRECO_CAB   AS TC ON TC.ID_TABELA = TI.ID_TABELA " +
                                            " WHERE P.ID_PRODUTO = '" + id_produto + "'";
                                else
                                    SQL = "SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + id_produto + "'";

                                List<Produto> listaProduto = produtoDAO.getLista(SQL,"");
                                if (listaProduto.size() > 0)
                                    try {
                                        if (listaProduto.get(0).getTravar_fat_sem_estoque().equalsIgnoreCase("S"))
                                            controlaEstoque = true;
                                        else
                                            controlaEstoque = false;
                                    } catch (Exception e) {
                                    }
                                if (controlaEstoque) {
                                    if (saldo_estoque > 0) {
                                        ViewGroup viewGroup = ActivityProdutoImagem.this.findViewById(android.R.id.content);
                                        View dialogView = LayoutInflater.from(ActivityProdutoImagem.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProdutoImagem.this);
                                        builder.setView(dialogView);
                                        AlertDialog alertDialog = builder.create();
                                        alertDialog.setCancelable(false);

                                        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
                                        textView.setText("Deseja enviar o produto " + nome_produto + " para o pedido?");

                                        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
                                        btnNao.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog.dismiss();
                                            }
                                        });
                                        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
                                        btnSim.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog.dismiss();
                                                PedidoHelper.setProduto(listaProduto.get(0));
                                                Intent intent = new Intent(ActivityProdutoImagem.this, ActivityProdutoPedido.class);
                                                startActivityForResult(intent, position);
                                                setResult(RESULT_OK, intent);
                                                finish();
                                            }
                                        });
                                        alertDialog.show();
                                        break;
                                    } else {
                                        util.showMsgAlerta("O produto " + nome_produto + " está com saldo em estoque zerado e por isso não pode ser lançado no pedido", ActivityProdutoImagem.this);
                                        break;
                                    }
                                } else {
                                    if (!produtoRepetido) {
                                        //List<Produto> listaProduto = db.listaProduto("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + id_produto + "' ");
                                        PedidoHelper.setProduto(listaProduto.get(0));
                                        Intent it = new Intent(ActivityProdutoImagem.this, ActivityProdutoPedido.class);
                                        startActivityForResult(it, position);
                                        setResult(RESULT_OK, it);
                                        finish();
                                        break;
                                    }
                                }
                            } else {
                                util.showMsgAlerta("O produto " + nome_produto + " já esta nesse pedido e não pode ser lançado novamente, somente alterado!", ActivityProdutoImagem.this);
                                break;
                            }
                    }
                    return true;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode.finish();
                    actionMode = null;
                    finish();
                }
            });
        }
    }
}
