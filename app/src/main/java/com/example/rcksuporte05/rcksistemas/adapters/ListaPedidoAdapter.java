package com.example.rcksuporte05.rcksistemas.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.viewHolder.PedidoViewHolder;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RCK 03 on 01/12/2017.
 */

public class ListaPedidoAdapter extends RecyclerView.Adapter<PedidoViewHolder> {
    private List<WebPedido> pedidos;
    private PedidoAdapterListener listener;
    private SparseBooleanArray selectedItems;
    private Activity activity;

    public ListaPedidoAdapter(List<WebPedido> pedidos, PedidoAdapterListener listener, Activity activity) {
        this.pedidos = pedidos;
        this.selectedItems = new SparseBooleanArray();
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public PedidoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pedido_lista_new, parent, false);
        return new PedidoViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(PedidoViewHolder holder, int position) {
        holder.btnLinhaTempo.setVisibility(View.GONE);
        if ( pedidos.get(position).getId_web_pedido_servidor() != null && !pedidos.get(position).getId_web_pedido_servidor().equals("")) {
            holder.txtIdPedido.setText("Nº " + MascaraUtil.numeroZeros(pedidos.get(position).getId_web_pedido_servidor(), 5));
            holder.btnExcluir.setVisibility(View.GONE);
            holder.btnEnviar.setVisibility(View.GONE);
            holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_pendente));
            holder.txtStatus.setText("");
            //holder.lyLinhaTempo.setVisibility(View.VISIBLE);
            holder.btnLinhaTempo.setVisibility(View.VISIBLE);
            try {
                if (pedidos.get(position).getAnalise_credito().equalsIgnoreCase("B") || pedidos.get(position).getExcluido().equalsIgnoreCase("S")) {
                    holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_excluido_negado));
                    if ( pedidos.get(position).getAnalise_credito().equalsIgnoreCase("B"))
                        holder.txtStatus.setText("Crédito negado".toUpperCase());
                    else if( pedidos.get(position).getExcluido().equalsIgnoreCase("S"))
                        holder.txtStatus.setText("Cancelado/Excluido".toUpperCase());
                } else {
                    try {
                        if ( Integer.parseInt(pedidos.get(position).getId_nota_fiscal()) > 0)  {
                            holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_faturado)); //mudado para cor_pedido_autorizado-faturado
                            holder.txtStatus.setText("Faturado".toUpperCase());
                        } else {
                            holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_analise));
                            holder.txtStatus.setText("Em ánalise".toUpperCase());
                        }
                    } catch ( NullPointerException e ) {
                        holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_analise));
                        holder.txtStatus.setText("Em ánalise".toUpperCase());
                    } catch ( Exception e) {
                        holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_analise));
                        holder.txtStatus.setText("Em ánalise".toUpperCase());
                    }
                }

            } catch ( NullPointerException e ) {
                holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_analise));
                holder.txtStatus.setText("Em ánalise".toUpperCase());
            } catch ( Exception e) {
                holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_analise));
                holder.txtStatus.setText("Em ánalise".toUpperCase());
            }

        } else {
            holder.txtStatus.setVisibility(View.INVISIBLE);
            holder.btnLinhaTempo.setVisibility(View.INVISIBLE);
            //holder.btnLinhaTempo.setVisibility(View.INVISIBLE);
            try {
                if (pedidos.get(position).getPedido_enviado().equalsIgnoreCase("N") && (pedidos.get(position).getId_web_pedido_servidor() != null && !pedidos.get(position).getId_web_pedido_servidor().equals("")))
                    holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_atencao));
                else
                    holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_pendente));
                if ( pedidos.get(position).getDescontoIndevido().equalsIgnoreCase("S"))
                    holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_atencao));
            } catch ( NullPointerException e) {
                holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_pendente));
            } catch ( Exception e) {
                holder.cor.setBackground(activity.getDrawable(R.drawable.cor_pedido_pendente));
            }
            holder.txtIdPedido.setText("Nº " + MascaraUtil.numeroZeros(pedidos.get(position).getId_web_pedido(), 5));
        }

        if (pedidos.get(position).getFinalizado() != null && !pedidos.get(position).getFinalizado().equals("S")) {
            holder.btnEnviar.setVisibility(View.GONE);
            holder.btnCompartilhar.setVisibility(View.GONE);
        }

        if ( pedidos.get(position).getCadastro().getId_cadastro() == 0) {
            holder.txtNomeCliente.setText(pedidos.get(position).getNome_extenso());
            holder.txtFantasiaCliente.setText("-");
            holder.btnEnviar.setVisibility(View.GONE);
            holder.btnDuplic.setVisibility(View.GONE);
            holder.btnCompartilhar.setVisibility(View.VISIBLE);
            holder.txtNomeCliente.setTextColor(Color.RED);
        } else {
            holder.txtNomeCliente.setText(pedidos.get(position).getCadastro().getNome_cadastro());
            holder.txtFantasiaCliente.setText(pedidos.get(position).getCadastro().getNome_fantasia());
            if ( pedidos.get(position).getId_web_pedido_servidor() != null && !pedidos.get(position).getId_web_pedido_servidor().equals("")) {
                holder.btnEnviar.setVisibility(View.INVISIBLE);
            } else {
                holder.btnEnviar.setVisibility(View.VISIBLE);
            }
            holder.btnDuplic.setVisibility(View.VISIBLE);
            holder.btnCompartilhar.setVisibility(View.VISIBLE);
            holder.txtNomeCliente.setTextColor(Color.parseColor("#000000"));
        }

        if (pedidos.get(position).getValor_total() != null)
            holder.txtPrecoPedido.setText(MascaraUtil.mascaraReal(pedidos.get(position).getValor_total()));
        else
            holder.txtPrecoPedido.setText("");

        try {
            holder.txtDataEmissaoPedido.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(pedidos.get(position).getData_emissao())));
            holder.txtDataEntrega.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(pedidos.get(position).getData_prev_entrega())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.itemView
               .setBackgroundColor(selectedItems.get(position) ? Color.parseColor("#dfdfdf")
                        : Color.TRANSPARENT);
        DBHelper db = new DBHelper(activity);
        holder.txtOperacao.setText(db.consultaDados("SELECT NOME_OPERACAO FROM TBL_OPERACAO_ESTOQUE WHERE ID_OPERACAO = " + pedidos.get(position).getId_operacao() + ";", "NOME_OPERACAO"));
        if (!pedidos.get(position).getId_condicao_pagamento().equals("0"))
            holder.txtCondicaoPagamento.setText(db.consultaDados("SELECT * FROM TBL_CONDICOES_PAG_CAB WHERE ID_CONDICAO = " + pedidos.get(position).getId_condicao_pagamento() + ";", "NOME_CONDICAO"));
        else
            holder.txtCondicaoPagamento.setText("");
        try {
            if (!pedidos.get(position).getId_moeda_padrao().equals("0"))
                holder.txtMoeda.setText(db.consultaDados("SELECT * FROM TBL_MOEDA WHERE ID = " + pedidos.get(position).getId_moeda_padrao() + ";", "NOME_MOEDA"));
            else
                holder.txtMoeda.setText("");
        } catch ( NullPointerException e) {
            holder.txtMoeda.setText("");
        }catch (Exception e) {
            holder.txtMoeda.setText("");
        }
        try {
            if (pedidos.get(position).getFinalizado().equals("N") || pedidos.get(position).getDescontoIndevido().equalsIgnoreCase("S")) {
                if (pedidos.get(position).getDescontoIndevido().equalsIgnoreCase("S")) {
                    holder.txvAbandonado.setText("DESCONTO INDEVIDO");
                    holder.abandonado.setBackgroundColor(Color.parseColor("#FF8C00"));
                    holder.cor.setBackgroundColor(Color.parseColor("#FF8C00"));
                } else {
                    holder.txvAbandonado.setText("ABANDONADO");
                    holder.abandonado.setBackgroundColor(Color.parseColor("#F34F4F"));
                    holder.cor.setBackgroundColor(Color.parseColor("#F34F4F"));
                }
                holder.abandonado.setVisibility(View.VISIBLE);
                holder.txtPrecoPedido.setVisibility(View.INVISIBLE);
                holder.lyEntrega.setVisibility(View.INVISIBLE);
                holder.btnEnviar.setVisibility(View.INVISIBLE);
                holder.btnCompartilhar.setVisibility(View.INVISIBLE);
                holder.btnDuplic.setVisibility(View.INVISIBLE);
            } else {
                holder.abandonado.setVisibility(View.INVISIBLE);
                holder.txtPrecoPedido.setVisibility(View.VISIBLE);
                holder.lyEntrega.setVisibility(View.VISIBLE);
            }
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
        //holder.itemView.setActivated(selectedItems.get(position, false));
        holder.btnExcluir.setOnClickListener(listener.onClickExcluir(position));
        holder.btnEnviar.setOnClickListener(listener.onClickEnviar(position));
        holder.btnDuplic.setOnClickListener(listener.onClickDuplic(position));
        holder.btnCompartilhar.setOnClickListener(listener.onClickCompartilhar(position));
        holder.btnRastreio.setOnClickListener(listener.onClickRastrear(position));
        holder.btnLinhaTempo.setOnClickListener(listener.onClickLinhaTempo(position));
        applyClickEvents(holder, position);
    }

    private void applyClickEvents(PedidoViewHolder holder, final int position) {
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPedidoRowClicked(position);
            }
        });

    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false))
            selectedItems.delete(pos);
        else
            selectedItems.put(pos, true);
        notifyItemChanged(pos);
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public WebPedido getItem(int position) {
        return pedidos.get(position);
    }

    public List<WebPedido> getItensSelecionados() {
        List<WebPedido> pedidosSelecionados = new ArrayList<>();
        for (int i = 0; i < selectedItems.size(); i++)
            pedidosSelecionados.add(pedidos.get(selectedItems.keyAt(i)));
        return pedidosSelecionados;
    }

    public void remove(WebPedido pedido) {
        pedidos.remove(pedido);
        notifyDataSetChanged();
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }

    public interface PedidoAdapterListener {
        void onPedidoRowClicked(int position);
        void onRowLongClicked(int position);
        View.OnClickListener onClickExcluir(int position);
        View.OnClickListener onClickEnviar(int position);
        View.OnClickListener onClickDuplic(int position);
        View.OnClickListener onClickCompartilhar(int position);
        View.OnClickListener onClickRastrear(int position);
        View.OnClickListener onClickLinhaTempo(int position);
    }
}
