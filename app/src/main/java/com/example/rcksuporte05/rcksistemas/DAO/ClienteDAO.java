package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;

import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.ClienteResumo;

import java.util.ArrayList;
import java.util.List;

public class ClienteDAO {
    private DBHelper db;

    public ClienteDAO(DBHelper db) {
        this.db = db;
    }

    public long add(Cliente cliente) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ATIVO", cliente.getAtivo());
            content.put("ID_EMPRESA", cliente.getId_empresa());
            //content.put("ID_CADASTRO", cliente.getId_cadastro());
            content.put("ID_CADASTRO_SERVIDOR", cliente.getId_cadastro_servidor());
            content.put("ID_PROSPECT", cliente.getId_prospect());
            content.put("PESSOA_F_J", cliente.getPessoa_f_j());
            content.put("DATA_ANIVERSARIO", cliente.getData_aniversario());
            content.put("NOME_CADASTRO", cliente.getNome_cadastro());
            content.put("NOME_FANTASIA", cliente.getNome_fantasia());
            content.put("CPF_CNPJ", cliente.getCpf_cnpj());
            content.put("INSCRI_ESTADUAL", cliente.getInscri_estadual());

            content.put("ENDERECO", cliente.getEndereco());
            content.put("ENDERECO_BAIRRO", cliente.getEndereco_bairro());
            content.put("ENDERECO_NUMERO", cliente.getEndereco_numero());
            content.put("ENDERECO_COMPLEMENTO", cliente.getEndereco_complemento());
            content.put("ENDERECO_UF", cliente.getEndereco_uf());
            content.put("NOME_MUNICIPIO", cliente.getNome_municipio());
            content.put("ENDERECO_CEP", cliente.getEndereco_cep());
            content.put("USUARIO_ID", cliente.getUsuario_id());
            content.put("USUARIO_NOME", cliente.getUsuario_nome());
            content.put("USUARIO_DATA", cliente.getUsuario_data());
            content.put("F_CLIENTE", cliente.getF_cliente());
            content.put("F_FORNECEDOR", cliente.getF_fornecedor());
            content.put("F_FUNCIONARIO", cliente.getF_funcionario());
            content.put("F_VENDEDOR", cliente.getF_vendedor());
            content.put("F_TRANSPORTADOR", cliente.getF_transportador());
            content.put("DATA_ULTIMA_COMPRA", cliente.getData_ultima_compra());
            content.put("NOME_VENDEDOR", cliente.getNome_vendedor());
            content.put("F_ID_CLIENTE", cliente.getF_id_cliente());
            content.put("ID_ENTIDADE", cliente.getId_entidade());
            content.put("F_ID_FORNECEDOR", cliente.getF_id_fornecedor());
            content.put("F_ID_VENDEDOR", cliente.getF_id_vendedor());
            content.put("F_ID_TRANSPORTADOR", cliente.getF_id_transportador());
            content.put("TELEFONE_PRINCIPAL", cliente.getTelefone_principal());
            content.put("EMAIL_PRINCIPAL", cliente.getEmail_principal());
            content.put("ID_PAIS", cliente.getId_pais());
            content.put("F_ID_FUNCIONARIO", cliente.getF_id_funcionario());
            content.put("AVISAR_COM_DIAS", cliente.getAvisar_com_dias());
            content.put("OBSERVACOES", cliente.getObservacoes());
            content.put("PADRAO_ID_C_CUSTO", cliente.getPadrao_id_c_custo());
            content.put("PADRAO_ID_C_GERENCIADORA", cliente.getPadrao_id_c_gerenciadora());
            content.put("PADRAO_ID_C_ANALITICA", cliente.getPadrao_id_c_analitica());
            content.put("COB_ENDERECO", cliente.getCob_endereco());
            content.put("COB_ENDERECO_BAIRRO", cliente.getCob_endereco_bairro());
            content.put("COB_ENDERECO_NUMERO", cliente.getCob_endereco_numero());
            content.put("COB_ENDERECO_COMPLEMENTO", cliente.getCob_endereco_complemento());
            content.put("COB_ENDERECO_UF", cliente.getCob_endereco_uf());
            content.put("NOME_COB_MUNICIPIO", cliente.getNome_cob_municipio());
            content.put("COB_ENDERECO_CEP", cliente.getCob_endereco_cep());
            content.put("COB_ENDERECO_ID_PAIS", cliente.getCob_endereco_id_pais());
            content.put("LIMITE_CREDITO", cliente.getLimite_credito());
            content.put("LIMITE_DISPONIVEL", cliente.getLimite_disponivel());
            content.put("PESSOA_CONTATO_FINANCEIRO", cliente.getPessoa_contato_financeiro());
            content.put("EMAIL_FINANCEIRO", cliente.getEmail_financeiro());
            content.put("OBSERVACOES_FATURAMENTO", cliente.getObservacoes_faturamento());
            content.put("OBSERVACOES_FINANCEIRO", cliente.getObservacoes_financeiro());
            content.put("TELEFONE_DOIS", cliente.getTelefone_dois());
            content.put("TELEFONE_TRES", cliente.getTelefone_tres());
            content.put("PESSOA_CONTATO_PRINCIPAL", cliente.getPessoa_contato_principal());
            content.put("IND_DA_IE_DESTINATARIO", cliente.getInd_da_ie_destinatario());
            content.put("COMISSAO_PERCENTUAL", cliente.getComissao_percentual());
            content.put("ID_SETOR", cliente.getId_setor());
            content.put("NFE_EMAIL_ENVIAR", cliente.getNfe_email_enviar());
            content.put("NFE_EMAIL_UM", cliente.getNfe_email_um());
            content.put("NFE_EMAIL_DOIS", cliente.getNfe_email_dois());
            content.put("NFE_EMAIL_TRES", cliente.getNfe_email_tres());
            content.put("NFE_EMAIL_QUATRO", cliente.getNfe_email_quatro());
            content.put("NFE_EMAIL_CINCO", cliente.getNfe_email_cinco());
            content.put("ID_GRUPO_VENDEDOR", cliente.getId_grupo_vendedor());
            content.put("VENDEDOR_USA_PORTAL", cliente.getVendedor_usa_portal());
            content.put("VENDEDOR_ID_USER_PORTAL", cliente.getVendedor_id_user_portal());
            content.put("F_TARIFA", cliente.getF_tarifa());
            content.put("F_ID_TARIFA", cliente.getF_id_tarifa());
            content.put("F_PRODUTOR", cliente.getF_produtor());
            content.put("RG_NUMERO", cliente.getRg_numero());
            content.put("RG_SSP", cliente.getRg_ssp());
            content.put("CONTA_CONTABIL", cliente.getConta_contabil());
            content.put("MOTORISTA", cliente.getMotorista());
            content.put("F_ID_MOTORISTA", cliente.getF_id_motorista());
            content.put("HABILITACAO_NUMERO", cliente.getHabilitacao_numero());
            content.put("HABILITACAO_CATEGORIA", cliente.getHabilitacao_categoria());
            content.put("HABILITACAO_VENCIMENTO", cliente.getHabilitacao_vencimento());
            content.put("MOT_ID_TRANSPORTADORA", cliente.getMot_id_transportadora());
            content.put("LOCAL_CADASTRO", cliente.getLocal_cadastro());
            content.put("ID_CATEGORIA", cliente.getIdCategoria());
            content.put("ID_VENDEDOR", cliente.getId_vendedor());
            content.put("ALTERADO", "N");
            content.put("SITUACAO_PREDIO", cliente.getSituacaoPredio());
            content.put("FINALIZADO", cliente.getFinalizado());
            content.put("ID_TABELA_PRECO", cliente.getId_tabela_preco());
            content.put("NOME_TABELA_PRECO", cliente.getNome_tabela_preco());
            content.put("UTILIZA_TABELA_PRECO", cliente.getUtiliza_tabela_preco());
            content.put("TIPO_TABELA_PRECO", cliente.getTipo_tabela_preco());
            content.put("FINANCEIRO_VENCIDO", cliente.getFinanceiro_vencido());
            content.put("FINANCEIRO_VENCENDO", cliente.getFinanceiro_vencendo());
            content.put("PEDIDO_ANALISE", cliente.getPedido_analise());
            content.put("LATITUDE", cliente.getLatitude());
            content.put("LONGITUDE", cliente.getLongitude());
            System.gc();
            return db.addDados("TBL_CADASTRO", content);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int update(Cliente cliente) {
        ContentValues content = new ContentValues();
        try {
            content.put("ATIVO", cliente.getAtivo());
            content.put("ID_EMPRESA", cliente.getId_empresa());
            content.put("ID_CADASTRO", cliente.getId_cadastro());
            content.put("ID_CADASTRO_SERVIDOR", cliente.getId_cadastro_servidor());
            content.put("ID_PROSPECT", cliente.getId_prospect());
            content.put("PESSOA_F_J", cliente.getPessoa_f_j());
            content.put("DATA_ANIVERSARIO", cliente.getData_aniversario());
            content.put("NOME_CADASTRO", cliente.getNome_cadastro());
            content.put("NOME_FANTASIA", cliente.getNome_fantasia());
            content.put("CPF_CNPJ", cliente.getCpf_cnpj());
            content.put("INSCRI_ESTADUAL", cliente.getInscri_estadual());
            content.put("ENDERECO", cliente.getEndereco());
            content.put("ENDERECO_BAIRRO", cliente.getEndereco_bairro());
            content.put("ENDERECO_NUMERO", cliente.getEndereco_numero());
            content.put("ENDERECO_COMPLEMENTO", cliente.getEndereco_complemento());
            content.put("ENDERECO_UF", cliente.getEndereco_uf());
            content.put("NOME_MUNICIPIO", cliente.getNome_municipio());
            content.put("ENDERECO_CEP", cliente.getEndereco_cep());
            content.put("USUARIO_ID", cliente.getUsuario_id());
            content.put("USUARIO_NOME", cliente.getUsuario_nome());
            content.put("USUARIO_DATA", cliente.getUsuario_data());
            content.put("F_CLIENTE", cliente.getF_cliente());
            content.put("F_FORNECEDOR", cliente.getF_fornecedor());
            content.put("F_FUNCIONARIO", cliente.getF_funcionario());
            content.put("F_VENDEDOR", cliente.getF_vendedor());
            content.put("F_TRANSPORTADOR", cliente.getF_transportador());
            content.put("DATA_ULTIMA_COMPRA", cliente.getData_ultima_compra());
            content.put("NOME_VENDEDOR", cliente.getNome_vendedor());
            content.put("F_ID_CLIENTE", cliente.getF_id_cliente());
            content.put("ID_ENTIDADE", cliente.getId_entidade());
            content.put("F_ID_FORNECEDOR", cliente.getF_id_fornecedor());
            content.put("F_ID_VENDEDOR", cliente.getF_id_vendedor());
            content.put("F_ID_TRANSPORTADOR", cliente.getF_id_transportador());
            content.put("TELEFONE_PRINCIPAL", cliente.getTelefone_principal());
            content.put("EMAIL_PRINCIPAL", cliente.getEmail_principal());
            content.put("ID_PAIS", cliente.getId_pais());
            content.put("F_ID_FUNCIONARIO", cliente.getF_id_funcionario());
            content.put("AVISAR_COM_DIAS", cliente.getAvisar_com_dias());
            content.put("OBSERVACOES", cliente.getObservacoes());
            content.put("PADRAO_ID_C_CUSTO", cliente.getPadrao_id_c_custo());
            content.put("PADRAO_ID_C_GERENCIADORA", cliente.getPadrao_id_c_gerenciadora());
            content.put("PADRAO_ID_C_ANALITICA", cliente.getPadrao_id_c_analitica());
            content.put("COB_ENDERECO", cliente.getCob_endereco());
            content.put("COB_ENDERECO_BAIRRO", cliente.getCob_endereco_bairro());
            content.put("COB_ENDERECO_NUMERO", cliente.getCob_endereco_numero());
            content.put("COB_ENDERECO_COMPLEMENTO", cliente.getCob_endereco_complemento());
            content.put("COB_ENDERECO_UF", cliente.getCob_endereco_uf());
            content.put("NOME_COB_MUNICIPIO", cliente.getNome_cob_municipio());
            content.put("COB_ENDERECO_CEP", cliente.getCob_endereco_cep());
            content.put("COB_ENDERECO_ID_PAIS", cliente.getCob_endereco_id_pais());
            content.put("LIMITE_CREDITO", cliente.getLimite_credito());
            content.put("LIMITE_DISPONIVEL", cliente.getLimite_disponivel());
            content.put("PESSOA_CONTATO_FINANCEIRO", cliente.getPessoa_contato_financeiro());
            content.put("EMAIL_FINANCEIRO", cliente.getEmail_financeiro());
            content.put("OBSERVACOES_FATURAMENTO", cliente.getObservacoes_faturamento());
            content.put("OBSERVACOES_FINANCEIRO", cliente.getObservacoes_financeiro());
            content.put("TELEFONE_DOIS", cliente.getTelefone_dois());
            content.put("TELEFONE_TRES", cliente.getTelefone_tres());
            content.put("PESSOA_CONTATO_PRINCIPAL", cliente.getPessoa_contato_principal());
            content.put("IND_DA_IE_DESTINATARIO", cliente.getInd_da_ie_destinatario());
            content.put("COMISSAO_PERCENTUAL", cliente.getComissao_percentual());
            content.put("ID_SETOR", cliente.getId_setor());
            content.put("NFE_EMAIL_ENVIAR", cliente.getNfe_email_enviar());
            content.put("NFE_EMAIL_UM", cliente.getNfe_email_um());
            content.put("NFE_EMAIL_DOIS", cliente.getNfe_email_dois());
            content.put("NFE_EMAIL_TRES", cliente.getNfe_email_tres());
            content.put("NFE_EMAIL_QUATRO", cliente.getNfe_email_quatro());
            content.put("NFE_EMAIL_CINCO", cliente.getNfe_email_cinco());
            content.put("ID_GRUPO_VENDEDOR", cliente.getId_grupo_vendedor());
            content.put("VENDEDOR_USA_PORTAL", cliente.getVendedor_usa_portal());
            content.put("VENDEDOR_ID_USER_PORTAL", cliente.getVendedor_id_user_portal());
            content.put("F_TARIFA", cliente.getF_tarifa());
            content.put("F_ID_TARIFA", cliente.getF_id_tarifa());
            content.put("F_PRODUTOR", cliente.getF_produtor());
            content.put("RG_NUMERO", cliente.getRg_numero());
            content.put("RG_SSP", cliente.getRg_ssp());
            content.put("CONTA_CONTABIL", cliente.getConta_contabil());
            content.put("MOTORISTA", cliente.getMotorista());
            content.put("F_ID_MOTORISTA", cliente.getF_id_motorista());
            content.put("HABILITACAO_NUMERO", cliente.getHabilitacao_numero());
            content.put("HABILITACAO_CATEGORIA", cliente.getHabilitacao_categoria());
            content.put("HABILITACAO_VENCIMENTO", cliente.getHabilitacao_vencimento());
            content.put("MOT_ID_TRANSPORTADORA", cliente.getMot_id_transportadora());
            content.put("LOCAL_CADASTRO", cliente.getLocal_cadastro());
            content.put("ID_CATEGORIA", cliente.getIdCategoria());
            content.put("ID_VENDEDOR", cliente.getId_vendedor());
            content.put("ALTERADO", cliente.getAlterado());
            content.put("SITUACAO_PREDIO", cliente.getSituacaoPredio());
            content.put("FINALIZADO", cliente.getFinalizado());
            content.put("ID_TABELA_PRECO", cliente.getId_tabela_preco());
            content.put("NOME_TABELA_PRECO", cliente.getNome_tabela_preco());
            content.put("UTILIZA_TABELA_PRECO", cliente.getUtiliza_tabela_preco());
            content.put("TIPO_TABELA_PRECO", cliente.getTipo_tabela_preco());
            content.put("FINANCEIRO_VENCIDO", cliente.getFinanceiro_vencido());
            content.put("FINANCEIRO_VENCENDO", cliente.getFinanceiro_vencendo());
            content.put("PEDIDO_ANALISE", cliente.getPedido_analise());
            content.put("LATITUDE", cliente.getLatitude());
            content.put("LONGITUDE", cliente.getLongitude());
            System.gc();
            return db.updateDados("TBL_CADASTRO", content, "ID_CADASTRO = " + cliente.getId_cadastro());
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int delete(Cliente cliente, String tipo) {
        //Alteração no codigo tratando excessao
        //Roberto caceres
        //SQLiteDatabase db = getWritableDatabase();
        try {
            if (tipo.equalsIgnoreCase("0"))
                db.deleteDados("TBL_CADASTRO_CONTATO", "ID_CADASTRO=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro()), "1"});
            else
                db.deleteDados("TBL_CADASTRO_CONTATO", "ID_CADASTRO_SERVIDOR=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro_servidor()), "1"});
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (tipo.equalsIgnoreCase("0"))
                db.deleteDados("TBL_REFERENCIA_BANCARIA", "ID_CADASTRO=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro()), "1"});
            else
                db.deleteDados("TBL_REFERENCIA_BANCARIA", "ID_CADASTRO_SERVIDOR=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro_servidor()), "1"});
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (tipo.equalsIgnoreCase("0"))
                db.deleteDados("TBL_REFERENCIA_COMERCIAL", "ID_CADASTRO=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro()), "1"});

            else
                db.deleteDados("TBL_REFERENCIA_COMERCIAL", "ID_CADASTRO_SERVIDOR=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro_servidor()), "1"});
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (tipo.equalsIgnoreCase("0"))
                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro()), "1"});
            else
                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO_SERVIDOR=? AND ID_ENTIDADE=?", new String[]{String.valueOf(cliente.getId_cadastro_servidor()), "1"});
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (tipo.equalsIgnoreCase("0"))
                return db.deleteDados("TBL_CADASTRO", "ID_CADASTRO=?", new String[]{String.valueOf(cliente.getId_cadastro())});
            else
                return db.deleteDados("TBL_CADASTRO", "ID_CADASTRO_SERVIDOR=?", new String[]{String.valueOf(cliente.getId_cadastro_servidor())});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Cliente> getLista(String SQL) {
        List<Cliente> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        Cursor cursorPro = null;
        if (cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        do {
            Cliente cliente = new Cliente();
            try {
                cliente.setAtivo(cursor.getString(cursor.getColumnIndex("ATIVO")));
                cliente.setId_empresa(cursor.getInt(cursor.getColumnIndex("ID_EMPRESA")));
                cliente.setId_cadastro(cursor.getInt(cursor.getColumnIndex("ID_CADASTRO")));
                cliente.setId_prospect(cursor.getInt(cursor.getColumnIndex("ID_PROSPECT")));
                cliente.setId_cadastro_servidor(cursor.getInt(cursor.getColumnIndex("ID_CADASTRO_SERVIDOR")));
                cliente.setPessoa_f_j(cursor.getString(cursor.getColumnIndex("PESSOA_F_J")));
                cliente.setData_aniversario(cursor.getString(cursor.getColumnIndex("DATA_ANIVERSARIO")));
                cliente.setNome_cadastro(cursor.getString(cursor.getColumnIndex("NOME_CADASTRO")));
                cliente.setNome_fantasia(cursor.getString(cursor.getColumnIndex("NOME_FANTASIA")));
                cliente.setCpf_cnpj(cursor.getString(cursor.getColumnIndex("CPF_CNPJ")));
                cliente.setInscri_estadual(cursor.getString(cursor.getColumnIndex("INSCRI_ESTADUAL")));
                cliente.setEndereco(cursor.getString(cursor.getColumnIndex("ENDERECO")));
                cliente.setEndereco_bairro(cursor.getString(cursor.getColumnIndex("ENDERECO_BAIRRO")));
                cliente.setEndereco_numero(cursor.getString(cursor.getColumnIndex("ENDERECO_NUMERO")));
                cliente.setEndereco_complemento(cursor.getString(cursor.getColumnIndex("ENDERECO_COMPLEMENTO")));
                cliente.setEndereco_uf(cursor.getString(cursor.getColumnIndex("ENDERECO_UF")));
                cliente.setNome_municipio(cursor.getString(cursor.getColumnIndex("NOME_MUNICIPIO")));
                cliente.setEndereco_cep(cursor.getString(cursor.getColumnIndex("ENDERECO_CEP")));
                cliente.setUsuario_id(cursor.getInt(cursor.getColumnIndex("USUARIO_ID")));
                cliente.setUsuario_nome(cursor.getString(cursor.getColumnIndex("USUARIO_NOME")));
                cliente.setUsuario_data(cursor.getString(cursor.getColumnIndex("USUARIO_DATA")));
                cliente.setF_cliente(cursor.getString(cursor.getColumnIndex("F_CLIENTE")));
                cliente.setF_fornecedor(cursor.getString(cursor.getColumnIndex("F_FORNECEDOR")));
                cliente.setF_funcionario(cursor.getString(cursor.getColumnIndex("F_FUNCIONARIO")));
                cliente.setF_vendedor(cursor.getString(cursor.getColumnIndex("F_VENDEDOR")));
                cliente.setF_transportador(cursor.getString(cursor.getColumnIndex("F_TRANSPORTADOR")));
                cliente.setData_ultima_compra(cursor.getString(cursor.getColumnIndex("DATA_ULTIMA_COMPRA")));
                cliente.setNome_vendedor(cursor.getString(cursor.getColumnIndex("NOME_VENDEDOR")));
                cliente.setF_id_cliente(cursor.getString(cursor.getColumnIndex("F_ID_CLIENTE")));
                cliente.setId_entidade(cursor.getString(cursor.getColumnIndex("ID_ENTIDADE")));
                cliente.setF_id_fornecedor(cursor.getString(cursor.getColumnIndex("F_ID_FORNECEDOR")));
                cliente.setF_id_vendedor(cursor.getString(cursor.getColumnIndex("F_ID_VENDEDOR")));
                cliente.setF_id_transportador(cursor.getString(cursor.getColumnIndex("F_ID_TRANSPORTADOR")));
                cliente.setTelefone_principal(cursor.getString(cursor.getColumnIndex("TELEFONE_PRINCIPAL")));
                cliente.setEmail_principal(cursor.getString(cursor.getColumnIndex("EMAIL_PRINCIPAL")));
                cliente.setId_pais(cursor.getInt(cursor.getColumnIndex("ID_PAIS")));
                cliente.setF_id_funcionario(cursor.getString(cursor.getColumnIndex("F_ID_FUNCIONARIO")));
                cliente.setAvisar_com_dias(cursor.getString(cursor.getColumnIndex("AVISAR_COM_DIAS")));
                cliente.setObservacoes(cursor.getString(cursor.getColumnIndex("OBSERVACOES")));
                cliente.setPadrao_id_c_custo(cursor.getString(cursor.getColumnIndex("PADRAO_ID_C_CUSTO")));
                cliente.setPadrao_id_c_gerenciadora(cursor.getString(cursor.getColumnIndex("PADRAO_ID_C_GERENCIADORA")));
                cliente.setPadrao_id_c_analitica(cursor.getString(cursor.getColumnIndex("PADRAO_ID_C_ANALITICA")));
                cliente.setCob_endereco(cursor.getString(cursor.getColumnIndex("COB_ENDERECO")));
                cliente.setCob_endereco_bairro(cursor.getString(cursor.getColumnIndex("COB_ENDERECO_BAIRRO")));
                cliente.setCob_endereco_numero(cursor.getString(cursor.getColumnIndex("COB_ENDERECO_NUMERO")));
                cliente.setCob_endereco_complemento(cursor.getString(cursor.getColumnIndex("COB_ENDERECO_COMPLEMENTO")));
                cliente.setCob_endereco_uf(cursor.getString(cursor.getColumnIndex("COB_ENDERECO_UF")));
                cliente.setNome_cob_municipio(cursor.getString(cursor.getColumnIndex("NOME_COB_MUNICIPIO")));
                cliente.setCob_endereco_cep(cursor.getString(cursor.getColumnIndex("COB_ENDERECO_CEP")));
                cliente.setCob_endereco_id_pais(cursor.getInt(cursor.getColumnIndex("COB_ENDERECO_ID_PAIS")));
                cliente.setLimite_credito(cursor.getString(cursor.getColumnIndex("LIMITE_CREDITO")));
                cliente.setLimite_disponivel(cursor.getString(cursor.getColumnIndex("LIMITE_DISPONIVEL")));
                cliente.setPessoa_contato_financeiro(cursor.getString(cursor.getColumnIndex("PESSOA_CONTATO_FINANCEIRO")));
                cliente.setEmail_financeiro(cursor.getString(cursor.getColumnIndex("EMAIL_FINANCEIRO")));
                cliente.setObservacoes_faturamento(cursor.getString(cursor.getColumnIndex("OBSERVACOES_FATURAMENTO")));
                cliente.setObservacoes_financeiro(cursor.getString(cursor.getColumnIndex("OBSERVACOES_FINANCEIRO")));
                cliente.setTelefone_dois(cursor.getString(cursor.getColumnIndex("TELEFONE_DOIS")));
                cliente.setTelefone_tres(cursor.getString(cursor.getColumnIndex("TELEFONE_TRES")));
                cliente.setPessoa_contato_principal(cursor.getString(cursor.getColumnIndex("PESSOA_CONTATO_PRINCIPAL")));
                cliente.setInd_da_ie_destinatario(cursor.getString(cursor.getColumnIndex("IND_DA_IE_DESTINATARIO")));
                cliente.setComissao_percentual(cursor.getString(cursor.getColumnIndex("COMISSAO_PERCENTUAL")));
                cliente.setId_setor(cursor.getString(cursor.getColumnIndex("ID_SETOR")));
                cliente.setNfe_email_enviar(cursor.getString(cursor.getColumnIndex("NFE_EMAIL_ENVIAR")));
                cliente.setNfe_email_um(cursor.getString(cursor.getColumnIndex("NFE_EMAIL_UM")));
                cliente.setNfe_email_dois(cursor.getString(cursor.getColumnIndex("NFE_EMAIL_DOIS")));
                cliente.setNfe_email_tres(cursor.getString(cursor.getColumnIndex("NFE_EMAIL_TRES")));
                cliente.setNfe_email_quatro(cursor.getString(cursor.getColumnIndex("NFE_EMAIL_QUATRO")));
                cliente.setNfe_email_cinco(cursor.getString(cursor.getColumnIndex("NFE_EMAIL_CINCO")));
                cliente.setId_grupo_vendedor(cursor.getString(cursor.getColumnIndex("ID_GRUPO_VENDEDOR")));
                cliente.setVendedor_usa_portal(cursor.getString(cursor.getColumnIndex("VENDEDOR_USA_PORTAL")));
                cliente.setVendedor_id_user_portal(cursor.getString(cursor.getColumnIndex("VENDEDOR_ID_USER_PORTAL")));
                cliente.setF_tarifa(cursor.getString(cursor.getColumnIndex("F_TARIFA")));
                cliente.setF_id_tarifa(cursor.getString(cursor.getColumnIndex("F_ID_TARIFA")));
                cliente.setF_produtor(cursor.getString(cursor.getColumnIndex("F_PRODUTOR")));
                cliente.setRg_numero(cursor.getString(cursor.getColumnIndex("RG_NUMERO")));
                cliente.setRg_ssp(cursor.getString(cursor.getColumnIndex("RG_SSP")));
                cliente.setConta_contabil(cursor.getString(cursor.getColumnIndex("CONTA_CONTABIL")));
                cliente.setMotorista(cursor.getString(cursor.getColumnIndex("MOTORISTA")));
                cliente.setF_id_motorista(cursor.getString(cursor.getColumnIndex("F_ID_MOTORISTA")));
                cliente.setHabilitacao_numero(cursor.getString(cursor.getColumnIndex("HABILITACAO_NUMERO")));
                cliente.setHabilitacao_categoria(cursor.getString(cursor.getColumnIndex("HABILITACAO_CATEGORIA")));
                cliente.setHabilitacao_vencimento(cursor.getString(cursor.getColumnIndex("HABILITACAO_VENCIMENTO")));
                cliente.setMot_id_transportadora(cursor.getString(cursor.getColumnIndex("MOT_ID_TRANSPORTADORA")));
                cliente.setLocal_cadastro(cursor.getString(cursor.getColumnIndex("LOCAL_CADASTRO")));
                cliente.setIdCategoria(cursor.getInt(cursor.getColumnIndex("ID_CATEGORIA")));
                cliente.setId_vendedor(cursor.getInt(cursor.getColumnIndex("ID_VENDEDOR")));
                cliente.setAlterado(cursor.getString(cursor.getColumnIndex("ALTERADO")));
                cliente.setSituacaoPredio(cursor.getString(cursor.getColumnIndex("SITUACAO_PREDIO")));
                cliente.setFinalizado(cursor.getString(cursor.getColumnIndex("FINALIZADO")));
                cliente.setId_tabela_preco(cursor.getInt(cursor.getColumnIndex("ID_TABELA_PRECO")));
                cliente.setNome_tabela_preco(cursor.getString(cursor.getColumnIndex("NOME_TABELA_PRECO")));
                cliente.setUtiliza_tabela_preco(cursor.getString(cursor.getColumnIndex("UTILIZA_TABELA_PRECO")));
                cliente.setTipo_tabela_preco(cursor.getString(cursor.getColumnIndex("TIPO_TABELA_PRECO")));
                cliente.setFinanceiro_vencido(cursor.getString(cursor.getColumnIndex("FINANCEIRO_VENCIDO")));
                cliente.setFinanceiro_vencendo(cursor.getString(cursor.getColumnIndex("FINANCEIRO_VENCENDO")));
                cliente.setPedido_analise(cursor.getString(cursor.getColumnIndex("PEDIDO_ANALISE")));
                cliente.setLatitude(cursor.getString(cursor.getColumnIndex("LATITUDE")));
                cliente.setLongitude(cursor.getString(cursor.getColumnIndex("LONGITUDE")));
                try {
                    SQL = "SELECT P.ID_PROMOCAO AS ID_PROMOCAO FROM TBL_PROMOCAO_CLIENTE AS P" +
                            " INNER JOIN TBL_PROMOCAO_CAB AS PC ON PC.ID_PROMOCAO = P.ID_PROMOCAO" +
                            " WHERE P.ID_CADASTRO = '" + cursor.getInt(cursor.getColumnIndex("ID_CADASTRO_SERVIDOR")) + "'" +
                            " AND PC.ATIVO = 'S' AND PC.ID_EMPRESA = " + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice();
                    cursorPro = db.listaDados(SQL);
                    if (cursorPro.getCount() > 0) {
                        if (cursorPro.moveToNext()) {
                            cliente.setId_promocao(cursorPro.getString(cursorPro.getColumnIndex("ID_PROMOCAO")));
                        } else
                            cliente.setId_promocao("0");
                    } else
                        cliente.setId_promocao("0");
                } catch (Exception e) {
                    cliente.setId_promocao("0");
                }
                if (cursorPro != null)
                    cursorPro.close();
                lista.add(cliente);
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public int getTotalReg(String SQL) {
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("TOTALREG"));
    }


    public long add(ClienteResumo cliente) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("CPF_CNPJ", cliente.getCpf_cnpj());
            content.put("NOME", cliente.getNome());
            System.gc();
            return db.addDados("TBL_CADASTRO_RESUMO", content);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public ClienteResumo getCliente(String SQL) {
        Cursor cursor = db.listaDados(SQL);
        ClienteResumo cliente = null;
        if (cursor.getCount() <= 0)
            return cliente;
        cursor.moveToFirst();
        do {
            cliente = new ClienteResumo();
            try {
                cliente.setCpf_cnpj(cursor.getString(cursor.getColumnIndex("CPF_CNPJ")));
                cliente.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return cliente;

    }
}