package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;

import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.EmpresaParametro;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;

import java.util.ArrayList;
import java.util.List;

public class WebPedidoDAO {
    private DBHelper db;
    public WebPedidoDAO(DBHelper db) {
        this.db = db;
    }

    public long add(WebPedido webPedido) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ID_EMPRESA", webPedido.getId_empresa());
            if (webPedido.getCadastro().getId_cadastro_servidor() <= 0) {
                content.put("ID_CADASTRO", webPedido.getCadastro().getId_cadastro());
                content.put("CLIENTE_NOVO", "S");
            } else {
                content.put("ID_CADASTRO", webPedido.getCadastro().getId_cadastro_servidor());
                content.put("CLIENTE_NOVO", "N");
            }
            content.put("ID_VENDEDOR", webPedido.getId_vendedor());
            content.put("ID_CONDICAO_PAGAMENTO", webPedido.getId_condicao_pagamento());
            content.put("ID_OPERACAO", webPedido.getId_operacao());
            content.put("ID_TABELA", webPedido.getId_tabela());
            content.put("NOME_EXTENSO", webPedido.getCadastro().getNome_cadastro());
            content.put("DATA_EMISSAO", webPedido.getData_emissao());
            //content.put("DATA_EMISSAO", db.pegaDataAtual());
            content.put("VALOR_PRODUTOS", webPedido.getValor_produtos());
            content.put("VALOR_DESCONTO", webPedido.getValor_desconto());
            content.put("VALOR_DESCONTO_ADD", webPedido.getValor_desconto_add());
            content.put("DESCONTO_PER", webPedido.getDesconto_per());
            content.put("DESCONTO_PER_ADD", webPedido.getDesconto_per_add());
            content.put("VALOR_TOTAL", webPedido.getValor_total());
            content.put("EXCLUIDO", webPedido.getExcluido());
            content.put("EXCLUIDO_USUARIO_ID", webPedido.getExcluido_usuario_id());
            content.put("EXCLUIDO_USUARIO_NOME", webPedido.getExcluido_usuario_nome());
            content.put("EXCLUIDO_USUARIO_DATA", webPedido.getExcluido_usuario_data());
            content.put("JUSTIFICATIVA_EXCLUSAO", webPedido.getJustificativa_exclusao());
            content.put("USUARIO_LANCAMENTO_ID", webPedido.getUsuario_lancamento_id());
            content.put("USUARIO_LANCAMENTO_NOME", webPedido.getUsuario_lancamento_nome());
            content.put("USUARIO_LANCAMENTO_DATA", webPedido.getUsuario_lancamento_data());
            content.put("OBSERVACOES", webPedido.getObservacoes());
            content.put("STATUS", webPedido.getStatus());
            content.put("ID_PEDIDO_VENDA", webPedido.getId_pedido_venda());
            content.put("ID_NOTA_FISCAL", webPedido.getId_nota_fiscal());
            content.put("ID_TABELA_PRECO_FAIXA", webPedido.getId_tabela_preco_faixa());
            content.put("PONTOS_TOTAL", webPedido.getPontos_total());
            content.put("PONTOS_COEFICIENTE", webPedido.getPontos_coeficiente());
            content.put("PONTOS_COR", webPedido.getPontos_cor());
            content.put("COMISSAO_PERCENTUAL", webPedido.getComissao_percentual());
            content.put("COMISSAO_VALOR", webPedido.getComissao_valor());
            content.put("ID_FAIXA_FINAL", webPedido.getId_faixa_final());
            content.put("VALOR_BONUS_CREDOR", webPedido.getValor_bonus_credor());
            content.put("PERC_BONUS_CREDOR", webPedido.getPerc_bonus_credor());
            content.put("DATA_PREV_ENTREGA", webPedido.getData_prev_entrega());
            content.put("ID_WEB_PEDIDO_SERVIDOR", webPedido.getId_web_pedido_servidor());
            content.put("PEDIDO_ENVIADO", webPedido.getPedido_enviado());
            content.put("FINALIZADO", webPedido.getFinalizado());
            //NOVOS CAMPOS   29/01/2020
            content.put("ID_PEDIDO", webPedido.getId_pedido());
            content.put("WEB_IMPORTADO_DATA", webPedido.getWeb_importado_data());
            content.put("ANALISE_CREDITO", webPedido.getAnalise_credito());
            content.put("ANALISE_USUARIO_ID", webPedido.getAnalise_usuario_id());
            content.put("ANALISE_USUARIO_NOME", webPedido.getAnalise_usuario_nome());
            content.put("ANALISE_USUARIO_DATA", webPedido.getAnalise_usuario_data());
            content.put("ORDEM_CARREGAMENTO", webPedido.getOrdem_carregamento());
            content.put("ID_ORDEM_CARREGAMENTO", webPedido.getId_ordem_carregamento());
            content.put("ID_TRANSPORTADORA", webPedido.getId_transportadora());
            content.put("NOME_TRANSPORTADORA", webPedido.getNome_transportadora());
            content.put("NUMERO_NOTA_FISCAL", webPedido.getNumero_nota_fiscal());
            content.put("ID_NOTA_FISCAL", webPedido.getId_nota_fiscal());  //ANALISAR P PEGAR DA TABELA PEDIDO_VENDA
            content.put("DATA_NOTA_FISCAL", webPedido.getData_nota_fiscal());
            content.put("ESPECIE", webPedido.getEspecie());
            content.put("VOLUMES", webPedido.getVolumes());
            content.put("WEB_VALOR_TOTAL", webPedido.getWeb_valor_total());
            content.put("EXPEDICAO_STATUS", webPedido.getExpedicao_status());
            content.put("VALOR_FATURADO", webPedido.getValor_faturado());
            content.put("VALOR_ICMS_ST", webPedido.getValor_icms_st());
            try {
                content.put("VALOR_RECIBO", webPedido.getValor_recibo().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("VALOR_RECIBO", webPedido.getValor_recibo());
            } catch ( Exception e) {
                content.put("VALOR_RECIBO", webPedido.getValor_recibo());
            }
            try {
                if (!webPedido.getId_moeda_padrao().isEmpty() || webPedido.getId_moeda_padrao() != null)
                    content.put("ID_MOEDA_PADRAO", webPedido.getId_moeda_padrao());
                else
                    content.put("ID_MOEDA_PADRAO", "1");
            } catch ( NullPointerException e ) {
                content.put("ID_MOEDA_PADRAO", "1");
            }
            content.put("DESCONTO_INDEVIDO", "N");
            content.put("ID_PEDIDO_APP", webPedido.getId_pedido_app());
            try {
                content.put("PRAZO_CONFIRMADO", webPedido.getPrazo_confirmado());
            } catch ( NullPointerException e) {
                content.put("PRAZO_CONFIRMADO", "N");
            } catch (Exception e) {
                content.put("PRAZO_CONFIRMADO", "N");
            }
            try {
                content.put("VALOR_DESCONTO_ADIC", webPedido.getValor_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("VALOR_DESCONTO_ADIC", webPedido.getValor_desconto_adic());
            } catch ( Exception e) {
                content.put("VALOR_DESCONTO_ADIC", webPedido.getValor_desconto_adic());
            }
            try {
                content.put("PERC_DESCONTO_ADIC", webPedido.getPerc_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("PERC_DESCONTO_ADIC", webPedido.getPerc_desconto_adic());
            } catch ( Exception e) {
                content.put("PERC_DESCONTO_ADIC", webPedido.getPerc_desconto_adic());
            }
            System.gc();
            return db.addDados("TBL_WEB_PEDIDO", content);
        } catch ( NullPointerException|SQLException e){
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return 0;

    }
    public int update(WebPedido webPedido) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ID_WEB_PEDIDO", webPedido.getId_web_pedido());
            content.put("ID_EMPRESA", webPedido.getId_empresa());
            if (webPedido.getCadastro().getId_cadastro_servidor() <= 0) {
                content.put("ID_CADASTRO", webPedido.getCadastro().getId_cadastro());
                content.put("ID_PEDIDO_APP",   webPedido.getId_pedido_app().substring(0,3) + String.format("%08d", Integer.parseInt(webPedido.getId_web_pedido())) + String.format("%08d",webPedido.getCadastro().getId_cadastro()) + webPedido.getId_pedido_app().substring(19) );
                content.put("CLIENTE_NOVO", "S");
            } else {
                content.put("ID_CADASTRO", webPedido.getCadastro().getId_cadastro_servidor());
                content.put("ID_PEDIDO_APP",   webPedido.getId_pedido_app().substring(0,3) + String.format("%08d", Integer.parseInt(webPedido.getId_web_pedido())) + String.format("%08d",webPedido.getCadastro().getId_cadastro_servidor()) + webPedido.getId_pedido_app().substring(19) );
                content.put("CLIENTE_NOVO", "N");
            }
            content.put("ID_VENDEDOR", webPedido.getId_vendedor());
            content.put("ID_CONDICAO_PAGAMENTO", webPedido.getId_condicao_pagamento());
            content.put("ID_OPERACAO", webPedido.getId_operacao());
            content.put("ID_TABELA", webPedido.getId_tabela());
            content.put("NOME_EXTENSO", webPedido.getCadastro().getNome_cadastro());
            content.put("DATA_EMISSAO", webPedido.getData_emissao());
            content.put("VALOR_PRODUTOS", webPedido.getValor_produtos());
            content.put("VALOR_DESCONTO", webPedido.getValor_desconto());
            content.put("VALOR_DESCONTO_ADD", webPedido.getValor_desconto_add());
            content.put("DESCONTO_PER", webPedido.getDesconto_per());
            content.put("DESCONTO_PER_ADD", webPedido.getDesconto_per_add());
            content.put("VALOR_TOTAL", webPedido.getValor_total());
            content.put("EXCLUIDO", webPedido.getExcluido());
            content.put("EXCLUIDO_USUARIO_ID", webPedido.getExcluido_usuario_id());
            content.put("EXCLUIDO_USUARIO_NOME", webPedido.getExcluido_usuario_nome());
            content.put("EXCLUIDO_USUARIO_DATA", webPedido.getExcluido_usuario_data());
            content.put("JUSTIFICATIVA_EXCLUSAO", webPedido.getJustificativa_exclusao());
            content.put("USUARIO_LANCAMENTO_ID", webPedido.getUsuario_lancamento_id());
            content.put("USUARIO_LANCAMENTO_NOME", webPedido.getUsuario_lancamento_nome());
            content.put("USUARIO_LANCAMENTO_DATA", webPedido.getUsuario_lancamento_data());
            content.put("OBSERVACOES", webPedido.getObservacoes());
            content.put("STATUS", webPedido.getStatus());
            content.put("ID_PEDIDO_VENDA", webPedido.getId_pedido_venda());
            content.put("ID_NOTA_FISCAL", webPedido.getId_nota_fiscal());
            content.put("ID_TABELA_PRECO_FAIXA", webPedido.getId_tabela_preco_faixa());
            content.put("PONTOS_TOTAL", webPedido.getPontos_total());
            content.put("PONTOS_COEFICIENTE", webPedido.getPontos_coeficiente());
            content.put("PONTOS_COR", webPedido.getPontos_cor());
            content.put("COMISSAO_PERCENTUAL", webPedido.getComissao_percentual());
            content.put("COMISSAO_VALOR", webPedido.getComissao_valor());
            content.put("ID_FAIXA_FINAL", webPedido.getId_faixa_final());
            content.put("VALOR_BONUS_CREDOR", webPedido.getValor_bonus_credor());
            content.put("PERC_BONUS_CREDOR", webPedido.getPerc_bonus_credor());
            content.put("DATA_PREV_ENTREGA", webPedido.getData_prev_entrega());
            content.put("PEDIDO_ENVIADO", webPedido.getPedido_enviado());
            content.put("ID_WEB_PEDIDO_SERVIDOR", webPedido.getId_web_pedido_servidor());
            content.put("FINALIZADO", webPedido.getFinalizado());
            content.put("ID_MOEDA_PADRAO", webPedido.getId_moeda_padrao());
            content.put("DESCONTO_INDEVIDO", webPedido.getDescontoIndevido());
            content.put("PRAZO_CONFIRMADO", webPedido.getPrazo_confirmado());
            content.put("VALOR_ICMS_ST", webPedido.getValor_icms_st());
            content.put("VALOR_ICMS_ST", webPedido.getValor_icms_st());
            try {
                content.put("VALOR_RECIBO", webPedido.getValor_recibo().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("VALOR_RECIBO", webPedido.getValor_recibo());
            } catch ( Exception e) {
                content.put("VALOR_RECIBO", webPedido.getValor_recibo());
            }
            try {
                content.put("VALOR_DESCONTO_ADIC", webPedido.getValor_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("VALOR_DESCONTO_ADIC", webPedido.getValor_desconto_adic());
            } catch ( Exception e) {
                content.put("VALOR_DESCONTO_ADIC", webPedido.getValor_desconto_adic());
            }
            try {
                content.put("PERC_DESCONTO_ADIC", webPedido.getPerc_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("PERC_DESCONTO_ADIC", webPedido.getPerc_desconto_adic());
            } catch ( Exception e) {
                content.put("PERC_DESCONTO_ADIC", webPedido.getPerc_desconto_adic());
            }
            System.gc();
            return db.updateDados("TBL_WEB_PEDIDO", content, "ID_WEB_PEDIDO = " + webPedido.getId_web_pedido());
        } catch ( NullPointerException|SQLException e){
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int updateDescontoIndevido(WebPedido webPedido, String desconto_indevido) {
        ContentValues content = new ContentValues();
        try {
            content.put("ID_WEB_PEDIDO", webPedido.getId_web_pedido());
            content.put("DESCONTO_INDEVIDO", desconto_indevido);
            System.gc();
            return db.updateDados("TBL_WEB_PEDIDO", content, "ID_WEB_PEDIDO = " + webPedido.getId_web_pedido());
        } catch ( NullPointerException|SQLException e){
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    public int updateClientePedido(int id_cadastro_antigo, int id_cadastro_servidor){

        ContentValues content = new ContentValues();
        try {
            content.put("ID_CADASTRO", id_cadastro_servidor);
            content.put("CLIENTE_NOVO", "N");
            System.gc();
            return db.updateDados("TBL_WEB_PEDIDO", content, "ID_CADASTRO = '" + id_cadastro_antigo + "' AND PEDIDO_ENVIADO = 'N'");
        } catch ( NullPointerException|SQLException e){
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return 0;
    }



    public boolean delete( WebPedido pedido) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
         try {
            if( db.deleteDados( "TBL_WEB_PEDIDO", "ID_WEB_PEDIDO=?", new String[]{pedido.getId_web_pedido()} ) >= 1)
                db.deleteDados( "TBL_WEB_PEDIDO_ITENS", "ID_PEDIDO =?", new String[]{pedido.getId_web_pedido()} ) ;
            else
                return false;
        } catch ( Exception e) {
            return false;
        }
        return true;
    }

    public List<WebPedido> getLista(String SQL) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        List<WebPedido> lista = new ArrayList<>();
        List<Cliente> listaCliente = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        ClienteDAO clienteDAO = new ClienteDAO(db);
        do {
            WebPedido webPedido = new WebPedido();
            try {
                webPedido.setId_web_pedido(cursor.getString(cursor.getColumnIndex("ID_WEB_PEDIDO")));
                webPedido.setId_empresa(cursor.getString(cursor.getColumnIndex("ID_EMPRESA")));
                webPedido.setCliente_novo(cursor.getString(cursor.getColumnIndex("CLIENTE_NOVO")));
                try {
                     if (webPedido.getCliente_novo().equalsIgnoreCase("S")) {
                         listaCliente = clienteDAO.getLista("SELECT * FROM TBL_CADASTRO WHERE ID_CADASTRO = " +  cursor.getString(cursor.getColumnIndex("ID_CADASTRO"))
                                 + " AND (ID_CADASTRO_SERVIDOR <= 0 OR ID_CADASTRO_SERVIDOR IS NULL) ");
                     } else{
                         listaCliente = clienteDAO.getLista("SELECT * FROM TBL_CADASTRO WHERE ID_CADASTRO_SERVIDOR = " + cursor.getString(cursor.getColumnIndex("ID_CADASTRO")));
                     }
                } catch (NullPointerException e) {
                        listaCliente = clienteDAO.getLista("SELECT * FROM TBL_CADASTRO WHERE (ID_CADASTRO_SERVIDOR = "
                                + cursor.getString(cursor.getColumnIndex("ID_CADASTRO")) + " AND FINALIZADO <> 'N' AND F_CLIENTE <> 'S' " /* AND ATIVO = 'S'*/ + "AND F_VENDEDOR = 'N') OR (ID_CADASTRO = "
                                +  cursor.getString(cursor.getColumnIndex("ID_CADASTRO")) + " AND (ID_CADASTRO_SERVIDOR <= 0 OR ID_CADASTRO_SERVIDOR IS NULL) )");
                }
                if ( listaCliente.size() > 0) {
                    webPedido.setCadastro( listaCliente.get(0));
                } else {
                    listaCliente = clienteDAO.getLista("SELECT * FROM TBL_CADASTRO WHERE ID_CADASTRO_SERVIDOR = " + cursor.getString(cursor.getColumnIndex("ID_CADASTRO")));
                    if ( listaCliente.size() > 0) {
                        webPedido.setCadastro( listaCliente.get(0) );
                    } else {
                        if ( cursor.getString(cursor.getColumnIndex("ID_CADASTRO")).equalsIgnoreCase("0")){
                            Cliente cliente = new Cliente();
                            cliente.setIdCategoria(1);
                            cliente.setId_cadastro_servidor(0);
                            cliente.setId_cadastro(0);
                            cliente.setNome_cadastro("CLIENTE NÃO INFORMADO");

                            cliente.setNome_fantasia("-");
                            EmpresaParametro empresaParametro = new EmpresaParametro();
                            EmpresaParametroDAO empresaParametroDAO = new EmpresaParametroDAO(db);
                            empresaParametro = empresaParametroDAO.getLista("SELECT * FROM TBL_EMPRESA_PARAMETRO").get(0);
                            cliente.setTipo_tabela_preco(empresaParametro.getTipo_tabela_preco());
                            cliente.setUtiliza_tabela_preco(empresaParametro.getUtiliza_tabela_preco());
                            webPedido.setCadastro(cliente);
                        }else {
                            Cliente cliente = new Cliente();
                            cliente.setIdCategoria(1);
                            cliente.setId_cadastro_servidor(0);
                            cliente.setId_cadastro(0);
                            cliente.setNome_cadastro("CLIENTE NÃO LOCALIZADO");

                            cliente.setNome_fantasia("-");
                            EmpresaParametro empresaParametro = new EmpresaParametro();
                            EmpresaParametroDAO empresaParametroDAO = new EmpresaParametroDAO(db);
                            empresaParametro = empresaParametroDAO.getLista("SELECT * FROM TBL_EMPRESA_PARAMETRO").get(0);
                            cliente.setTipo_tabela_preco(empresaParametro.getTipo_tabela_preco());
                            cliente.setUtiliza_tabela_preco(empresaParametro.getUtiliza_tabela_preco());
                            webPedido.setCadastro(cliente);
                        }
                    }
                }
                webPedido.setId_vendedor(cursor.getString(cursor.getColumnIndex("ID_VENDEDOR")));
                webPedido.setId_condicao_pagamento(cursor.getString(cursor.getColumnIndex("ID_CONDICAO_PAGAMENTO")));
                webPedido.setId_operacao(cursor.getString(cursor.getColumnIndex("ID_OPERACAO")));
                webPedido.setId_tabela(cursor.getString(cursor.getColumnIndex("ID_TABELA")));
                webPedido.setNome_extenso(cursor.getString(cursor.getColumnIndex("NOME_EXTENSO")));
                webPedido.setData_emissao(cursor.getString(cursor.getColumnIndex("DATA_EMISSAO")));
                webPedido.setValor_produtos(cursor.getString(cursor.getColumnIndex("VALOR_PRODUTOS")));
                webPedido.setValor_desconto(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO")));
                webPedido.setValor_desconto_add(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_ADD")));
                webPedido.setDesconto_per(cursor.getString(cursor.getColumnIndex("DESCONTO_PER")));
                webPedido.setDesconto_per_add(cursor.getString(cursor.getColumnIndex("DESCONTO_PER_ADD")));
                webPedido.setValor_total(cursor.getString(cursor.getColumnIndex("VALOR_TOTAL")));
                webPedido.setExcluido(cursor.getString(cursor.getColumnIndex("EXCLUIDO")));
                webPedido.setExcluido_usuario_id(cursor.getString(cursor.getColumnIndex("EXCLUIDO_USUARIO_ID")));
                webPedido.setExcluido_usuario_nome(cursor.getString(cursor.getColumnIndex("EXCLUIDO_USUARIO_NOME")));
                webPedido.setExcluido_usuario_data(cursor.getString(cursor.getColumnIndex("EXCLUIDO_USUARIO_DATA")));
                webPedido.setJustificativa_exclusao(cursor.getString(cursor.getColumnIndex("JUSTIFICATIVA_EXCLUSAO")));
                webPedido.setUsuario_lancamento_id(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_ID")));
                webPedido.setUsuario_lancamento_nome(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_NOME")));
                webPedido.setUsuario_lancamento_data(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_DATA")));
                webPedido.setObservacoes(cursor.getString(cursor.getColumnIndex("OBSERVACOES")));
                webPedido.setStatus(cursor.getString(cursor.getColumnIndex("STATUS")));
                webPedido.setId_pedido_venda(cursor.getString(cursor.getColumnIndex("ID_PEDIDO_VENDA")));
                webPedido.setId_nota_fiscal(cursor.getString(cursor.getColumnIndex("ID_NOTA_FISCAL")));
                webPedido.setId_tabela_preco_faixa(cursor.getString(cursor.getColumnIndex("ID_TABELA_PRECO_FAIXA")));
                webPedido.setPontos_total(cursor.getString(cursor.getColumnIndex("PONTOS_TOTAL")));
                webPedido.setPontos_coeficiente(cursor.getString(cursor.getColumnIndex("PONTOS_COEFICIENTE")));
                webPedido.setPontos_cor(cursor.getString(cursor.getColumnIndex("PONTOS_COR")));
                webPedido.setComissao_percentual(cursor.getString(cursor.getColumnIndex("COMISSAO_PERCENTUAL")));
                webPedido.setComissao_valor(cursor.getString(cursor.getColumnIndex("COMISSAO_VALOR")));
                webPedido.setId_faixa_final(cursor.getString(cursor.getColumnIndex("ID_FAIXA_FINAL")));
                webPedido.setValor_bonus_credor(cursor.getString(cursor.getColumnIndex("VALOR_BONUS_CREDOR")));
                webPedido.setPerc_bonus_credor(cursor.getString(cursor.getColumnIndex("PERC_BONUS_CREDOR")));
                webPedido.setId_web_pedido_servidor(cursor.getString(cursor.getColumnIndex("ID_WEB_PEDIDO_SERVIDOR")));
                webPedido.setData_prev_entrega(cursor.getString(cursor.getColumnIndex("DATA_PREV_ENTREGA")));
                webPedido.setPedido_enviado(cursor.getString(cursor.getColumnIndex("PEDIDO_ENVIADO")));
                webPedido.setFinalizado(cursor.getString(cursor.getColumnIndex("FINALIZADO")));
                //Novos campos
                webPedido.setId_pedido(cursor.getString(cursor.getColumnIndex("ID_PEDIDO")));
                webPedido.setWeb_importado_data(cursor.getString(cursor.getColumnIndex("WEB_IMPORTADO_DATA")));
                webPedido.setAnalise_credito(cursor.getString(cursor.getColumnIndex("ANALISE_CREDITO")));
                webPedido.setAnalise_usuario_id(cursor.getString(cursor.getColumnIndex("ANALISE_USUARIO_ID")));
                webPedido.setAnalise_usuario_nome(cursor.getString(cursor.getColumnIndex("ANALISE_USUARIO_NOME")));
                webPedido.setAnalise_usuario_data(cursor.getString(cursor.getColumnIndex("ANALISE_USUARIO_DATA")));
                webPedido.setOrdem_carregamento(cursor.getString(cursor.getColumnIndex("ORDEM_CARREGAMENTO")));
                webPedido.setId_ordem_carregamento(cursor.getString(cursor.getColumnIndex("ID_ORDEM_CARREGAMENTO")));
                webPedido.setId_transportadora(cursor.getString(cursor.getColumnIndex("ID_TRANSPORTADORA")));
                webPedido.setNome_transportadora(cursor.getString(cursor.getColumnIndex("NOME_TRANSPORTADORA")));
                webPedido.setNumero_nota_fiscal(cursor.getString(cursor.getColumnIndex("NUMERO_NOTA_FISCAL")));
                webPedido.setData_nota_fiscal(cursor.getString(cursor.getColumnIndex("DATA_NOTA_FISCAL")));
                webPedido.setEspecie(cursor.getString(cursor.getColumnIndex("ESPECIE")));
                webPedido.setVolumes(cursor.getString(cursor.getColumnIndex("VOLUMES")));
                webPedido.setWeb_valor_total(cursor.getString(cursor.getColumnIndex("WEB_VALOR_TOTAL")));
                webPedido.setExpedicao_status(cursor.getString(cursor.getColumnIndex("EXPEDICAO_STATUS")));
                webPedido.setValor_faturado(cursor.getString(cursor.getColumnIndex("VALOR_FATURADO")));
                webPedido.setId_moeda_padrao(cursor.getString(cursor.getColumnIndex("ID_MOEDA_PADRAO")));
                try {
                    webPedido.setValor_recibo(cursor.getString(cursor.getColumnIndex("VALOR_RECIBO")));
                } catch ( NullPointerException e) {
                    webPedido.setValor_recibo("0.00");
                    e.printStackTrace();
                } catch ( Exception e) {
                    webPedido.setValor_recibo("0.00");
                    e.printStackTrace();
                }
                try {
                    webPedido.setValor_icms_st(cursor.getString(cursor.getColumnIndex("VALOR_ICMS_ST")));
                } catch ( NullPointerException e) {
                    webPedido.setValor_icms_st("0.00");
                } catch ( Exception e) {
                    webPedido.setValor_icms_st("0.00");
                }
                try {
                    if ( cursor.getString(cursor.getColumnIndex("DESCONTO_INDEVIDO")).isEmpty() || cursor.getString(cursor.getColumnIndex("DESCONTO_INDEVIDO")) == null )
                        webPedido.setDescontoIndevido("N");
                    else
                        webPedido.setDescontoIndevido(cursor.getString(cursor.getColumnIndex("DESCONTO_INDEVIDO")));
                } catch ( NullPointerException e) {
                    webPedido.setDescontoIndevido("N");
                } catch ( Exception e) {
                    webPedido.setDescontoIndevido("N");
                }

                try {
                    if ( cursor.getString(cursor.getColumnIndex("ID_PEDIDO_APP")).isEmpty() || cursor.getString(cursor.getColumnIndex("ID_PEDIDO_APP")) == null )
                        webPedido.setId_pedido_app("");
                    else
                        webPedido.setId_pedido_app(cursor.getString(cursor.getColumnIndex("ID_PEDIDO_APP")));
                } catch ( NullPointerException e) {
                    webPedido.setId_pedido_app("");
                } catch ( Exception e) {
                    webPedido.setId_pedido_app("");
                }

                try {
                    if ( cursor.getString(cursor.getColumnIndex("PRAZO_CONFIRMADO")).isEmpty() || cursor.getString(cursor.getColumnIndex("PRAZO_CONFIRMADO")) == null )
                        webPedido.setPrazo_confirmado("N");
                    else
                        webPedido.setPrazo_confirmado(cursor.getString(cursor.getColumnIndex("PRAZO_CONFIRMADO")));
                } catch ( NullPointerException e) {
                    webPedido.setPrazo_confirmado("N");
                } catch ( Exception e) {
                    webPedido.setPrazo_confirmado("N");
                }

                try {
                    webPedido.setValor_desconto_adic(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_ADIC")));
                    webPedido.setPerc_desconto_adic(cursor.getString(cursor.getColumnIndex("PERC_DESCONTO_ADIC")));
                } catch ( NullPointerException e) {
                    webPedido.setValor_desconto_adic("0.00");
                    webPedido.setPerc_desconto_adic("0.00");
                }catch ( Exception e) {
                    webPedido.setValor_desconto_adic("0.00");
                    webPedido.setPerc_desconto_adic("0.00");
                }
                lista.add(webPedido);
            } catch (CursorIndexOutOfBoundsException|NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {

            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public int getTotalReg(String SQL) {
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("TOTALREG"));
    }
}
