package com.example.rcksuporte05.rcksistemas.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuItemCompat;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.BO.CadastroAnexoBO;
import com.example.rcksuporte05.rcksistemas.BO.PedidoBO;
import com.example.rcksuporte05.rcksistemas.BO.UsuarioBO;
import com.example.rcksuporte05.rcksistemas.BuildConfig;
import com.example.rcksuporte05.rcksistemas.DAO.CadastroAnexoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.CampanhaComercialCabDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ClienteDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.EmpresaParametroDAO;
import com.example.rcksuporte05.rcksistemas.DAO.PreferenciaDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ProdutoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.VendedorBonusResumoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoItensDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ListaPedidoAdapter;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.model.CadastroAnexo;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.EmpresaParametro;
import com.example.rcksuporte05.rcksistemas.model.Preferencia;
import com.example.rcksuporte05.rcksistemas.model.Produto;
import com.example.rcksuporte05.rcksistemas.model.Sincronia;
import com.example.rcksuporte05.rcksistemas.model.Usuario;
import com.example.rcksuporte05.rcksistemas.model.VendedorBonusResumo;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.PDFPedidoICMSST;
import com.example.rcksuporte05.rcksistemas.util.PDFPedidoUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPedidoPendente extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ListaPedidoAdapter.PedidoAdapterListener {

    ActionModeCallback actionModeCallback;
    @BindView(R.id.listaPedidosPendentes)
    RecyclerView recyclerView;
    private List<WebPedido> listaPedido = new ArrayList();
    private EditText edtTotalPedidos;
    private DBHelper db = new DBHelper(this);
    private WebPedidoDAO webPedidoDAO = new WebPedidoDAO(db);
    private WebPedidoItensDAO webPedidoItensDAO = new WebPedidoItensDAO(db);
    private PreferenciaDAO preferenciaDAO = new PreferenciaDAO(db);
    private EmpresaParametroDAO empresaParametroDAO = new EmpresaParametroDAO(db);
    private Configuracao configuracao = new Configuracao();
    private Usuario usuario;
    private ProgressDialog progress;
    private PedidoBO pedidoBO = new PedidoBO();
    private ListaPedidoAdapter listaPedidoAdapter;
    private MenuItem geraPDF;
    private MenuItem emailPDF;
    private MenuItem duplicaPedido;
    private MenuItem enviaPedido;
    private ActionMode actionMode;

    private int offSet = 0;
    private int totalReg = 0;
    private int limitReg = 50;
    private String SQL;
    PackageInfo pInfo = null;



    @OnClick(R.id.btnNovoPedido)
    public void btnNovoPedido() {
        startActivity(new Intent(this, ActivityPedidoMain.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem_pedido_pendente);
        ButterKnife.bind(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        webPedidoDAO = new WebPedidoDAO(db);
        webPedidoItensDAO = new WebPedidoItensDAO(db);
        Toolbar toolbar = findViewById(R.id.toolbarPedidoPendente);
        toolbar.setTitle("Pedidos Pendentes");
        setSupportActionBar(toolbar);
        usuario = UsuarioHelper.getUsuario();
        edtTotalPedidos = findViewById(R.id.edtNumeroPedidoPendente);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        recyclerView.addOnScrollListener(new ActivityPedidoPendente.ScrollListener());
        actionModeCallback = new ActionModeCallback();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_pedido_pendente:
                List<WebPedido> pedidosPendentes = webPedidoDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario() + " AND FINALIZADO = 'S' AND ID_CADASTRO > 0 ORDER BY ID_WEB_PEDIDO DESC;");
                if (pedidosPendentes.size() > 0)
                    if (pedidosPendentes.size() == 1)
                        showMsgSimNao(null, item.getItemId(), pedidosPendentes, "Deseja enviar o pedido pendente não sincronizado  para ser faturado?");
                    else
                        showMsgSimNao(null, item.getItemId(), pedidosPendentes, "Deseja enviar todos os pedidos pendentes não sincronizados para serem faturados?");
                else
                    showMsgSucesso("Não há pedidos pendentes para enviar!");
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void getConfiguracao() {
        ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
        configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
    }
    private class ScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            try {
                if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("N")) {
                    edtTotalPedidos.setText("(" + (getFirstItem() + 1) + " A " + (getLastItem() + 1) + ")/" + totalReg);
                    return;
                }
            } catch (NullPointerException e) {
                return;
            }
            if (!recyclerView.canScrollVertically(1)) {
                if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S")) {
                    offSet += limitReg;
                    listaPedido.addAll( webPedidoDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet));
                    listaPedidoAdapter.notifyDataSetChanged();
                }
            }
            edtTotalPedidos.setText( "(" + (getFirstItem() + 1) + " A " + (getLastItem() + 1) + ")/" + totalReg);
        }

    }
    private int getFirstItem(){
        return ((LinearLayoutManager)recyclerView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private int getLastItem() {
        return ((LinearLayoutManager) recyclerView.getLayoutManager())
                .findLastVisibleItemPosition();
    }

    public void sincronizaPedidos(final List<WebPedido> listaParaEnvio) {
        final NotificationCompat.Builder notificacao = new NotificationCompat.Builder(ActivityPedidoPendente.this)
                .setSmallIcon(R.mipmap.ic_enviar_pedidos)
                .setContentTitle("Enviando pedidos")
                .setContentText("Estabelecendo Conexão")
                .setProgress(0, 0, true)
                .setPriority(2);
        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificacao.build());
        prepararItensPedidos(listaParaEnvio);

        final Rotas apiRotas = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Sincronia sincronia = new Sincronia(true,
                true);
        sincronia.setListaWebPedidosPendentes( listaParaEnvio );
        Call<Sincronia> call =  apiRotas.sincronizaPedido( UsuarioHelper.getUsuario().getId_usuario(), sincronia ,cabecalho);

        call.enqueue(new Callback<Sincronia>() {
            @Override
            public void onResponse(Call<Sincronia> call, Response<Sincronia> response) {
                if (response.code() == 200) {
                    final Sincronia sincronia1 = response.body();
                    final List<WebPedido> webPedidosEnviados = sincronia1.getListaWebPedidosPendentes();
                    int totalSincronizado = 0;
                    int totalAtualizado = 0;
                    try {
                        if (sincronia1.getListaProduto().size() > 0) {
                            ProdutoDAO produtoDAO = new ProdutoDAO(db);
                            for (Produto produto : sincronia1.getListaProduto())
                                if (produtoDAO.updateSaldoEstoque(produto) > 0)
                                    System.gc();
                            System.gc();
                        }
                    } catch ( NullPointerException e) {
                    } catch ( Exception e) {
                    }
                    ClienteDAO clienteDAO = new ClienteDAO(db);
                    for (WebPedido pedido : webPedidosEnviados) {
                        totalSincronizado = webPedidosEnviados.size();
                        final CadastroAnexoDAO cadastroAnexoDAO = new CadastroAnexoDAO(db);
                        pedido.getCadastro().setAlterado("N");
                        clienteDAO.update(pedido.getCadastro());
                        try {
                            db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(pedido.getCadastro().getId_cadastro())});
                        } catch ( Exception e) {
                            e.printStackTrace();
                        }
                        if (pedido.getCadastro().getListaCadastroAnexo().size() > 0)
                            for (CadastroAnexo cadastroAnexo : pedido.getCadastro().getListaCadastroAnexo()) {
                                if (cadastroAnexo.getExcluido().equals("N"))
                                    cadastroAnexoDAO.addUpdate(cadastroAnexo);
                                else if (cadastroAnexo.getExcluido().equals("S"))
                                    try {
                                        db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_ANEXO = ?", new String[]{String.valueOf(cadastroAnexo.getIdAnexo())});
                                    } catch ( Exception e) {
                                        e.printStackTrace();
                                    }
                            }
                        else
                            try {
                                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(pedido.getCadastro().getId_cadastro())});
                            } catch ( Exception e) {
                                e.printStackTrace();
                            }
                        if ( webPedidoDAO.update(pedido) <= 0)
                            totalSincronizado--;
                        for (WebPedidoItens pedidoIten : pedido.getWebPedidoItens())
                            webPedidoItensDAO.update(pedidoIten);
                        excluirPedidoSincronizado();
                    }

                    PendingIntent pendingIntent = PendingIntent.getActivity(ActivityPedidoPendente.this, 0, new Intent(ActivityPedidoPendente.this, ActivityPedidoEnviado.class), 0);
                    notificacao.setContentTitle("Pedidos enviados com sucesso!")
                            .setContentText(webPedidosEnviados.size() + " pedidos enviados.")
                            .setSmallIcon(R.mipmap.ic_sincronia_sucesso)
                            .setPriority(2)
                            .setDefaults(NotificationCompat.DEFAULT_ALL)
                            .addAction(0, "PEDIDOS ENVIADOS", pendingIntent)
                            .setProgress(0, 0, false)
                            .setAutoCancel(true);
                    notificationManager.notify(0, notificacao.build());
                    onResume();
                    progress.dismiss();
                    showMsgSucesso(String.valueOf(totalSincronizado) +  " pedido(s) sincronizado(s) com sucesso\n");
                } else {
                    boolean produtoAtivo = true;
                    String produtosInativo = "";
                    for (int x = 0; x < listaParaEnvio.size(); x++) {
                        if (listaParaEnvio.get(x).getWebPedidoItens().size() > 0) {
                            for (int i = 0; i < listaParaEnvio.get(x).getWebPedidoItens().size(); i++) {
                                if (!listaParaEnvio.get(x).getWebPedidoItens().get(i).getProdutoAtivo()) {
                                    produtoAtivo = false;
                                    if (!produtosInativo.contains(listaParaEnvio.get(x).getWebPedidoItens().get(i).getNome_produto()))
                                        produtosInativo += listaParaEnvio.get(x).getWebPedidoItens().get(i).getNome_produto() + "\n";
                                }
                            }
                        }
                    }
                    if (!produtoAtivo)
                        showMsgAlerta("O(s) produto(s) " + produtosInativo + " foi/foram inativado(s)/excluido(s) da base de dados\nFavor verificar/corrigir o(s) pedido(s) antes de exportar!");
                    else
                        showMsgAlerta("Não foi possivel enviar o pedido! Favor verificar o pedido e seus produtos");
                    progress.dismiss();
                    progress.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Sincronia> call, Throwable t) {
                try {
                    if (!configuracao.getId().isEmpty() && !configuracao.getUrl_ip_2().isEmpty()) {
                        sincronizaPedidos2(listaParaEnvio);
                    } else {
                        onResume();
                        progress.dismiss();
                        notificacao.setContentText("Não foi possivel enviar os pedidos")
                                .setContentTitle("Problema de conexão")
                                .setProgress(0, 0, false)
                                .setSmallIcon(R.mipmap.ic_sem_internet)
                                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                                .setPriority(2);
                        notificationManager.notify(0, notificacao.build());
                        showMsgAlerta("Não foi possivel enviar os pedidos! Verifique sua conexão a rede/internet");

                    }
                } catch (NullPointerException e) {
                    onResume();
                    progress.dismiss();
                    notificacao.setContentText("Não foi possivel enviar os pedidos")
                            .setContentTitle("Problema de conexão")
                            .setProgress(0, 0, false)
                            .setSmallIcon(R.mipmap.ic_sem_internet)
                            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                            .setPriority(2);
                    notificationManager.notify(0, notificacao.build());
                    showMsgAlerta("Não foi possivel enviar os pedidos! Verifique sua conexão a rede/internet");
                }
            }
        });

    }

    public void sincronizaPedidos2(final List<WebPedido> listaParaEnvio) {
        final NotificationCompat.Builder notificacao = new NotificationCompat.Builder(ActivityPedidoPendente.this)
                .setSmallIcon(R.mipmap.ic_enviar_pedidos)
                .setContentTitle("Enviando pedidos")
                .setContentText("Estabelecendo Conexão(2)")
                .setProgress(0, 0, true)
                .setPriority(2);
        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificacao.build());

        prepararItensPedidos(listaParaEnvio);

        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String version = pInfo.versionName.substring(0, 5);

        String url = configuracao.getUrl_ip_2().trim().toString() + ":" + Integer.toString(configuracao.getPorta_2()).trim() + "/rckwhalleAPI" + version + "/ws/";
        Api.url2 = (configuracao.getConexao_segura().isEmpty() || configuracao.getConexao_segura().equalsIgnoreCase("N")) ?
                "http://" + url :
                "https://" + url;

        final Rotas apiRotas2 = Api.buildRetrofit2(true);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Sincronia sincronia = new Sincronia(true,
                true);
        sincronia.setListaWebPedidosPendentes( listaParaEnvio );


        Call<Sincronia> call =  apiRotas2.sincronizaPedido( UsuarioHelper.getUsuario().getId_usuario(), sincronia ,cabecalho);

        call.enqueue(new Callback<Sincronia>() {
            @Override
            public void onResponse(Call<Sincronia> call, Response<Sincronia> response) {
                if (response.code() == 200) {
                    final Sincronia sincronia1 = response.body();
                    final List<WebPedido> webPedidosEnviados = sincronia1.getListaWebPedidosPendentes();
                    int totalSincronizado = 0;
                    int totalAtualizado = 0;
                    try {
                        if (sincronia1.getListaProduto().size() > 0) {
                            ProdutoDAO produtoDAO = new ProdutoDAO(db);
                            for (Produto produto : sincronia1.getListaProduto())
                                if (produtoDAO.updateSaldoEstoque(produto) > 0)
                                    System.gc();
                            System.gc();
                        }
                    } catch ( NullPointerException e) {
                    } catch ( Exception e) {
                    }
                    ClienteDAO clienteDAO = new ClienteDAO(db);
                    for (WebPedido pedido : webPedidosEnviados) {
                        totalSincronizado = webPedidosEnviados.size();
                        final CadastroAnexoDAO cadastroAnexoDAO = new CadastroAnexoDAO(db);
                        pedido.getCadastro().setAlterado("N");
                        clienteDAO.update(pedido.getCadastro());
                        try {
                            db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(pedido.getCadastro().getId_cadastro())});
                        } catch ( Exception e) {
                            e.printStackTrace();
                        }
                        if (pedido.getCadastro().getListaCadastroAnexo().size() > 0)
                            for (CadastroAnexo cadastroAnexo : pedido.getCadastro().getListaCadastroAnexo()) {
                                if (cadastroAnexo.getExcluido().equals("N"))
                                    cadastroAnexoDAO.addUpdate(cadastroAnexo);
                                else if (cadastroAnexo.getExcluido().equals("S"))
                                    try {
                                        db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_ANEXO = ?", new String[]{String.valueOf(cadastroAnexo.getIdAnexo())});
                                    } catch ( Exception e) {
                                        e.printStackTrace();
                                    }
                            }
                        else
                            try {
                                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(pedido.getCadastro().getId_cadastro())});
                            } catch ( Exception e) {
                                e.printStackTrace();
                            }
                        if ( webPedidoDAO.update(pedido) <= 0)
                            totalSincronizado--;
                        for (WebPedidoItens pedidoIten : pedido.getWebPedidoItens())
                            webPedidoItensDAO.update(pedidoIten);
                        excluirPedidoSincronizado();
                    }

                    PendingIntent pendingIntent = PendingIntent.getActivity(ActivityPedidoPendente.this, 0, new Intent(ActivityPedidoPendente.this, ActivityPedidoEnviado.class), 0);
                    notificacao.setContentTitle("Pedidos enviados com sucesso!")
                            .setContentText(webPedidosEnviados.size() + " pedidos enviados.")
                            .setSmallIcon(R.mipmap.ic_sincronia_sucesso)
                            .setPriority(2)
                            .setDefaults(NotificationCompat.DEFAULT_ALL)
                            .addAction(0, "PEDIDOS ENVIADOS", pendingIntent)
                            .setProgress(0, 0, false)
                            .setAutoCancel(true);
                    notificationManager.notify(0, notificacao.build());
                    onResume();
                    progress.dismiss();
                    showMsgSucesso(String.valueOf(totalSincronizado) +  " pedido(s) sincronizado(s) com sucesso\n");
                } else {
                    boolean produtoAtivo = true;
                    String produtosInativo = "";
                    for (int x = 0; x < listaParaEnvio.size(); x++) {
                        if (listaParaEnvio.get(x).getWebPedidoItens().size() > 0) {
                            for (int i = 0; i < listaParaEnvio.get(x).getWebPedidoItens().size(); i++) {
                                if (!listaParaEnvio.get(x).getWebPedidoItens().get(i).getProdutoAtivo()) {
                                    produtoAtivo = false;
                                    if (!produtosInativo.contains(listaParaEnvio.get(x).getWebPedidoItens().get(i).getNome_produto()))
                                        produtosInativo += listaParaEnvio.get(x).getWebPedidoItens().get(i).getNome_produto() + "\n";
                                }
                            }
                        }
                    }
                    if (!produtoAtivo)
                        showMsgAlerta("O(s) produto(s) " + produtosInativo + " foi/foram inativado(s)/excluido(s) da base de dados\nFavor verificar/corrigir o(s) pedido(s) antes de exportar!!");
                    else
                        showMsgAlerta("Não foi possivel enviar o pedido! Favor verificar o pedido e seus produtos, e tente novamente!");
                    progress.dismiss();
                    progress.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Sincronia> call, Throwable t) {
                onResume();
                progress.dismiss();
                notificacao.setContentText("Não foi possivel enviar os pedidos!!")
                        .setContentTitle("Problema de conexão!")
                        .setProgress(0, 0, false)
                        .setSmallIcon(R.mipmap.ic_sem_internet)
                        .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                        .setPriority(2);
                notificationManager.notify(0, notificacao.build());
                showMsgAlerta("Não foi possivel enviar os pedidos! Verifique sua conexão a rede/internet e tente novamente!");
            }
        });

    }

    public void sincronizaPedido(final WebPedido webPedido) {
        boolean novoCliente = false;
        final NotificationCompat.Builder notificacao = new NotificationCompat.Builder(ActivityPedidoPendente.this)
                .setSmallIcon(R.mipmap.ic_enviar_pedidos)
                .setContentTitle("Enviando pedido " + webPedido.getId_web_pedido())
                .setProgress(0, 0, true)
                .setPriority(2);
        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificacao.build());

        if (webPedido.getCadastro().getId_cadastro_servidor() <= -1) {
            novoCliente = true;
            if (db.contagem("SELECT COUNT(ID_ANEXO) FROM TBL_CADASTRO_ANEXOS WHERE ID_CADASTRO = " + webPedido.getCadastro().getId_cadastro() + " AND EXCLUIDO = 'N';") > 0) {
                final CadastroAnexoBO cadastroAnexoBO = new CadastroAnexoBO();
                List<CadastroAnexo> listaCadastroAnexo = cadastroAnexoBO.listaCadastroAnexoComMiniatura(ActivityPedidoPendente.this, webPedido.getCadastro().getId_cadastro());
                webPedido.getCadastro().setListaCadastroAnexo(listaCadastroAnexo);
            }
            //webPedido.getCadastro().setId_cadastro_servidor(0);
        }
        if ( webPedido.getId_vendedor().isEmpty() || webPedido.getId_vendedor().equalsIgnoreCase("0") || webPedido.getId_vendedor().equalsIgnoreCase(null)) {
            try {
                if (!UsuarioHelper.getUsuario().getId_usuario().isEmpty()) {
                    UsuarioBO usuarioBO = new UsuarioBO();
                    UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
                }
                webPedido.setId_vendedor( UsuarioHelper.getUsuario().getId_quando_vendedor());
            } catch ( NullPointerException e) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
                webPedido.setId_vendedor( UsuarioHelper.getUsuario().getId_quando_vendedor());
            } catch ( Exception e) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
                webPedido.setId_vendedor( UsuarioHelper.getUsuario().getId_quando_vendedor());
            }

        }
        final boolean finalNovoCliente = novoCliente;
        final List<WebPedido> pedido = new ArrayList<>();
        pedido.add(prepararItemPedido(webPedido));

        final Rotas apiRotas = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Sincronia sincronia = new Sincronia(true,
                true);
        sincronia.setListaWebPedidosPendentes( pedido );
        Call<Sincronia> call =  apiRotas.sincronizaPedido( UsuarioHelper.getUsuario().getId_usuario(), sincronia ,cabecalho);
        call.enqueue(new Callback<Sincronia>() {
            @Override
            public void onResponse(Call<Sincronia> call, Response<Sincronia> response) {
                if (response.code() == 200) {
                    final Sincronia sincronia1 = response.body();
                    final List<WebPedido> webPedidosEnviados = sincronia1.getListaWebPedidosPendentes();
                    try {
                        if (sincronia1.getListaProduto().size() > 0) {
                            ProdutoDAO produtoDAO = new ProdutoDAO(db);
                            for (Produto produto : sincronia1.getListaProduto())
                                if (produtoDAO.updateSaldoEstoque(produto) > 0)
                                    System.gc();
                            System.gc();
                        }
                    } catch (NullPointerException e) {
                    } catch (Exception e) {
                    }
                    int totalSincronizado = 1;
                    if (finalNovoCliente) {
                        ClienteDAO clienteDAO = new ClienteDAO(db);
                        final CadastroAnexoDAO cadastroAnexoDAO = new CadastroAnexoDAO(db);
                        webPedidosEnviados.get(0).getCadastro().setAlterado("N");
                        clienteDAO.update(webPedidosEnviados.get(0).getCadastro());
                        try {
                            db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(webPedidosEnviados.get(0).getCadastro().getId_cadastro())});
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (webPedidosEnviados.get(0).getCadastro().getListaCadastroAnexo().size() > 0)
                            for (CadastroAnexo cadastroAnexo : webPedidosEnviados.get(0).getCadastro().getListaCadastroAnexo()) {
                                if (cadastroAnexo.getExcluido().equals("N"))
                                    cadastroAnexoDAO.addUpdate(cadastroAnexo);
                                else if (cadastroAnexo.getExcluido().equals("S"))
                                    try {
                                        db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_ANEXO = ?", new String[]{String.valueOf(cadastroAnexo.getIdAnexo())});
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                            }
                        else
                            try {
                                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(webPedidosEnviados.get(0).getCadastro().getId_cadastro())});
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                    }
                    if (webPedidoDAO.update(webPedidosEnviados.get(0)) <= 0)
                        totalSincronizado--;
                    for (WebPedidoItens pedidoIten : webPedidosEnviados.get(0).getWebPedidoItens())
                        webPedidoItensDAO.update(pedidoIten);
                    excluirPedidoSincronizado();
                    PendingIntent pendingIntent = PendingIntent.getActivity(ActivityPedidoPendente.this, 0, new Intent(ActivityPedidoPendente.this, ActivityPedidoEnviado.class), 0);
                    notificacao.setContentTitle("Pedido " + webPedidosEnviados.get(0).getId_web_pedido_servidor() + " enviado com sucesso!")
                            .setSmallIcon(R.mipmap.ic_sincronia_sucesso)
                            .setPriority(2)
                            .setDefaults(NotificationCompat.DEFAULT_ALL)
                            .addAction(0, "PEDIDOS ENVIADOS", pendingIntent)
                            .setProgress(0, 0, false)
                            .setAutoCancel(true);
                    notificationManager.notify(0, notificacao.build());
                    onResume();
                    progress.dismiss();
                } else {
                    boolean produtoAtivo = true;
                    String produtosInativo = "";
                    if (pedido.get(0).getWebPedidoItens().size() > 0) {
                        for (int i = 0; i < pedido.get(0).getWebPedidoItens().size(); i++) {
                            if (!pedido.get(0).getWebPedidoItens().get(i).getProdutoAtivo()) {
                                produtoAtivo = false;
                                produtosInativo += pedido.get(0).getWebPedidoItens().get(i).getNome_produto() + "\n";
                            }
                        }

                    }
                    if ( !produtoAtivo)
                        showMsgAlerta("O(s) produto(s) " + produtosInativo + " foi/foram inativado(s)/excluido(s) da base de dados\nFavor verificar/corrigir o pedido antes de exportar!");
                    else
                        showMsgAlerta("Não foi possivel enviar o pedido! Favor verificar o pedido e seus produtos");
                    progress.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Sincronia> call, Throwable t) {
                try {
                    if (!configuracao.getId().isEmpty() && !configuracao.getUrl_ip_2().isEmpty()) {
                        sincronizaPedidos2(pedido);
                    } else {
                        notificacao.setContentText("Não foi possivel enviar os pedidos")
                                .setContentTitle("Problema de conexão")
                                .setProgress(0, 0, false)
                                .setSmallIcon(R.mipmap.ic_sem_internet)
                                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                                .setPriority(2);
                        notificationManager.notify(0, notificacao.build());
                        onResume();
                        progress.dismiss();
                        showMsgAlerta("Não foi possivel enviar o pedido! Verifique sua conexão a rede/internet");
                    }
                } catch (NullPointerException e) {
                    notificacao.setContentText("Não foi possivel enviar os pedidos")
                            .setContentTitle("Problema de conexão")
                            .setProgress(0, 0, false)
                            .setSmallIcon(R.mipmap.ic_sem_internet)
                            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                            .setPriority(2);
                    notificationManager.notify(0, notificacao.build());
                    onResume();
                    progress.dismiss();
                    showMsgAlerta("Não foi possivel enviar o pedido! Verifique sua conexão a rede/internet");
                }

            }
        });
    }

    public void sincronizaPedido2(final WebPedido webPedido) {
        boolean novoCliente = false;
        final NotificationCompat.Builder notificacao = new NotificationCompat.Builder(ActivityPedidoPendente.this)
                .setSmallIcon(R.mipmap.ic_enviar_pedidos)
                .setContentTitle("Enviando pedido " + webPedido.getId_web_pedido())
                .setProgress(0, 0, true)
                .setPriority(2);
        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificacao.build());

        if (webPedido.getCadastro().getId_cadastro_servidor() <= -1) {
            novoCliente = true;
            if (db.contagem("SELECT COUNT(ID_ANEXO) FROM TBL_CADASTRO_ANEXOS WHERE ID_CADASTRO = " + webPedido.getCadastro().getId_cadastro() + " AND EXCLUIDO = 'N';") > 0) {
                final CadastroAnexoBO cadastroAnexoBO = new CadastroAnexoBO();
                List<CadastroAnexo> listaCadastroAnexo = cadastroAnexoBO.listaCadastroAnexoComMiniatura(ActivityPedidoPendente.this, webPedido.getCadastro().getId_cadastro());
                webPedido.getCadastro().setListaCadastroAnexo(listaCadastroAnexo);
            }
            //webPedido.getCadastro().setId_cadastro_servidor(0);
        }
        if ( webPedido.getId_vendedor().isEmpty() || webPedido.getId_vendedor().equalsIgnoreCase("0") || webPedido.getId_vendedor().equalsIgnoreCase(null)) {
            try {
                if (!UsuarioHelper.getUsuario().getId_usuario().isEmpty()) {
                    UsuarioBO usuarioBO = new UsuarioBO();
                    UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
                }
                webPedido.setId_vendedor( UsuarioHelper.getUsuario().getId_quando_vendedor());
            } catch ( NullPointerException e) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
                webPedido.setId_vendedor( UsuarioHelper.getUsuario().getId_quando_vendedor());
            } catch ( Exception e) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
                webPedido.setId_vendedor( UsuarioHelper.getUsuario().getId_quando_vendedor());
            }

        }
        final boolean finalNovoCliente = novoCliente;
        final List<WebPedido> pedido = new ArrayList<>();
        pedido.add(prepararItemPedido(webPedido));


        String version = pInfo.versionName.substring(0, 5);

        String url = configuracao.getUrl_ip_2().trim().toString() + ":" + Integer.toString(configuracao.getPorta_2()).trim() + "/rckwhalleAPI" + version + "/ws/";
        Api.url2 = (configuracao.getConexao_segura().isEmpty() || configuracao.getConexao_segura().equalsIgnoreCase("N")) ?
                "http://" + url :
                "https://" + url;

        final Rotas apiRotas2 = Api.buildRetrofit2(true);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Sincronia sincronia = new Sincronia(true,
                true);
        sincronia.setListaWebPedidosPendentes( pedido );
        Call<Sincronia> call =  apiRotas2.sincronizaPedido( UsuarioHelper.getUsuario().getId_usuario(), sincronia ,cabecalho);
        call.enqueue(new Callback<Sincronia>() {
            @Override
            public void onResponse(Call<Sincronia> call, Response<Sincronia> response) {
                if (response.code() == 200) {
                    final Sincronia sincronia1 = response.body();
                    final List<WebPedido> webPedidosEnviados = sincronia1.getListaWebPedidosPendentes();
                    try {
                        if (sincronia1.getListaProduto().size() > 0) {
                            ProdutoDAO produtoDAO = new ProdutoDAO(db);
                            for (Produto produto : sincronia1.getListaProduto())
                                if (produtoDAO.updateSaldoEstoque(produto) > 0)
                                    System.gc();
                            System.gc();
                        }
                    } catch (NullPointerException e) {
                    } catch (Exception e) {
                    }
                    int totalSincronizado = 1;
                    if (finalNovoCliente) {
                        ClienteDAO clienteDAO = new ClienteDAO(db);
                        final CadastroAnexoDAO cadastroAnexoDAO = new CadastroAnexoDAO(db);
                        webPedidosEnviados.get(0).getCadastro().setAlterado("N");
                        clienteDAO.update(webPedidosEnviados.get(0).getCadastro());
                        try {
                            db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(webPedidosEnviados.get(0).getCadastro().getId_cadastro())});
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (webPedidosEnviados.get(0).getCadastro().getListaCadastroAnexo().size() > 0)
                            for (CadastroAnexo cadastroAnexo : webPedidosEnviados.get(0).getCadastro().getListaCadastroAnexo()) {
                                if (cadastroAnexo.getExcluido().equals("N"))
                                    cadastroAnexoDAO.addUpdate(cadastroAnexo);
                                else if (cadastroAnexo.getExcluido().equals("S"))
                                    try {
                                        db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_ANEXO = ?", new String[]{String.valueOf(cadastroAnexo.getIdAnexo())});
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                            }
                        else
                            try {
                                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(webPedidosEnviados.get(0).getCadastro().getId_cadastro())});
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                    }
                    if (webPedidoDAO.update(webPedidosEnviados.get(0)) <= 0)
                        totalSincronizado--;
                    for (WebPedidoItens pedidoIten : webPedidosEnviados.get(0).getWebPedidoItens())
                        webPedidoItensDAO.update(pedidoIten);
                    excluirPedidoSincronizado();
                    PendingIntent pendingIntent = PendingIntent.getActivity(ActivityPedidoPendente.this, 0, new Intent(ActivityPedidoPendente.this, ActivityPedidoEnviado.class), 0);
                    notificacao.setContentTitle("Pedido " + webPedidosEnviados.get(0).getId_web_pedido_servidor() + " enviado com sucesso!")
                            .setSmallIcon(R.mipmap.ic_sincronia_sucesso)
                            .setPriority(2)
                            .setDefaults(NotificationCompat.DEFAULT_ALL)
                            .addAction(0, "PEDIDOS ENVIADOS", pendingIntent)
                            .setProgress(0, 0, false)
                            .setAutoCancel(true);
                    notificationManager.notify(0, notificacao.build());
                    onResume();
                    progress.dismiss();
                } else {
                    boolean produtoAtivo = true;
                    String produtosInativo = "";
                    if (pedido.get(0).getWebPedidoItens().size() > 0) {
                        for (int i = 0; i < pedido.get(0).getWebPedidoItens().size(); i++) {
                            if (!pedido.get(0).getWebPedidoItens().get(i).getProdutoAtivo()) {
                                produtoAtivo = false;
                                produtosInativo += pedido.get(0).getWebPedidoItens().get(i).getNome_produto() + "\n";
                            }
                        }

                    }
                    if ( !produtoAtivo)
                        showMsgAlerta("O(s) produto(s) " + produtosInativo + " foi/foram inativado(s)/excluido(s) da base de dados\nFavor verificar/corrigir o pedido antes de exportar!");
                    else
                        showMsgAlerta("Não foi possivel enviar o pedido! Favor verificar o pedido e seus produtos");
                    progress.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Sincronia> call, Throwable t) {
                notificacao.setContentText("Não foi possivel enviar os pedidos")
                        .setContentTitle("Problema de conexão")
                        .setProgress(0, 0, false)
                        .setSmallIcon(R.mipmap.ic_sem_internet)
                        .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                        .setPriority(2);
                notificationManager.notify(0, notificacao.build());
                onResume();
                progress.dismiss();
                showMsgAlerta("Não foi possivel enviar o pedido! Verifique sua conexão a rede/internet");
            }
        });
    }


    private void excluirPedidoSincronizado() {
        try {
            Preferencia preferencia = preferenciaDAO.getPreferencia("SELECT * FROM TBL_PREFERENCIA");
            if (!preferencia.getId().isEmpty()) {
                if (preferencia.getDias_hist_pedido() > 0) {
                    String data;
                    data = preferencia.getDias_hist_pedido() == 1 ? db.pegaDataExclusao("  SELECT date('now')") : db.pegaDataExclusao("  SELECT date('now',  '-" + ( preferencia.getDias_hist_pedido() + 1 ) + " days')");
                    List<WebPedido> listaPedido = new ArrayList<>();
                     if ( !data.isEmpty())
                         try {
                             listaPedido = webPedidoDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'S' AND USUARIO_LANCAMENTO_ID = " + UsuarioHelper.getUsuario().getId_usuario() + " AND DATA_EMISSAO <= '" + data + "' ORDER BY ID_WEB_PEDIDO_SERVIDOR DESC");
                             for (int i = 0; i < listaPedido.size(); i++)
                                 webPedidoDAO.delete(listaPedido.get(i));
                         } catch (NullPointerException e) {
                             e.printStackTrace();
                         } catch (Exception e) {
                             e.printStackTrace();
                         }
                     else
                         showMsgAlerta("falha ao obter o periodo para exclusão do(s) pedido(s) sincronizado(s)! Pedido(s) não excluido(s)");
                }
            }
        } catch ( NullPointerException e) {
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
    }

    public void prepararItensPedidos(List<WebPedido> listaPedido) {
        for (WebPedido pedido : listaPedido) {
            if (pedido.getCadastro().getId_cadastro_servidor() <= 0) {
                if (db.contagem("SELECT COUNT(ID_ANEXO) FROM TBL_CADASTRO_ANEXOS WHERE ID_CADASTRO = " + pedido.getCadastro().getId_cadastro() + " AND EXCLUIDO = 'N';") > 0) {
                    final CadastroAnexoBO cadastroAnexoBO = new CadastroAnexoBO();
                    List<CadastroAnexo> listaCadastroAnexo = cadastroAnexoBO.listaCadastroAnexoComMiniatura(ActivityPedidoPendente.this, pedido.getCadastro().getId_cadastro());
                    pedido.getCadastro().setListaCadastroAnexo(listaCadastroAnexo);
                }
            }
            List<WebPedidoItens> webPedidoItenses;
            webPedidoItenses = webPedidoItensDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + pedido.getId_web_pedido() + " ORDER BY ID_WEB_ITEM");
            pedido.setWebPedidoItens(webPedidoItenses);
        }
    }

    public WebPedido prepararItemPedido(WebPedido webPedido) {
        List<WebPedidoItens> webPedidoItenses;
        webPedidoItenses = webPedidoItensDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + webPedido.getId_web_pedido() + " ORDER BY ID_WEB_ITEM");
        webPedido.setWebPedidoItens(webPedidoItenses);
        return webPedido;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_pedido_pendente, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView;
        final MenuItem item = menu.findItem(R.id.busca_pedido_pendente);
        searchView = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? (SearchView) item.getActionView() : (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(final String query) {
                if (query.trim().equals("")) {
                    offSet= 0;
                    SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario();
                    totalReg = webPedidoDAO.getTotalReg(SQL);
                    SQL = "SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario() + " ORDER BY ID_WEB_PEDIDO DESC ";
                    if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S"))
                        listaPedido = webPedidoDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet);
                    else
                        listaPedido = webPedidoDAO.getLista(SQL);
                    setRecyclerView(listaPedido);
                    //edtTotalPedidos.setText(listaPedido.size() + ": Pedidos Pendentes");
                } else {
                    List<WebPedido> listaBusca = buscaPedidoPendente(listaPedido, query);
                    try {
                        totalReg = listaBusca.size();
                    } catch ( NumberFormatException|NullPointerException e ){
                        totalReg = 0;
                    } catch ( Exception e) {
                        totalReg = 0;
                    }
                    setRecyclerView(listaBusca);
                    //if (listaBusca.size() > 0)
                        //edtTotalPedidos.setText(listaBusca.size() + ": Pedidos Encontrados");
                    //else
                        //edtTotalPedidos.setText("Nenum pedido encontrado");
                }
                System.gc();
                return false;
            }
        });
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Nome Cliente / Nº pedido");
        return true;
    }

    public List<WebPedido> buscaPedidoPendente(List<WebPedido> webPedidos, String query) {
        final String upperCaseQuery = query.toUpperCase();
        final List<WebPedido> lista = new ArrayList<>();
        for (WebPedido webPedido : webPedidos) {
            try {
                final String nomeCliente = webPedido.getNome_extenso().toUpperCase();
                final String numeroPedido = webPedido.getId_web_pedido().toUpperCase();
                if (nomeCliente.contains(upperCaseQuery) || numeroPedido.equals(upperCaseQuery))
                    lista.add(webPedido);
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch ( Exception e) {
                e.printStackTrace();
            }
        }
        return lista;
    }

    public void setRecyclerView(List<WebPedido> lista) {
        listaPedidoAdapter = new ListaPedidoAdapter(lista, this, this);
        recyclerView.setAdapter(listaPedidoAdapter);
        listaPedidoAdapter.notifyDataSetChanged();
        if (lista.size() > 0)
            edtTotalPedidos.setText("1/" + totalReg);
        else
            edtTotalPedidos.setText("0/" + totalReg);

    }

    @Override
    protected void onResume() {
        getConfiguracao();
        //ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
        //configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        offSet = 0;
        try {
            if ( !configuracao.getLista_todos_pedidos().isEmpty()  && configuracao.getLista_todos_pedidos().equalsIgnoreCase("S")) {
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N'";
                totalReg = webPedidoDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' ORDER BY ID_WEB_PEDIDO DESC ";
            } else {
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario();
                totalReg = webPedidoDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario() + " ORDER BY ID_WEB_PEDIDO DESC ";
            }
            if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S"))
                listaPedido = webPedidoDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet);
            else
                listaPedido = webPedidoDAO.getLista(SQL);
            setRecyclerView(listaPedido);
            if (actionMode != null)
                actionMode.finish();
            listaPedidoAdapter.clearSelections();
        } catch (CursorIndexOutOfBoundsException | NullPointerException e) {
            listaPedido.clear();
            edtTotalPedidos.setText("0/" + totalReg);
            //Toast.makeText(this, "Nenhum pedido pendente!\n" + e.getMessage(), Toast.LENGTH_LONG).show();
        } catch ( Exception e) {
            listaPedido.clear();
            edtTotalPedidos.setText("0/" + totalReg);
            //Toast.makeText(this, "Nenhum pedido pendente!\n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        ClienteHelper.clear();
        super.onResume();
    }

    @Override
    public void onPedidoRowClicked(int position) {
        if (listaPedidoAdapter.getSelectedItemCount() > 0)
            enableActionMode(position);
        else {
            PedidoHelper.setIdPedido(Integer.parseInt(listaPedidoAdapter.getItem(position).getId_web_pedido()));
            startActivity(new Intent(ActivityPedidoPendente.this, ActivityPedidoMain.class));
        }
    }

    @Override
    public void onRowLongClicked(int position) {
        enableActionMode(position);
        try {
            if (listaPedidoAdapter.getItem(position).getDescontoIndevido().equalsIgnoreCase("S"))
                showMsgAlerta("Pedido(s) com desconto indevido!\nÉ aconselhavel a correção do mesmo, antes do envio/sincronização ao servidor!");
            else if( Integer.parseInt( listaPedidoAdapter.getItem(position).getId_web_pedido_servidor()) > 0)
                showMsgAlerta("Pedido(s) ja sincronizado!\nÉ aconselhavel a confirmação junto a empresa, antes do envio/sincronização ao servidor!");
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
    }

    @Override
    public View.OnClickListener onClickExcluir(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMsgSimNao( view.getId(), listaPedidoAdapter.getItem(position), "Deseja realmente excluir o pedido " + listaPedidoAdapter.getItem(position).getId_web_pedido() + "?");
            }
        };
    }


    @Override
    public View.OnClickListener onClickEnviar(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMsgSimNao( view.getId(), listaPedidoAdapter.getItem(position), "Deseja enviar o pedido " + listaPedidoAdapter.getItem(position).getId_web_pedido() + " para ser faturado?");
            }
        };
    }

    @Override
    public View.OnClickListener onClickDuplic(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( listaPedidoAdapter.getItem(position).getCadastro().getId_cadastro() == 0){
                    showMsgAlerta("Pré-pedido não pode ser duplicado!");
                    return;
                }
                showMsgSimNao( view.getId(), listaPedidoAdapter.getItem(position), "Deseja duplicar o pedido " + listaPedidoAdapter.getItem(position).getId_web_pedido() + " para poder faturá-lo novamente?\n" +
                        "Atenção!!\n" +
                        "Todos os produtos/descontos/promoções serão revisados, caso algum desconto/promoção não exista/e ou não sejam mais permitidos, os mesmos serão removidos de cada item, onde não sejam mais aplicáveis!" );
            }
        };
    }

    @Override
    public View.OnClickListener onClickCompartilhar(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMsgSimNao( view.getId(), listaPedidoAdapter.getItem(position), "Deseja criar um arquivo PDF do pedido " + listaPedidoAdapter.getItem(position).getId_web_pedido() + " para ser compartilhado?" );
            }
        };
    }

    @Override
    public View.OnClickListener onClickRastrear(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ActivityPedidoPendente.this, "Em desenvolvimento!", Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public View.OnClickListener onClickLinhaTempo(int position) {
        return null;
    }

    private void enableActionMode(int position) {
        if (actionMode == null)
            actionMode = startSupportActionMode(actionModeCallback);
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        listaPedidoAdapter.toggleSelection(position);
        //int count = listaPedidoAdapter.getSelectedItemCount();
        if (listaPedidoAdapter.getSelectedItemCount() == 0) {
            actionMode.finish();
        } else {
            if (geraPDF != null)
                if (listaPedidoAdapter.getSelectedItemCount() > 1)
                    geraPDF.setVisible(false);
                else
                    geraPDF.setVisible(true);
            if (emailPDF != null)
                if (listaPedidoAdapter.getSelectedItemCount() > 1)
                    emailPDF.setVisible(false);
                else
                    emailPDF.setVisible(true);
            if (duplicaPedido != null)
                if (listaPedidoAdapter.getSelectedItemCount() > 1)
                    duplicaPedido.setVisible(false);
                else
                    duplicaPedido.setVisible(true);
            boolean finalizado = false;
            boolean pre_pedido = false;
            boolean descto_indevido = false;
            if (enviaPedido != null) {
                for (WebPedido webPedido : listaPedidoAdapter.getItensSelecionados()) {
                    if (webPedido.getFinalizado().equals("N"))
                        finalizado = true;
                    if (webPedido.getCadastro().getId_cadastro() == 0)
                        pre_pedido = true;
                    if ( webPedido.getDescontoIndevido().equalsIgnoreCase("S"))
                        descto_indevido = true;
                }
                if (finalizado) {
                    enviaPedido.setVisible(false);
                    geraPDF.setVisible(false);
                    emailPDF.setVisible(false);
                    duplicaPedido.setVisible(false);
                } else {
                    enviaPedido.setVisible(true);
                    geraPDF.setVisible(true);
                    emailPDF.setVisible(true);
                    duplicaPedido.setVisible(true);
                }
                if ( pre_pedido){
                    enviaPedido.setVisible(false);
                    duplicaPedido.setVisible(false);
                }
                if ( descto_indevido)
                    duplicaPedido.setVisible(false);
            }
            actionMode.setTitle(String.valueOf(listaPedidoAdapter.getSelectedItemCount()));
            actionMode.invalidate();
        }
    }

    @Override
    public void onRefresh() {
        listaPedido = webPedidoDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario() + " ORDER BY ID_WEB_PEDIDO DESC;");
        setRecyclerView(listaPedido);
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
            geraPDF = menu.findItem(R.id.action_pdf_pedido);
            emailPDF = menu.findItem(R.id.action_pdf_pedido_email);
            duplicaPedido = menu.findItem(R.id.action_duplica_pedido);
            enviaPedido = menu.findItem(R.id.action_mode_menu_pedido_pendente);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            final List<WebPedido> pedidosSelecionados = listaPedidoAdapter.getItensSelecionados();
            switch (item.getItemId()) {
                case R.id.action_delete:
                    if (pedidosSelecionados.size() == 1)
                        showMsgSimNao(mode, item.getItemId() , pedidosSelecionados, "Deseja realmente excluir o pedido " + pedidosSelecionados.get(0).getId_web_pedido() + "?");
                    else
                        showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja realmente excluir os " + pedidosSelecionados.size() + " pedidos selecionados?");
                    return true;
                case R.id.action_mode_menu_pedido_pendente:
                    if (pedidosSelecionados.size() == 1)
                        showMsgSimNao(mode, item.getItemId() , pedidosSelecionados, "Deseja enviar o pedido " + pedidosSelecionados.get(0).getId_web_pedido() + " para ser faturado?");
                    else
                        showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja enviar o(s) pedido(s) selecionado(s) para ser(em) faturado(s)?");
                    return true;
                case R.id.action_pdf_pedido:
                    showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja gerar um arquivo PDF do pedido " + pedidosSelecionados.get(0).getId_web_pedido() + "?");
                    return true;
                case R.id.action_pdf_pedido_email:
                    if (pedidosSelecionados.size() == 1)
                        showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja enviar um arquivo PDF do pedido " + pedidosSelecionados.get(0).getId_web_pedido() + " para o email do cliente ?");
                    else
                        showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja enviar os " + pedidosSelecionados.size() + " pedidos selecionados para os emails de seus clientes?");
                    return true;
                case R.id.action_duplica_pedido:
                    showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja duplicar o pedido " + pedidosSelecionados.get(0).getId_web_pedido() + " para poder faturá-lo novamente?\n" +
                            "Atenção!!\n" +
                            "Todos os produtos/descontos/promoções serão revisados, caso algum desconto/promoção não exista/e ou não sejam mais permitidos, os mesmos serão removidos de cada item, onde não sejam mais aplicáveis!" );
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            listaPedidoAdapter.clearSelections();
            actionMode = null;
            onRefresh();
        }
    }

    public void showMsgSucesso(String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoPendente.this).inflate(R.layout.dialog_sucesso_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoPendente.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public  void showMsgAlerta(String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoPendente.this).inflate(R.layout.dialog_alerta_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoPendente.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void showMsgSimNao(ActionMode mode, final int id,  List<WebPedido> pedidosSelecionados, String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoPendente.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoPendente.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mode.finish();
                } catch ( NullPointerException e) {
                    e.printStackTrace();
                }
                alertDialog.dismiss();
            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( id == R.id.action_delete) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    try {
                        for (int i = 0; i < pedidosSelecionados.size(); i++) {
                            if (webPedidoDAO.delete(pedidosSelecionados.get(i))) {
                                alertDialog.dismiss();
                                onResume();
                                if (pedidosSelecionados.size() == 1)
                                    showMsgSucesso("O Pedido selecionado foi excluido com sucesso!");
                                else
                                    showMsgSucesso("Os Pedidos selecionados foram excluidos com sucesso!");
                            } else {
                                showMsgAlerta("Falha ao exclui o(s) pedido(s) selecionado(s)");
                                onResume();
                            }
                        }
                        listaPedidoAdapter.remove(pedidosSelecionados.get(0));
                        onResume();
                    } catch (Exception e) {
                        alertDialog.dismiss();
                        Toast.makeText(ActivityPedidoPendente.this, "Não foi possivel excluir o(s) pedido(s)! Tente novamente!", Toast.LENGTH_LONG).show();
                    }
                } else if ( id == R.id.action_mode_menu_pedido_pendente || id == R.id.menu_pedido_pendente) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    alertDialog.dismiss();
                    progress = new ProgressDialog(ActivityPedidoPendente.this);
                    progress.setMessage("Enviando o(s) pedido(s)! Aguarde...");
                    progress.setTitle("Atenção!");
                    progress.setCancelable(false);
                    progress.show();
                    if (pedidosSelecionados.size() == 1)
                        sincronizaPedido(pedidosSelecionados.get(0));
                    else
                        sincronizaPedidos(pedidosSelecionados);

                } else if ( id == R.id.action_pdf_pedido) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        alertDialog.dismiss();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }
                    geraArquivoPDF(pedidosSelecionados);
                } else if( id == R.id.action_pdf_pedido_email ) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        alertDialog.dismiss();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }

                    final Intent intent = new Intent(Intent.ACTION_SENDTO);
                    try {
                        if (pedidosSelecionados.get(0).getCadastro() == null) {
                            pedidosSelecionados.get(0).setCadastro(new Cliente());
                            pedidosSelecionados.get(0).getCadastro().setId_cadastro(0);
                        }
                    } catch ( NullPointerException e) {
                        pedidosSelecionados.get(0).setCadastro(new Cliente());
                        pedidosSelecionados.get(0).getCadastro().setId_cadastro(0);
                    } catch ( Exception e) {
                        pedidosSelecionados.get(0).setCadastro(new Cliente());
                        pedidosSelecionados.get(0).getCadastro().setId_cadastro(0);
                    }
                    try {
                        if (pedidosSelecionados.get(0).getCadastro().getId_cadastro() == 0) {
                            final EditText txtNomeCliente = new EditText(ActivityPedidoPendente.this);
                            new AlertDialog.Builder(ActivityPedidoPendente.this)
                                    .setTitle("RCK SISTEMAS ESPECIFICOS")
                                    .setMessage("Informe o nome/razão social do cliente ")
                                    .setView(txtNomeCliente)
                                    .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            pedidosSelecionados.get(0).getCadastro().setNome_cadastro(txtNomeCliente.getText().toString());
                                            EmpresaParametro empresa = getEmpresa(pedidosSelecionados.get(0).getId_empresa());
                                            if (empresa == null) {
                                                Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa , favor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            File filePDF;
                                            if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                                                PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                                filePDF = pdfPedidoUtil.criandoPdf();
                                            } else {
                                                PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                                filePDF = pdfPedidoUtil.criandoPdf();
                                            }
                                            if ( filePDF == null) {
                                                return;
                                            }

                                            if (pedidosSelecionados.get(0).getCadastro().getEmail_principal() != null && !pedidosSelecionados.get(0).getCadastro().getEmail_principal().trim().equals(""))
                                                intent.setData(Uri.parse("mailto: " + pedidosSelecionados.get(0).getCadastro().getEmail_principal()));
                                            else if (pedidosSelecionados.get(0).getCadastro().getEmail_financeiro() != null && !pedidosSelecionados.get(0).getCadastro().getEmail_financeiro().trim().equals(""))
                                                intent.setData(Uri.parse("mailto: " + listaPedidoAdapter.getItensSelecionados().get(0).getCadastro().getEmail_financeiro()));
                                            else
                                                intent.setData(Uri.parse("mailto: Informe o email do cliente"));
                                            startActivity(intent
                                                    .putExtra(Intent.EXTRA_SUBJECT, "Espelho do pedido " + pedidosSelecionados.get(0).getId_web_pedido())
                                                    .putExtra(Intent.EXTRA_TEXT, "Segue em anexo o espelho do pedido")
                                                    .putExtra(Intent.EXTRA_STREAM, Uri.fromFile(filePDF)));
                                        }
                                    })
                                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            Toast.makeText(ActivityPedidoPendente.this, "Ação cancelada!", Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .show();
                        } else {
                            EmpresaParametro empresa = getEmpresa(pedidosSelecionados.get(0).getId_empresa());
                            if (empresa == null) {
                                Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa , favor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            File filePDF;
                            if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                                PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                filePDF = pdfPedidoUtil.criandoPdf();
                            } else {
                                PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                filePDF = pdfPedidoUtil.criandoPdf();
                            }

                            if ( filePDF == null) {
                                return;
                            }
                            if (pedidosSelecionados.get(0).getCadastro().getEmail_principal() != null && !pedidosSelecionados.get(0).getCadastro().getEmail_principal().trim().equals(""))
                                intent.setData(Uri.parse("mailto: " + pedidosSelecionados.get(0).getCadastro().getEmail_principal()));
                            else if (pedidosSelecionados.get(0).getCadastro().getEmail_financeiro() != null && !pedidosSelecionados.get(0).getCadastro().getEmail_financeiro().trim().equals(""))
                                intent.setData(Uri.parse("mailto: " + listaPedidoAdapter.getItensSelecionados().get(0).getCadastro().getEmail_financeiro()));
                            else
                                intent.setData(Uri.parse("mailto: Informe o email do cliente"));

                            startActivity(intent
                                    .putExtra(Intent.EXTRA_SUBJECT, "Espelho do pedido " + pedidosSelecionados.get(0).getId_web_pedido())
                                    .putExtra(Intent.EXTRA_TEXT, "Segue em anexo o espelho do pedido")
                                    .putExtra(Intent.EXTRA_STREAM, Uri.fromFile(filePDF)));
                        }
                    } catch (NullPointerException e) {
                        showAlertInfoApp();
                    } catch (Exception e) {
                        Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                    }
                } else if ( id == R.id.action_duplica_pedido) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    alertDialog.dismiss();
                    duplicaPedido( pedidosSelecionados.get(0));
               }
            }
        });
        alertDialog.show();
    }

    private void geraArquivoPDF(List<WebPedido> pedidosSelecionados) {
        try {
            if (pedidosSelecionados.get(0).getCadastro() == null) {
               pedidosSelecionados.get(0).setCadastro(new Cliente());
               pedidosSelecionados.get(0).getCadastro().setId_cadastro(0);
            }
        } catch ( NullPointerException e) {
            pedidosSelecionados.get(0).setCadastro(new Cliente());
            pedidosSelecionados.get(0).getCadastro().setId_cadastro(0);
        } catch ( Exception e) {
            pedidosSelecionados.get(0).setCadastro(new Cliente());
            pedidosSelecionados.get(0).getCadastro().setId_cadastro(0);
        }
        if ( Build.VERSION.SDK_INT >+ Build.VERSION_CODES.N) {
            if (pedidosSelecionados.get(0).getCadastro().getId_cadastro() == 0) {
                final EditText txtNomeCliente = new EditText(ActivityPedidoPendente.this);
                new AlertDialog.Builder(ActivityPedidoPendente.this)
                        .setTitle("RCK SISTEMAS ESPECIFICOS")
                        .setMessage("Informe o nome/razão social do cliente ")
                        .setView(txtNomeCliente)
                        .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                pedidosSelecionados.get(0).getCadastro().setNome_cadastro(txtNomeCliente.getText().toString());
                                EmpresaParametro empresa = getEmpresa(pedidosSelecionados.get(0).getId_empresa());
                                if (empresa == null) {
                                    Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa!\nEntre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                File filePDF;
                                if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                                    PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                    filePDF = pdfPedidoUtil.criandoPdf();
                                } else {
                                    PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                    filePDF = pdfPedidoUtil.criandoPdf();
                                }
                                if ( filePDF == null) {
                                    return;
                                }
                                try {
                                    Uri contentUri = FileProvider.getUriForFile(ActivityPedidoPendente.this, getApplicationContext().getPackageName() + ".my.package.name.provider", filePDF/*pdfPedidoUtil.criandoPdf()*/);
                                    startActivity(new Intent()
                                            .setAction(Intent.ACTION_VIEW)
                                            .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                            .setDataAndType(contentUri, "application/pdf"));
                                } catch (NullPointerException e) {
                                    showAlertInfoApp();
                                } catch (Exception e) {
                                    Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Toast.makeText(ActivityPedidoPendente.this, "Ação cancelada!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            } else {
                try {
                    EmpresaParametro empresa = getEmpresa(pedidosSelecionados.get(0).getId_empresa());
                    if (empresa == null) {
                        Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa!!\nEntre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    File filePDF;
                    if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                        PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                        filePDF = pdfPedidoUtil.criandoPdf();

                    } else {
                        PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                        filePDF = pdfPedidoUtil.criandoPdf();
                    }
                    if ( filePDF == null) {
                        return;
                    }
                    Uri contentUri = FileProvider.getUriForFile(ActivityPedidoPendente.this, getApplicationContext().getPackageName() + ".my.package.name.provider", filePDF/*pdfPedidoUtil.criandoPdf()*/);
                    startActivity(new Intent()
                            .setAction(Intent.ACTION_VIEW)
                            .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            .setDataAndType(contentUri, "application/pdf"));
                } catch (NullPointerException e) {
                    showAlertInfoApp();
                } catch (Exception e) {
                    Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                }
            }
        } else{
            if (pedidosSelecionados.get(0).getCadastro().getId_cadastro() == 0) {
                final EditText txtNomeCliente = new EditText(ActivityPedidoPendente.this);
                new AlertDialog.Builder(ActivityPedidoPendente.this)
                        .setTitle("RCK SISTEMAS ESPECIFICOS")
                        .setMessage("Informe o nome/razão social do cliente ")
                        .setView(txtNomeCliente)
                        .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    pedidosSelecionados.get(0).getCadastro().setNome_cadastro(txtNomeCliente.getText().toString());
                                    EmpresaParametro empresa = getEmpresa(pedidosSelecionados.get(0).getId_empresa());
                                    if (empresa == null) {
                                        Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa!\nFavor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    File filePDF;
                                    if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                                        PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                        filePDF = pdfPedidoUtil.criandoPdf();
                                    } else {
                                        PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                                        filePDF = pdfPedidoUtil.criandoPdf();
                                    }
                                    if ( filePDF == null) {
                                        return;
                                    }
                                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW)
                                            .setDataAndType(Uri.fromFile(filePDF), "application/pdf"), "Abrir arquivo"));
                                } catch (NullPointerException e) {
                                    showAlertInfoApp();
                                } catch (Exception e) {
                                    Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Toast.makeText(ActivityPedidoPendente.this, "Ação cancelada!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            } else {
                try {
                    EmpresaParametro empresa = getEmpresa(pedidosSelecionados.get(0).getId_empresa());
                    if (empresa == null) {
                        Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa!!\nFavor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    File filePDF;

                    if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                        PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                        filePDF = pdfPedidoUtil.criandoPdf();
                    } else {
                        PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa, ActivityPedidoPendente.this);
                        filePDF = pdfPedidoUtil.criandoPdf();
                    }
                    if ( filePDF == null) {
                        return;
                    }
                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW)
                            .setDataAndType(Uri.fromFile(filePDF), "application/pdf"), "Abrir arquivo"));
                } catch (NullPointerException e) {
                    showAlertInfoApp();
                } catch (Exception e) {
                    Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    private EmpresaParametro getEmpresa(String id_empresa) {
        EmpresaParametro empresa = null;
        try {
            empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO WHERE ID = '" + id_empresa + "'");
            if (empresa == null) {
                empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO");
                if (empresa == null) {
                    Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa!!!\nFavor entrar em contato com o suporte técnico!", Toast.LENGTH_SHORT).show();
                    return null;
                }
            }
        } catch (NullPointerException e) {
            empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO");
        } catch (Exception e) {
            empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO");
        }
        return empresa;
    }

    private void showAlertInfoApp() {
        new AlertDialog.Builder(ActivityPedidoPendente.this)
                .setTitle("RCK SISTEMAS ESPECIFICOS!")
                .setMessage("Aplicativo sem permissão para criação/armazenamento de arquivos!\n Gostaria de analisar/conceder permissão para de armazenamento para este aplicativo? ")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(ActivityPedidoPendente.this, "Ação cancelada!", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showMsgSimNao( final int id ,WebPedido webPedido, String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoPendente.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoPendente.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id == R.id.lyExcluir || id == R.id.btnExcluir) {
                    try {
                        if (webPedidoDAO.delete(webPedido)) {
                            listaPedidoAdapter.remove(webPedido);
                            alertDialog.dismiss();
                            showMsgSucesso("O Pedido foi excluido com sucesso!");
                            onResume();
                        } else {
                            showMsgAlerta("Falha ao excluir o pedido " + webPedido.getId_web_pedido() + " de " + webPedido.getNome_extenso());
                            onResume();
                        }
                    } catch (Exception e) {
                        alertDialog.dismiss();
                        onResume();
                        e.printStackTrace();
                        Toast.makeText(ActivityPedidoPendente.this, "Não foi possivel excluir o(s) pedido(s)! Tente novamente!", Toast.LENGTH_LONG).show();
                    }
                } else if ( id == R.id.lyEnvia || id == R.id.btnEnviar) {
                    alertDialog.dismiss();
                    progress = new ProgressDialog(ActivityPedidoPendente.this);
                    progress.setMessage("Enviando o pedido! Aguarde...");
                    progress.setTitle("Atenção!");
                    progress.setCancelable(false);
                    progress.show();
                    sincronizaPedido( webPedido);
                } else if ( id == R.id.btnDuplic) {
                    if ( webPedido.getCadastro().getId_cadastro() == 0) {
                        showMsgAlerta("Pré-pedido não pode ser duplicado!!");
                        return;
                    }
                    alertDialog.dismiss();
                    duplicaPedido(webPedido);
                } else if ( id == R.id.btnCompartilhar) {
                    try {
                        alertDialog.dismiss();
                    } catch ( NullPointerException e) {

                    } catch ( Exception e) {

                    }
                    try {
                        if (webPedido.getCadastro() == null) {
                            webPedido.setCadastro(new Cliente());
                            webPedido.getCadastro().setId_cadastro(0);
                        }
                    } catch ( NullPointerException e) {
                        webPedido.setCadastro(new Cliente());
                        webPedido.getCadastro().setId_cadastro(0);
                    } catch ( Exception e) {
                        webPedido.setCadastro(new Cliente());
                        webPedido.getCadastro().setId_cadastro(0);
                    }
                    if (webPedido.getCadastro().getId_cadastro() == 0) {
                        final EditText txtNomeCliente = new EditText(ActivityPedidoPendente.this);
                        new AlertDialog.Builder(ActivityPedidoPendente.this)
                                .setTitle("RCK SISTEMAS ESPECIFICOS")
                                .setMessage("Informe o nome/razão social do cliente ")
                                .setView(txtNomeCliente)
                                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        webPedido.getCadastro().setNome_cadastro(txtNomeCliente.getText().toString());
                                        try {
                                            EmpresaParametro empresa = getEmpresa(webPedido.getId_empresa());
                                            if (empresa == null) {
                                                Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa , favor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            File filePDF;
                                            if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                                                PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(webPedido, empresa, ActivityPedidoPendente.this);
                                                filePDF = pdfPedidoUtil.criandoPdf();
                                            } else {
                                                PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(webPedido, empresa,ActivityPedidoPendente.this);
                                                filePDF = pdfPedidoUtil.criandoPdf();
                                            }

                                            if ( filePDF == null) {
                                                return;
                                            }
                                            startActivity(Intent.createChooser(new Intent(Intent.ACTION_SEND)
                                                    .setType("pdf/*")
                                                    .putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(ActivityPedidoPendente.this, getApplicationContext().getPackageName() + ".my.package.name.provider", filePDF))
                                                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION), "Compartilhar arquivo do pedido " + webPedido.getId_web_pedido()));

                                        } catch (NullPointerException e) {
                                            showAlertInfoApp();
                                        } catch (Exception e) {
                                            Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo a ser compartilhado! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Toast.makeText(ActivityPedidoPendente.this, "Ação cancelada!", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .show();
                    } else {
                        try {
                            EmpresaParametro empresa = getEmpresa(webPedido.getId_empresa());
                            if (empresa == null) {
                                Toast.makeText(ActivityPedidoPendente.this, "Falha ao obter as informações da empresa , favor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            File filePDF;
                            if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                                PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(webPedido, empresa, ActivityPedidoPendente.this);
                                filePDF = pdfPedidoUtil.criandoPdf();
                            } else {
                                PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(webPedido, empresa,ActivityPedidoPendente.this);
                                filePDF = pdfPedidoUtil.criandoPdf();
                            }
                            if ( filePDF == null) {
                                return;
                            }
                           startActivity(Intent.createChooser(new Intent(Intent.ACTION_SEND)
                                    .setType("pdf/*")
                                    .putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(ActivityPedidoPendente.this, getApplicationContext().getPackageName() + ".my.package.name.provider", filePDF))
                                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION), "Compartilhar arquivo do pedido " + webPedido.getId_web_pedido()));

                        } catch (NullPointerException e) {
                            showAlertInfoApp();
                        } catch (Exception e) {
                            Toast.makeText(ActivityPedidoPendente.this, "Aplicativo sem permissão para criar o arquivo a ser compartilhado! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        alertDialog.show();
    }

    private void duplicaPedido(WebPedido webPedido) {
        try {
            if (!UsuarioHelper.getUsuario().getId_usuario().isEmpty()) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
            }
        } catch (NullPointerException e) {
            UsuarioBO usuarioBO = new UsuarioBO();
            UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
        } catch (Exception e) {
            UsuarioBO usuarioBO = new UsuarioBO();
            UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPedidoPendente.this));
        }
        String SQL = "SELECT * FROM TBL_CONFIGURACAO";
        configuracao = new ConfiguracaoDAO(db).getConfiguracao(SQL);
        SQL = "SELECT * FROM TBL_EMPRESA_PARAMETRO WHERE ID = '" + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice() + "'";
        EmpresaParametro empresaParametro = new EmpresaParametroDAO(db).getEmpresa(SQL);
        Float saldoVerba = 0.00f;
        try {
            if (configuracao.getTrabalha_com_verba_pedido().equalsIgnoreCase("S")) {
                VendedorBonusResumo vendedorBonusResumo = new VendedorBonusResumoDAO(db).getVendedor("SELECT * FROM TBL_VENDEDOR_BONUS_RESUMO WHERE ID_VENDEDOR = '" + UsuarioHelper.getUsuario().getId_quando_vendedor() + "'");
                saldoVerba = Float.parseFloat(vendedorBonusResumo.getValor_saldo());
            }
        } catch ( NullPointerException|CursorIndexOutOfBoundsException|NumberFormatException e) {
        } catch ( Exception e) {
        }

        WebPedido webPedidoDuplicado = webPedido;
        WebPedidoItensDAO webPedidoItensDAO = new WebPedidoItensDAO(db);
        CampanhaComercialCabDAO campanhaComercialCabDAO = new CampanhaComercialCabDAO(db);
        SQL = "SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + webPedidoDuplicado.getId_web_pedido() + " ORDER BY ID_WEB_ITEM";
        webPedidoDuplicado.setWebPedidoItens(webPedidoItensDAO.getListaDuplicacao(SQL, configuracao, empresaParametro, saldoVerba, webPedidoDuplicado.getCadastro(), campanhaComercialCabDAO));
        webPedidoDuplicado.setId_web_pedido(null);
        webPedidoDuplicado.setId_web_pedido_servidor(null);
        webPedidoDuplicado.setPedido_enviado("N");
        webPedidoDuplicado.setFinalizado("N");
        try {
            webPedido.setId_empresa(UsuarioHelper.getUsuario().getIdEmpresaMultiDevice());
            webPedido.setId_vendedor(UsuarioHelper.getUsuario().getId_quando_vendedor());
            webPedido.setUsuario_lancamento_id(UsuarioHelper.getUsuario().getId_usuario());
            webPedido.setUsuario_lancamento_nome(UsuarioHelper.getUsuario().getNome_usuario());
        } catch (NullPointerException e) {
        } catch ( Exception e) {
        }
        try {
            ClienteHelper.setCliente(webPedidoDuplicado.getCadastro());
        } catch (NullPointerException e) {
            showMsgAlerta("Cliente não localizado!");
        } catch (Exception e) {
            showMsgAlerta("Cadastro do cliente não localizado!");
        }
        for (WebPedidoItens webPedidoItens : webPedidoDuplicado.getWebPedidoItens()) {
            webPedidoItens.setId_web_item_servidor(null);
            webPedidoItens.setId_pedido("");
            webPedidoItens.setId_web_item(null);
        }
        PedidoHelper.setWebPedido(webPedidoDuplicado);
        PedidoHelper.setListaWebPedidoItens(webPedidoDuplicado.getWebPedidoItens());
        //Intent intent = new Intent(ListagemPedidoPendente.this, ActivityPedidoMain.class);
        startActivity(new Intent(ActivityPedidoPendente.this, ActivityPedidoMain.class));
    }





}
