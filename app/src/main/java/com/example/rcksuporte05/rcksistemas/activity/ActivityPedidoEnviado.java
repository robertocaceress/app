package com.example.rcksuporte05.rcksistemas.activity;

import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.BO.UsuarioBO;
import com.example.rcksuporte05.rcksistemas.BuildConfig;
import com.example.rcksuporte05.rcksistemas.DAO.CampanhaComercialCabDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.EmpresaParametroDAO;
import com.example.rcksuporte05.rcksistemas.DAO.UsuarioDAO;
import com.example.rcksuporte05.rcksistemas.DAO.VendedorBonusResumoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoItensDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ListaPedidoAdapter;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.EmpresaParametro;
import com.example.rcksuporte05.rcksistemas.model.Usuario;
import com.example.rcksuporte05.rcksistemas.model.VendedorBonusResumo;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.PDFPedidoICMSST;
import com.example.rcksuporte05.rcksistemas.util.PDFPedidoUtil;
import com.example.rcksuporte05.rcksistemas.util.Utilitaria;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPedidoEnviado extends AppCompatActivity implements ListaPedidoAdapter.PedidoAdapterListener {

    @BindView(R.id.listaPedidoEnviados)
    RecyclerView recyclerView;
    @BindView(R.id.edtNumeroPedidoEnviado)
    TextView edtTotalPedidos;
    @BindView(R.id.toolbarPedidoEnviado)
    Toolbar toolbar;
    ActivityPedidoEnviado.ActionModeCallback actionModeCallback;
    private List<WebPedido> listaPedido;
    private DBHelper db = new DBHelper(this);
    private WebPedidoDAO webPedidoDAO = new WebPedidoDAO(db);
    private EmpresaParametroDAO empresaParametroDAO = new EmpresaParametroDAO(db);
    private Configuracao configuracao = new Configuracao();
    private Usuario usuario;
    private ListaPedidoAdapter listaPedidoAdapter;
    private ActionMode actionMode;
    Utilitaria util = new Utilitaria();

    private int offSet = 0;
    private int totalReg = 0;
    private int limitReg = 50;
    private String SQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem_pedido_enviado);
        ButterKnife.bind(this);
        webPedidoDAO = new WebPedidoDAO(db);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
        toolbar.setTitle("Pedidos Enviados");
        setSupportActionBar(toolbar);
        try {
            UsuarioDAO usuarioDAO = new UsuarioDAO(db);
            usuario = usuarioDAO.getLista("SELECT * FROM TBL_WEB_USUARIO WHERE LOGIN = '" + db.consultaDados("SELECT * FROM TBL_LOGIN;", "LOGIN") + "';").get(0);
        } catch ( NullPointerException | ArrayIndexOutOfBoundsException e) {
            util.showMsgAlerta("Usuario logado não localizado! Por gentileza refaça seu login!", ActivityPedidoEnviado.this);
            return;
        } catch ( Exception e ) {
             util.showMsgAlerta("Usuario logado não localizado! Por gentileza refaça seu login!!", ActivityPedidoEnviado.this);
             return;
        }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new ActivityPedidoEnviado.ScrollListener());
        getConfiguracao();
        showDados();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionModeCallback = new ActivityPedidoEnviado.ActionModeCallback();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_pedido_enviados, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView;
        MenuItem item = menu.findItem(R.id.busca_pedido_enviados);
        searchView = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? (SearchView) item.getActionView() : (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String query) {
                if (query.trim().equals("")) {
                    offSet= 0;
                    SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'S' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario();
                    totalReg = webPedidoDAO.getTotalReg(SQL);
                    SQL = "SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'S' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario() + " ORDER BY ID_WEB_PEDIDO DESC ";
                    if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S"))
                        listaPedido = webPedidoDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet);
                    else
                        listaPedido = webPedidoDAO.getLista(SQL);
                    setRecyclerView(listaPedido);

                } else {
                    List<WebPedido> listaBusca = buscaPedidoEnviado(listaPedido, query);
                    try {
                        totalReg = listaBusca.size();
                    } catch ( NumberFormatException|NullPointerException e ){
                        totalReg = 0;
                    } catch ( Exception e) {
                        totalReg = 0;
                    }
                    setRecyclerView(listaBusca);
                }
                System.gc();
                return false;
            }
        });
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Nome Cliente / Nº pedido");
        return true;
    }

    private void showDados() {
        try {
            SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'S' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario();
            totalReg = webPedidoDAO.getTotalReg(SQL);
            SQL = "SELECT * FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'S' AND USUARIO_LANCAMENTO_ID = " + usuario.getId_usuario() + " ORDER BY ID_WEB_PEDIDO_SERVIDOR DESC ";
            if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S"))
                listaPedido = webPedidoDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet);
            else
                listaPedido = webPedidoDAO.getLista(SQL);
            setRecyclerView(listaPedido);
            if (actionMode == null)
                listaPedidoAdapter.clearSelections();
            else
                actionMode.finish();
        } catch (CursorIndexOutOfBoundsException | NullPointerException e) {
            listaPedido.clear();
            edtTotalPedidos.setText("0/" + totalReg);
        } catch ( Exception e) {
            listaPedido.clear();
            edtTotalPedidos.setText("0/" + totalReg);
        }
    }

    private void getConfiguracao() {
        ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
        configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
    }
    private class ScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            try {
                if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("N")) {
                    edtTotalPedidos.setText("(" + (getFirstItem() + 1) + " A " + (getLastItem() + 1) + ")/" + totalReg);
                    return;
                }
            } catch (NullPointerException e) {
                return;
            }
            if (!recyclerView.canScrollVertically(1)) {
                if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S")) {
                    offSet += limitReg;
                    listaPedido.addAll( webPedidoDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet));
                    listaPedidoAdapter.notifyDataSetChanged();
                }
            }
            edtTotalPedidos.setText( "(" + (getFirstItem() + 1) + " A " + (getLastItem() + 1) + ")/" + totalReg);
        }

    }
    private int getFirstItem(){
        return ((LinearLayoutManager)recyclerView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private int getLastItem() {
        return ((LinearLayoutManager) recyclerView.getLayoutManager())
                .findLastVisibleItemPosition();
    }

    public List<WebPedido> buscaPedidoEnviado(List<WebPedido> webPedidos, String query) {
        final String upperCaseQuery = query.toUpperCase();

        final List<WebPedido> lista = new ArrayList<>();
        for (WebPedido webPedido : webPedidos) {
            try {
                final String nomeCliente = webPedido.getNome_extenso().toUpperCase();
                final String numeroPedido = webPedido.getId_web_pedido_servidor().toUpperCase();
                if (nomeCliente.contains(upperCaseQuery) || numeroPedido.equals(upperCaseQuery))
                    lista.add(webPedido);
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch ( Exception e) {
                e.printStackTrace();
            }
        }
        return lista;
    }

    public void setRecyclerView(List<WebPedido> lista) {
        listaPedidoAdapter = new ListaPedidoAdapter(lista, this, this);
        recyclerView.setAdapter(listaPedidoAdapter);
        listaPedidoAdapter.notifyDataSetChanged();
        if (lista.size() > 0)
            edtTotalPedidos.setText("1/" + totalReg);
        else
            edtTotalPedidos.setText("0/" + totalReg);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onPedidoRowClicked(int position) {
        if (listaPedidoAdapter.getSelectedItemCount() > 0)
            enableActionMode(position);
        else {
            PedidoHelper.setIdPedido(Integer.parseInt(listaPedidoAdapter.getItem(position).getId_web_pedido()));
            PedidoHelper.setWebPedido(listaPedidoAdapter.getItem(position));
            startActivity(new Intent(ActivityPedidoEnviado.this, ActivityPedidoMain.class).putExtra("vizualizacao", 1));
        }
    }

    @Override
    public void onRowLongClicked(int position) {
        enableActionMode(position);
    }

    @Override
    public View.OnClickListener onClickExcluir(int position) {
        return null;
    }

    @Override
    public View.OnClickListener onClickEnviar(int position) {
        return null;
    }

    @Override
    public View.OnClickListener onClickDuplic(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMsgSimNao( view.getId(), listaPedidoAdapter.getItem(position), "Deseja duplicar o pedido " + listaPedidoAdapter.getItem(position).getId_web_pedido() + " para poder faturá-lo novamente?\n" +
                        "Atenção!!\n" +
                        "Todos os produtos/descontos/promoções serão revisados, caso algum desconto/promoção não exista/e ou não sejam mais permitidos, os mesmos serão removidos de cada item, onde não sejam mais aplicáveis!");
            }
        };
    }

    @Override
    public View.OnClickListener onClickCompartilhar(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMsgSimNao( view.getId(), listaPedidoAdapter.getItem(position), "Deseja criar um arquivo PDF do pedido " + listaPedidoAdapter.getItem(position).getId_web_pedido() + " para ser compartilhado?" );
            }
        };
    }

    @Override
    public View.OnClickListener onClickRastrear(final int position) {
        return null;
    }

    @Override
    public View.OnClickListener onClickLinhaTempo(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebPedido webPedido = listaPedidoAdapter.getItem(position);
                PedidoHelper.setWebPedido(webPedido);
                startActivity( new Intent(ActivityPedidoEnviado.this, ActivityAnalisePedido.class));
            }
        };
    };

    private void enableActionMode(int position) {
        if (actionMode == null)
            actionMode = startSupportActionMode(actionModeCallback);
        if (listaPedidoAdapter.getSelectedItemCount() <= 0 || listaPedidoAdapter.getItem(position).getId_web_pedido_servidor().equals(listaPedidoAdapter.getItensSelecionados().get(0).getId_web_pedido_servidor()))
            toggleSelection(position);
        else
            Toast.makeText(ActivityPedidoEnviado.this, "Somente é permitido a seleção de um pedido por vez", Toast.LENGTH_SHORT).show();
    }

    private void toggleSelection(int position) {
        listaPedidoAdapter.toggleSelection(position);
        if (listaPedidoAdapter.getSelectedItemCount() == 0)
            actionMode.finish();
        else {
            actionMode.setTitle(String.valueOf(listaPedidoAdapter.getSelectedItemCount()));
            actionMode.invalidate();
        }
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_action_mode_pdf, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            final List<WebPedido> pedidosSelecionados = listaPedidoAdapter.getItensSelecionados();
            switch (item.getItemId()) {
                case R.id.action_pdf_pedido:
                    showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja gerar um arquivo PDF do pedido " + pedidosSelecionados.get(0).getId_web_pedido() + "?");
                    return true;
                case R.id.action_pdf_pedido_email:
                    if (pedidosSelecionados.size() == 1)
                        showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja enviar um arquivo PDF do pedido " + pedidosSelecionados.get(0).getId_web_pedido() + " para o email do cliente ?");
                    else
                        showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja enviar os " + pedidosSelecionados.size() + " pedidos selecionados para os emails de seus clientes?");
                    return true;
                case R.id.action_duplica_pedido:
                    showMsgSimNao(mode, item.getItemId(), pedidosSelecionados, "Deseja duplicar o pedido " + pedidosSelecionados.get(0).getId_web_pedido() + " para poder faturá-lo novamente?\n" +
                            "Atenção!!\n" +
                            "Todos os produtos/descontos/promoções serão revisados, caso algum desconto/promoção não exista/e ou não sejam mais permitidos, os mesmos serão removidos de cada item, onde não sejam mais aplicáveis!" );
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            listaPedidoAdapter.clearSelections();
            actionMode = null;
        }
    }

    public void showMsgSimNao(ActionMode mode, final int id,  List<WebPedido> pedidosSelecionados, String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoEnviado.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoEnviado.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mode.finish();
                } catch ( NullPointerException e) {
                    e.printStackTrace();
                }
                alertDialog.dismiss();
            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( id == R.id.action_pdf_pedido) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    alertDialog.dismiss();
                    EmpresaParametro empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO WHERE ID = '" + pedidosSelecionados.get(0).getId_empresa() + "'");
                    if (empresa == null) {
                        Toast.makeText(ActivityPedidoEnviado.this, "Falha ao obter as informações da empresa!!!\nFavor entrar em contato com o suporte técnico!!!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    File filePDF;
                    if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                        PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoEnviado.this);
                        filePDF = pdfPedidoUtil.criandoPdf();
                    } else {
                        PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa,ActivityPedidoEnviado.this);
                        filePDF = pdfPedidoUtil.criandoPdf();
                    }
                    if ( filePDF == null) {
                        return;
                    }
                    if ( Build.VERSION.SDK_INT >+ Build.VERSION_CODES.N) {
                        try {
                            Uri contentUri = FileProvider.getUriForFile(ActivityPedidoEnviado.this, getApplicationContext().getPackageName() + ".my.package.name.provider", filePDF);
                            startActivity( new Intent()
                                    .setAction(Intent.ACTION_VIEW)
                                    .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                    .setDataAndType(contentUri, "application/pdf"));
                        } catch (NullPointerException e) {
                            showAlertInfoApp();
                        } catch (Exception e) {
                            Toast.makeText(ActivityPedidoEnviado.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        try {
                            startActivity(Intent.createChooser(new Intent(Intent.ACTION_VIEW)
                                    .setDataAndType(Uri.fromFile( filePDF), "application/pdf"), "Abrir arquivo"));
                        } catch (NullPointerException e) {
                            Toast.makeText(ActivityPedidoEnviado.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(ActivityPedidoEnviado.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if( id == R.id.action_pdf_pedido_email ) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    alertDialog.dismiss();
                    final Intent intent = new Intent(Intent.ACTION_SENDTO);
                    try {
                        EmpresaParametro empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO WHERE ID = '" + pedidosSelecionados.get(0).getId_empresa() + "'");
                        if (empresa == null) {
                            Toast.makeText(ActivityPedidoEnviado.this, "Falha ao obter as informações da empresa!\nFavor entrar em contato com o suporte técnico!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa,ActivityPedidoEnviado.this);
                        File filePDF;
                        if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                            PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(pedidosSelecionados.get(0), empresa, ActivityPedidoEnviado.this);
                            filePDF = pdfPedidoUtil.criandoPdf();
                        } else {
                            PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa,ActivityPedidoEnviado.this);
                            filePDF = pdfPedidoUtil.criandoPdf();
                        }
                        if ( filePDF == null) {
                            return;
                        }

                        if (pedidosSelecionados.get(0).getCadastro().getEmail_principal() != null && !pedidosSelecionados.get(0).getCadastro().getEmail_principal().trim().equals(""))
                            intent.setData(Uri.parse("mailto: " + pedidosSelecionados.get(0).getCadastro().getEmail_principal()));
                        else if (pedidosSelecionados.get(0).getCadastro().getEmail_financeiro() != null && !pedidosSelecionados.get(0).getCadastro().getEmail_financeiro().trim().equals(""))
                            intent.setData(Uri.parse("mailto: " + listaPedidoAdapter.getItensSelecionados().get(0).getCadastro().getEmail_financeiro()));
                        else
                            intent.setData(Uri.parse("mailto: Informe o email do cliente"));
                        startActivity(intent
                                .putExtra(Intent.EXTRA_SUBJECT, "Espelho do pedido " + pedidosSelecionados.get(0).getId_web_pedido())
                                .putExtra(Intent.EXTRA_TEXT, "Segue em anexo o espelho do pedido")
                                .putExtra(Intent.EXTRA_STREAM, Uri.fromFile( filePDF)));
                    } catch (NullPointerException e) {
                        showAlertInfoApp();
                        //Toast.makeText(ActivityPedidoEnviado.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(ActivityPedidoEnviado.this, "Aplicativo sem permissão para criar o arquivo! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                    }
                } else if ( id == R.id.action_duplica_pedido) {
                    try {
                        mode.finish();
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    alertDialog.dismiss();
                    duplicaPedido( pedidosSelecionados.get(0));
                }
            }
        });
        alertDialog.show();
    }

    public void showMsgSimNao( final int id ,WebPedido webPedido, String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoEnviado.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoEnviado.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( id == R.id.btnDuplic) {
                    alertDialog.dismiss();
                    duplicaPedido(webPedido);
                } else if ( id == R.id.btnCompartilhar) {
                    alertDialog.dismiss();
                    try {
                        EmpresaParametro empresa = empresaParametroDAO.getEmpresaPDF("SELECT * FROM TBL_EMPRESA_PARAMETRO WHERE ID = '" + webPedido.getId_empresa() + "'");
                        if (empresa == null) {
                            Toast.makeText(ActivityPedidoEnviado.this, "Falha ao obter as informações da empresa!!\nFavor entrar em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(pedidosSelecionados.get(0), empresa,ActivityPedidoEnviado.this);
                        File filePDF;
                        if ( empresa.getUtiliza_icms_app().equalsIgnoreCase("S")) {
                            PDFPedidoICMSST pdfPedidoUtil = new PDFPedidoICMSST(webPedido, empresa, ActivityPedidoEnviado.this);
                            filePDF = pdfPedidoUtil.criandoPdf();
                        } else {
                            PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil(webPedido, empresa,ActivityPedidoEnviado.this);
                            filePDF = pdfPedidoUtil.criandoPdf();
                        }
                        if ( filePDF == null) {
                            return;
                        }
                        //PDFPedidoUtil pdfPedidoUtil = new PDFPedidoUtil( webPedido, empresa, ActivityPedidoEnviado.this);
                        startActivity(Intent.createChooser(new Intent(Intent.ACTION_SEND)
                                .setType("pdf/*")
                                .putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(ActivityPedidoEnviado.this, getApplicationContext().getPackageName() + ".my.package.name.provider", filePDF))
                                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION), "Compartilhar arquivo do pedido " + webPedido.getId_web_pedido() ));
                    } catch ( NullPointerException e ) {
                        showAlertInfoApp();
                    } catch ( Exception e) {
                        Toast.makeText(ActivityPedidoEnviado.this, "Aplicativo sem permissão para criar o arquivo a ser compartilhado! De permissão de acesso manualmente,ou entre em contato com o suporte técnico!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });
        alertDialog.show();
    }

    /*
    private void duplicaPedido( WebPedido webPedido) {
        WebPedido webPedidoDuplicado = webPedido;
        WebPedidoItensDAO webPedidoItensDAO = new WebPedidoItensDAO(db);
        webPedidoDuplicado.setWebPedidoItens(webPedidoItensDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + webPedidoDuplicado.getId_web_pedido() + " ORDER BY ID_WEB_ITEM"));
        webPedidoDuplicado.setId_web_pedido(null);
        webPedidoDuplicado.setId_web_pedido_servidor(null);
        webPedidoDuplicado.setPedido_enviado("N");
        webPedidoDuplicado.setFinalizado("N");
        ClienteHelper.setCliente(webPedidoDuplicado.getCadastro());
        for (WebPedidoItens webPedidoItens : webPedidoDuplicado.getWebPedidoItens()) {
            webPedidoItens.setId_web_item_servidor(null);
            webPedidoItens.setId_pedido("");
            webPedidoItens.setId_web_item(null);
        }
        PedidoHelper.setWebPedido(webPedidoDuplicado);
        PedidoHelper.setListaWebPedidoItens(webPedidoDuplicado.getWebPedidoItens());
        startActivity(new Intent(ActivityPedidoEnviado.this, ActivityPedidoMain.class));
    }

     */

    private void duplicaPedido(WebPedido webPedido) {
        try {
            if (!UsuarioHelper.getUsuario().getId_usuario().isEmpty()) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPedidoEnviado.this));
            }
        } catch (NullPointerException e) {
            UsuarioBO usuarioBO = new UsuarioBO();
            UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPedidoEnviado.this));
        } catch (Exception e) {
            UsuarioBO usuarioBO = new UsuarioBO();
            UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPedidoEnviado.this));
        }
        String SQL = "SELECT * FROM TBL_CONFIGURACAO";
        configuracao = new ConfiguracaoDAO(db).getConfiguracao(SQL);
        SQL = "SELECT * FROM TBL_EMPRESA_PARAMETRO WHERE ID = '" + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice() + "'";
        EmpresaParametro empresaParametro = new EmpresaParametroDAO(db).getEmpresa(SQL);
        Float saldoVerba = 0.00f;
        try {
            if (configuracao.getTrabalha_com_verba_pedido().equalsIgnoreCase("S")) {
                VendedorBonusResumo vendedorBonusResumo = new VendedorBonusResumoDAO(db).getVendedor("SELECT * FROM TBL_VENDEDOR_BONUS_RESUMO WHERE ID_VENDEDOR = '" + UsuarioHelper.getUsuario().getId_quando_vendedor() + "'");
                saldoVerba = Float.parseFloat(vendedorBonusResumo.getValor_saldo());
            }
        } catch ( NullPointerException|CursorIndexOutOfBoundsException|NumberFormatException e) {
        } catch ( Exception e) {
        }
        CampanhaComercialCabDAO campanhaComercialCabDAO = new CampanhaComercialCabDAO(db);

        WebPedido webPedidoDuplicado = webPedido;
        WebPedidoItensDAO webPedidoItensDAO = new WebPedidoItensDAO(db);
        SQL = "SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + webPedidoDuplicado.getId_web_pedido() + " ORDER BY ID_WEB_ITEM";
        webPedidoDuplicado.setWebPedidoItens(webPedidoItensDAO.getListaDuplicacao(SQL, configuracao, empresaParametro, saldoVerba, webPedidoDuplicado.getCadastro(), campanhaComercialCabDAO));
        webPedidoDuplicado.setId_web_pedido(null);
        webPedidoDuplicado.setId_web_pedido_servidor(null);
        webPedidoDuplicado.setPedido_enviado("N");
        webPedidoDuplicado.setFinalizado("N");
        try {
            webPedido.setId_empresa(UsuarioHelper.getUsuario().getIdEmpresaMultiDevice());
            webPedido.setId_vendedor(UsuarioHelper.getUsuario().getId_quando_vendedor());
            webPedido.setUsuario_lancamento_id(UsuarioHelper.getUsuario().getId_usuario());
            webPedido.setUsuario_lancamento_nome(UsuarioHelper.getUsuario().getNome_usuario());
        } catch (NullPointerException e) {
        } catch ( Exception e) {
        }
        try {
            ClienteHelper.setCliente(webPedidoDuplicado.getCadastro());
        } catch (NullPointerException e) {
            showMsgAlerta("Cliente não localizado!");
        } catch (Exception e) {
            showMsgAlerta("Cadastro do cliente não localizado!");
        }
        for (WebPedidoItens webPedidoItens : webPedidoDuplicado.getWebPedidoItens()) {
            webPedidoItens.setId_web_item_servidor(null);
            webPedidoItens.setId_pedido("");
            webPedidoItens.setId_web_item(null);
        }
        PedidoHelper.setWebPedido(webPedidoDuplicado);
        PedidoHelper.setListaWebPedidoItens(webPedidoDuplicado.getWebPedidoItens());
        //Intent intent = new Intent(ListagemPedidoPendente.this, ActivityPedidoMain.class);
        startActivity(new Intent(ActivityPedidoEnviado.this, ActivityPedidoMain.class));
    }
    private void showAlertInfoApp() {
        new AlertDialog.Builder(ActivityPedidoEnviado.this)
                .setTitle("RCK SISTEMAS ESPECIFICOS!")
                .setMessage("Aplicativo sem permissão para criação/armazenamento de arquivos!\n Gostaria de analisar/conceder permissão para de armazenamento para este aplicativo? ")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(ActivityPedidoEnviado.this, "Ação cancelada!", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public  void showMsgAlerta(String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoEnviado.this).inflate(R.layout.dialog_alerta_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoEnviado.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}


