package com.example.rcksuporte05.rcksistemas.activity;


import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.rcksuporte05.rcksistemas.Helper.HistoricoFinanceiroHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.model.HistoricoFinanceiro;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoAnalise;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAnalisePedido extends AppCompatActivity {

    @BindView(R.id.tblAnalisePedido)
    Toolbar toolbar;

    @BindView(R.id.edtMotivo)
    EditText edtMotivo;

    @BindView(R.id.txvCredito)
    TextView txvCredito;

    @BindView(R.id.txvCarregamento)
    TextView txvCarregamento;

    @BindView(R.id.txvExpedicao)
    TextView txvExpedicao;

    @BindView(R.id.txvFaturamento)
    TextView txvFaturamento;

    @BindView(R.id.txvBoletoEmitido)
    TextView txvBoletoEmitido;

    @BindView(R.id.txvTransportador)
    TextView txvTransportador;

    @BindView(R.id.txvEspecie)
    TextView txvEspecie;

    @BindView(R.id.txvVolume)
    TextView txvVolume;

    @BindView(R.id.txvDataImportacao)
    TextView txvDataImportacao;
    public WebPedidoAnalise webPedidoAnalise;
    public WebPedido webPedido;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analise_pedido);
        ButterKnife.bind(this);
        webPedido = PedidoHelper.getWebPedido();
        toolbar.setTitle( "Pedido: " + webPedido.getId_web_pedido_servidor());
        WebPedidoAnalise webPedidoAnalise = new WebPedidoAnalise();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sincronizaAnalisePedidoAPI();
    }

    private String  getIdPedido(WebPedido webPedido) {
        try {
            if (!webPedido.getId_pedido().isEmpty())
                if (!webPedido.getId_pedido().equalsIgnoreCase(webPedido.getId_web_pedido_servidor()))
                    return webPedido.getId_web_pedido_servidor();
        }catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
        return webPedido.getId_web_pedido_servidor();
    }

    private String getIdOrdemCarregamento(WebPedido webPedido) {
        try {
            if (Integer.parseInt(webPedido.getId_ordem_carregamento()) > 0)
                return webPedido.getId_ordem_carregamento();
        } catch (NullPointerException|NumberFormatException e) {
        } catch (Exception e){
        }
        return "0";
    }

    public void sincronizaAnalisePedidoAPI() {
        final Rotas apiRotas = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Call<WebPedidoAnalise> call = apiRotas.listaAnalise( getIdPedido( webPedido), getIdOrdemCarregamento(webPedido), cabecalho);
        call.enqueue(new Callback<WebPedidoAnalise>() {
            @Override
            public void onResponse(Call<WebPedidoAnalise> call, Response<WebPedidoAnalise> response) {
                switch (response.code()) {
                    case 200:
                        showDados(response.body());
                        break;
                    default:
                        Toast.makeText( ActivityAnalisePedido.this, "Falha ao analisar o histórico do pedido!"
                                +"\n("+ String.valueOf(response.code()) + ")", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<WebPedidoAnalise> call, Throwable t) {
                toolbar.setTitle("Pedido:" + webPedido.getId_web_pedido_servidor());
                Toast.makeText( ActivityAnalisePedido.this, "Falha ao analisar o histórico do pedido!!"
                        + "\n" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showDados( WebPedidoAnalise webPedidoAnalise) {
        this.webPedidoAnalise = webPedidoAnalise;
        try {
            if (Integer.parseInt(webPedidoAnalise.getId_pedido()) > 0) {

                if ( webPedidoAnalise.getData_importacao().equalsIgnoreCase("")) {
                    Toast.makeText( ActivityAnalisePedido.this, "Pedido " + webPedidoAnalise.getId_pedido() + " de " + webPedido.getCadastro().getNome_cadastro() + " não importado!" , Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    edtMotivo.setText("");
                    txvDataImportacao.setText( new SimpleDateFormat("dd/MM/yy hh:mm").format(new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(webPedidoAnalise.getData_importacao())));
                    if ( webPedidoAnalise.getExcluido().equalsIgnoreCase("S")) {
                        edtMotivo.setText(webPedidoAnalise.getJustificativa_exclusao().toUpperCase());
                        txvCredito.setText("Cancelado/Excluido".toUpperCase());
                        Toast.makeText( ActivityAnalisePedido.this, "Pedido " + webPedidoAnalise.getId_pedido() + " de " + webPedido.getCadastro().getNome_cadastro() + " foi cancelado/excluido do sistema ERP em "
                                +  new SimpleDateFormat("dd/MM/yy hh:mm").format(new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(webPedidoAnalise.getExcluido_usuario_data())) , Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (webPedidoAnalise.getAnalise_credito().equalsIgnoreCase("N") || webPedidoAnalise.getAnalise_credito().isEmpty())
                        txvCredito.setText("Pendente".toUpperCase());
                    else if (webPedidoAnalise.getAnalise_credito().equalsIgnoreCase("T"))
                        txvCredito.setText("Crédito revisado e bloqueado".toUpperCase());
                    else if (webPedidoAnalise.getAnalise_credito().equalsIgnoreCase("S"))
                        txvCredito.setText("Crédito autorizado".toUpperCase());
                    else if (webPedidoAnalise.getAnalise_credito().equalsIgnoreCase("B"))
                        txvCredito.setText("Crédito negado".toUpperCase());
                    edtMotivo.setText( !webPedidoAnalise.getEvento_descricao().isEmpty() ? webPedidoAnalise.getEvento_descricao() .toUpperCase(): "");

                    try {
                        if (Integer.parseInt(webPedidoAnalise.getId_ordem_carregamento()) > 0)
                            txvCarregamento.setText(webPedidoAnalise.getData_ordem_carregamento() + "  Nº " + webPedidoAnalise.getId_ordem_carregamento());
                        else {
                            if (webPedidoAnalise.getAnalise_credito().equalsIgnoreCase("N") ||
                                    webPedidoAnalise.getAnalise_credito().isEmpty() ||
                                    webPedidoAnalise.getAnalise_credito().equalsIgnoreCase("B"))
                                txvCarregamento.setText("");
                            else
                                txvCarregamento.setText("Pendente".toUpperCase());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        txvTransportador.setText(webPedidoAnalise.getNome_transportadora().toUpperCase());
                    } catch ( NullPointerException e) {
                        e.printStackTrace();
                    }
                    txvEspecie.setText(webPedidoAnalise.getEspecie());
                    txvVolume.setText(webPedidoAnalise.getVolumes());
                    try {
                        if (Integer.parseInt(webPedidoAnalise.getId_nota_fiscal()) > 0) {
                            try {
                                if (Integer.parseInt(webPedidoAnalise.getNumero_nota_fiscal()) > 0)
                                    txvFaturamento.setText(new SimpleDateFormat("dd/MM/yy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedidoAnalise.getData_nota_fiscal())) + " NF-e: " + webPedidoAnalise.getNumero_nota_fiscal());
                                else
                                    txvFaturamento.setText(new SimpleDateFormat("dd/MM/yy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedidoAnalise.getData_nota_fiscal())) + webPedidoAnalise.getNumero_nota_fiscal());
                                if (Integer.parseInt(webPedidoAnalise.getId_ordem_carregamento()) <= 0)
                                    txvCarregamento.setText("");
                            } catch (Exception e ) {
                                txvFaturamento.setText(new SimpleDateFormat("dd/MM/yy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedidoAnalise.getData_nota_fiscal())) + webPedidoAnalise.getNumero_nota_fiscal());
                            }
                        }
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }
                    if ( webPedidoAnalise.getBoleto_emitido().equalsIgnoreCase("S"))
                        txvBoletoEmitido.setText( "Emitido");
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        } catch ( NullPointerException e) {
            Toast.makeText( ActivityAnalisePedido.this, "Pedido " + webPedidoAnalise.getId_pedido() + " de " + webPedido.getCadastro().getNome_cadastro() + " não importado" , Toast.LENGTH_SHORT).show();
        } catch ( Exception e) {
             Toast.makeText( ActivityAnalisePedido.this, "Pedido " + webPedidoAnalise.getId_pedido() + " de " + webPedido.getCadastro().getNome_cadastro() + " não importado" , Toast.LENGTH_SHORT).show();
        }
    }
}
