package com.example.rcksuporte05.rcksistemas.adapters.viewHolder;


import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ListaPedidoAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RCK 03 on 01/12/2017.
 */

public class PedidoViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

    //@BindView(R.id.edtNomeProduto)
    @BindView(R.id.edtNumeroPedido)
    public TextView txtIdPedido;

    @BindView(R.id.txtNomeCliente)
    public TextView txtNomeCliente;

    @BindView(R.id.txtFantasiaCliente)
    public TextView txtFantasiaCliente;

    @BindView(R.id.txtPrecoPedido)
    public TextView txtPrecoPedido;

    @BindView(R.id.txtDataEntrega)
    public TextView txtDataEntrega;

    @BindView(R.id.txtDataEmissaoPedido)
    public TextView txtDataEmissaoPedido;

    @BindView(R.id.item_lista_pedido)
    public LinearLayout itemListaPedido;

    @BindView(R.id.cor)
    public View cor;

    //@BindView(R.id.lyExcluir)
   // public LinearLayout lyExcluir;

    //@BindView(R.id.lyEnvia)
    //public LinearLayout lyEnvia;

    //@BindView(R.id.lyCompartilhar)
   // public LinearLayout lyCompartilhar;

    @BindView(R.id.btnExcluir)
    public Button btnExcluir;

    @BindView(R.id.btnEnviar)
    public Button btnEnviar;

    @BindView(R.id.btnDuplic)
    public Button btnDuplic;

    @BindView(R.id.btnCompartilhar)
    public Button btnCompartilhar;

    @BindView(R.id.btnRastreio)
    public Button btnRastreio;

    @BindView(R.id.txtCondicaoPagamento)
    public TextView txtCondicaoPagamento;

    @BindView(R.id.txtOperacao)
    public TextView txtOperacao;

    @BindView(R.id.txtMoeda)
    public TextView txtMoeda;

    @BindView(R.id.txtStatus)
    public TextView txtStatus;


    @BindView(R.id.abandonado)
    public LinearLayout abandonado;

    @BindView(R.id.txvAbandonado)
    public TextView txvAbandonado;

    @BindView(R.id.lyEntrega)
    public LinearLayout lyEntrega;

    //@BindView(R.id.lyLinhaTempo)
    //public LinearLayout lyLinhaTempo;

    @BindView(R.id.btnLinhaTempo)
    public Button btnLinhaTempo;

    @BindView(R.id.itemView)
    public LinearLayout itemView;

    ListaPedidoAdapter.PedidoAdapterListener listener;

    public PedidoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public boolean onLongClick(View view) {
        return true;
    }
}
