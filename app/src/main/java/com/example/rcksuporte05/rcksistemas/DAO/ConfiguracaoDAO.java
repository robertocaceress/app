package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ComponentName;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;

import com.example.rcksuporte05.rcksistemas.model.Configuracao;

public class ConfiguracaoDAO {
    private DBHelper db;

    public ConfiguracaoDAO(DBHelper db) {
        this.db = db;
    }

    public boolean add(Configuracao configuracao) {
        try {
            db.deleteDados("TBL_CONFIGURACAO", "1", null);
        } catch (SQLException e) {
        }
        try {
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_VERBA_PEDIDO VARCHAR(1);");
        } catch (SQLException e) {
        } catch (Exception e) {
        }
        try{
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP VARCHAR(1);");
        } catch (SQLException e) {
        } catch (Exception e) {
        }
        try {
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_ICMS_ST VARCHAR(1);");
        } catch (SQLException e) {
        } catch (Exception e) {
        }
        try {
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN LISTA_TODOS_PEDIDOS VARCHAR(1);");
        } catch (SQLException e) {
        } catch (Exception e) {
        }
        try {
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN ID_CON_PGTO_DEFAULT_APP INTEGER ");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN LAYOUT_VENDA_ERP INTEGER ");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_PAGINACAO_DADOS VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_UNIDADE_MANUT_ESTOQUE VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_CONFIRMACAO_PRAZO VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN PESQUISA_PRODUTO_POR_LINHA_GRUPO VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_PRE_PEDIDO VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_LIMITE_CREDITO VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_DSCTO_REAIS VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_CAMPANHAS VARCHAR(1);");
            db.alterar("ALTER TABLE TBL_CONFIGURACAO ADD COLUMN TRABALHA_COM_COBRANCA VARCHAR(1);");

        } catch (SQLException e) {
        } catch (Exception e) {
        }
        ContentValues content = new ContentValues();
        content.put("URL_IP_1", configuracao.getUrl_ip_1());
        content.put("PORTA_1", configuracao.getPorta_1());
        content.put("URL_IP_DESC_1", configuracao.getUrl_ip_desc_1());
        content.put("URL_IP_2", configuracao.getUrl_ip_2());
        content.put("PORTA_2", configuracao.getPorta_2());
        content.put("URL_IP_DESC_2", configuracao.getUrl_ip_desc_2());
        content.put("CONEXAO_SEGURA", configuracao.getConexao_segura());
        content.put("TRABALHA_COM_CAMPANHAS", configuracao.getTrabalha_com_campanhas());
        content.put("TRABALHA_COM_DSCTO_REAIS", configuracao.getTrabalha_com_dscto_reais());
        content.put("TRABALHA_COM_LIMITE_CREDITO", configuracao.getTrabalha_com_limite_cred());
        content.put("TRABALHA_COM_PRE_PEDIDO", configuracao.getTrabalha_com_pre_pedido());
        content.put("PESQUISA_PRODUTO_POR_LINHA_GRUPO", configuracao.getPesquisa_produto_por_linha_grupo());
        content.put("TRABALHA_COM_CONFIRMACAO_PRAZO", configuracao.getTrabalha_com_confirmacao_prazo());
        content.put("TRABALHA_COM_UNIDADE_MANUT_ESTOQUE", configuracao.getTrabalha_com_unidade_manut_estoque());
        content.put("TRABALHA_COM_PAGINACAO_DADOS", configuracao.getTrabalha_com_paginacao_dados());
        content.put("ID_CON_PGTO_DEFAULT_APP", configuracao.getId_con_pgto_default_app());
        content.put("TRABALHA_COM_COBRANCA", configuracao.getTrabalha_com_cobranca());
        content.put("TRABALHA_COM_ICMS_ST", configuracao.getTrabalha_com_icms_st());
        content.put("PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP", configuracao.getTrabalha_com_icms_st());
        content.put("SINCRONIA_AUTOMATICA_APP", configuracao.getSincronia_automatica_app());
        content.put("TRABALHA_COM_VERBA_PEDIDO", configuracao.getTrabalha_com_verba_pedido());
        content.put("LISTA_TODOS_PEDIDOS", configuracao.getLista_todos_pedidos());
        content.put("LAYOUT_VENDA_ERP", configuracao.getLayout_venda_erp());
        //PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP
        try {
            if ( db.addDados("TBL_CONFIGURACAO",  content) > 0)
                return true;
        } catch (SQLException e) {
        }
        return false;
    }

    public int update(String campanha, String desconto, String limite, String pre_pedido, String conf_prazo, String usa_unid_estoque, String usa_paginacao, String layout_venda_erp,
                      String id_con_pgto_default_app, String usa_cobranca_app, String usa_icms_app, String desc_automatico_app, String sincronia_automatica, String usa_verba_pedido) {  //Trabalha com campanha/Trabalha com desconto em valor
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("TRABALHA_COM_CAMPANHAS", campanha);
            content.put("TRABALHA_COM_DSCTO_REAIS", desconto);
            content.put("TRABALHA_COM_LIMITE_CREDITO", limite);
            content.put("TRABALHA_COM_PRE_PEDIDO", pre_pedido);
            content.put("TRABALHA_COM_CONFIRMACAO_PRAZO", conf_prazo);
            content.put("TRABALHA_COM_UNIDADE_MANUT_ESTOQUE", usa_unid_estoque);
            content.put("TRABALHA_COM_PAGINACAO_DADOS", usa_paginacao);
            content.put("LAYOUT_VENDA_ERP", layout_venda_erp);
            content.put("ID_CON_PGTO_DEFAULT_APP", id_con_pgto_default_app);
            content.put("TRABALHA_COM_COBRANCA", "N");
            try {
                if (usa_cobranca_app != null || !usa_cobranca_app.isEmpty())
                    content.put("TRABALHA_COM_COBRANCA", usa_cobranca_app);
            } catch ( NullPointerException e) {
            } catch ( Exception e) {
            }
            content.put("TRABALHA_COM_ICMS_ST", usa_icms_app);
            content.put("PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP", desc_automatico_app);
            content.put("SINCRONIA_AUTOMATICA_APP", sincronia_automatica);
            content.put("TRABALHA_COM_VERBA_PEDIDO", usa_verba_pedido);
            System.gc();
            return db.updateDados("TBL_CONFIGURACAO", content, null);
        } catch ( NullPointerException|SQLException e){
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Configuracao getConfiguracao(String SQL) {
        Cursor cursor = db.listaDados(SQL);
        Configuracao configuracao = new Configuracao();
        if (cursor.moveToNext()) {
            configuracao.setId(cursor.getString(cursor.getColumnIndex("ID")));
            configuracao.setUsuario(cursor.getString(cursor.getColumnIndex("USUARIO")));
            configuracao.setSenha(cursor.getString(cursor.getColumnIndex("SENHA")));

            configuracao.setUrl_ip_1(cursor.getString(cursor.getColumnIndex("URL_IP_1")));
            configuracao.setPorta_1(cursor.getInt(cursor.getColumnIndex("PORTA_1")));
            configuracao.setUrl_ip_desc_1(cursor.getString(cursor.getColumnIndex("URL_IP_DESC_1")));

            configuracao.setUrl_ip_2(cursor.getString(cursor.getColumnIndex("URL_IP_2")));
            configuracao.setPorta_2(cursor.getInt(cursor.getColumnIndex("PORTA_2")));
            configuracao.setUrl_ip_desc_2(cursor.getString(cursor.getColumnIndex("URL_IP_DESC_2")));
            configuracao.setConexao_segura(cursor.getString(cursor.getColumnIndex("CONEXAO_SEGURA")));
            try {
                configuracao.setTrabalha_com_campanhas(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_CAMPANHAS")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_campanhas("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_campanhas("N");
            }
            try {
                configuracao.setTrabalha_com_dscto_reais(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_DSCTO_REAIS")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_dscto_reais("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_dscto_reais("N");
            }
            try {
                configuracao.setTrabalha_com_limite_cred(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_LIMITE_CREDITO")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_limite_cred("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_limite_cred("N");
            }
            try {
                configuracao.setTrabalha_com_pre_pedido(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_PRE_PEDIDO")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_pre_pedido("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_pre_pedido("N");
            }
            try {
                configuracao.setPesquisa_produto_por_linha_grupo(cursor.getString(cursor.getColumnIndex("PESQUISA_PRODUTO_POR_LINHA_GRUPO")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setPesquisa_produto_por_linha_grupo("G");
            } catch (Exception e) {
                configuracao.setPesquisa_produto_por_linha_grupo("G");
            }

            try {
                configuracao.setTrabalha_com_confirmacao_prazo(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_CONFIRMACAO_PRAZO")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_confirmacao_prazo("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_confirmacao_prazo("N");
            }

            try {
                configuracao.setTrabalha_com_unidade_manut_estoque(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_UNIDADE_MANUT_ESTOQUE")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_unidade_manut_estoque("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_unidade_manut_estoque("N");
            }

            try {
                configuracao.setTrabalha_com_paginacao_dados(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_PAGINACAO_DADOS")));
            } catch (IllegalStateException | NullPointerException e) {
                configuracao.setTrabalha_com_paginacao_dados("N");
            } catch (Exception e) {
                configuracao.setTrabalha_com_paginacao_dados("N");
            }
            try {
                if ( cursor.getString(cursor.getColumnIndex("LAYOUT_VENDA_ERP")) != null  && !cursor.getString(cursor.getColumnIndex("LAYOUT_VENDA_ERP")).isEmpty() ) {
                    configuracao.setLayout_venda_erp(cursor.getString(cursor.getColumnIndex("LAYOUT_VENDA_ERP")));
                } else {
                    configuracao.setLayout_venda_erp("0");
                }
            } catch ( NullPointerException e) {
                configuracao.setLayout_venda_erp("0");
            } catch ( Exception e) {
                configuracao.setLayout_venda_erp("0");
            }

            try {
                if (cursor.getString(cursor.getColumnIndex("ID_CON_PGTO_DEFAULT_APP")) != null & !cursor.getString(cursor.getColumnIndex("ID_CON_PGTO_DEFAULT_APP")).isEmpty())
                    configuracao.setId_con_pgto_default_app(cursor.getString(cursor.getColumnIndex("ID_CON_PGTO_DEFAULT_APP")));
                else
                    configuracao.setId_con_pgto_default_app("0");
            } catch ( NullPointerException e) {
                configuracao.setId_con_pgto_default_app("0");
            } catch ( Exception e) {
                configuracao.setId_con_pgto_default_app("0");
            }

            try {
                if ( cursor.getString(cursor.getColumnIndex("TRABALHA_COM_COBRANCA")) != null && !cursor.getString(cursor.getColumnIndex("TRABALHA_COM_COBRANCA")).isEmpty())
                    configuracao.setTrabalha_com_cobranca(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_COBRANCA")));
                else
                    configuracao.setTrabalha_com_cobranca("N");
            } catch ( NullPointerException e) {
                configuracao.setTrabalha_com_cobranca("N");
            } catch ( Exception e) {
                configuracao.setTrabalha_com_cobranca("N");
            }

            try {
                if (cursor.getString(cursor.getColumnIndex("TRABALHA_COM_ICMS_ST")) != null && !cursor.getString(cursor.getColumnIndex("TRABALHA_COM_ICMS_ST")).isEmpty())
                    configuracao.setTrabalha_com_icms_st(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_ICMS_ST")));
                else
                    configuracao.setTrabalha_com_icms_st("N");
            } catch ( NullPointerException e) {
                configuracao.setTrabalha_com_icms_st("N");
            } catch ( Exception e) {
                configuracao.setTrabalha_com_icms_st("N");
            }

            try {
                if (cursor.getString(cursor.getColumnIndex("PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP")) != null && !cursor.getString(cursor.getColumnIndex("PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP")).isEmpty())
                    configuracao.setTrabalha_com_desc_auto_app(cursor.getString(cursor.getColumnIndex("PEDIDO_ITEM_PUXAR_DESC_PADRAO_APP")));
                else
                    configuracao.setTrabalha_com_desc_auto_app("N");
            } catch ( NullPointerException e) {
                configuracao.setTrabalha_com_desc_auto_app("N");
            } catch ( Exception e) {
                configuracao.setTrabalha_com_desc_auto_app("N");
            }

            try {
                configuracao.setSincronia_automatica_app(cursor.getString(cursor.getColumnIndex("SINCRONIA_AUTOMATICA_APP")));
            } catch ( NullPointerException e) {
                configuracao.setSincronia_automatica_app("N");
            } catch ( Exception e) {
                configuracao.setSincronia_automatica_app("N");
            }

            try {
                if (cursor.getString(cursor.getColumnIndex("TRABALHA_COM_VERBA_PEDIDO")) != null && !cursor.getString(cursor.getColumnIndex("TRABALHA_COM_VERBA_PEDIDO")).isEmpty())
                    configuracao.setTrabalha_com_verba_pedido(cursor.getString(cursor.getColumnIndex("TRABALHA_COM_VERBA_PEDIDO")));
                else
                    configuracao.setTrabalha_com_verba_pedido("N");
            } catch ( NullPointerException e) {
                configuracao.setTrabalha_com_verba_pedido("N");
            } catch ( Exception e) {
                configuracao.setTrabalha_com_verba_pedido("N");
            }
            try {
                if (cursor.getString(cursor.getColumnIndex("LISTA_TODOS_PEDIDOS")) != null && !cursor.getString(cursor.getColumnIndex("LISTA_TODOS_PEDIDOS")).isEmpty())
                    configuracao.setLista_todos_pedidos(cursor.getString(cursor.getColumnIndex("LISTA_TODOS_PEDIDOS")));
                else
                    configuracao.setLista_todos_pedidos("N");
            } catch ( NullPointerException e) {
                configuracao.setLista_todos_pedidos("N");
            } catch ( Exception e) {
                configuracao.setLista_todos_pedidos("N");
            }

        } else
            configuracao.setId("");
        return configuracao;
    }
}
