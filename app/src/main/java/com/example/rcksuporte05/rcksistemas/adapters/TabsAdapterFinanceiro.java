package com.example.rcksuporte05.rcksistemas.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.rcksuporte05.rcksistemas.fragment.FinanceiroQuitado;
import com.example.rcksuporte05.rcksistemas.fragment.FinanceiroVencer;
import com.example.rcksuporte05.rcksistemas.fragment.FinanceiroVencido;

public class TabsAdapterFinanceiro extends FragmentPagerAdapter {
    private String[] titles = { "Titulo(s) vencido(s)", "Titulo(s) a vencer", "Titulo(s) pago(s)" };
    public TabsAdapterFinanceiro(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        switch (position) {
            case 0:
                frag = new FinanceiroVencido();
                break;
            case 1:
                frag = new FinanceiroVencer();
                break;
            case 2:
                frag = new FinanceiroQuitado();
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (titles[position]);
    }
}
