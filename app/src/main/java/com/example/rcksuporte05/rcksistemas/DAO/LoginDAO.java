package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Usuario;

public class LoginDAO {
    private DBHelper db;
    public LoginDAO(DBHelper db) {
        this.db = db;
    }

    public long add(Usuario usuario) {
        ContentValues content = new ContentValues();
        try {
            content.put("ID_LOGIN", 1);
            content.put("LOGIN", usuario.getLogin());
            content.put("SENHA", usuario.getSenha());
            content.put("LOGADO", usuario.getLogado());
            content.put("APARELHO_ID", usuario.getAparelho_id());
            content.put("TOKEN", usuario.getToken());
            System.gc();
            return db.addDados("TBL_LOGIN", content);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int update(Usuario usuario) {
        ContentValues content = new ContentValues();
        try {
            content.put("LOGIN", usuario.getLogin());
            content.put("SENHA", usuario.getSenha());
            content.put("LOGADO", usuario.getLogado());
            content.put("APARELHO_ID", usuario.getAparelho_id());
            content.put("TOKEN", usuario.getToken());
            System.gc();
            return db.updateDados("TBL_LOGIN", content, "ID_LOGIN = " + 1);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
