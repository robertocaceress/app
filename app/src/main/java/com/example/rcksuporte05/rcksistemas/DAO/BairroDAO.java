package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;

import com.example.rcksuporte05.rcksistemas.model.Generico;
import com.example.rcksuporte05.rcksistemas.model.Moeda;

import java.util.ArrayList;
import java.util.List;

public class BairroDAO {
    private DBHelper db;

    public BairroDAO(DBHelper db) {
        this.db = db;
    }


    public long add(Generico bairro) {
        ContentValues content = new ContentValues();
        try {
            content.put("ID", bairro.getId());
            content.put("ID_CIDADE", bairro.getId_generico());  //id DA CIDADE
            content.put("NOME_BAIRRO", bairro.getNome());
            content.put("ORDEM", bairro.getOrdem());
            System.gc();
            return db.addDados("TBL_CADASTRO_BAIRRO", content);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List<Generico> getLista(String SQL) {
        List<Generico> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        do {
            Generico bairro = new Generico();
            try {
                bairro.setId(cursor.getString(cursor.getColumnIndex("ID")));
                bairro.setId_generico(cursor.getString(cursor.getColumnIndex("ID_CIDADE")));
                bairro.setNome(cursor.getString(cursor.getColumnIndex("NOME_BAIRRO")));
                bairro.setOrdem(cursor.getString(cursor.getColumnIndex("ORDEM")));
                lista.add(bairro);
            } catch (CursorIndexOutOfBoundsException e) {
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }
}
