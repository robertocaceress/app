package com.example.rcksuporte05.rcksistemas.model;

import androidx.annotation.NonNull;

public class Generico {

    private String id;
    private String nome;
    private String ativo;
    private String id_generico;
    private String ordem;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getId_generico() {
        return id_generico;
    }

    public void setId_generico(String id_generico) {
        this.id_generico = id_generico;
    }

    public String getOrdem() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem = ordem;
    }

    @NonNull
    @Override
    public String toString() {
        return nome;
    }
}
