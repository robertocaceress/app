package com.example.rcksuporte05.rcksistemas.adapters.viewHolder;


import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ListaAdapterProdutoPedido;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RCK 03 on 30/11/2017.
 */

public class ProdutoPedidoViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener, View.OnLongClickListener*/ {
    @BindView(R.id.idPosition)
    public TextView idPosition;

    @BindView(R.id.nomeListaProduto)
    public TextView nomeListaProduto;

    @BindView(R.id.precoProduto)
    public TextView precoProduto;

    @BindView(R.id.textViewUnidadeMedida)
    public TextView textViewUnidadeMedida;

    @BindView(R.id.textViewCampanha)
    public TextView textViewCampanha;

    @BindView(R.id.nomeCampanha)
    public TextView nomeCampanha;

    @BindView(R.id.txtDesconto)
    public TextView txtDesconto;

    @BindView(R.id.rlProdutoPedido)
    public RelativeLayout itemView;

    @BindView(R.id.btnExcluirProduto)
    public Button btnExcluirProduto;

    ListaAdapterProdutoPedido.ProdutoPedidoAdapterListener listener;

    public ProdutoPedidoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
