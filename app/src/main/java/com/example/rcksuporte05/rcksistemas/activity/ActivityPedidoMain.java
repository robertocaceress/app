package com.example.rcksuporte05.rcksistemas.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;

import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.BO.UsuarioBO;
import com.example.rcksuporte05.rcksistemas.DAO.CategoriaDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.HistoricoFinanceiroHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.TabsAdapterPedido;
import com.example.rcksuporte05.rcksistemas.fragment.Pedido1;
import com.example.rcksuporte05.rcksistemas.model.Categoria;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.SlidingTabLayout;
import com.example.rcksuporte05.rcksistemas.util.Utilitaria;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityPedidoMain extends AppCompatActivity {

    public static ActionMode actionMode;
    private static Cliente objetoCliente = null;
    @BindView(R.id.toolbarFragsPedido)
    public Toolbar toolbar;
    @BindView(R.id.txtNomeCliente)
    public TextView txtNomeCliente;
    @BindView(R.id.txtNomeFantasia)
    public TextView txtNomeFantasia;
    @BindView(R.id.stl_tabsPedido)
    SlidingTabLayout stl_tabsPedido;
    @BindView(R.id.vp_tabsPedido)

    ViewPager mViewPager;

    @BindView(R.id.btnBuscaCliente)
    Button btnBuscaCliente;
    @BindView(R.id.ifCliente)
    LinearLayout ifCliente;

    @BindView(R.id.lyvalorIcmsST)
    LinearLayout lyvalorIcmsST;

    ActionModeCallback actionModeCallback;
    private PedidoHelper pedidoHelper;
    private TabsAdapterPedido tabsAdapterPedido;
    private int vizualizacao;
    private DBHelper db;
    private Bundle bundle = new Bundle();
    private WebPedido webPedido;
    private Pedido1 pedido1 = new Pedido1();
    private HashMap<Integer, Categoria> listaCategoria;
    private CategoriaDAO categoriaDAO;
    private WebPedidoDAO webPedidoDAO;
    public static boolean pedidoEnviado = false;
    //private ConfiguracaoDAO configuracaoDAO;
    Utilitaria util = new Utilitaria();

    @OnClick(R.id.btnBuscaCliente)
    public void buscaCliente() {
        if (vizualizacao != 1)
            showCliente();
    }

    @OnClick(R.id.ifCliente)
    public void nomeCliente() {
        if (vizualizacao != 1)
            showCliente();
    }

    private void showCliente() {
        bundle = new Bundle();
        bundle.putInt("acao", 1);
        Intent intent = new Intent(this, ActivityClienteDrawer.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
    private void showHideLayoutIcmsST(){
        //ViewGroup.LayoutParams params = lyvalorIcmsST.getLayoutParams();
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) lyvalorIcmsST.getLayoutParams();
        params.weight = 0;
        params.height = 0;
        params.width = 0;
        lyvalorIcmsST.setLayoutParams(params);
        ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
        Configuracao configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        try {
            if ( configuracao.getTrabalha_com_icms_st().equalsIgnoreCase("S")){
                params.height =  LinearLayout.LayoutParams.WRAP_CONTENT;
                params.width  =  LinearLayout.LayoutParams.WRAP_CONTENT;
                params.weight = 1;
                lyvalorIcmsST.setLayoutParams(params);
            }
        } catch ( NullPointerException e){
        } catch ( Exception e) {
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_main);
        ButterKnife.bind(this);
        db = new DBHelper(this);
        showHideLayoutIcmsST();
        pedidoHelper = new PedidoHelper(this);
        vizualizacao = getIntent().getIntExtra("vizualizacao", 0);
        pedidoEnviado = false;
        try {
            if (!UsuarioHelper.getUsuario().getId_usuario().isEmpty()) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPedidoMain.this));
            }
        } catch (NullPointerException e) {
            UsuarioBO usuarioBO = new UsuarioBO();
            UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPedidoMain.this));
        } catch ( Exception e) {
        }

        tabsAdapterPedido = new TabsAdapterPedido(getSupportFragmentManager(), ActivityPedidoMain.this, UsuarioHelper.getUsuario(), vizualizacao);
        mViewPager.setAdapter(tabsAdapterPedido);

        stl_tabsPedido.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        stl_tabsPedido.setSelectedIndicatorColors(Color.WHITE);
        stl_tabsPedido.setViewPager(mViewPager);

        if (vizualizacao == 1) {
            toolbar.setTitle("Vizualização de Pedido");
            txtNomeCliente.setFocusable(false);
            this.setTheme(R.style.Theme_MeuTemaPedido);
            pedidoEnviado = true;
        } else if (pedidoHelper.getIdPedido() > 0) {
            toolbar.setTitle("Alteração do pedido");
        } else {
            toolbar.setTitle("Lançamento de Pedido");
            if (ClienteHelper.getCliente() != null) {
                if (ClienteHelper.getCliente().getId_cadastro() == 0)
                    toolbar.setTitle("Pré-pedido");

                if (PedidoHelper.getListaWebPedidoItens() != null && PedidoHelper.getListaWebPedidoItens().size() > 0) {
                    //System.out.println("pedido duplicado!");
                } else {
                    objetoCliente = ClienteHelper.getCliente();
                    Intent intent = new Intent(this, ActivityProdutoDrawer.class);
                    intent.putExtra("acao", 1);
                    intent.putExtra("idPedido", PedidoHelper.getIdPedido());
                    intent.putExtra("idCliente", ClienteHelper.getCliente().getId_cadastro());
                    startActivity(intent);
                }
            } else
                btnBuscaCliente.callOnClick();

        }

        categoriaDAO = new CategoriaDAO(db);
        webPedidoDAO = new WebPedidoDAO(db);
        try {
            listaCategoria = categoriaDAO.listaHashCategoria();
        } catch (CursorIndexOutOfBoundsException e) {
        }

        if (PedidoHelper.getIdPedido() > 0) {
            webPedido = webPedidoDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO WHERE ID_WEB_PEDIDO = " + PedidoHelper.getIdPedido() + ";").get(0);
            objetoCliente = webPedido.getCadastro();
            ClienteHelper.setCliente(objetoCliente);
            if (webPedido.getId_web_pedido_servidor() != null)
                toolbar.setSubtitle("Pedido: " + webPedido.getId_web_pedido_servidor());
            else {
                if (ClienteHelper.getCliente().getId_cadastro() == 0)
                    toolbar.setSubtitle("Pré-pedido: " + webPedido.getId_web_pedido());
                else
                    toolbar.setSubtitle("Pedido: " + webPedido.getId_web_pedido());
            }
        } else if (PedidoHelper.getWebPedido() != null) {
            webPedido = PedidoHelper.getWebPedido();
            objetoCliente = webPedido.getCadastro();
            if ( objetoCliente == null) {
                //Mudei aqui 01/04/2021
                objetoCliente = ClienteHelper.getCliente();
            }
            ClienteHelper.setCliente(objetoCliente);
        } else {
            webPedido = new WebPedido();
            webPedido.setPrazo_confirmado("N");
        }


        if (objetoCliente != null)
            try {
                txtNomeCliente.setText(objetoCliente.getNome_cadastro());
                txtNomeFantasia.setText(objetoCliente.getNome_fantasia());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        actionModeCallback = new ActionModeCallback();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public WebPedido salvaPedido() {
        if (vizualizacao != 1) {
            webPedido.setCadastro(objetoCliente);
            try {
                if (!UsuarioHelper.getUsuario().getId_usuario().isEmpty()) {
                    UsuarioBO usuarioBO = new UsuarioBO();
                    UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPedidoMain.this));
                }
                setDadosVendedor();
            } catch (NullPointerException e) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPedidoMain.this));
            } catch ( Exception e) {
                UsuarioBO usuarioBO = new UsuarioBO();
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPedidoMain.this));
                setDadosVendedor();
            }
            webPedido.setExcluido("N");
            webPedido.setStatus("A");
            webPedido.setPedido_enviado("N");
        }
        return webPedido;
    }

    private void setDadosVendedor() {
        try {
            webPedido.setId_empresa(UsuarioHelper.getUsuario().getIdEmpresaMultiDevice());
            webPedido.setId_vendedor(UsuarioHelper.getUsuario().getId_quando_vendedor());
            webPedido.setUsuario_lancamento_id(UsuarioHelper.getUsuario().getId_usuario());
            webPedido.setUsuario_lancamento_nome(UsuarioHelper.getUsuario().getNome_usuario());
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
    }

    @Override
    public void onBackPressed() {
        if (vizualizacao != 1)
            if (mViewPager.getCurrentItem() != 0)
                mViewPager.setCurrentItem(0);
            else
                showMsgSimNao("Tem certeza que deseja sair do pedido? As informações não salvas serão perdidas");
        else
            finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (getIntent().getIntExtra("vizualizacao", 0) != 1)
            getMenuInflater().inflate(R.menu.menu_produto_pedido, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_salvar_produto:
                if (mViewPager.getCurrentItem() == 0)
                    mViewPager.setCurrentItem(1);
                else {
                    ProgressDialog dialog = new ProgressDialog(PedidoHelper.getActivityPedidoMain());
                    dialog.setTitle("Atenção!");
                    dialog.setMessage("Salvando o Pedido");
                    dialog.setCancelable(false);
                    dialog.show();
                    if (pedidoHelper.salvaPedido())
                        util.showMsgSucesso("Pedido salvo com sucesso!", ActivityPedidoMain.this);
                    dialog.dismiss();
                }
                break;
            case android.R.id.home:
                if (vizualizacao != 1)
                    if (mViewPager.getCurrentItem() != 0)
                        mViewPager.setCurrentItem(0);
                    else
                        showMsgSimNao("Tem certeza que deseja sair do pedido? As informações não salvas serão perdidas");
                else
                    finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pegaCliente(Cliente cliente) {
        objetoCliente = cliente;
        ClienteHelper.setCliente(cliente);
    }

    @Override
    protected void onDestroy() {
        pedidoHelper.limparDados();
        objetoCliente = null;
        HistoricoFinanceiroHelper.limparDados();
        System.gc();
        super.onDestroy();
    }

    public boolean verificaCliente() {
        return objetoCliente != null;
    }

    public boolean isPedidoEnviado() {
        return  pedidoEnviado;
    }

    public void updatePedido() {
        if (vizualizacao != 1)
            pedidoHelper.salvaPedidoParcial();
    }

    @Override
    protected void onResume() {
        try {
            if (PedidoHelper.getListaWebPedidoItens().size() <= 0 && verificaCliente() && txtNomeCliente.getText().toString().equals("Toque aqui para selecionar o seu cliente")) {
                Intent intent = new Intent(this, ActivityProdutoDrawer.class);
                intent.putExtra("acao", 1);
                intent.putExtra("idPedido", PedidoHelper.getIdPedido());
                intent.putExtra("idCliente", ClienteHelper.getCliente().getId_cadastro());
                startActivity(intent);
            }
            txtNomeCliente.setText(objetoCliente.getNome_cadastro());
            txtNomeFantasia.setText(objetoCliente.getNome_fantasia());
            txtNomeFantasia.setVisibility(View.VISIBLE);
            ifCliente.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        if (verificaCliente())
            if (vizualizacao != 1)
                pedidoHelper.salvaPedidoParcial();
        super.onResume();
    }

    public void enableActionMode(int position) {
        if (actionMode == null)
            actionMode = startSupportActionMode(actionModeCallback);
        pedido1.toggleSelection(position);
    }

    private class ActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_action_mode_produtos, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ActivityPedidoMain.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoMain.this);
                    builder.setView(dialogView);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.setCancelable(false);
                    TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
                    textView.setText("Tem certeza que deseja excluir o(s) produto(s) selecionado(s)");
                    Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
                    btnNao.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                    Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
                    btnSim.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            excluiProdutos( mode, pedido1.getListaAdapterProdutoPedido().getItensSelecionados());
                        }
                    });
                    alertDialog.show();
                    return true;
            }
            return false;
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            Pedido1.getListaAdapterProdutoPedido().clearSelections();
            actionMode = null;
        }
    }

    public void excluiProdutos(ActionMode mode, List<WebPedidoItens> lista) {
        if ( !pedido1.removeProdutos(lista) )
            Toast.makeText( ActivityPedidoMain.this, "Falha ao excluir o(s) produto(s)! Tente novamente", Toast.LENGTH_SHORT).show();
        pedidoHelper.salvaPedidoParcial();
        try {
            mode.finish();
        } catch ( NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        onResume();
    }


    public void showMsgSimNao( String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoMain.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoMain.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });

        alertDialog.show();
    }
    /*
    public void showMsgSucesso(String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPedidoMain.this).inflate(R.layout.dialog_sucesso_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPedidoMain.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PedidoHelper.getActivityPedidoMain().finish();
            }
        });
        alertDialog.show();
    }

     */
}
