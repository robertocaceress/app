package com.example.rcksuporte05.rcksistemas.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.BO.CadastroAnexoBO;
import com.example.rcksuporte05.rcksistemas.DAO.CadastroAnexoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ClienteDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.HistoricoFinanceiroHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ListaClienteAdapter;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.fragment.Pedido2;
import com.example.rcksuporte05.rcksistemas.model.CadastroAnexo;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.util.Common;
import com.example.rcksuporte05.rcksistemas.util.DividerItemDecoration;
import com.example.rcksuporte05.rcksistemas.util.Utilitaria;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityClienteDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.listaRecycler)
    RecyclerView recyclerView;

    @BindView(R.id.edtTotalClientes)
    EditText edtTotalClientes;

    @BindView(R.id.tb_cliente)
    Toolbar toolbar;

    @BindView(R.id.buscaCliente)
    SearchView buscaCliente;

    @BindView((R.id.helpCliente))
    ImageView helpCliente;

    @BindView(R.id.btnInserirCliente)
    Button btnInserirCliente;

    private DBHelper db = new DBHelper(this);
    private ClienteDAO clienteDAO = new ClienteDAO(db);

    private ListaClienteAdapter adapter;
    private ListaClienteAdapter.Listener listener;
    private List<Cliente> lista;
    private ActionMode actionMode;
    Utilitaria util = new Utilitaria();
    private ProgressDialog progress;
    private boolean addNovo = false;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Context context;
    private Configuracao configuracao = new Configuracao();

    private int offSet = 0;
    private int totalReg = 0;
    private int limitReg = 50;
    private String SQL;

    @OnClick(R.id.btnInserirCliente)
    public void inserirCliente() {
        Cliente cliente = new Cliente();
        ClienteHelper.setCliente(cliente);
        if (db.contagem("SELECT COUNT(*) FROM TBL_CADASTRO WHERE FINALIZADO = 'N'") >= 1) {
            androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(ActivityClienteDrawer.this);
            alert.setTitle("Atenção!");
            alert.setMessage("Foi detectado um cadastro de cliente em andamento, deseja continua-lo?");
            alert.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    addNovo = true;
                    novoCadastro();
                }
            });
            alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        continuaCadastro();
                    } catch (CursorIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            });
            alert.show();
        } else {
            androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(ActivityClienteDrawer.this);
            alert.setTitle("Atenção!");
            alert.setMessage("Após a conclusão do novo cadastro, consulte a lista de clientes não efetivados, para efetuar a sincronização do mesmo!");
            alert.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    addNovo = true;
                    startActivity(new Intent(ActivityClienteDrawer.this, ActivityCpfCnpjCliente.class).putExtra("novo", 1));
                }
            });
            alert.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_cliente_drawer);
        ButterKnife.bind(this);
        toolbar.setTitle("Lista de Clientes");
        getConfiguracao();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new ActivityClienteDrawer.ScrollListener());
        buscaCliente.setOnQueryTextListener(new QueryTextListener());
        buscaCliente.setIconifiedByDefault(true);
        if (getIntent().getIntExtra("acao", 0) == 1)
            btnInserirCliente.setVisibility(View.GONE);
        setListenerCliente();
        setRecyclerView(getListaClientes(0), 2);
        drawerLayout = findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(ActivityClienteDrawer.this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        navigationView.setItemTextColor(ColorStateList.valueOf(Color.WHITE));
        addMenuDrawerItem();
        helpCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(ActivityClienteDrawer.this, ActivityDialogCliente.class));
            }
        });
        System.gc();
    }

    @Override
    protected void onResume() {
        try {
            if (buscaCliente != null && !buscaCliente.getQuery().toString().trim().isEmpty())
                System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (addNovo) {
            setRecyclerView(getListaClientes(2), 2);
            addNovo = false;
        }
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                System.gc();
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class ScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            try {
                if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("N")) {
                    edtTotalClientes.setText("(" + (getFirstItem() + 1) + " A " + (getLastItem() + 1) + ")/" + totalReg);
                    return;
                }
            } catch (NullPointerException e) {
                return;
            }
            if (!recyclerView.canScrollVertically(1)) {
                if (configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S")) {
                    offSet += limitReg;
                    lista.addAll(clienteDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet));;
                    adapter.notifyDataSetChanged();
                }
            }
            edtTotalClientes.setText(  "(" + (getFirstItem() + 1) + " A " + (getLastItem() + 1) + ")/" + totalReg);
        }
    }

    private int getFirstItem(){
        return ((LinearLayoutManager)recyclerView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private int getLastItem() {
        return ((LinearLayoutManager) recyclerView.getLayoutManager())
                .findLastVisibleItemPosition();
    }

    private void getConfiguracao() {
        ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
        configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
    }

    private class QueryTextListener implements SearchView.OnQueryTextListener {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
        @Override
        public boolean onQueryTextChange(String query) {
            try {
                if (query.trim().equals(""))
                    setRecyclerView(getListaClientes(0), 2);
                else {
                    setRecyclerView(getListaClientes(query), 2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
        }
    }

    private void addMenuDrawerItem() {
        Menu menu = navigationView.getMenu();
        SubMenu subMenu = menu.addSubMenu(" Status/Pendência(s) ");
        subMenu.add(0, 0, 0, "TODOS OS CLIENTES").setIcon(R.drawable.ic_lista_todos_drawner);
        subMenu.add(0, 1, 1, "T. EFETIVADOS").setIcon(R.drawable.ic_lista_drawner);
        subMenu.add(0, 2, 2, "T. NÃO EFETIVADOS").setIcon(R.drawable.ic_lista_drawner);
        subMenu.add(0, 3, 3, "T. COM PENDÊNCIA(S) FINANCEIRA").setIcon(R.drawable.ic_lista_drawner);
        subMenu.add(0, 4, 4, "T. COM PEDIDO(S) EM ANALISE").setIcon(R.drawable.ic_lista_drawner);
        navigationView.invalidate();
    }

    public List<Cliente> getListaClientes(final String query) {
        try {
            offSet = 0;
            SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' "/*AND ATIVO = 'S'*/ + "AND (NOME_CADASTRO LIKE '%" + query + "%' OR NOME_FANTASIA LIKE '%" + query + "%' OR CPF_CNPJ LIKE '" + query + "%' OR TELEFONE_PRINCIPAL LIKE '%" + query + "%' OR ID_CADASTRO_SERVIDOR LIKE '%" + query + "%')";
            totalReg =clienteDAO.getTotalReg(SQL);
            SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' " /*AND ATIVO = 'S'*/ + "AND (NOME_CADASTRO LIKE '%" + query + "%' OR NOME_FANTASIA LIKE '%" + query + "%' OR CPF_CNPJ LIKE '" + query + "%' OR TELEFONE_PRINCIPAL LIKE '%" + query + "%' OR ID_CADASTRO_SERVIDOR LIKE '%" + query + "%') ORDER BY NOME_CADASTRO ";
            if ( configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S"))
                lista = clienteDAO.getLista( SQL + "LIMIT " + limitReg + " OFFSET " + offSet);
            else
                lista = clienteDAO.getLista(SQL);
            edtTotalClientes.setText( lista.size() + "/" + totalReg);
            return lista;
        } catch (CursorIndexOutOfBoundsException e) {
            edtTotalClientes.setText("0/0");
            Toast.makeText(this, "Nenhum registro encontrado", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    public List<Cliente> getListaClientes(int idStatus) {
        try {
            offSet = 0;
            if ( idStatus == 0) {
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' ";/*AND ATIVO = 'S'"*/;
                totalReg =clienteDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' " /*AND ATIVO = 'S'*/ + "ORDER BY NOME_CADASTRO ";
            }else if ( idStatus == 1) {//Efetivados
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE = 'S' " /*AND ATIVO = 'S'*/ + "AND ID_CADASTRO_SERVIDOR > 0";
                totalReg =clienteDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE = 'S' " /*AND ATIVO = 'S'*/ + "AND ID_CADASTRO_SERVIDOR > 0 ORDER BY NOME_CADASTRO ";
            } else if ( idStatus == 2) {//Não efetivados
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE <> 'S' " /*AND ATIVO = 'S'*/ + "AND F_VENDEDOR = 'N'";
                totalReg =clienteDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE <> 'S' " /*AND ATIVO = 'S'*/ + "AND F_VENDEDOR = 'N' ORDER BY ID_PROSPECT, ID_CADASTRO DESC, NOME_CADASTRO ";
            } else if (idStatus == 3) {//Efetivado com pendencia financeira
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE = 'S' " /*AND ATIVO = 'S'*/ + "AND ID_CADASTRO_SERVIDOR > 0 AND ( FINANCEIRO_VENCIDO <> '0.0' OR FINANCEIRO_VENCENDO <> '0.0' )";
                totalReg =clienteDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE = 'S' " /*AND ATIVO = 'S'*/ + "AND ID_CADASTRO_SERVIDOR > 0 AND ( FINANCEIRO_VENCIDO <> '0.0' OR FINANCEIRO_VENCENDO <> '0.0' ) ORDER BY NOME_CADASTRO ";
            } else if ( idStatus == 4) {//Efetivado com pedido em analise
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE = 'S' " /*AND ATIVO = 'S'*/ + "AND ID_CADASTRO_SERVIDOR > 0 AND  PEDIDO_ANALISE <> '0.0'";
                totalReg =clienteDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' AND F_CLIENTE = 'S' " /*AND ATIVO = 'S'*/ + "AND ID_CADASTRO_SERVIDOR > 0 AND  PEDIDO_ANALISE <> '0.0' ORDER BY NOME_CADASTRO ";
            } else {
                SQL = "SELECT COUNT(*) AS TOTALREG FROM TBL_CADASTRO WHERE FINALIZADO <> 'N'";// AND ATIVO = 'S'";
                totalReg =clienteDAO.getTotalReg(SQL);
                SQL = "SELECT * FROM TBL_CADASTRO WHERE FINALIZADO <> 'N' " /*AND ATIVO = 'S'*/ + "ORDER BY NOME_CADASTRO ";
            }
            if ( configuracao.getTrabalha_com_paginacao_dados().equalsIgnoreCase("S"))
                lista = clienteDAO.getLista(SQL + "LIMIT " + limitReg + " OFFSET " + offSet);
            else
                lista = clienteDAO.getLista(SQL);

            if (lista.size() > 0)
                edtTotalClientes.setText( lista.size() + "/" + totalReg);
            else {
                edtTotalClientes.setText("0/0");
                Toast.makeText(this, "Nenhum registro encontrado", Toast.LENGTH_SHORT).show();
            }
            return lista;
        } catch (CursorIndexOutOfBoundsException e) {
            edtTotalClientes.setText("0/0");
            Toast.makeText(this, "Nenhum registro encontrado", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    private void setListenerCliente() {
        if (getIntent().getIntExtra("acao", 0) == 1) {
            listener = new ListaClienteAdapter.Listener() {
                @Override
                public void onClickListener(int position) {
                    boolean abrePedido = true;
                    if (adapter.getItem(position).getIdCategoria() <= 0) {
                        abrePedido = false;
                        util.showMsgAlerta("Este cliente não tem categoria definida!", ActivityClienteDrawer.this);
                    } else if (adapter.getItem(position).getAtivo().equalsIgnoreCase("N")) {
                        abrePedido = false;
                        util.showMsgAlerta("Este cliente esta inátivo/bloqueado!", ActivityClienteDrawer.this);
                    } else if (configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                        float limiteCredito = Float.parseFloat(adapter.getItem(position).getLimite_credito());
                        float limitUtilizado = Float.parseFloat(adapter.getItem(position).getFinanceiro_vencido()) + Float.parseFloat(adapter.getItem(position).getFinanceiro_vencendo());
                        if ((limiteCredito - limitUtilizado) < 0.00f) {
                            abrePedido = false;
                            showMsgSimNao("Cliente sem limite disponivel para compra a prazo! Pedido só podera ser feito para pagamento à vista!\nDeseja continuar?", position, true);
                        } else{
                            try {
                                ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
                                activityPedidoMain.pegaCliente(adapter.getItem(position));
                                Pedido2 pedido2 = new Pedido2();
                                pedido2.pegaCliente(adapter.getItem(position));
                                System.gc();
                                finish();
                            } catch (CursorIndexOutOfBoundsException e) {
                                e.printStackTrace();
                                Toast.makeText(ActivityClienteDrawer.this, "Financeiro não encontrado, por favor faça a sincronia e tente novamente!", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        if (abrePedido)
                            try {
                                ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
                                activityPedidoMain.pegaCliente(adapter.getItem(position));
                                Pedido2 pedido2 = new Pedido2();
                                pedido2.pegaCliente(adapter.getItem(position));
                                System.gc();
                                finish();
                            } catch (CursorIndexOutOfBoundsException e) {
                                e.printStackTrace();
                                Toast.makeText(ActivityClienteDrawer.this, "Financeiro não encontrado, por favor faça a sincronia e tente novamente!", Toast.LENGTH_LONG).show();
                            }

                    }

                }

                @Override
                public void onLongClickListener(int position) {
                }

                @Override
                public View.OnClickListener onClickDowload(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showMsgSimNao(view.getId(),position, "Deseja efetuar a sincronização/atualização dos dados do cliente?");
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickChamada(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (!adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").trim().isEmpty())
                                    if (adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").length() >= 8 && adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").length() <= 11)
                                        showMsgSimNao(view.getId(), position, "Deseja ligar para o número " + adapter.getItem(position).getTelefone_principal() + " ?");
                                    else
                                        util.showMsgAlerta("Este numero de telefone não é válido!", ActivityClienteDrawer.this);
                                else
                                    util.showMsgAlerta("Nenhum numero de telefone informado!", ActivityClienteDrawer.this);
                            } catch (Exception e) {
                                util.showMsgAlerta("Nenhum numero de telefone informado/ou número inválido!", ActivityClienteDrawer.this);
                            }
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickGps(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (adapter.getItem(position).getEndereco().equals("Nenhum endereço informado!"))
                                util.showMsgAlerta("Nenhum endereço informado!", ActivityClienteDrawer.this);
                            else
                                showMsgSimNao(view.getId(), position, "Deseja navegar para a localização para " + adapter.getItem(position).getNome_cadastro() + " no GPS?");
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickEmail(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (adapter.getItem(position).getEmail_principal().isEmpty())
                                    util.showMsgAlerta("Nenhum e-mail informado!", ActivityClienteDrawer.this);
                                else
                                    showMsgSimNao(view.getId(), position, "Deseja enviar um email a " + adapter.getItem(position).getNome_cadastro() + "?");
                            } catch (NullPointerException e) {
                                util.showMsgAlerta("Nenhum e-mail informado!", ActivityClienteDrawer.this);
                            }
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickFinanceiro(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showMsgSimNao(view.getId(), position, "Deseja efetuar consulta ao historico financeiro?");
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickNovoPedido(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            boolean abrePedido = true;
                            if (adapter.getItem(position).getIdCategoria() <= 0) {
                                abrePedido = false;
                                util.showMsgAlerta("Este cliente não tem uma categoria definida", ActivityClienteDrawer.this);
                            } else if (adapter.getItem(position).getAtivo().equalsIgnoreCase("N")) {
                                abrePedido = false;
                                util.showMsgAlerta("Este cliente esta inátivo/bloqueado!", ActivityClienteDrawer.this);
                            } else if (configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                                float limiteCredito = Float.parseFloat(adapter.getItem(position).getLimite_credito());
                                float limitUtilizado = Float.parseFloat(adapter.getItem(position).getFinanceiro_vencido()) + Float.parseFloat(adapter.getItem(position).getFinanceiro_vencendo());
                                if ((limiteCredito - limitUtilizado) < 0.00f)
                                    showMsgSimNao("Cliente sem limite disponivel para compra a prazo! Pedido só podera ser feito para pagamento à vista!\nDeseja continuar?", position, false);
                                else
                                    showMsgSimNao(view.getId(), position, "Deseja iniciar um novo pedido para " + adapter.getItem(position).getNome_cadastro() + "?");

                            } else {
                                if (abrePedido)
                                    showMsgSimNao(view.getId(), position, "Deseja iniciar um novo pedido para " + adapter.getItem(position).getNome_cadastro() + "?");

                            }
                        }
                    };
                }
            };
        } else {
            listener = new ListaClienteAdapter.Listener() {
                @Override
                public void onClickListener(int position) {
                    if (adapter.getSelectedItensCount() > 0)
                        enableActionMode(position);
                    else {
                        Intent intent;
                        if (adapter.getItem(position).getF_cliente().equals("S")) {
                            intent = new Intent(ActivityClienteDrawer.this, ActivityContato.class);
                            ClienteHelper.setCliente(adapter.getItem(position));
                            if (adapter.getItem(position).getId_cadastro_servidor() > 0)
                                intent.putExtra("vizualizacao", 1);
                        } else {
                            intent = new Intent(ActivityClienteDrawer.this, CadastroClienteMain.class);
                            intent.putExtra("novo", 1);
                            ClienteHelper.setCliente(adapter.getItem(position));
                        }
                        System.gc();
                        startActivity(intent);
                    }
                }

                @Override
                public void onLongClickListener(int position) {
                    enableActionMode(position);
                }

                @Override
                public View.OnClickListener onClickDowload(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showMsgSimNao(view.getId(),position, "Deseja efetuar a sincronização/atualização dos dados do cliente?");
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickChamada(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (!adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").trim().isEmpty())
                                    if (adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").length() >= 8 && adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").length() <= 11)
                                        showMsgSimNao(view.getId(), position, "Deseja ligar para o número " + adapter.getItem(position).getTelefone_principal() + " ?");
                                    else
                                        util.showMsgAlerta("Este numero de telefone não é válido!", ActivityClienteDrawer.this);
                                else
                                    util.showMsgAlerta("Nenhum numero de telefone informado!", ActivityClienteDrawer.this);
                            } catch (Exception e) {
                                util.showMsgAlerta("Nenhum numero de telefone informado/ou número inválido!", ActivityClienteDrawer.this);
                            }
                        }
                    };
                }
                @Override
                public View.OnClickListener onClickGps(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (adapter.getItem(position).getEndereco().equals("Nenhum endereço informado!"))
                                util.showMsgAlerta("Nenhum endereço informado!", ActivityClienteDrawer.this);
                            else
                                showMsgSimNao(view.getId(), position, "Deseja navegar para a localização para " + adapter.getItem(position).getNome_cadastro() + " no GPS?");
                        }
                    };
                }
                @Override
                public View.OnClickListener onClickEmail(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (adapter.getItem(position).getEmail_principal().isEmpty())
                                    util.showMsgAlerta("Nenhum e-mail informado!", ActivityClienteDrawer.this);
                                else
                                    showMsgSimNao(view.getId(), position, "Deseja enviar um email a " + adapter.getItem(position).getNome_cadastro() + "?");
                            } catch (NullPointerException e) {
                                util.showMsgAlerta("Nenhum e-mail informado!", ActivityClienteDrawer.this);
                            }
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickFinanceiro(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showMsgSimNao(view.getId(), position, "Deseja efetuar consulta ao historico financeiro?");
                        }
                    };
                }

                @Override
                public View.OnClickListener onClickNovoPedido(int position) {
                    return new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            boolean abrePedido = true;
                            if (adapter.getItem(position).getIdCategoria() <= 0) {
                                abrePedido = false;
                                util.showMsgAlerta("Este cliente não tem uma categoria definida", ActivityClienteDrawer.this);
                            } else if (adapter.getItem(position).getAtivo().equalsIgnoreCase("N")) {
                                abrePedido = false;
                                util.showMsgAlerta("Este cliente esta inátivo/bloqueado!", ActivityClienteDrawer.this);
                            } else if (configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                                float limiteCredito = Float.parseFloat(adapter.getItem(position).getLimite_credito());
                                float limitUtilizado = Float.parseFloat(adapter.getItem(position).getFinanceiro_vencido()) + Float.parseFloat(adapter.getItem(position).getFinanceiro_vencendo());
                                if ((limiteCredito - limitUtilizado) < 0.00f) {
                                    abrePedido = false;
                                    showMsgSimNao("Cliente sem limite disponivel para compra a prazo! Pedido só podera ser feito para pagamento à vista!\nDeseja continuar?", position, false);
                                } else {
                                    showMsgSimNao(view.getId(), position, "Deseja iniciar um novo pedido para " + adapter.getItem(position).getNome_cadastro() + "?");
                                }
                            } else {
                                if (abrePedido)
                                    showMsgSimNao(view.getId(), position, "Deseja iniciar um novo pedido para " + adapter.getItem(position).getNome_cadastro() + "?");

                            }
                        }
                    };
                }
            };
        }
    };

    public void setRecyclerView(List<Cliente> lista) {
        adapter = new ListaClienteAdapter(ActivityClienteDrawer.this, this.lista, listener);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if ( lista.size() > 0)
             edtTotalClientes.setText(   "1/" + totalReg);
        else
            edtTotalClientes.setText( "0/0");
    }


    public void setRecyclerView(List<Cliente> lista, final int numCols) {  //V
        int numColsList = 0;
        GridLayoutManager layoutManager;
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            adapter = new ListaClienteAdapter(ActivityClienteDrawer.this, this.lista, listener);
            layoutManager = new GridLayoutManager( this, 1);
            numColsList = 1;
        } else {
            adapter = new ListaClienteAdapter(ActivityClienteDrawer.this, this.lista, listener);
            layoutManager = new GridLayoutManager( this, 2);
            numColsList = 2;
        }// encida
        //adapter = new ListaClienteAdapter(ActivityClienteDrawer.this, this.lista, listener, 1);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new GridLayoutManager( this, Common.NUM_OF_COL_PORT);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        int finalNumColsList = numColsList;
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {

                    if (adapter != null) {
                        adapter.setNumCols(finalNumColsList);
                        switch (adapter.getItemViewType(i)) {
                            case 1:
                                return 1;
                            case 0:
                                return finalNumColsList;
                            default:
                                return -1;
                        }
                    }else
                        return -1;
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        while (recyclerView.getItemDecorationCount() > 0)
            recyclerView.removeItemDecorationAt(0);

        //recyclerView.addItemDecoration(new SpaceItemDecoration(Common.NUM_OF_SPACE_MESA));
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

    }

    public void enableActionMode(final int position) {
        if (adapter.getItem(position).getId_cadastro_servidor() <= 0 || (adapter.getItem(position).getId_cadastro_servidor() > 0 && adapter.getItem(position).getAlterado().equals("S"))) {
            if (adapter.getSelectedItensCount() == 0 || (adapter.getItensSelecionados().get(adapter.getSelectedItensCount() - 1).getAlterado().equals(adapter.getItem(position).getAlterado()))) {
                if (actionMode == null) {
                    actionMode = startActionMode(new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                            mode.getMenuInflater().inflate(R.menu.menu_cadastro_cliente, menu);
                            if (adapter.getItem(position).getId_cadastro_servidor() > 0)
                                menu.findItem(R.id.excluir_cliente).setVisible(false);
                            return true;
                        }
                        @Override
                        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                            return false;
                        }
                        @Override
                        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.subir_cliente:
                                    if (adapter.getSelectedItensCount() > 1)
                                        showMsgSimNao(item.getItemId(), 0, "Deseja enviar os " + adapter.getSelectedItensCount() + " cadastros de clientes selecionandos ao servidor?");
                                    else
                                        showMsgSimNao(item.getItemId(), 0, "Deseja enviar o cliente " + adapter.getItensSelecionados().get(0).getNome_cadastro() + " ao servidor?");
                                    break;
                                case R.id.excluir_cliente:
                                    if (adapter.getSelectedItensCount() > 1)
                                        showMsgSimNao(item.getItemId(), 0, "Deseja excluir esses " + adapter.getSelectedItensCount() + " cadastros?");
                                    else
                                        showMsgSimNao(item.getItemId(), 0, "Deseja excluir o cadastro de " + adapter.getItensSelecionados().get(0).getNome_cadastro() + " ?");
                                    break;
                            }
                            return true;
                        }
                        @Override
                        public void onDestroyActionMode(ActionMode mode) {
                            actionMode.finish();
                            adapter.clearSelection();
                            actionMode = null;
                        }
                    });
                }
                toggleSelection(position);
            } else {
                Toast.makeText(ActivityClienteDrawer.this, "O cadastro " + adapter.getItem(position).getNome_cadastro() + " não faz parte do grupo de seleção ativado", Toast.LENGTH_SHORT).show();
            }
        } else if (adapter.getItem(position).getF_cliente().equals("S")) {
            Toast.makeText(ActivityClienteDrawer.this, "Esse cliente já está efetivado", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ActivityClienteDrawer.this, "Cliente foi enviado e esta aguardando análise para efetivação", Toast.LENGTH_SHORT).show();
        }
    }

    public void toggleSelection(int position) {
        adapter.toggleSelection(position);
        if (adapter.getSelectedItensCount() == 0) {
            actionMode.finish();
            actionMode = null;
            adapter.clearSelection();
        } else {
            actionMode.setTitle(String.valueOf(adapter.getSelectedItensCount()));
            actionMode.invalidate();
        }
    }

    private void novoCadastro() {
        db.deleteDados("TBL_CADASTRO", "FINALIZADO = ?", new String[]{"N"});
        ClienteHelper.getCliente().setNome_municipio("");
        ClienteHelper.setPosicaoMunicipio(0);
        startActivity(new Intent(ActivityClienteDrawer.this, ActivityCpfCnpjCliente.class).putExtra("novo", 1));
    }

    private void continuaCadastro() {
        ClienteHelper.setCliente(clienteDAO.getLista("SELECT * FROM TBL_CADASTRO WHERE FINALIZADO = 'N'").get(0));
        if (ClienteHelper.getCliente().getId_cadastro() > 0) {
            if (db.contagem("SELECT COUNT(ID_ANEXO) FROM TBL_CADASTRO_ANEXOS WHERE ID_CADASTRO = " + ClienteHelper.getCliente().getId_cadastro() + " AND EXCLUIDO = 'N';") > 0) {
                final ProgressDialog progress = new ProgressDialog(ActivityClienteDrawer.this);
                progress.setTitle("Aguarde");
                progress.setMessage("Carregando anexos do cliente");
                progress.setCancelable(false);
                progress.show();
                final CadastroAnexoBO cadastroAnexoBO = new CadastroAnexoBO();
                Thread a = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<CadastroAnexo> listaCadastroAnexo = cadastroAnexoBO.listaCadastroAnexoComMiniatura(ActivityClienteDrawer.this, ClienteHelper.getCliente().getId_cadastro());
                        ClienteHelper.setListaCadastroAnexo(listaCadastroAnexo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.dismiss();
                            }
                        });
                    }
                });
                a.start();
            }
        } else {
            if (ClienteHelper.getCliente().getListaCadastroAnexo().size() > 0)
                ClienteHelper.setListaCadastroAnexo(ClienteHelper.getCliente().getListaCadastroAnexo());
        }
        startActivity(new Intent(ActivityClienteDrawer.this, CadastroClienteMain.class).putExtra("novo", 1));
    }

    public void addUpdateClienteAPI(List<Cliente> listaClientes) {
        showHideProgressDialog(true);
        Rotas apiRetrofit = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        final CadastroAnexoDAO cadastroAnexoDAO = new CadastroAnexoDAO(db);
        try {
            for (Cliente cliente : listaClientes)
                try {
                    cliente.setListaCadastroAnexo(cadastroAnexoDAO.getLista(cliente.getId_cadastro(), 0));
                } catch (CursorIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        Call<List<Cliente>> call = apiRetrofit.salvarClientes(cabecalho, listaClientes);
        call.enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                switch (response.code()) {
                    case 200:
                        List<Cliente> clientesRetorno = response.body();
                        if (clientesRetorno != null && clientesRetorno.size() > 0)
                            updateCliente(clientesRetorno, listaClientes);
                        else
                            limpaListaSelecionados();
                        showHideProgressDialog(false);
                        break;
                    case 500:
                        limpaListaSelecionados();
                        showHideProgressDialog(false);
                        util.showMsgAlerta("Erro ao sincronizar o(s) cadastro(s)!", ActivityClienteDrawer.this);
                        break;
                    default:
                        showHideProgressDialog(false);
                        util.showMsgAlerta("Erro ao sincronizar o(s) cadastro(s)!\n(" + response.code() + ")", ActivityClienteDrawer.this);
                        break;
                }
            }
            @Override
            public void onFailure(Call<List<Cliente>> call, Throwable t) {
                limpaListaSelecionados();
                showHideProgressDialog(false);
                util.showMsgAlerta("Falha ao sincronizar o(s) cadastro(s)!", ActivityClienteDrawer.this);
            }
        });
    }

    public void downLoadClienteAPI(int id_cadastro_servidor, int position) {
        showHideProgressDialog(true);
        Rotas apiRetrofit = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Call<List<Cliente>> call = apiRetrofit.buscarCliente( UsuarioHelper.getUsuario().getId_usuario(), id_cadastro_servidor, cabecalho );
        call.enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                switch (response.code()) {
                    case 200:
                        List<Cliente> lista1 = response.body();
                        if (lista1 != null && lista1.size() > 0) {
                            db.deleteDados("TBL_CADASTRO", "ID_CADASTRO_SERVIDOR = ?", new String[]{String.valueOf(lista1.get(0).getId_cadastro_servidor())});
                            lista1.get(0).setAlterado("N");
                            clienteDAO.add(lista1.get(0));
                            lista.set(position, lista1.get(0));
                            Toast.makeText(ActivityClienteDrawer.this, "Cadastro atualizado com sucesso!!", Toast.LENGTH_SHORT).show();
                            adapter.notifyDataSetChanged();
                        } else
                            util.showMsgAlerta("Erro ao sincronizar o cadastro!", ActivityClienteDrawer.this);
                        showHideProgressDialog(false);
                        break;
                    case 500:
                        showHideProgressDialog(false);
                        util.showMsgAlerta("Erro ao sincronizar o cadastro!", ActivityClienteDrawer.this);
                        break;
                    default:
                        showHideProgressDialog(false);
                        util.showMsgAlerta("Erro ao sincronizar o cadastro!\n(" + response.code() + ")", ActivityClienteDrawer.this);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Cliente>> call, Throwable t) {
                showHideProgressDialog(false);
                util.showMsgAlerta("Falha ao sincronizar o cadastro!", ActivityClienteDrawer.this);
            }
        });
    }

    public void showMsgSimNao(final int id, int position, String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityClienteDrawer.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityClienteDrawer.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                final Intent intent;
                switch (id) {
                    case R.id.lyDownload:
                    case R.id.btnDownload:
                        if (adapter.getItem(position).getId_cadastro_servidor() > 0) {
                            downLoadClienteAPI( adapter.getItem(position).getId_cadastro_servidor() , position);
                         } else {
                            List<Cliente> lista1 = new ArrayList<>();
                            lista1.add( adapter.getItem(position));
                            addUpdateClienteAPI( lista1 );
                        }
                        break;
                    case R.id.lyChamada:
                    case R.id.btnChamada:
                        showTelefoneApp(position);
                        break;
                    case R.id.lyGps:
                    case R.id.btnGps:
                        showGpsApp(position);
                        break;
                    case R.id.lyEmail:
                    case R.id.btnEmail:
                        startActivity(new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("mailto: " + adapter.getItem(position).getEmail_principal())));
                        break;
                    case R.id.lyFinanceiro:
                    case R.id.btnFinanceiro:
                        showActivityHistoricoFinanceiro(position);
                        break;
                    case R.id.lyNovoPedido:
                    case R.id.btnNovoPedido:
                        showActivityPedido(position);//Inicia um novo pedido
                        break;
                    case R.id.subir_cliente:
                        addUpdateClienteAPI(adapter.getItensSelecionados());
                        break;
                    case R.id.excluir_cliente:
                        deleteCliente();
                        break;
                }
            }
        });
        alertDialog.show();
    }

    private void showTelefoneApp(int position) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        if (adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").length() == 10)
            intent.setData(Uri.parse("tel:" + "0" + adapter.getItem(position).getTelefone_principal()));
        else if (adapter.getItem(position).getTelefone_principal().replaceAll("[^0-9]", "").length() == 11)
            intent.setData(Uri.parse("tel:" + "0" + adapter.getItem(position).getTelefone_principal()));
        else
            intent.setData(Uri.parse("tel:" + adapter.getItem(position).getTelefone_principal()));
        startActivity(intent);
    }

    private void showGpsApp(int position) {
        String endereco = adapter.getItem(position).getEndereco().replace(",", "")
                + ", " + adapter.getItem(position).getEndereco_numero() + " - "
                + adapter.getItem(position).getEndereco_cep();
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + endereco);
        startActivity(new Intent(Intent.ACTION_VIEW, gmmIntentUri).setPackage("com.google.android.apps.maps"));
    }

    private void showActivityHistoricoFinanceiro(int position) {
        ClienteHelper.setCliente(adapter.getItem(position));
        HistoricoFinanceiroHelper.setCliente(ClienteHelper.getCliente());
        System.gc();
        startActivity(new Intent(ActivityClienteDrawer.this, ActivityHistoricoFinanceiro.class));
    }

    private void showActivityPedido(int position) {
        ClienteHelper.setCliente(adapter.getItem(position));
        try {
            if (ClienteHelper.getCliente().getAtivo().equalsIgnoreCase("N")) {
                Toast.makeText(this, "Este cliente esta inativado/bloqueado!", Toast.LENGTH_SHORT).show();
                return;
            }else if (ClienteHelper.getCliente().getIdCategoria() <= 0) {
                    util.showMsgAlerta("Este cliente não tem uma categoria definida", ActivityClienteDrawer.this);
                    return;
            }else if ( configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                float limiteCredito = Float.parseFloat(ClienteHelper.getCliente().getLimite_credito());
                float limitUtilizado = Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencido()) + Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencendo());
                if ( (limiteCredito - limitUtilizado) < 0.00f) {
                    showMsgSimNao( "Cliente sem limite disponivel para compra a prazo! Pedido só podera ser feito para pagamento à vista!\nDeseja continuar?", position, false);
                    return;
                }
            }
        } catch ( NullPointerException e) {
            e.printStackTrace();
        }
        ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
        Pedido2 pedido2 = new Pedido2();
        activityPedidoMain.pegaCliente(ClienteHelper.getCliente());
        pedido2.pegaCliente(ClienteHelper.getCliente());
        startActivity(new Intent(ActivityClienteDrawer.this, ActivityPedidoMain.class));
    }

    private void deleteCliente() {
        ClienteDAO clienteDAO = new ClienteDAO(db);
        for (Cliente cliente : adapter.getItensSelecionados())
            if (clienteDAO.delete(cliente, "0") <= 0) {
                util.showMsgAlerta("Falha ao excluir o cadastro do cliente\n" + cliente.getNome_cadastro() + "\nExclusão cancelada! ", ActivityClienteDrawer.this);
                break;
                //0 = local 1 = servidor
            }
        actionMode.finish();
        adapter.clearSelection();
        setRecyclerView(getListaClientes(2), 2);
        actionMode = null;
        onResume();
    }

    private void updateCliente(List<Cliente> clientesRetorno, List<Cliente> clientesAntigo) {
        ClienteDAO clienteDAO = new ClienteDAO(db);
        WebPedidoDAO webPedidoDAO = new WebPedidoDAO(db);
        CadastroAnexoDAO cadastroAnexoDAO = new CadastroAnexoDAO(db);
        for (Cliente cliente : clientesRetorno) {
            cliente.setAlterado("N");
            if (clienteDAO.update(cliente) > 0)
                for (Cliente clienteAntigo : clientesAntigo)
                    if (clienteAntigo.getCpf_cnpj().equalsIgnoreCase(cliente.getCpf_cnpj())) {
                        webPedidoDAO.updateClientePedido(clienteAntigo.getId_cadastro(), cliente.getId_cadastro_servidor());
                        break;
                    }


            try {
                db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(cliente.getId_cadastro())});
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (cliente.getListaCadastroAnexo().size() > 0) {
                for (CadastroAnexo cadastroAnexo : cliente.getListaCadastroAnexo()) {
                    if (cadastroAnexo.getExcluido().equals("N"))
                        cadastroAnexoDAO.addUpdate(cadastroAnexo);
                    if (cadastroAnexo.getExcluido().equals("S"))
                        try {
                            db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_ANEXO = ?", new String[]{String.valueOf(cadastroAnexo.getIdAnexo())});
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            } else
                try {
                    db.deleteDados("TBL_CADASTRO_ANEXOS", "ID_CADASTRO = ?", new String[]{String.valueOf(cliente.getId_cadastro())});
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }
        try {
            limpaListaSelecionados();
        } catch ( NullPointerException e) {
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        setRecyclerView(getListaClientes(2), 2);
        Toast.makeText(ActivityClienteDrawer.this, "Cadastro(s) sincronizado(s) com sucesso!", Toast.LENGTH_SHORT).show();
        onResume();
    }

    private void limpaListaSelecionados() {
        adapter.clearSelection();
        actionMode.finish();
        actionMode = null;
    }

    private void showHideProgressDialog(boolean visivel) {
        if (!visivel) {
            progress.dismiss();
            return;
        }
        progress = new ProgressDialog(ActivityClienteDrawer.this);
        progress.setMessage("Aguarde...!");
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setRecyclerView(getListaClientes(item.getItemId()), 2);
        drawerLayout.closeDrawers();
        return true;
    }

    public void showMsgSimNao(  String mensagem, int position, boolean pedidoIniciado) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityClienteDrawer.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(ActivityClienteDrawer.this);
        builder.setView(dialogView);
        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                ClienteHelper.setCliente(adapter.getItem(position));
                if (!pedidoIniciado) {
                    ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
                    activityPedidoMain.pegaCliente(adapter.getItem(position));
                    Pedido2 pedido2 = new Pedido2();
                    pedido2.pegaCliente(adapter.getItem(position));
                    startActivity(new Intent(ActivityClienteDrawer.this, ActivityPedidoMain.class));
                } else {
                    ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
                    activityPedidoMain.pegaCliente(adapter.getItem(position));
                    Pedido2 pedido2 = new Pedido2();
                    pedido2.pegaCliente(adapter.getItem(position));
                    System.gc();
                    finish();
                }
            }
        });
        alertDialog.show();
    }

}