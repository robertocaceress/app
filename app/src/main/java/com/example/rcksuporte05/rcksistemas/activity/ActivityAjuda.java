package com.example.rcksuporte05.rcksistemas.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.example.rcksuporte05.rcksistemas.BO.UsuarioBO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.Usuario;

import java.net.URLEncoder;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RCK 03 on 26/10/2017.
 */

public class ActivityAjuda extends AppCompatActivity {

    @BindView(R.id.ajuda_toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_ajuda_configuracao)
    Button btn_ajuda_configuracao;

    //@BindView(R.id.btn_ajuda_tecnico)
    //Button btn_ajuda_tecnico;

    @BindView(R.id.btn_ajuda_preferencia)
    Button btn_ajuda_preferencia;

    @BindView(R.id.btn_ajuda_lista_usuario)
    Button btn_ajuda_lista_usuario;

    @BindView(R.id.btn_ajuda_suporte_whats)
    Button btn_ajuda_suporte_whats;

    @BindView(R.id.btn_ajuda_suporte_telefone)
    Button btn_ajuda_suporte_telefone;

    @BindView(R.id.btn_ajuda_suporte_email)
    Button btn_ajuda_suporte_email;

    @BindView(R.id.txv_ajuda_informacoes)
    TextView txv_ajuda_informacoes;

    @BindView(R.id.btn_ajuda_sobre)
    Button btn_ajuda_informacoes;

    @BindView(R.id.progress_ajuda)
    ProgressBar progress_ajuda;

    @BindView(R.id.progress_title)
    TextView progress_title;

    Context context ;
    DBHelper db = new DBHelper(this);
    PackageInfo pInfo = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_dark_theme", false)) {
            //setTheme(R.style.AppTheme);
        //}
        //getTheme().applyStyle(R.style.Theme_JHDistribuidora, true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajuda);
        ButterKnife.bind(this);
        toolbar.setTitle("Ajuda");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
    }

    @OnClick(R.id.btn_ajuda_sobre)
    public void showActivitySobre(){
        startActivity(new Intent(ActivityAjuda.this, ActivitySobre.class));
    }

    @OnClick(R.id.btn_ajuda_configuracao)
    public void showActivityConfiguracao() {
        startActivity( new Intent( ActivityAjuda.this, ActivityConfiguracao.class));
    }

    @OnClick(R.id.btn_ajuda_preferencia)
    public void showActivityPreferencia() {
        startActivity( new Intent( ActivityAjuda.this, ActivityPreferencia.class));
    }

    @OnClick(R.id.btn_ajuda_lista_usuario)
    public void getListaUsuario() {
        progress_ajuda.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(ActivityAjuda.this, R.color.branco), PorterDuff.Mode.SRC_IN );
        showHideProgressBar(true);
        Rotas apiRotas = Api.buildRetrofit(false);
        Call<List<Usuario>> call = apiRotas.getListaUsuarios();
        execCall(call, false);
    }

    public void getListaUsuario2(Configuracao configuracao) {
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName.substring(0, 5);
        progress_ajuda.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(ActivityAjuda.this, R.color.branco), PorterDuff.Mode.SRC_IN );
        showHideProgressBar(true);
        String url = configuracao.getUrl_ip_2().trim().toString() + ":" + Integer.toString(configuracao.getPorta_2()).trim() + "/rckwhalleAPI" + version + "/ws/";
        Api.context = context;
        Api.url2 = (configuracao.getConexao_segura().isEmpty() || configuracao.getConexao_segura().equalsIgnoreCase("N")) ?
                "http://" + url :
                "https://" + url;
        Rotas apiRotas2 = Api.buildRetrofit2(true);
        Call<List<Usuario>> call = apiRotas2.getListaUsuarios();
        execCall(call, true);
    }

    private void execCall( Call<List<Usuario>> call, boolean isConexao2) {
        call.enqueue(new Callback<List<Usuario>>() {
            @Override
            public void onResponse(Call<List<Usuario>> call, Response<List<Usuario>> response) {
                switch (response.code()) {
                    case 200:
                        showHideProgressBar(false);
                        UsuarioBO usuarioBO = new UsuarioBO();
                        List<Usuario> lista = response.body();
                        if (!usuarioBO.add(lista, ActivityAjuda.this))
                            Toast.makeText(ActivityAjuda.this, "Houve um erro ao atualizar a lista de usúario(s)!" +
                                    "\n(" + String.valueOf(response.code()) + ")", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(ActivityAjuda.this, "Lista de usúario(s) atualizada com sucesso!", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        showHideProgressBar(false);
                        Toast.makeText(ActivityAjuda.this, "Houve um erro ao atualizar a lista de usúario(s)!!" +
                                "\n(" + String.valueOf(response.code()) + ")", Toast.LENGTH_LONG).show();
                        break;
                }
            }
            @Override
            public void onFailure(Call<List<Usuario>> call, Throwable t) {
                showHideProgressBar(false);
                if ( !isConexao2) {
                    try {
                        Configuracao configuracao = new ConfiguracaoDAO(db).getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
                        if (!configuracao.getId().isEmpty() && !configuracao.getUrl_ip_2().isEmpty()) {
                            getListaUsuario2(configuracao);
                        } else {
                            Toast.makeText(ActivityAjuda.this, "Não foi possivel sincronizar com o servidor, por favor verifique sua conexão!" +
                                    "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } catch (NullPointerException e) {
                        Toast.makeText(ActivityAjuda.this, "Não foi possivel sincronizar com o servidor, por favor verifique sua conexão!!" +
                                "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(ActivityAjuda.this, "Não foi possivel sincronizar com o servidor, por favor verifique sua conexão!!!" +
                            "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void showHideProgressBar(boolean visivel) {
        progress_ajuda.setVisibility( visivel ? View.VISIBLE : View.INVISIBLE);
        progress_title.setVisibility(visivel ? View.VISIBLE : View.INVISIBLE);
    }

    @OnClick(R.id.btn_ajuda_suporte_whats)
    public void showWhatsApp() {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            String url = "https://api.whatsapp.com/send?phone=5567993235558&text=" + URLEncoder.encode("Ola! Tudo bem? ", "UTF-8");
            intent.setPackage("com.whatsapp").setData(Uri.parse(url));
            if (intent.resolveActivity(packageManager) != null)
                context.startActivity(intent);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_ajuda_suporte_telefone)
    public void showTelefoneApp() {
        startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:06733833878")));
    }

    @OnClick(R.id.btn_ajuda_suporte_email)
    public void showEmailApp() {
        startActivity(new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("mailto: rcksistemassuporte@rcksistemas.com.br")));
    }
}

