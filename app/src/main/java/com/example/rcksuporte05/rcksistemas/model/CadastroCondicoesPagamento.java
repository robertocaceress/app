package com.example.rcksuporte05.rcksistemas.model;

public class CadastroCondicoesPagamento {
    private String id_condicao;
    private String id_cadastro;

    public String getId_condicao() {
        return id_condicao;
    }

    public void setId_condicao(String id_condicao) {
        this.id_condicao = id_condicao;
    }

    public String getId_cadastro() {
        return id_cadastro;
    }

    public void setId_cadastro(String id_cadastro) {
        this.id_cadastro = id_cadastro;
    }
}
