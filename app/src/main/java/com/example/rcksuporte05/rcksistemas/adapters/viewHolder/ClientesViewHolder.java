package com.example.rcksuporte05.rcksistemas.adapters.viewHolder;


import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RCK 03 on 29/11/2017.
 */

public class ClientesViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.nomeListaCliente)
    public TextView textViewNome;

    @BindView(R.id.txtDataUltimaCompra)
    public TextView txtDataUltimaCompra;

    @BindView(R.id.textViewNomeFantasia)
    public TextView textViewNomeFantasia;

    @BindView(R.id.telefoneCliente)
    public TextView telefoneCliente;

    @BindView(R.id.txtClienteAguarda)
    public TextView txtClienteAguarda;
    @BindView(R.id.idCliente)
    public TextView idCliente;
    @BindView(R.id.nomeTabelaPreco)
    public TextView nomeTabelaPreco;

    @BindView(R.id.txtCategoria)
    public TextView txtCategoria;

    @BindView(R.id.txtSegmento)
    public TextView txtSegmento;
    //Setor
    @BindView(R.id.imStatus)
    public ImageView imStatus;

    @BindView(R.id.imgFinanceiroVencendo)
    public ImageView imgFinanceiroVencendo;

    @BindView(R.id.imgFinanceiroVencido)
    public ImageView imgFinanceiroVencido;

    @BindView(R.id.imgPedidoAnalise)
    public ImageView imgPedidoAnalise;

    @BindView(R.id.imgPromocaoCliente)
    public ImageView imgPromocaoCliente;

    @BindView(R.id.imgClienteBloqueado)
    public ImageView imgClienteBloqueado;

    @BindView(R.id.lyCliente)
    public RelativeLayout itemView;


    @BindView(R.id.botoes)
    public LinearLayout lyBotoes;

    @BindView(R.id.lyDownload)
    public LinearLayout lyDownload;

    @BindView(R.id.lyChamada)
    public LinearLayout lyChamada;

    @BindView(R.id.lyGps)
    public LinearLayout lyGps;

    @BindView(R.id.lyEmail)
    public LinearLayout lyEmail;

    @BindView(R.id.lyFinanceiro)
    public LinearLayout lyFinanceiro;

    @BindView(R.id.lyNovoPedido)
    public LinearLayout lyNovoPedido;

    @BindView(R.id.btnDownload)
    public Button btnDownload;

    @BindView(R.id.btnChamada)
    public Button btnChamada;

    @BindView(R.id.btnGps)
    public Button btnGps;

    @BindView(R.id.btnEmail)
    public Button btnEmail;

    @BindView(R.id.btnFinanceiro)
    public Button btnFinanceiro;

    @BindView(R.id.btnNovoPedido)
    public Button btnNovoPedido;

    public ClientesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
