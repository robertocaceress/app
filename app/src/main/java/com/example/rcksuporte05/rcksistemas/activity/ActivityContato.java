package com.example.rcksuporte05.rcksistemas.activity;

import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.rcksuporte05.rcksistemas.DAO.CadastroFinanceiroResumoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.CategoriaDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.PromocaoDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.HistoricoFinanceiroHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.fragment.Pedido2;
import com.example.rcksuporte05.rcksistemas.model.CadastroFinanceiroResumo;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.Promocao;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;
import com.example.rcksuporte05.rcksistemas.util.Utilitaria;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityContato extends AppCompatActivity {

    @BindView(R.id.txtCategoria)
    TextView txtCategoria;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imFisicaJuridica)
    ImageView imFisicaJuridica;
    @BindView(R.id.txtRazaoSocial)
    TextView txtRazaoSocial;
    @BindView(R.id.txtNomeFantasia)
    TextView txtNomeFantasia;
    @BindView(R.id.txtTelefone)
    TextView txtTelefone;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtEndereco)
    TextView txtEndereco;
    @BindView(R.id.lyDetalhes)
    RelativeLayout lyDetalhes;
    @BindView(R.id.lyChamada)
    RelativeLayout lyChamada;
    @BindView(R.id.lyEmail)
    RelativeLayout lyEmail;
    @BindView(R.id.lyGps)
    RelativeLayout lyGps;
    @BindView(R.id.lyNomeCliente)
    RelativeLayout lyNomeCliente;
    @BindView(R.id.lyFinanceiro)
    RelativeLayout lyFinanceiro;

    @BindView(R.id.txtLimiteCredito)
    TextView txtLimiteCredito;
    @BindView(R.id.txtLimiteUtilizado)
    TextView txtLimiteUtilizado;
    @BindView(R.id.txtLimiteDisponivel)
    TextView txtLimiteDisponivel;
    @BindView(R.id.txtPendenciaFinanceira)
    TextView txtPendenciaFinanceira;
    @BindView(R.id.txtPedidoPendente)
    TextView txtPedidoPendente;
    @BindView(R.id.txtPedidoAnalise)
    TextView txtPedidoAnalise;



    @BindView(R.id.edtDataCadastro)
    TextView edtDataCadastro;


    @BindView(R.id.txtPromocaoCliente)
    TextView txtPromocaoCliente;
    @BindView(R.id.txtNomeVendedor)
    TextView txtNomeVendedor;

    private Cliente cliente;
    private CadastroFinanceiroResumo cadastroFinanceiroResumo;
    Utilitaria util = new Utilitaria();
    private DBHelper db = new DBHelper(this);
    CategoriaDAO categoriaDAO = new CategoriaDAO(db);
    ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
    @OnClick(R.id.lyNomeVendedor)
    public void showActivityContatoVendedor() {
        startActivity(new Intent(ActivityContato.this, ActivityContatoVendedor.class));
    }

    @OnClick(R.id.novoPedido)
    public void novoPedido() {
        Configuracao configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        try {
            if (ClienteHelper.getCliente().getIdCategoria() <= 0) {
                Toast.makeText(this, "Este cliente não tem uma categoria definida", Toast.LENGTH_SHORT).show();
                return;
            } else if (ClienteHelper.getCliente().getAtivo().equalsIgnoreCase("N")) {
                Toast.makeText(this, "Este cliente esta inátivo/bloqueado!", Toast.LENGTH_SHORT).show();
                return;
            } else if (configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                if (Float.parseFloat(cliente.getLimite_credito()) - (Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo())) < 0.0f) {
                    showMsgSimNao(R.id.novoPedido, "Cliente sem limite disponivel para compra a prazo! Pedido só podera ser feito para pagamento à vista!\nDeseja continuar?");
                    return;
                }
            }
        } catch (NullPointerException | NumberFormatException e) {

        }
        ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
        Pedido2 pedido2 = new Pedido2();
        activityPedidoMain.pegaCliente(ClienteHelper.getCliente());
        pedido2.pegaCliente(ClienteHelper.getCliente());
        startActivity(new Intent(ActivityContato.this, ActivityPedidoMain.class));
    }

    @OnClick(R.id.btnNovoPedido)
    public void btnNovoPedido() {
        novoPedido();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contato);
        ButterKnife.bind(this);
        DBHelper db = new DBHelper(this);
        showDados();
        lyDetalhes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //visualiza os dados
                startActivity(new Intent(ActivityContato.this, CadastroClienteMain.class).putExtra("vizualizacao", 1));
            }
        });

        lyChamada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fazerChamada(txtTelefone.getText().toString(), cliente.getNome_cadastro());
            }
        });

        lyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtEmail.getText().toString().equals("Nenhum email informado!"))
                    util.showMsgAlerta("Nenhum e-mail informado!", ActivityContato.this);
                else
                    showMsgSimNao(v.getId(), "Deseja enviar um email a " + cliente.getNome_cadastro() + "?");
            }
        });

        lyGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtEndereco.getText().toString().equals("Nenhum endereço informado!"))
                    util.showMsgAlerta("Nenhum endereço informado!", ActivityContato.this);
                else
                    showMsgSimNao(v.getId(), "Deseja navegar para a localização de " + cliente.getNome_cadastro() + " no GPS?");
            }
        });

        lyNomeCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityContato.this, CadastroClienteMain.class).putExtra("vizualizacao", 1));
            }
        });

        lyFinanceiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    HistoricoFinanceiroHelper.setCliente(ClienteHelper.getCliente());
                    System.gc();
                    startActivity(new Intent(ActivityContato.this, ActivityHistoricoFinanceiro.class));
                } catch (CursorIndexOutOfBoundsException e) {
                    Toast.makeText(ActivityContato.this, "Financeiro não encontrado, por favor faça a sincronia e tente novamente!", Toast.LENGTH_LONG).show();
                }

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                System.gc();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDados() {
        try {
            cliente = ClienteHelper.getCliente();
            try {
                txtCategoria.setText(categoriaDAO.getCategoria(cliente.getIdCategoria()).getNomeCategoria());
            } catch (NullPointerException e) {
                txtCategoria.setText("Este cliente não tem categoria definida!");
            }
            toolbar.setTitle("Contato");
            txtRazaoSocial.setText(cliente.getNome_cadastro());

            txtNomeFantasia.setText(cliente.getNome_fantasia());

            try {
                txtNomeVendedor.setText(db.consultaDados("SELECT NOME_CADASTRO FROM TBL_CADASTRO WHERE F_ID_VENDEDOR = " + cliente.getId_vendedor(), "NOME_CADASTRO"));
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
                txtNomeVendedor.setText(String.valueOf(cliente.getId_vendedor()));
            }

            if (cliente.getPessoa_f_j() != null)
                switch (cliente.getPessoa_f_j()) {
                    case "F":
                        imFisicaJuridica.setImageResource(R.mipmap.ic_pessoa_fisica);
                        break;
                    case "J":
                        imFisicaJuridica.setImageResource(R.mipmap.ic_pessoa_juridica);
                        break;
                    default:
                        imFisicaJuridica.setImageResource(R.mipmap.ic_pessoa_duvida);
                        break;
                }
            else
                imFisicaJuridica.setImageResource(R.mipmap.ic_pessoa_duvida);

            if (cliente.getTelefone_principal() != null && !cliente.getTelefone_principal().replaceAll("[^0-9]", "").trim().isEmpty() && cliente.getTelefone_principal().replaceAll("[^0-9]", "").length() >= 8 && cliente.getTelefone_principal().replaceAll("[^0-9]", "").length() <= 11)
                txtTelefone.setText(formataTelefone(cliente.getTelefone_principal()));
            else if (cliente.getTelefone_dois() != null && !cliente.getTelefone_dois().replaceAll("[^0-9]", "").trim().isEmpty() && cliente.getTelefone_dois().replaceAll("[^0-9]", "").length() >= 8 && cliente.getTelefone_dois().replaceAll("[^0-9]", "").length() <= 11)
                txtTelefone.setText(formataTelefone(cliente.getTelefone_dois()));
            else if (cliente.getTelefone_tres() != null && !cliente.getTelefone_tres().replaceAll("[^0-9]", "").trim().isEmpty() && cliente.getTelefone_tres().replaceAll("[^0-9]", "").length() >= 8 && cliente.getTelefone_tres().replaceAll("[^0-9]", "").length() <= 11)
                txtTelefone.setText(formataTelefone(cliente.getTelefone_tres()));
            else
                txtTelefone.setText("Nenhum telefone válido informado!");

            if (cliente.getEmail_principal() != null && !cliente.getEmail_principal().trim().equals(""))
                txtEmail.setText(cliente.getEmail_principal());
            else if (cliente.getEmail_financeiro() != null && !cliente.getEmail_financeiro().trim().equals(""))
                txtEmail.setText(cliente.getEmail_financeiro());
            else
                txtEmail.setText("Nenhum email informado!");

            String endereco;
            if (cliente.getEndereco() != null && !cliente.getEndereco().trim().equals("")) {
                endereco = cliente.getEndereco().replace(",", "") + ", ";
                if (cliente.getEndereco_numero() != null && !cliente.getEndereco_numero().isEmpty())
                    endereco += cliente.getEndereco_numero();
                if ( cliente.getEndereco_complemento() != null && !cliente.getEndereco_complemento().isEmpty())
                    endereco += " - " + cliente.getEndereco_complemento();
                if ( cliente.getEndereco_bairro() != null && !cliente.getEndereco_bairro().isEmpty())
                    endereco += " - " + cliente.getEndereco_bairro();

                try {
                    if (cliente.getEndereco_cep() != null && !cliente.getEndereco_cep().isEmpty()) {
                        endereco +=  MascaraUtil.formataCep(cliente.getEndereco_cep() + " ");
                    }
                } catch ( NullPointerException e) {

                }

                if ( cliente.getNome_municipio() != null && !cliente.getNome_municipio().isEmpty())
                    endereco += " - " + cliente.getNome_municipio();
                if (cliente.getEndereco_uf() != null && !cliente.getEndereco_uf().isEmpty())
                    endereco += " - " + cliente.getEndereco_uf();

            }else if (cliente.getCob_endereco() != null && !cliente.getCob_endereco().trim().equals(""))
                endereco = cliente.getCob_endereco().replace(",", "") + ", " + cliente.getCob_endereco_numero() + " - CEP" + cliente.getCob_endereco_cep();
            else
                endereco = "Nenhum endereço informado!";

            txtEndereco.setText(endereco.replace(", null", ""));

            if (cliente.getLimite_credito() != null && !cliente.getLimite_credito().trim().isEmpty()) {
                txtLimiteCredito.setText("(+) " + MascaraUtil.mascaraReal(cliente.getLimite_credito()));
                txtLimiteDisponivel.setText( "(=) " + MascaraUtil.mascaraReal(Float.parseFloat(cliente.getLimite_credito()) - (Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo())) ));
                txtLimiteUtilizado.setText("(-) " + MascaraUtil.mascaraReal( Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo())));
                if ( Float.parseFloat(cliente.getLimite_credito()) - (Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo())) < 0.00f)
                        txtLimiteDisponivel.setTextColor(Color.RED);
            }

            try {
                String dataCadastro = new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(cliente.getUsuario_data()));
                edtDataCadastro.setText(dataCadastro);
            } catch (Exception e) {
                e.printStackTrace();
            }
            txtPedidoAnalise.setText("(-) " + MascaraUtil.mascaraReal(cliente.getPedido_analise()));
            txtPendenciaFinanceira.setText("(-) " + MascaraUtil.mascaraReal( Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo())) );
            if ( Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo()) > 0.0f)
                txtPendenciaFinanceira.setTextColor(Color.RED);
            try {
                if (Integer.parseInt(cliente.getId_promocao()) > 0) {
                    PromocaoDAO promocaoDAO = new PromocaoDAO(db);
                        Promocao promocao = promocaoDAO.getPromocao( cliente.getId_promocao());
                        if ( promocao != null)
                            txtPromocaoCliente.setText("(" +MascaraUtil.duasCasaDecimal(promocao.getDescontoPerc()) + "%) " + promocao.getNomePromocao().toUpperCase());
                        else
                            txtPromocaoCliente.setText("");
                }else{
                    txtPromocaoCliente.setText("");
                }
            } catch ( NullPointerException|NumberFormatException e) {
            } catch ( Exception e ) {
            }
            sincronizaFinanceiroAPI(cliente.getId_cadastro_servidor());
            sincronizaPendenciaAPI(cliente.getId_cadastro_servidor());
        } catch (CursorIndexOutOfBoundsException | NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(ActivityContato.this, "Erro ao carregar Cliente", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public String formataTelefone(String telefone) {
        telefone = telefone.trim().replaceAll("[^0-9]", "");
        switch (telefone.length()) {
            case 10:
                return "(" + telefone.substring(0, 2) + ") " + telefone.substring(2, 6) + "-" + telefone.substring(6, 10);
            case 11:
                return "(" + telefone.substring(0, 2) + ") " + telefone.substring(2, 7) + "-" + telefone.substring(7, 11);
            case 9:
                return telefone.substring(0, 5) + "-" + telefone.substring(5, 9);
            case 8:
                return telefone.substring(0, 4) + "-" + telefone.substring(4, 8);
            default:
                return telefone;
        }
    }

    public void fazerChamada(final String telefone, final String nome) {
        try {
            if (!telefone.replaceAll("[^0-9]", "").trim().isEmpty())
                if (telefone.replaceAll("[^0-9]", "").length() >= 8 && telefone.replaceAll("[^0-9]", "").length() <= 11) {
                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ActivityContato.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityContato.this);
                    builder.setView(dialogView);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.setCancelable(false);

                    TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
                    textView.setText("Deseja ligar para " + nome + " usando o número " + telefone + " ?");

                    Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
                    btnNao.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();

                        }
                    });
                    Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
                    btnSim.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(Intent.ACTION_DIAL);
                            if (telefone.replaceAll("[^0-9]", "").length() == 10)
                                intent.setData(Uri.parse("tel:" + "0" + telefone));
                            else if (telefone.replaceAll("[^0-9]", "").length() == 11)
                                intent.setData(Uri.parse("tel:" + "0" + telefone));
                            else
                                intent.setData(Uri.parse("tel:" + telefone));
                            alertDialog.dismiss();
                            startActivity(intent);
                        }
                    });
                    alertDialog.show();
                } else
                    util.showMsgAlerta("Este numero de Telefone não é valido!", ActivityContato.this);
            else
                util.showMsgAlerta("Nenhum numero de telefone informado!", ActivityContato.this);

        } catch (Exception e) {
            //System.out.println(e.getMessage());
        }
    }



    @Override
    protected void onDestroy() {
        ClienteHelper.clear();
        super.onDestroy();
    }

    public void showMsgSimNao( final int id , String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityContato.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityContato.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if ( id == R.id.lyEmail)
                    startActivity(new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("mailto: " + txtEmail.getText().toString())));
                else if ( id == R.id.lyGps)
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + txtEndereco.getText().toString())).setPackage("com.google.android.apps.maps"));
                else if ( id == R.id.novoPedido) {
                    ActivityPedidoMain activityPedidoMain = new ActivityPedidoMain();
                    Pedido2 pedido2 = new Pedido2();
                    activityPedidoMain.pegaCliente(ClienteHelper.getCliente());
                    pedido2.pegaCliente(ClienteHelper.getCliente());
                    startActivity(new Intent(ActivityContato.this, ActivityPedidoMain.class));
                }
            }
        });
        alertDialog.show();
    }

    public void sincronizaFinanceiroAPI(int idCadastroServidor) {

        Rotas apiRotas = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Call<CadastroFinanceiroResumo> call = apiRotas.sincronizaFinanceiro(idCadastroServidor, cabecalho);
        call.enqueue(new Callback<CadastroFinanceiroResumo>() {
            @Override
            public void onResponse(Call<CadastroFinanceiroResumo> call, Response<CadastroFinanceiroResumo> response) {
                CadastroFinanceiroResumoDAO cadastroFinanceiroResumoDAO = new CadastroFinanceiroResumoDAO(db);
                cadastroFinanceiroResumoDAO.addUpdate(response.body());
                HistoricoFinanceiroHelper.setCadastroFinanceiroResumo(response.body());
                atualizaDados();
            }

            @Override
            public void onFailure(Call<CadastroFinanceiroResumo> call, Throwable t) {
            }
        });
    }

    public void atualizaDados() {
        if (HistoricoFinanceiroHelper.getCadastroFinanceiroResumo().getLimiteCredito() != null && HistoricoFinanceiroHelper.getCadastroFinanceiroResumo().getLimiteCredito() > 0)
            txtLimiteCredito.setText("(+)" + MascaraUtil.mascaraReal(HistoricoFinanceiroHelper.getCadastroFinanceiroResumo().getLimiteCredito()));
        else
            txtLimiteCredito.setText("(+)" + MascaraUtil.mascaraReal(0.f));
        String sql = "SELECT SUM(VALOR_TOTAL) FROM TBL_WEB_PEDIDO " +
                "WHERE PEDIDO_ENVIADO = 'N' AND ID_CONDICAO_PAGAMENTO <> 1 " +
                "AND ID_WEB_PEDIDO <> " + PedidoHelper.getIdPedido() + " " +
                "AND ID_CADASTRO = " + ClienteHelper.getCliente().getId_cadastro_servidor() + ";";

        Float limiteUltilizado = db.soma(sql) + cadastroFinanceiroResumo.getLimiteUtilizado();
        Float saldoRestante = cadastroFinanceiroResumo.getLimiteCredito() - limiteUltilizado - getIntent().getFloatExtra("valorPedido", 0) - cadastroFinanceiroResumo.getFinanceiroVencido();
        txtLimiteDisponivel.setText("(=)" + MascaraUtil.mascaraReal(saldoRestante));
        txtLimiteUtilizado.setText( limiteUltilizado != null && limiteUltilizado > 0 ? "(-)" + MascaraUtil.mascaraReal(limiteUltilizado) : "(-)" + MascaraUtil.mascaraReal(0.f));
        txtLimiteDisponivel.setTextColor( saldoRestante < 0 || cadastroFinanceiroResumo.getFinanceiroVencido() > 0 ? Color.RED: Color.parseColor("#0277BD") );
    }

    public void sincronizaPendenciaAPI(int idCadastroServidor) {
        Float total = 0.0f;
        Float pedidPendente = db.soma("SELECT SUM(VALOR_TOTAL) AS VALOR_TOTAL FROM TBL_WEB_PEDIDO WHERE PEDIDO_ENVIADO = 'N' AND FINALIZADO = 'S' AND EXCLUIDO = 'N' AND ID_CADASTRO = " + cliente.getId_cadastro_servidor() + ";");// + cadastroFinanceiroResumo.getLimiteUtilizado();
        try {
            if (pedidPendente > 0.0f)
                total += pedidPendente;
            txtPedidoPendente.setText("(-) " + MascaraUtil.mascaraReal(pedidPendente));
        } catch ( NullPointerException|NumberFormatException e) {

        }catch ( Exception e) {

        }
        Rotas apiRotas = Api.buildRetrofit(false);
        Map<String, String> cabecalho = new HashMap<>();
        cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
        Call<List<CadastroFinanceiroResumo>> call = apiRotas.getFinanceiroPendente(idCadastroServidor, cabecalho);
        Float finalTotal = total;
        call.enqueue(new Callback<List<CadastroFinanceiroResumo>>() {
            @Override
            public void onResponse(Call<List<CadastroFinanceiroResumo>> call, Response<List<CadastroFinanceiroResumo>> response) {
                switch (response.code()) {
                    case 200:
                        try {
                            List<CadastroFinanceiroResumo> lista = response.body();
                            if (lista.size() > 0) {
                                if (cliente.getLimite_credito() != null && !cliente.getLimite_credito().trim().isEmpty()) {
                                    txtLimiteUtilizado.setText("(-) " + MascaraUtil.mascaraReal((lista.get(0).getLimiteUtilizado() )));
                                    txtLimiteDisponivel.setText("(=) " +  MascaraUtil.mascaraReal(Float.parseFloat(cliente.getLimite_credito()) - (lista.get(0).getLimiteUtilizado() /*+ (finalTotal + lista.get(0).getPedidosAnalise())*/ ) ));
                                    //txtPedidoAnalise.setText("(-) " + MascaraUtil.mascaraReal(lista.get(0).getPedidosAnalise()));
                                    if ( ( Float.parseFloat(cliente.getLimite_credito()) - (lista.get(0).getLimiteUtilizado() /*+ (finalTotal + lista.get(0).getPedidosAnalise())*/ ) ) < 0.00f )
                                        txtLimiteDisponivel.setTextColor(Color.RED);
                                }
                                txtPendenciaFinanceira.setText("(-) " + MascaraUtil.mascaraReal(lista.get(0).getFinanceiroPendente()));
                                if ( lista.get(0).getFinanceiroPendente() > 0.0f)
                                    txtPendenciaFinanceira.setTextColor(Color.RED);
                                txtPedidoAnalise.setText("(-) " + MascaraUtil.mascaraReal(lista.get(0).getPedidosAnalise()));
                            }
                        } catch ( NullPointerException | CursorIndexOutOfBoundsException e) {
                        } catch ( Exception e){
                        }
                        break;
                    default:
                        break;

                }
            }

            @Override
            public void onFailure(Call<List<CadastroFinanceiroResumo>> call, Throwable t) {
                //progress.dismiss();
                //util.showMsgAlerta("Não foi possivel atualizar o financeiro deste cliente!\n        Verifique sua conexão com a internet/servidor!", ContatoActivity.this);
            }
        });
    }

}
