package com.example.rcksuporte05.rcksistemas.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.BO.SincroniaBO;
import com.example.rcksuporte05.rcksistemas.BO.UsuarioBO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.EmpresaParametroDAO;
import com.example.rcksuporte05.rcksistemas.DAO.UsuarioDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.EmpresaParametro;
import com.example.rcksuporte05.rcksistemas.model.Sincronia;
import com.example.rcksuporte05.rcksistemas.model.Usuario;
import com.google.android.material.navigation.NavigationView;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    @BindView(R.id.btnCliente)
    Button btnCliente;

    @BindView(R.id.btnProduto)
    Button btnProduto;

    @BindView(R.id.btnPedido)
    Button btnPedidos;

    @BindView(R.id.btnSincroniza)
    Button btnSincroniza;

    @BindView(R.id.btnPedidoFinalizado)
    Button btnPedidoFinalizado;

    @BindView(R.id.btnPedidoPendente)
    Button btnPedidoPendente;

    @BindView(R.id.btnProspectNovo)
    Button btnProspectNovo;

    @BindView(R.id.btnCampanha)
    Button btnCampanha;

    @BindView(R.id.txvProspectNovo)
    TextView txvProspectNovo;

    @BindView(R.id.ivInternet)
    ImageView ivInternet;

    //@BindView(R.id.edtDataSincronia)
    static TextView edtDataSincronia;

    private int aperta = 0;
    private SincroniaBO sincroniaBO = new SincroniaBO();
    private Sincronia sincronia;
    private List<Usuario> usuarioList = new ArrayList<>();
    private UsuarioBO usuarioBO = new UsuarioBO();
    private DBHelper db = new DBHelper(this);
    private ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
    private EmpresaParametroDAO empresaParametroDAO = new EmpresaParametroDAO(db);
    private Configuracao configuracao;
    private Uri uri;
    public Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Context context;
    private static  Context scontext;
    PackageInfo pInfo = null;
    public static boolean sincronizando = false;
    private Intent itSincronia = null;

    //ImageView img_avalia_1, img_avalia_2, img_avalia_3 , img_avalia_4 , img_avalia_5 ;
    //ImageView img_whatss, img_telefone, img_email, img_tecnico, img_sobre;
    @Override
    protected void onRestart() {
        try {
            if (configuracao.getSincronia_automatica_app().equalsIgnoreCase("S"))
                verificaDataHoraSincronia();
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
        super.onRestart();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_drawer);
        ButterKnife.bind(this);
        configuracao = new Configuracao();
        configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        try {
            if (configuracao.getTrabalha_com_cobranca().equalsIgnoreCase("S")) {
                btnProspectNovo.setBackgroundResource(R.mipmap.ic_conta_receber);
                btnProspectNovo.refreshDrawableState();
                btnProspectNovo.refreshDrawableState();
            }else {
                btnProspectNovo.setBackgroundResource(R.mipmap.ic_add_prospect);
                txvProspectNovo.setVisibility(View.INVISIBLE);
            }
        } catch (NullPointerException e) {
            btnProspectNovo.setBackgroundResource(R.mipmap.ic_add_prospect);
            txvProspectNovo.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            btnProspectNovo.setBackgroundResource(R.mipmap.ic_add_prospect);
            txvProspectNovo.setVisibility(View.INVISIBLE);
        }

        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        scontext = this;
        edtDataSincronia = (TextView) findViewById(R.id.edtDataSincronia);
        try {
            edtDataSincronia.setText(new SimpleDateFormat("dd/MM/yy").format(new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(db.consultaDados("SELECT DATA_SINCRONIA FROM TBL_LOGIN", "DATA_SINCRONIA"))));
            edtDataSincronia.setText(edtDataSincronia.getText().toString() + "\n " + new SimpleDateFormat("HH:mm:ss").format(new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(db.consultaDados("SELECT DATA_SINCRONIA FROM TBL_LOGIN", "DATA_SINCRONIA"))));
        } catch (ParseException| NullPointerException e) {
            edtDataSincronia.setText("00/00/00" + "\n " + "00:00:00");
            e.printStackTrace();
        }
        btnPedidos.setOnClickListener(this);
        btnProduto.setOnClickListener(this);
        btnCliente.setOnClickListener(this);
        btnSincroniza.setOnClickListener(this);
        btnPedidoPendente.setOnClickListener(this);
        btnPedidoFinalizado.setOnClickListener(this);
        btnProspectNovo.setOnClickListener(this);
        btnCampanha.setOnClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        navigationView.setItemTextColor(ColorStateList.valueOf(Color.WHITE));
        //navigationView.getMenu().clear();
        //navigationView.inflateMenu(R.menu.nav_menu);
        try {
            if (configuracao.getTrabalha_com_cobranca().equalsIgnoreCase("N"))
                navigationView.getMenu().findItem(R.id.nav_principal_cliente).setVisible(false);
        } catch ( NullPointerException e) {
            navigationView.getMenu().findItem(R.id.nav_principal_cliente).setVisible(false);
        }
        try {
            if (UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("V")) {
                navigationView.getMenu().findItem(R.id.nav_principal_tecnico).setVisible(false);
                navigationView.getMenu().findItem(R.id.nav_principal_financeiro).setVisible(false);
                navigationView.getMenu().findItem(R.id.nav_principal_estoque).setVisible(false);

            }
        } catch ( NullPointerException e) {
            navigationView.getMenu().findItem(R.id.nav_principal_tecnico).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_principal_financeiro).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_principal_estoque).setVisible(false);

        }

        try {
            if (configuracao.getSincronia_automatica_app().equalsIgnoreCase("S"))
                verificaDataHoraSincronia();
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
        /*
        img_whatss = findViewById(R.id.img_whatss);
        img_telefone = findViewById(R.id.img_telefone);
        img_email = findViewById(R.id.img_email);
        img_tecnico = findViewById(R.id.img_tecnico);
        img_sobre = findViewById(R.id.img_sobre);

        img_avalia_1 = findViewById(R.id.img_avalia_1);
        img_avalia_2 = findViewById(R.id.img_avalia_2);
        img_avalia_3 = findViewById(R.id.img_avalia_3);
        img_avalia_4 = findViewById(R.id.img_avalia_4);
        img_avalia_5 = findViewById(R.id.img_avalia_5);

        img_whatss.setOnClickListener(this);
        img_telefone.setOnClickListener(this);
        img_email.setOnClickListener(this);
        img_tecnico.setOnClickListener(this);
        img_sobre.setOnClickListener(this);

        img_avalia_1.setOnClickListener(this);
        img_avalia_2.setOnClickListener(this);
        img_avalia_3.setOnClickListener(this);
        img_avalia_4.setOnClickListener(this);
        img_avalia_5.setOnClickListener(this);
        try {
            setAvalicaoApp(UsuarioHelper.getUsuario().getUsuario_avaliacao_app(), false);
        } catch ( NullPointerException e) {
            setAvalicaoApp(0, false);
        } catch ( Exception e) {

        }

         */
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, importance);
            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);
        }
        */


        //Displaying a notification locally
        //MyNotificationManager.getInstance(getApplicationContext()).displayNotification("Hi","Where are you?");


        //MyFirebaseInstanceIdService myFirebaseInstanceIdService = new  MyFirebaseInstanceIdService();
        //myFirebaseInstanceIdService.novoToken(UsuarioHelper.getUsuario());

    }


    public static void atualizaDataSincronia(String dataSincronia) {
            edtDataSincronia.setText(dataSincronia);
    }
    public static  void updateApp(String url){
        try {
            if (!url.isEmpty())
                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                scontext.startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }catch ( NullPointerException e ) {
            Toast.makeText(scontext, "A url/link para update do aplicativo é inválida!", Toast.LENGTH_SHORT).show();
        } catch ( ActivityNotFoundException e) {
            Toast.makeText(scontext, "A url/link para update do aplicativo é inválida!!", Toast.LENGTH_SHORT).show();
        } catch ( Exception e) {
            Toast.makeText(scontext, "A url/link para update do aplicativo é inválida!!!", Toast.LENGTH_SHORT).show();
        }
    }
    public  void verificaDataHoraSincronia() {
        String dataSincronia = "";
        String tempoSincronia = "0";

        try {
            dataSincronia = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(db.consultaDados("SELECT DATA_SINCRONIA FROM TBL_LOGIN", "DATA_SINCRONIA")));
            tempoSincronia = db.consultaDados("SELECT TEMPO_AUTO_SINCRONIA FROM TBL_EMPRESA_PARAMETRO", "TEMPO_AUTO_SINCRONIA");
            if ( tempoSincronia.isEmpty() )
                tempoSincronia = "0";
        } catch (ParseException|NullPointerException|CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        if (!dataSincronia.equals(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))) {
            if ( !getSincronizando()) {
                setSincronizando();
                executaSincronia();
            }

        }else
            try {
                Date dataSinc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(db.consultaDados("SELECT DATA_SINCRONIA FROM TBL_LOGIN", "DATA_SINCRONIA"));
                Date dataAtual = new Date();
                long difference = dataAtual.getTime() - dataSinc.getTime();
                int days = (int) (difference / (1000 * 60 * 60 * 24));
                int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                if ( tempoSincronia.equalsIgnoreCase("null")) {
                    if ( !getSincronizando()) {
                        setSincronizando();
                        executaSincronia();
                    }
                }else {
                    if (!tempoSincronia.equalsIgnoreCase("0")) {
                        if (hours >= Integer.parseInt(tempoSincronia.trim())) {
                            if ( !getSincronizando()) {
                                setSincronizando();
                                executaSincronia();
                            }
                        }
                    }
                }
            } catch (ParseException|NullPointerException|NumberFormatException e) {
            } catch ( Exception e) {
            }
        }

    public static boolean getSincronizando(){
        return sincronizando;
    }
    public static  boolean setSincronizando() {
        sincronizando = !sincronizando;
        return sincronizando;
    }
    public  void executaSincronia() {
        Toast.makeText(ActivityPrincipal.this, "A sincronização automatica de dados sera iniciada! Favor aguarde a finalização!", Toast.LENGTH_SHORT).show();
        //Intent intent = new Intent(ActivityPrincipalDrawer.this, ActivityDialogSincronia.class);
        //intent.putExtra("autoSincroniza", true);
        SincroniaBO.setActivity(ActivityPrincipal.this);
        startActivity(new Intent(ActivityPrincipal.this, ActivityDialogSincronia.class).putExtra("autoSincroniza", true));
    }

    public void setTituloToolBar(){
        toolbar.setTitle(getString(R.string.app_name));
        try {
            EmpresaParametro empresaParametro = empresaParametroDAO.getLista("SELECT * FROM TBL_EMPRESA_PARAMETRO").get(0);
            if ( !empresaParametro.getNome_fantasia().isEmpty())
                toolbar.setTitle( empresaParametro.getNome_fantasia().toUpperCase());
        } catch ( NullPointerException|IndexOutOfBoundsException e) {
        } catch ( Exception e){
        }
        try {
            toolbar.setSubtitle("Olá: " + UsuarioHelper.getUsuario().getNome_usuario());
        } catch ( Exception e) {
            toolbar.setSubtitle("Olá: ");
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //int id = item.getItemId();
        //final Intent intent;
        final UsuarioBO usuarioBO = new UsuarioBO();
        drawerLayout.closeDrawers();
        switch (item.getItemId()) {
            case R.id.nav_principal_logoff:
                showMsgSimNao("Tem certeza que deseja sair?\n(O login só é possível com acesso a internet/rede local)");
                break;
            case R.id.nav_principal_configuracao:
                startActivity(new Intent(ActivityPrincipal.this, ActivityConfiguracao.class));
                break;
            case R.id.nav_principal_preferencia:
                startActivity(new Intent(ActivityPrincipal.this, ActivityPreferencia.class));
                break;
            case R.id.nav_principal_cliente:
                configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
                try {
                    if (configuracao.getTrabalha_com_cobranca().equalsIgnoreCase("S"))
                        startActivity(new Intent(ActivityPrincipal.this, ActivitySemCobranca.class));
                } catch ( NullPointerException e) {
                } catch ( Exception e) {
                }
                break;
            //case R.id.nav_principal_cobranca:
            //     startActivity(new Intent(ActivityPrincipal.this, ActivityCobranca.class));
            //break;
                /*
            case R.id.nav_principal_alerta: {
                UsuarioHelper.setUsuario( usuarioBO.buscarUsuarioLogin( ActivityPrincipalDrawer.this));
                if (UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("D"))
                    startActivity(new Intent(ActivityPrincipalDrawer.this, ActivityAlerta.class));
                else
                    Toast.makeText(ActivityPrincipalDrawer.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
                break;
            }
            */
            /*
            case R.id.nav_principal_update:
                try {

                    loginNaApi(UsuarioHelper.getUsuario(), true);
                    break;
                } catch (NullPointerException e) {
                    Toast.makeText(ActivityPrincipal.this, "Aparelho não configurado para atualização!", Toast.LENGTH_SHORT).show();
                    break;
                } catch (Exception e) {
                    Toast.makeText(ActivityPrincipal.this, "Aparelho não configurado para atualização!", Toast.LENGTH_SHORT).show();
                    break;
                }
            */
            case R.id.nav_principal_usuario:
                startActivity(new Intent(ActivityPrincipal.this, ActivityUsuario.class)
                        .putExtra("razaoSocial", "")
                        .putExtra("url", "")
                        .putExtra("porta", "")
                        .putExtra("versaoAPI", "")
                        .putExtra("conexaoSegura", "")
                        .putExtra("acao", "usuario"));
                break;


            case R.id.nav_principal_biometria:
                startActivity(new Intent(ActivityPrincipal.this, ActivityBiometriaCadastro.class));
                break;
            case R.id.nav_principal_update:
                try {

                    loginNaApi(UsuarioHelper.getUsuario(), true);
                    break;
                } catch (NullPointerException e) {
                    Toast.makeText(ActivityPrincipal.this, "Aparelho não configurado para atualização!", Toast.LENGTH_SHORT).show();
                    break;
                } catch (Exception e) {
                    Toast.makeText(ActivityPrincipal.this, "Aparelho não configurado para atualização!", Toast.LENGTH_SHORT).show();
                    break;
                }
                 //case R.id.nav_principal_video_aula:
            //Toast.makeText(ActivityPrincipal.this, "Opção em desenvolvimento!", Toast.LENGTH_LONG).show();

            //startActivity(new Intent(ActivityPrincipal.this, ActivityVideoAula.class));
            //break;
            //Relatorios a partir daqui   Cobrança -   Gerencial contas a pagar/receber - saldo de etoque




            case R.id.nav_principal_financeiro:
                //Toast.makeText(ActivityPrincipal.this, "Opção em desenvolvimento!", Toast.LENGTH_LONG).show();
                try {

                    UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPrincipal.this));
                    if (UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("G") || UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("A") || UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("D"))
                        startActivity(new Intent(ActivityPrincipal.this, ActivityFinanca.class));
                    else
                        Toast.makeText(ActivityPrincipal.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
                } catch (NullPointerException e) {
                    Toast.makeText(ActivityPrincipal.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
                }
                break;
                case R.id.nav_principal_estoque:
                try {
                    UsuarioHelper.setUsuario(usuarioBO.getUsuarioLogin(ActivityPrincipal.this));
                    if (UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("G") || UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("A") || UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("D"))
                        startActivity(new Intent(ActivityPrincipal.this, ActivityEstoqueSaldo.class));
                    else
                        Toast.makeText(ActivityPrincipal.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
                } catch (NullPointerException e) {
                    Toast.makeText(ActivityPrincipal.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
                }
                break;


            /*
            case R.id.nav_principal_nota_fiscal: {
                //Intent it = new Intent(ActivityPrincipalDrawer.this, ActivityNotaFiscal.class);
                startActivity(new Intent(ActivityPrincipalDrawer.this, ActivityNotaFiscal.class));
                break;
            }
            */
            //Menu suporte tecnico/video aulas
            case R.id.nav_principal_video_aula:
                startActivity(new Intent(ActivityPrincipal.this, ActivityVideoAula.class));
                break;
            case R.id.nav_principal_whats:
                PackageManager packageManager = context.getPackageManager();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String message = "Ola! Tudo bem? ";
                String phone = "5567993235558";
                try {
                    String url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode(message, "UTF-8");
                    intent.setPackage("com.whatsapp");
                    intent.setData(Uri.parse(url));
                    if (intent.resolveActivity(packageManager) != null) {
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_principal_telefone:
                //intent = new Intent(Intent.ACTION_DIAL);
                //intent.setData(Uri.parse("tel:06733833878"));
                startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:06733833878")));
                break;

            case R.id.nav_principal_email:
                startActivity(new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("mailto: rcksistemassuporte@rcksistemas.com.br")));
                break;

            case R.id.nav_principal_tecnico:
                UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPrincipal.this));
                if (UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("D"))
                    startActivity(new Intent(ActivityPrincipal.this, ActivityTecnico.class));
                else
                    Toast.makeText(ActivityPrincipal.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
                break;

            //Menu sobre
            case R.id.nav_principal_sobre:
                startActivity(new Intent(ActivityPrincipal.this, ActivitySobre.class));
                break;


        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void DownloadFiles(String url){
        try {
            URL u = new URL(url);
            InputStream is = u.openStream();
            DataInputStream dis = new DataInputStream(is);

            byte[] buffer = new byte[1024];
            int length;

            FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/" + "data/rck-whalle.apk"));
            while ((length = dis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
            }

        } catch (MalformedURLException mue) {
            Log.e("SYNC getUpdate", "malformed url error", mue);
        } catch (IOException ioe) {
            Log.e("SYNC getUpdate", "io error", ioe);
        } catch (SecurityException se) {
            Log.e("SYNC getUpdate", "security error", se);
        } catch ( Exception e) {
            Log.e("SYNC getUpdate", "security error", e);
        }
    }
    @Override
    public void onClick(View view) {
        if (view == btnCliente) {
            getUsuarios();
            //Intent intent = new Intent(ActivityPrincipalDrawer.this, ActivityCliente.class);
            //startActivity(new Intent(ActivityPrincipal.this, ActivityCliente.class));
            startActivity(new Intent(ActivityPrincipal.this, ActivityClienteDrawer.class));
        } else if (view == btnProduto) {
            getUsuarios();
            //Intent intent = new Intent(ActivityPrincipalDrawer.this, ActivityProduto.class);
            //startActivity(new Intent(ActivityPrincipal.this, ActivityProduto.class));
            startActivity(new Intent(ActivityPrincipal.this, ActivityProdutoDrawer.class));
        } else if (view == btnPedidos) {
            getUsuarios();
            //Intent intent = new Intent(ActivityPrincipalDrawer.this, ActivityPedidoMain.class);
            startActivity(new Intent(ActivityPrincipal.this, ActivityPedidoMain.class));
        } else if (view == btnSincroniza) {
            //Intent intent = new Intent(ActivityPrincipalDrawer.this, ActivityDialogSincronia.class);
            SincroniaBO.setActivity(ActivityPrincipal.this);

            startActivity(new Intent(ActivityPrincipal.this, ActivityDialogSincronia.class));
        } else if (view == btnPedidoFinalizado) {
            getUsuarios();
            //ntent telaPedidoEnviado = new Intent(ActivityPrincipalDrawer.this, ListagemPedidoEnviado.class);
            startActivity(new Intent(ActivityPrincipal.this, ActivityPedidoEnviado.class));
        } else if (view == btnPedidoPendente) {
            getUsuarios();
            //Intent telaPedidoPendentes = new Intent(ActivityPrincipalDrawer.this, ListagemPedidoPendente.class);
            startActivity(new Intent(ActivityPrincipal.this, ActivityPedidoPendente.class));
        } else if (view == btnProspectNovo) {
            getUsuarios();
            configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
            try {
                if (configuracao.getTrabalha_com_cobranca().equalsIgnoreCase("S"))
                    startActivity(new Intent(ActivityPrincipal.this, ActivityCobranca.class));
            } catch (NullPointerException e) {
                startActivity( new Intent(ActivityPrincipal.this, ActivityProspectPendente.class));
            } catch (Exception e) {
                startActivity( new Intent(ActivityPrincipal.this, ActivityProspectPendente.class));
            }
        } else if (view == btnCampanha) {
            getUsuarios();
            configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
            try {
                if (configuracao.getTrabalha_com_campanhas().equalsIgnoreCase("S"))
                    startActivity(new Intent(ActivityPrincipal.this, ActivityCampanha.class));
                else
                    Toast.makeText(this, "Aplicativo não esta parametrizado para a utilização de campanha comercial!", Toast.LENGTH_SHORT).show();
            } catch (NullPointerException e) {
                Toast.makeText(this, "Falha ao ler o arquivo de configuração!", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this, "Falha ao ler o arquivo de configuração!!", Toast.LENGTH_SHORT).show();
            }
        } /*else if ( view == img_whatss) {
            drawerLayout.closeDrawers();
            PackageManager packageManager = context.getPackageManager();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String message = "Ola! Tudo bem? ";
            String phone = "5567993235558";
            try {
                String url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode(message, "UTF-8");
                intent.setPackage("com.whatsapp");
                intent.setData(Uri.parse(url));
                if (intent.resolveActivity(packageManager) != null) {
                    context.startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ( view == img_telefone) {
            drawerLayout.closeDrawers();
            startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:06733833878")));
        } else if ( view == img_email) {
            drawerLayout.closeDrawers();
            startActivity(new Intent(Intent.ACTION_SENDTO).setData(Uri.parse("mailto: rcksistemassuporte@rcksistemas.com.br")));
        } else if ( view == img_tecnico) {
            drawerLayout.closeDrawers();
            UsuarioHelper.setUsuario( usuarioBO.getUsuarioLogin( ActivityPrincipal.this));
            //if (UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("D") || UsuarioHelper.getUsuario().getUsuario_tipo().equalsIgnoreCase("A"))
                startActivity(new Intent(ActivityPrincipal.this, ActivityTecnico.class));
            //else
            //    Toast.makeText(ActivityPrincipal.this, "Usuario não tem permissão para acessar este módulo do aplicátivo!", Toast.LENGTH_LONG).show();
        } else if ( view == img_sobre) {
            drawerLayout.closeDrawers();
            startActivity(new Intent(ActivityPrincipal.this, ActivitySobre.class));
        } else if( view == img_avalia_1) {
                updateAvaliacaoAPP(1);
        } else if( view == img_avalia_2) {
                updateAvaliacaoAPP(2);
        } else if( view == img_avalia_3) {
                updateAvaliacaoAPP(3);
        } else if( view == img_avalia_4) {
                updateAvaliacaoAPP(4);
        } else if( view == img_avalia_5) {
                updateAvaliacaoAPP(5);
        }
        */

    }


    @Override
    public void onBackPressed() {
        aperta++;
        if (aperta >= 2)
            finish();
        else
            Toast.makeText(this, "Toque novamente para sair!", Toast.LENGTH_SHORT).show();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                aperta = 0;
                System.gc();
            }
        }, 2000, 5000);
    }

    public void getUsuarios() {
        Rotas apiRotas = Api.buildRetrofit(false);
        Call<List<Usuario>> call = apiRotas.getListaUsuarios();
        call.enqueue(new Callback<List<Usuario>>() {
            @Override
            public void onResponse(Call<List<Usuario>> call, Response<List<Usuario>> response) {
                usuarioList = response.body();
                //ivInternet.setVisibility(View.GONE);
                String dataSincronia = "";
                try {
                    dataSincronia = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(db.consultaDados("SELECT DATA_SINCRONIA FROM TBL_LOGIN", "DATA_SINCRONIA")));
                } catch (ParseException|NullPointerException e) {
                    e.printStackTrace();
                }
                if (!dataSincronia.equals(new SimpleDateFormat("yyyy-MM-dd").format(new Date())))
                    try {
                        if (configuracao.getSincronia_automatica_app().equalsIgnoreCase("S"))
                            Toast.makeText(ActivityPrincipal.this, "Sincronia atrasada, por favor faça a sincronia!", Toast.LENGTH_SHORT).show();
                    } catch ( NullPointerException e) {
                    } catch ( Exception e) {
                    }

                if (!usuarioBO.add(usuarioList, ActivityPrincipal.this)) {
                    Toast.makeText(ActivityPrincipal.this, "Houve um erro ao salvar os usuarios", Toast.LENGTH_LONG).show();
                } else {
                    String aparelhoId = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    try {
                        //Usuario usuarioLogin = db.listaUsuario("SELECT * FROM TBL_WEB_USUARIO WHERE ID_USUARIO = " + UsuarioHelper.getUsuario().getId_usuario()).get(0);
                        UsuarioDAO usuarioDAO = new UsuarioDAO(db);
                        Usuario usuarioLogin = usuarioDAO.getLista("SELECT * FROM TBL_WEB_USUARIO WHERE LOGIN = '" + db.consultaDados("SELECT * FROM TBL_LOGIN;", "LOGIN") + "';").get(0);
                        if (aparelhoId.equals(usuarioLogin.getAparelho_id()) || usuarioLogin.getUsuario_permite_n_logins().equalsIgnoreCase("S")) {
                            loginNaApi(usuarioLogin, false);
                        } else {
                            try {
                                if (!UsuarioHelper.getUsuario().getUsuario_permite_n_logins().equalsIgnoreCase("S")) {
                                    Toast.makeText(getApplicationContext(), "Este usuario está logado em outro aparelho!", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(getApplicationContext(), "Por favor, refaça o seu login!", Toast.LENGTH_LONG).show();
                                    db.update("UPDATE TBL_LOGIN SET LOGADO = 'N';");
                                    //Intent intent = new Intent(ActivityPrincipalDrawer.this, MainActivity.class);
                                    startActivity(new Intent(ActivityPrincipal.this, MainActivity.class));
                                    finish();
                                }
                            } catch ( NullPointerException e) {
                                Toast.makeText(getApplicationContext(), "Este usuario está logado em outro aparelho!", Toast.LENGTH_SHORT).show();
                                Toast.makeText(getApplicationContext(), "Por favor, refaça o seu login!", Toast.LENGTH_LONG).show();
                                db.update("UPDATE TBL_LOGIN SET LOGADO = 'N';");
                                //Intent intent = new Intent(ActivityPrincipalDrawer.this, MainActivity.class);
                                startActivity(new Intent(ActivityPrincipal.this, MainActivity.class));
                                finish();
                            }
                            System.gc();
                        }
                    } catch (CursorIndexOutOfBoundsException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Este usuario está logado em outro aparelho!", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Por favor, refaça o seu login!", Toast.LENGTH_LONG).show();
                        db.update("UPDATE TBL_LOGIN SET LOGADO = 'N';");
                        //Intent intent = new Intent(ActivityPrincipalDrawer.this, MainActivity.class);
                        startActivity(new Intent(ActivityPrincipal.this, MainActivity.class));
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Usuario>> call, Throwable t) {
                t.printStackTrace();
                //Toast.makeText(ActivityPrincipal.this, "Não foi possivel sincronizar com o servidor, por favor verifique sua conexão", Toast.LENGTH_LONG).show();
                //ivInternet.setVisibility(View.VISIBLE);
            }
        });
    }

    public void loginNaApi(final Usuario usuario, boolean verificaVersaoApp) {
        Rotas apiRotas = Api.buildRetrofit(false);
        String idAndroit = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Call<Usuario> call = apiRotas.logarAPI(idAndroit, usuario.getId_usuario(), db.consultaDados("SELECT * FROM TBL_LOGIN", "LOGIN"), db.consultaDados("SELECT * FROM TBL_LOGIN", "SENHA"), "-");
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Usuario usuario1 = response.body();
                switch (response.code()) {
                    case 200:
                        if (usuario1.getIdEmpresaMultiDevice() != null && Integer.parseInt(usuario1.getIdEmpresaMultiDevice()) > 0) {
                            System.gc();
                            //System.out.println("Usuario ok");
                            UsuarioHelper.setUsuario(usuario1);
                            if ( verificaVersaoApp) {
                                String version = pInfo.versionName.substring(0, 7);
                                String versao = UsuarioHelper.getUsuario().getAparelho_versao_app();
                                String url = UsuarioHelper.getUsuario().getAparelho_link_app();
                                try {
                                    if (!versao.isEmpty() && !url.isEmpty()) {
                                        if (!version.equalsIgnoreCase(UsuarioHelper.getUsuario().getAparelho_versao_app())) {
                                            Toast.makeText(getApplicationContext(), "Nova versão para atualização disponivel!", Toast.LENGTH_SHORT).show();
                                            try {
                                                //DownloadFiles(url);
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                                                /*File file = new File(String.valueOf(Uri.parse(url)));
                                                Intent intent1 = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                                                intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                                intent1.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");;
                                                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent1);
                                                */
                                            } catch (NullPointerException e) {
                                                Toast.makeText(ActivityPrincipal.this, "A url/link para update do aplicativo é inválida!", Toast.LENGTH_SHORT).show();
                                            } catch (ActivityNotFoundException e) {
                                                Toast.makeText(ActivityPrincipal.this, "A url/link para update do aplicativo é inválida!!", Toast.LENGTH_SHORT).show();
                                            } catch (Exception e) {
                                                Toast.makeText(ActivityPrincipal.this, "A url/link para update do aplicativo é inválida!!!", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(ActivityPrincipal.this, "Versão do aplicativo já atualizada", Toast.LENGTH_SHORT).show();
                                        }
                                        break;
                                    }
                                } catch ( NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            showMsgAlerta("O usuario em questão não tem o codigo da empresa informado em seu cadastro, é necessário a correção no cadastro deste usuário!\n    " +
                                    "Em caso de duvida, favor entrar em contato com a RCK sistemas!");
                        }
                        break;
                    case 500:
                        if (!UsuarioHelper.getUsuario().getLogin().equals("DC")) {
                            Toast.makeText(getApplicationContext(), "Foi encontrada uma divergencia em seu cadastro!", Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Por favor, refaça o seu login!", Toast.LENGTH_LONG).show();
                            db.update("UPDATE TBL_LOGIN SET LOGADO = 'N';");
                            //Intent intent = new Intent(ActivityPrincipalDrawer.this, MainActivity.class);
                            startActivity(new Intent(ActivityPrincipal.this, MainActivity.class));
                            finish();
                        }
                        System.gc();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(ActivityPrincipal.this, "Não foi possivel conectar com o servidor! Verifique sua conexão a rede/internet!\n(" + t.getMessage() + ")", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        if (getIntent().getIntExtra("alterado", 0) == 1) {
            String versaoApp = "-";
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                versaoApp = pInfo.versionName.substring(0,5);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            boolean cobranca = false;
            try {
                EmpresaParametroDAO empresaParametroDAO = new EmpresaParametroDAO(new DBHelper(this));
                EmpresaParametro empresaParametro = empresaParametroDAO.getEmpresa("SELECT * FROM TBL_EMPRESA_PARAMETRO");
                if (empresaParametro.getUtiliza_cobranca_offline().equalsIgnoreCase("S"))
                    cobranca = true;
            } catch ( NullPointerException|CursorIndexOutOfBoundsException e) {
            } catch ( Exception e) {
            }
            sincroniaBO.sincronizaApi(new Sincronia(true, true, true, false, false, false, false, false, cobranca), versaoApp);
            getIntent().putExtra("alterado", 0);
        }
        ClienteHelper.clear();
        try {
            edtDataSincronia.setText(new SimpleDateFormat("dd/MM/yy").format(new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(db.consultaDados("SELECT DATA_SINCRONIA_PRODUTO FROM TBL_LOGIN", "DATA_SINCRONIA_PRODUTO"))));
            edtDataSincronia.setText(edtDataSincronia.getText().toString() + "\n " + new SimpleDateFormat("HH:mm:ss").format(new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(db.consultaDados("SELECT DATA_SINCRONIA_PRODUTO FROM TBL_LOGIN", "DATA_SINCRONIA_PRODUTO"))));
            //edtDataSincronia.setText(new SimpleDateFormat("dd/MM/yy HH:mm").format(new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(db.consulta("SELECT DATA_SINCRONIA_PRODUTO FROM TBL_LOGIN", "DATA_SINCRONIA_PRODUTO"))));
        } catch (ParseException|NullPointerException e) {
            edtDataSincronia.setText("00/00/00" + "\n " + "00:00:00");
            e.printStackTrace();
        }
        setTituloToolBar();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        System.gc();
        super.onDestroy();
    }

    public void showMsgAlerta(String mensagem) {
        ViewGroup viewGroup = ActivityPrincipal.this.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from( ActivityPrincipal.this).inflate(R.layout.dialog_alerta_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPrincipal.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //Intent intent = new Intent(ActivityPrincipalDrawer.this, MainActivity.class);
                db.update("UPDATE TBL_LOGIN SET LOGADO = 'N'");
                startActivity(new Intent(ActivityPrincipal.this, MainActivity.class));
                finish();
                //PedidoHelper.getActivityPedidoMain().finish();
            }
        });
        alertDialog.show();
    }



    public void showMsgSimNao( String mensagem) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(ActivityPrincipal.this).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPrincipal.this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);

        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                DBHelper db = new DBHelper(ActivityPrincipal.this);
                db.update("UPDATE TBL_LOGIN SET LOGADO = 'N'");
                //Intent intent = new Intent(ActivityPrincipalDrawer.this, MainActivity.class);
                startActivity(new Intent(ActivityPrincipal.this, MainActivity.class));
                System.gc();
                finish();
            }
        });
        alertDialog.show();
    }

    /*private void setAvalicaoApp(int avalicao, boolean isClick) {
        try {
            if ( avalicao > 0 ) {
                if (avalicao == 1) {
                    img_avalia_1.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_1.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_2.setTag(R.drawable.ic_avaliacao_negativa);
                    img_avalia_3.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_3.setTag(R.drawable.ic_avaliacao_negativa);
                    img_avalia_4.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_4.setTag(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
                    updateValiacaoAPI( 1);
                } else if (avalicao == 2) {
                    img_avalia_1.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_1.setTag(R.drawable.ic_avaliacao_positiva);
                    if ( isClick) {
                        if (img_avalia_2.getTag().equals(R.drawable.ic_avaliacao_positiva)) {
                            img_avalia_2.setImageResource(R.drawable.ic_avaliacao_negativa);
                            img_avalia_2.setTag(R.drawable.ic_avaliacao_negativa);
                            updateValiacaoAPI( 1);
                        } else {
                            img_avalia_2.setImageResource(R.drawable.ic_avaliacao_positiva);
                            img_avalia_2.setTag(R.drawable.ic_avaliacao_positiva);
                            updateValiacaoAPI( 2);
                        }
                    } else {
                        img_avalia_2.setImageResource(R.drawable.ic_avaliacao_positiva);
                        img_avalia_2.setTag(R.drawable.ic_avaliacao_positiva);
                    }
                    img_avalia_3.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_3.setTag(R.drawable.ic_avaliacao_negativa);
                    img_avalia_4.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_4.setTag(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
                } else if (avalicao == 3) {
                    img_avalia_1.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_1.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setTag(R.drawable.ic_avaliacao_positiva);
                    if ( isClick) {
                        if (img_avalia_3.getTag().equals(R.drawable.ic_avaliacao_positiva)) {
                            img_avalia_3.setImageResource(R.drawable.ic_avaliacao_negativa);
                            img_avalia_3.setTag(R.drawable.ic_avaliacao_negativa);
                            updateValiacaoAPI( 2);
                        } else {
                            img_avalia_3.setImageResource(R.drawable.ic_avaliacao_positiva);
                            img_avalia_3.setTag(R.drawable.ic_avaliacao_positiva);
                            updateValiacaoAPI( 3);
                        }
                    } else {
                        img_avalia_3.setImageResource(R.drawable.ic_avaliacao_positiva);
                        img_avalia_3.setTag(R.drawable.ic_avaliacao_positiva);
                    }
                    img_avalia_4.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_4.setTag(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
                } else if ( avalicao == 4) {
                    img_avalia_1.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_1.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_3.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_3.setTag(R.drawable.ic_avaliacao_positiva);
                    if ( isClick) {
                        if (img_avalia_4.getTag().equals(R.drawable.ic_avaliacao_positiva)) {
                            img_avalia_4.setImageResource(R.drawable.ic_avaliacao_negativa);
                            img_avalia_4.setTag(R.drawable.ic_avaliacao_negativa);
                            updateValiacaoAPI( 3);
                        } else {
                            img_avalia_4.setImageResource(R.drawable.ic_avaliacao_positiva);
                            img_avalia_4.setTag(R.drawable.ic_avaliacao_positiva);
                            updateValiacaoAPI( 4);
                        }
                    } else {
                        img_avalia_4.setImageResource(R.drawable.ic_avaliacao_positiva);
                        img_avalia_4.setTag(R.drawable.ic_avaliacao_positiva);
                    }
                    img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
                    img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
                } else if ( avalicao == 5) {
                    img_avalia_1.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_1.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_2.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_3.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_3.setTag(R.drawable.ic_avaliacao_positiva);
                    img_avalia_4.setImageResource(R.drawable.ic_avaliacao_positiva);
                    img_avalia_4.setTag(R.drawable.ic_avaliacao_positiva);
                    if ( isClick) {
                        if (img_avalia_5.getTag().equals(R.drawable.ic_avaliacao_positiva)) {
                            img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
                            img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
                            updateValiacaoAPI(4);
                        } else {
                            img_avalia_5.setImageResource(R.drawable.ic_avaliacao_positiva);
                            img_avalia_5.setTag(R.drawable.ic_avaliacao_positiva);
                            updateValiacaoAPI( 5);
                        }
                    } else {
                        img_avalia_5.setImageResource(R.drawable.ic_avaliacao_positiva);
                        img_avalia_5.setTag(R.drawable.ic_avaliacao_positiva);
                    }
                }

            } else {
                img_avalia_1.setImageResource(R.drawable.ic_avaliacao_negativa);
                img_avalia_1.setTag(R.drawable.ic_avaliacao_negativa);
                img_avalia_2.setImageResource(R.drawable.ic_avaliacao_negativa);
                img_avalia_2.setTag(R.drawable.ic_avaliacao_negativa);
                img_avalia_3.setImageResource(R.drawable.ic_avaliacao_negativa);
                img_avalia_3.setTag(R.drawable.ic_avaliacao_negativa);
                img_avalia_4.setImageResource(R.drawable.ic_avaliacao_negativa);
                img_avalia_4.setTag(R.drawable.ic_avaliacao_negativa);
                img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
                img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
            }
        }catch ( NullPointerException e) {
            img_avalia_1.setImageResource(R.drawable.ic_avaliacao_negativa);
            img_avalia_1.setTag(R.drawable.ic_avaliacao_negativa);
            img_avalia_2.setImageResource(R.drawable.ic_avaliacao_negativa);
            img_avalia_2.setTag(R.drawable.ic_avaliacao_negativa);
            img_avalia_3.setImageResource(R.drawable.ic_avaliacao_negativa);
            img_avalia_3.setTag(R.drawable.ic_avaliacao_negativa);
            img_avalia_4.setImageResource(R.drawable.ic_avaliacao_negativa);
            img_avalia_4.setTag(R.drawable.ic_avaliacao_negativa);
            img_avalia_5.setImageResource(R.drawable.ic_avaliacao_negativa);
            img_avalia_5.setTag(R.drawable.ic_avaliacao_negativa);
        }
    }
    */

    private void updateValiacaoAPI(int avaliacao) {
        db = new DBHelper(ActivityPrincipal.this);
        final com.example.rcksuporte05.rcksistemas.model.Configuracao configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        if (!configuracao.getId().isEmpty()) {
            Rotas apiRotas = Api.buildRetrofit(false);
            Call<List<Usuario>> call = apiRotas.updateAvalicaoAPP(UsuarioHelper.getUsuario().getId_usuario(), String.valueOf(avaliacao) );
            call.enqueue(new Callback<List<Usuario>>() {
                @Override
                public void onResponse(Call<List<Usuario>> call, Response<List<Usuario>> response) {
                    List<Usuario> usuarioList = response.body();
                    if (response.code() == 200) {
                        if (!usuarioBO.add(usuarioList, getApplicationContext()))
                            Toast.makeText(getApplicationContext(), "Houve um erro atualizar a lista de usuários apos a avaliação!", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getApplicationContext(), "Avaliação registrada com sucesso!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Houve um erro atualizar a sua avaliação! Tente novamente", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<Usuario>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Não foi possivel sincronizar com o servidor, por favor verifique sua conexão", Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });
        } else
            Toast.makeText(getApplicationContext(), "Parametros para conexão não cadastrados, por gentileza clique no botão ajuda antes de continuar", Toast.LENGTH_SHORT).show();
    }

    /*
    private void updateAvaliacaoAPP(int avalicao) {
        UsuarioDAO usuarioDAO = new UsuarioDAO(db);
        try {
            setAvalicaoApp(avalicao, true);
            if ( usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), avalicao) >= 1) {
                if ( avalicao == 5 && img_avalia_5.getTag().equals(R.drawable.ic_avaliacao_positiva))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), avalicao);
                else if (avalicao == 5 && img_avalia_5.getTag().equals(R.drawable.ic_avaliacao_negativa))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), 4);
                else if (avalicao == 4 && img_avalia_4.getTag().equals(R.drawable.ic_avaliacao_positiva))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), avalicao);
                else if (avalicao == 4 && img_avalia_4.getTag().equals(R.drawable.ic_avaliacao_negativa))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), 3);
                else if (avalicao == 3 && img_avalia_3.getTag().equals(R.drawable.ic_avaliacao_positiva))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), avalicao);
                else if (avalicao == 3 && img_avalia_3.getTag().equals(R.drawable.ic_avaliacao_negativa))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), 2);
                else if (avalicao == 2 && img_avalia_2.getTag().equals(R.drawable.ic_avaliacao_positiva))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), avalicao);
                else if (avalicao == 2 && img_avalia_2.getTag().equals(R.drawable.ic_avaliacao_negativa))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), 1);

                else if (avalicao == 1 && img_avalia_1.getTag().equals(R.drawable.ic_avaliacao_positiva))
                    usuarioDAO.updateAvalicao(Integer.parseInt(UsuarioHelper.getUsuario().getId_usuario()), avalicao);
                //Toast.makeText(getApplicationContext(), "Avaliação registrada com sucesso!\n Grato", Toast.LENGTH_SHORT).show();
            }
        } catch ( NullPointerException e) {
            Toast.makeText(getApplicationContext(), "Falha ao registrar a avaliação!\n Tente novamente", Toast.LENGTH_SHORT).show();
        }

    }

     */

}

