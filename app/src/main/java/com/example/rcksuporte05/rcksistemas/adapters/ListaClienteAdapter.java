package com.example.rcksuporte05.rcksistemas.adapters;

import android.app.Activity;
import android.graphics.Color;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.DAO.CategoriaDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.SegmentoDAO;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.activity.ActivityClienteDrawer;
import com.example.rcksuporte05.rcksistemas.adapters.viewHolder.ClientesViewHolder;
import com.example.rcksuporte05.rcksistemas.model.Categoria;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.util.Common;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by RCK 03 on 30/11/2017.
 */

public class ListaClienteAdapter extends RecyclerView.Adapter<ClientesViewHolder> {
    private Activity context;
    private List<Cliente> lista;
    private Listener listener;
    private SparseBooleanArray selectedItems;
    private DBHelper db;
    private  CategoriaDAO categoriaDAO;
    private Categoria categoria = new Categoria();
    private SegmentoDAO segmentoDAO ;
    private ConfiguracaoDAO configuracaoDAO;
    private Configuracao configuracao;
    public Cliente cliente;
    int numCols;
    public ListaClienteAdapter(Activity context, List<Cliente> lista, Listener listener) {
        this.lista = lista;
        this.listener = listener;
        this.context = context;
        this.selectedItems = new SparseBooleanArray();
        this.db = new DBHelper(context);
        this.categoriaDAO =  new CategoriaDAO(this.db);
        this.segmentoDAO = new SegmentoDAO(this.db);
        this.configuracaoDAO = new ConfiguracaoDAO(this.db);
        this.configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
    }

    @Override
    public ClientesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cliente_lista, parent, false);
        return new ClientesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClientesViewHolder holder, int position) {
        cliente = lista.get(position);
        if (cliente.getId_cadastro_servidor() > 0) {
            holder.idCliente.setText(String.valueOf(cliente.getId_cadastro_servidor()));
            holder.btnDownload.setBackgroundResource(R.mipmap.ic_download_cliente_new);
            if (cliente.getAlterado().equals("S"))
            //if ( !cliente.getFinalizado().equalsIgnoreCase("S") && !cliente.getF_cliente().equalsIgnoreCase("S") && cliente.getF_vendedor().equalsIgnoreCase("N"))
                holder.btnDownload.setBackgroundResource(R.mipmap.ic_upload_cliente_new);
        } else {
            holder.idCliente.setText(String.valueOf(cliente.getId_cadastro()));
            holder.btnDownload.setBackgroundResource(R.mipmap.ic_upload_cliente_new);
        }
        holder.textViewNome.setText(cliente.getNome_cadastro());
        holder.textViewNomeFantasia.setText(cliente.getNome_fantasia());

        try {
            holder.telefoneCliente.setText(MascaraUtil.formataTelefone( cliente.getTelefone_principal() ));
            try {
                holder.telefoneCliente.setText( MascaraUtil.formataTelefone( cliente.getTelefone_principal()) + "  --  " + MascaraUtil.formataTelefone( cliente.getTelefone_dois() ) );
            } catch ( NullPointerException e) {
            }
        } catch ( NullPointerException e) {
        }

        try {
            if (!lista.get(position).getNome_tabela_preco().isEmpty())
                holder.nomeTabelaPreco.setText("Tabela: " + cliente.getNome_tabela_preco());

        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
        try {
            if (lista.get(position).getIdCategoria() > 0) {
                categoria = categoriaDAO.getCategoria( cliente.getIdCategoria());
                holder.txtCategoria.setText("Categoria: " + categoria.getNomeCategoria().toUpperCase());
            }
        } catch ( NullPointerException|IndexOutOfBoundsException e) {
        } catch ( Exception e) {
        }

        try {
            if ( !lista.get(position).getId_setor().isEmpty())
                 holder.txtSegmento.setText("Segmento: " + segmentoDAO.getLista("SELECT * FROM TBL_CADASTRO_SETOR WHERE ID = " + cliente.getId_setor(), false ).get(0).getNomeSetor());
        } catch ( NullPointerException|IndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }

        try {
            if (new SimpleDateFormat("yyyy-MM-dd").parse(cliente.getData_ultima_compra()).before(new SimpleDateFormat("yyyy-MM-dd").parse("2001-01-01")))
                holder.txtDataUltimaCompra.setText("Sem compra");
            else
                holder.txtDataUltimaCompra.setText("Não compra à " + calcularDias(new SimpleDateFormat("yyyy-MM-dd").parse(cliente.getData_ultima_compra()), new Date()) + " dias");
        } catch (Exception e) {
            holder.txtDataUltimaCompra.setText("Sem compra");
        }

        if (cliente.getId_cadastro_servidor() > 0) {
            if (cliente.getF_cliente().equals("S"))
                holder.imStatus.setVisibility(View.GONE);
            else {
                holder.imStatus.setImageResource(R.mipmap.ic_time);
                holder.txtClienteAguarda.setVisibility(View.VISIBLE);
            }
        } else
            holder.imStatus.setImageResource(R.mipmap.ic_prospect_pendente);

        if (cliente.getAlterado().equals("S")) {
            holder.txtClienteAguarda.setTextColor(Color.RED);
            holder.txtClienteAguarda.setText("Cliente com alterações pendentes");
        } else {
            if (!cliente.getFinalizado().equalsIgnoreCase("N") && cliente.getF_cliente().equalsIgnoreCase("S")) {
                holder.txtClienteAguarda.setTextColor(Color.BLACK);
                holder.txtClienteAguarda.setText("");

            } else {
                holder.txtClienteAguarda.setTextColor(Color.BLACK);
                holder.txtClienteAguarda.setText("Cliente aguardando análise para efetivação");
            }
        }
        holder.imgFinanceiroVencido.setVisibility(View.INVISIBLE);
        try {
            if (Float.parseFloat(cliente.getFinanceiro_vencido()) > 0.00) {
                holder.imgFinanceiroVencido.setVisibility(View.VISIBLE);
                holder.imgFinanceiroVencido.setImageResource(R.drawable.ic_financeiro_vencido);
            }
        } catch (NullPointerException|NumberFormatException e) {
        } catch (Exception e){
        }
        holder.imgFinanceiroVencendo.setVisibility(View.INVISIBLE);
        try {
        if ( Float.parseFloat(cliente.getFinanceiro_vencendo()) > 0.00) {
            holder.imgFinanceiroVencido.setVisibility(View.VISIBLE);
            holder.imgFinanceiroVencido.setImageResource(R.drawable.ic_financeiro_vencido);
        }
        } catch (NullPointerException|NumberFormatException e) {
        } catch (Exception e){
        }

        holder.imgPedidoAnalise.setVisibility(View.INVISIBLE);
        try {
            if ( Float.parseFloat(cliente.getPedido_analise()) > 0.00) {
                holder.imgPedidoAnalise.setVisibility(View.VISIBLE);
                holder.imgPedidoAnalise.setImageResource(R.drawable.ic_pedido_analise);
            }
        } catch (NullPointerException|NumberFormatException e) {
        } catch (Exception e){
        }


        holder.imgPromocaoCliente.setVisibility(View.INVISIBLE);
        try {
            if ( Integer.parseInt(cliente.getId_promocao()) > 0) {
                holder.imgPromocaoCliente.setVisibility(View.VISIBLE);
                holder.imgPromocaoCliente.setImageResource(R.drawable.ic_produto_promocao);
            }
        } catch (NullPointerException|NumberFormatException e) {
        } catch (Exception e){
        }

        holder.imgClienteBloqueado.setVisibility(View.INVISIBLE);
        try {
            if ( cliente.getAtivo().equalsIgnoreCase("N")) {
                holder.imgClienteBloqueado.setVisibility(View.VISIBLE);
                holder.imgClienteBloqueado.setImageResource(R.drawable.ic_cliente_bloqueado);
            }
            if ( configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                float limiteCredito = Float.parseFloat(cliente.getLimite_credito());
                float limitUtilizado = Float.parseFloat(cliente.getFinanceiro_vencido()) + Float.parseFloat(cliente.getFinanceiro_vencendo());
                if ( (limiteCredito - limitUtilizado) < 0.00f) {
                    if ( cliente.getAtivo().equalsIgnoreCase("S")) {
                        holder.imgClienteBloqueado.setVisibility(View.VISIBLE);
                        holder.imgClienteBloqueado.setImageResource(R.drawable.ic_cliente_atencao);
                    }
                }
            }
        } catch (NullPointerException|NumberFormatException e) {
        } catch (Exception e){
        }

        if ( context.getIntent().getIntExtra("acao", 0) == 1)
            holder.lyBotoes.setVisibility(View.GONE);
        else {
            holder.lyDownload.setOnClickListener(listener.onClickDowload(position));
            holder.btnDownload.setOnClickListener(listener.onClickDowload(position));
            holder.lyChamada.setOnClickListener(listener.onClickChamada(position));
            holder.btnChamada.setOnClickListener(listener.onClickChamada(position));
            holder.lyGps.setOnClickListener(listener.onClickGps(position));
            holder.btnGps.setOnClickListener(listener.onClickGps(position));
            holder.lyEmail.setOnClickListener(listener.onClickEmail(position));
            holder.btnEmail.setOnClickListener(listener.onClickEmail(position));
            holder.lyFinanceiro.setOnClickListener(listener.onClickFinanceiro(position));
            holder.btnFinanceiro.setOnClickListener(listener.onClickFinanceiro(position));
            holder.lyNovoPedido.setOnClickListener(listener.onClickNovoPedido(position));
            holder.btnNovoPedido.setOnClickListener(listener.onClickNovoPedido(position));
        }
        holder.itemView
                .setBackgroundColor(selectedItems.get(position) ? Color.parseColor("#dfdfdf")
                        : Color.TRANSPARENT);
        applyCLickEnvents(holder, position);
        System.gc();
    }

    public Cliente getItem(int position) {
        return lista.get(position);
    }


    @Override
    public int getItemCount() {
        if (lista != null)
            return lista.size();
        return 0;
    }

    public int setNumCols(int qtdCols) {
        return numCols = qtdCols;
    }

    @Override
    public int getItemViewType(int position) {
        if (lista.size() == 1)
            return 0;
        else {
            if (lista.size() % numCols == 0)
                return 1;
            else
                return (position > 1 && position == lista.size() - 1) ? 0 : 1;
        }
    }

    private void applyCLickEnvents(ClientesViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listener.onClickListener(position);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                try {
                    listener.onLongClickListener(position);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false))
            selectedItems.delete(pos);
        else
            selectedItems.put(pos, true);
        notifyDataSetChanged();
    }

    public int getSelectedItensCount() {
        return selectedItems.size();
    }

    public List<Cliente> getItensSelecionados() {
        List<Cliente> lista = new ArrayList<>();
        for (int i = 0; i < selectedItems.size(); i++)
            lista.add(this.lista.get(selectedItems.keyAt(i)));
        return lista;
    }

    public void clearSelection() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    private String calcularDias(Date data1, Date data2) {
        return String.valueOf((data2.getTime() - data1.getTime()) / 86400000L);
    }

    public interface Listener {
        void onClickListener(int position);
        void onLongClickListener(int position);
        View.OnClickListener onClickChamada(int position);
        View.OnClickListener onClickGps(int position);
        View.OnClickListener onClickEmail(int position);
        View.OnClickListener onClickFinanceiro(int position);
        View.OnClickListener onClickNovoPedido(int position);
        View.OnClickListener onClickDowload(int position);
    }

}
