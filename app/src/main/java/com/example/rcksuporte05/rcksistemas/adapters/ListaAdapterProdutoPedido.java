package com.example.rcksuporte05.rcksistemas.adapters;

import android.content.Context;
import android.graphics.Color;

import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.activity.ActivityPedidoMain;
import com.example.rcksuporte05.rcksistemas.adapters.viewHolder.ProdutoPedidoViewHolder;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.util.ArrayList;
import java.util.List;

public class ListaAdapterProdutoPedido extends RecyclerView.Adapter<ProdutoPedidoViewHolder> {
    private static int currentSelectedIndex = -1;
    private List<WebPedidoItens> lista;
    private ProdutoPedidoAdapterListener listener;
    private SparseBooleanArray selectedItems;
    private ProdutoPedidoViewHolder holder;
    private boolean campanha;
    //private Context context;

    public ListaAdapterProdutoPedido(List<WebPedidoItens> lista, ProdutoPedidoAdapterListener listener, boolean campanha/*, Context context*/) {
        this.listener = listener;
        this.lista = lista;
        this.selectedItems = new SparseBooleanArray();
        this.campanha = campanha;
        //this.context = context;
    }

    @Override
    public ProdutoPedidoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.produto_pedido_lista_new, parent, false);
        return new ProdutoPedidoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProdutoPedidoViewHolder holder, int position) {
        if ( !lista.get(position).getProdutoAtivo())
            holder.nomeListaProduto.setTextColor(Color.parseColor("#FF0000"));
        else
            holder.nomeListaProduto.setTextColor(Color.parseColor("#000000"));
        holder.nomeListaProduto.setText(lista.get(position).getNome_produto().trim());
        Float valorProduto;
        if (lista.get(position).getValor_desconto_per() != null && !lista.get(position).getValor_desconto_per().trim().isEmpty() && Float.parseFloat(lista.get(position).getValor_desconto_per()) > 0) {
            valorProduto = lista.get(position).getValor_unitario() - (Float.parseFloat(lista.get(position).getValor_desconto_real()) / Float.parseFloat(lista.get(position).getQuantidade()));
            holder.txtDesconto.setText("Desconto: " + MascaraUtil.duasCasaDecimal(lista.get(position).getValor_desconto_per()) + "%");
            try {
                if (Float.parseFloat(lista.get(position).getPerc_desconto_adic()) > 0.00f) {
                    holder.txtDesconto.setText("Desconto(s): " + MascaraUtil.duasCasaDecimal(lista.get(position).getValor_desconto_per()) + "% + " + MascaraUtil.duasCasaDecimal(lista.get(position).getPerc_desconto_adic()) + "%");
                    valorProduto = lista.get(position).getValor_unitario() - ( Float.parseFloat(lista.get(position).getValor_desconto_real()) + Float.parseFloat(lista.get(position).getValor_desconto_adic()))  / Float.parseFloat(lista.get(position).getQuantidade());

                }
            } catch ( NullPointerException|NumberFormatException e) {
            } catch ( Exception e) {
            }
        } else {
            valorProduto = lista.get(position).getValor_unitario();
            holder.txtDesconto.setText("");
        }
        holder.precoProduto.setText(MascaraUtil.duasCasaDecimal(lista.get(position).getQuantidade()) + " x " + MascaraUtil.mascaraReal(valorProduto) + " = " + MascaraUtil.mascaraReal(lista.get(position).getValor_total()));
        try {
            if ( Float.parseFloat( lista.get(position).getIcms_st_valor()) > 0.00 ) {
                holder.precoProduto.setText(MascaraUtil.duasCasaDecimal(lista.get(position).getQuantidade()) + " x ("
                        + MascaraUtil.mascaraReal(valorProduto) +" + "  +MascaraUtil.mascaraReal(lista.get(position).getIcms_st_valor() )+ ") = " + MascaraUtil.mascaraReal(lista.get(position).getValor_total()));
            }
        } catch ( NullPointerException|NumberFormatException e) {
        } catch ( Exception e) {
        }
        if ( !lista.get(position).getProdutoAtivo()) {
            holder.textViewUnidadeMedida.setText("*****");
            holder.textViewUnidadeMedida.setTextColor(Color.parseColor("#FF0000"));
            if ( !PedidoHelper.getActivityPedidoMain().isPedidoEnviado())
                Toast.makeText( PedidoHelper.getActivityPedidoMain(), "O produto + " + lista.get(position).getNome_produto().trim() + "  não foi localizado/e ou foi inativado, favor verificar antes de prosseguir!", Toast.LENGTH_LONG).show();
        } else {
            try {
                if ( lista.get(position).getId_sku() > 0) {
                    holder.textViewUnidadeMedida.setText(lista.get(position).getNome_sku());
                    holder.textViewUnidadeMedida.setTextColor(Color.parseColor("#000000"));
                } else {
                    holder.textViewUnidadeMedida.setText(lista.get(position).getDescricao());
                    holder.textViewUnidadeMedida.setTextColor(Color.parseColor("#000000"));
                }
            } catch ( NullPointerException e) {
                holder.textViewUnidadeMedida.setText(lista.get(position).getDescricao());
                holder.textViewUnidadeMedida.setTextColor(Color.parseColor("#000000"));
            } catch ( Exception e) {
                holder.textViewUnidadeMedida.setText(lista.get(position).getDescricao());
                holder.textViewUnidadeMedida.setTextColor(Color.parseColor("#000000"));
            }
        }
        holder.idPosition.setText(String.valueOf(position + 1));
        if (campanha)
            try {
                if (lista.get(position).getIdCampanha() > 0)
                    holder.nomeCampanha.setText(lista.get(position).getNomeCampanha());
                else {
                    holder.nomeCampanha.setText(" ");
                    holder.textViewCampanha.setVisibility(View.INVISIBLE);
                }
            } catch (NullPointerException e) {
            } catch (Exception e) {
            }
        else {
            holder.nomeCampanha.setText(" ");
            holder.textViewCampanha.setVisibility(View.INVISIBLE);
        }
        holder.itemView
                .setBackgroundColor(selectedItems.get(position) ? Color.parseColor("#dfdfdf")
                        : Color.TRANSPARENT);
        if (lista.get(position).getDescontoIndevido())
            holder.txtDesconto.setTextColor(Color.parseColor("#FF0000"));
        else
            holder.txtDesconto.setTextColor(Color.parseColor("#000000"));
        this.holder = holder;
        applyClickEvents(holder, position);
    }

    @Override
    public int getItemCount() {
        if (lista != null)
            return lista.size();
        return 0;
    }

    private void applyClickEvents(final ProdutoPedidoViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRowClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.CONTEXT_CLICK);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onLongRowClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                return false;
            }
        });
        holder.btnExcluirProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonClicked(position);
            }
        });
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false))
            selectedItems.delete(pos);
        else
            selectedItems.put(pos, true);
        notifyItemChanged(pos);
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public WebPedidoItens getItem(int position) {
        return lista.get(position);
    }

    public List<WebPedidoItens> getItensSelecionados() {
        List<WebPedidoItens> webPedidoItensSelecionados = new ArrayList<>();
        for (int i = 0; i < selectedItems.size(); i++)
            webPedidoItensSelecionados.add(lista.get(selectedItems.keyAt(i)));
        return webPedidoItensSelecionados;
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public void getList(List<WebPedidoItens> webPedidoItens) {
        lista = webPedidoItens;
    }

    public interface ProdutoPedidoAdapterListener {

        void onRowClicked(int position);

        void onLongRowClicked(int position);

        void onButtonClicked( int position);
    }
}
