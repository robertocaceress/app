package com.example.rcksuporte05.rcksistemas.model;

import android.annotation.SuppressLint;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ClienteData extends ExpandableGroup<ClienteChildData> {

    @SuppressLint("ParcelCreator")
    public ClienteData(String name, List<ClienteChildData> items) {
        super(name, items);
    }
}
