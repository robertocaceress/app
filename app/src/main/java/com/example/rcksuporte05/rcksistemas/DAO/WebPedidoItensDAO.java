package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;

import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.model.CampanhaComercialCab;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.EmpresaParametro;
import com.example.rcksuporte05.rcksistemas.model.Produto;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class WebPedidoItensDAO {
    private DBHelper db;

    public WebPedidoItensDAO(DBHelper db) {
        this.db = db;
    }

    public long add(WebPedidoItens webPedidoItem) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ID_WEB_ITEM_SERVIDOR", webPedidoItem.getId_web_item_servidor());
            content.put("ID_PEDIDO", webPedidoItem.getId_pedido());
            content.put("ID_PRODUTO", webPedidoItem.getId_produto());
            content.put("ID_EMPRESA", webPedidoItem.getId_empresa());
            content.put("QUANTIDADE", webPedidoItem.getQuantidade());
            content.put("VALOR_UNITARIO", webPedidoItem.getVenda_preco());
            content.put("VALOR_BRUTO", webPedidoItem.getValor_bruto());
            content.put("VALOR_DESCONTO_REAL", webPedidoItem.getValor_desconto_real());
            content.put("VALOR_DESCONTO_PER_ADD", webPedidoItem.getValor_desconto_per_add());
            content.put("VALOR_DESCONTO_REAL_ADD", webPedidoItem.getValor_desconto_real_add());
            content.put("VALOR_TOTAL", webPedidoItem.getValor_total());
            content.put("DATA_MOVIMENTACAO", db.pegaDataAtual());
            content.put("USUARIO_LANCAMENTO_ID", webPedidoItem.getUsuario_lancamento_id());
            content.put("USUARIO_LANCAMENTO_DATA", db.pegaDataHoraAtual());
            content.put("ID_ITEM_DESCONTO", webPedidoItem.getId_item_desconto());
            content.put("PONTOS_UNITARIO", webPedidoItem.getPontos_unitario());
            content.put("PONTOS_TOTAL", webPedidoItem.getPontos_total());
            content.put("PONTOS_COEFICIENTE", webPedidoItem.getPontos_coeficiente());
            content.put("COMISSAO_PERCENTUAL", webPedidoItem.getComissao_percentual());
            content.put("COMISSAO_VALOR", webPedidoItem.getComissao_valor());
            content.put("VALOR_BONUS_CREDOR", webPedidoItem.getValor_bonus_credor());
            content.put("PERC_BONUS_CREDOR", webPedidoItem.getPerc_bonus_credor());
            content.put("VALOR_DESCONTO_PER_ORIG", webPedidoItem.getValor_desconto_per_orig());
            content.put("VALOR_DESCONTO_REAL_ORIG", webPedidoItem.getValor_desconto_real_orig());
            content.put("VALOR_DESCONTO_PER_ADD_ORIG", webPedidoItem.getValor_desconto_per_add_orig());
            content.put("VALOR_DESCONTO_REAL_ADD_ORIG", webPedidoItem.getValor_desconto_real_add_orig());
            content.put("ID_TABELA_PRECO_FAIXA_ORIG", webPedidoItem.getId_tabela_preco_faixa_orig());
            content.put("VALOR_TOTAL_ORIG", webPedidoItem.getValor_total_orig());
            content.put("PONTOS_UNITARIO_ORIG", webPedidoItem.getPontos_unitario_orig());
            content.put("PONTOS_COEFICIENTE_ORIG", webPedidoItem.getPontos_coeficiente_orig());
            content.put("COMISSAO_PERCENTUAL_ORIG", webPedidoItem.getComissao_percentual_orig());
            content.put("VALOR_BONUS_CREDOR_ORIG", webPedidoItem.getValor_bonus_credor_orig());
            content.put("PERC_BONUS_CREDOR_ORIG", webPedidoItem.getPerc_bonus_credor_orig());
            content.put("COMISSAO_VALOR_ORIG", webPedidoItem.getComissao_valor_orig());
            content.put("PONTOS_TOTAL_ORIG", webPedidoItem.getPontos_total_orig());
            content.put("PONTOS_COR_ORIG", webPedidoItem.getPontos_cor_orig());
            content.put("VALOR_PRECO_PAGO", webPedidoItem.getValor_preco_pago());
            content.put("TIPO_DESCONTO", webPedidoItem.getTipoDesconto());
            content.put("VALOR_DESCONTO_PER", webPedidoItem.getValor_desconto_per());
            content.put("NOME_PRODUTO", webPedidoItem.getNome_produto());
            content.put("PRODUTO_MATERIA_PRIMA", webPedidoItem.getProduto_materia_prima());
            content.put("PRODUTO_TERCERIZACAO", webPedidoItem.getProduto_tercerizacao());
 //           content.put("ID_LINHA_COLECAO", webPedidoItem.getIdLinhaColecao());
            content.put("ID_CAMPANHA", webPedidoItem.getIdCampanha());
            content.put("PRODUTO_PRECO_EDITADO", webPedidoItem.getProduto_preco_editado());
            content.put("ID_SKU", webPedidoItem.getId_sku());
            content.put("NOME_SKU", webPedidoItem.getNome_sku());
            content.put("ICMS_ST_VALOR", webPedidoItem.getIcms_st_valor());
            content.put("ICMS_ST_PERCENTUAL", webPedidoItem.getIcms_st_percentual());
            content.put("ICMS_ST_CALCULA", webPedidoItem.getIcms_st_calcula());
            try {
                content.put("VALOR_DESCONTO_ADIC", webPedidoItem.getValor_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("VALOR_DESCONTO_ADIC", webPedidoItem.getValor_desconto_adic());
            } catch ( Exception e) {
                content.put("VALOR_DESCONTO_ADIC", webPedidoItem.getValor_desconto_adic());
            }
            try {
                content.put("PERC_DESCONTO_ADIC", webPedidoItem.getPerc_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("PERC_DESCONTO_ADIC", webPedidoItem.getPerc_desconto_adic());
            } catch ( Exception e) {
                content.put("PERC_DESCONTO_ADIC", webPedidoItem.getPerc_desconto_adic());
            }
            System.gc();
            return db.addDados("TBL_WEB_PEDIDO_ITENS", content);
             //   return true;
            //return false;
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean update(WebPedidoItens webPedidoItem) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ID_WEB_ITEM", webPedidoItem.getId_web_item());
            content.put("ID_WEB_ITEM_SERVIDOR", webPedidoItem.getId_web_item_servidor());
            content.put("ID_PEDIDO", webPedidoItem.getId_pedido());
            content.put("ID_PRODUTO", webPedidoItem.getId_produto());
            content.put("ID_EMPRESA", webPedidoItem.getId_empresa());
            content.put("QUANTIDADE", webPedidoItem.getQuantidade());
            content.put("VALOR_UNITARIO", webPedidoItem.getValor_unitario());
            content.put("VALOR_BRUTO", webPedidoItem.getValor_bruto());
            content.put("VALOR_DESCONTO_REAL", webPedidoItem.getValor_desconto_real());
            content.put("VALOR_DESCONTO_PER_ADD", webPedidoItem.getValor_desconto_per_add());
            content.put("VALOR_DESCONTO_REAL_ADD", webPedidoItem.getValor_desconto_real_add());
            content.put("VALOR_TOTAL", webPedidoItem.getValor_total());
            content.put("DATA_MOVIMENTACAO", webPedidoItem.getData_movimentacao());
            content.put("USUARIO_LANCAMENTO_ID", webPedidoItem.getUsuario_lancamento_id());
            content.put("USUARIO_LANCAMENTO_DATA", webPedidoItem.getUsuario_lancamento_data());
            content.put("ID_ITEM_DESCONTO", webPedidoItem.getId_item_desconto());
            content.put("PONTOS_UNITARIO", webPedidoItem.getPontos_unitario());
            content.put("PONTOS_TOTAL", webPedidoItem.getPontos_total());
            content.put("PONTOS_COEFICIENTE", webPedidoItem.getPontos_coeficiente());
            content.put("PONTOS_COR", webPedidoItem.getPontos_cor());
            content.put("COMISSAO_PERCENTUAL", webPedidoItem.getComissao_percentual());
            content.put("COMISSAO_VALOR", webPedidoItem.getComissao_valor());
            content.put("VALOR_BONUS_CREDOR", webPedidoItem.getValor_bonus_credor());
            content.put("PERC_BONUS_CREDOR", webPedidoItem.getPerc_bonus_credor());
            content.put("ID_TABELA_PRECO", webPedidoItem.getId_tabela_preco());
            content.put("VALOR_DESCONTO_PER_ORIG", webPedidoItem.getValor_desconto_per_orig());
            content.put("VALOR_DESCONTO_REAL_ORIG", webPedidoItem.getValor_desconto_real_orig());
            content.put("VALOR_DESCONTO_PER_ADD_ORIG", webPedidoItem.getValor_desconto_per_add_orig());
            content.put("VALOR_DESCONTO_REAL_ADD_ORIG", webPedidoItem.getValor_desconto_real_add_orig());
            content.put("ID_TABELA_PRECO_FAIXA_ORIG", webPedidoItem.getId_tabela_preco_faixa_orig());
            content.put("VALOR_TOTAL_ORIG", webPedidoItem.getValor_total_orig());
            content.put("PONTOS_UNITARIO_ORIG", webPedidoItem.getPontos_unitario_orig());
            content.put("PONTOS_COEFICIENTE_ORIG", webPedidoItem.getPontos_coeficiente_orig());
            content.put("COMISSAO_PERCENTUAL_ORIG", webPedidoItem.getComissao_percentual_orig());
            content.put("VALOR_BONUS_CREDOR_ORIG", webPedidoItem.getValor_bonus_credor_orig());
            content.put("PERC_BONUS_CREDOR_ORIG", webPedidoItem.getPerc_bonus_credor_orig());
            content.put("COMISSAO_VALOR_ORIG", webPedidoItem.getComissao_valor_orig());
            content.put("PONTOS_TOTAL_ORIG", webPedidoItem.getPontos_total_orig());
            content.put("PONTOS_COR_ORIG", webPedidoItem.getPontos_cor_orig());
            content.put("VALOR_PRECO_PAGO", webPedidoItem.getValor_preco_pago());
            content.put("TIPO_DESCONTO", webPedidoItem.getTipoDesconto());
            content.put("VALOR_DESCONTO_PER", webPedidoItem.getValor_desconto_per());
            content.put("NOME_PRODUTO", webPedidoItem.getNome_produto());
            content.put("PRODUTO_MATERIA_PRIMA", webPedidoItem.getProduto_materia_prima());
            content.put("PRODUTO_TERCERIZACAO", webPedidoItem.getProduto_tercerizacao());
//            content.put("ID_LINHA_COLECAO", webPedidoItem.getIdLinhaColecao());
            content.put("ID_CAMPANHA", webPedidoItem.getIdCampanha());
            content.put("PRODUTO_PRECO_EDITADO", webPedidoItem.getProduto_preco_editado());
            content.put("ID_SKU", webPedidoItem.getId_sku());
            content.put("NOME_SKU", webPedidoItem.getNome_sku());
            content.put("ICMS_ST_VALOR", webPedidoItem.getIcms_st_valor());
            content.put("ICMS_ST_PERCENTUAL", webPedidoItem.getIcms_st_percentual());
            content.put("ICMS_ST_CALCULA", webPedidoItem.getIcms_st_calcula());
            try {
                content.put("VALOR_DESCONTO_ADIC", webPedidoItem.getValor_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("VALOR_DESCONTO_ADIC", webPedidoItem.getValor_desconto_adic());
            } catch ( Exception e) {
                content.put("VALOR_DESCONTO_ADIC", webPedidoItem.getValor_desconto_adic());
            }
            try {
                content.put("PERC_DESCONTO_ADIC", webPedidoItem.getPerc_desconto_adic().replaceAll("\\s+", "").replace(",", "."));
            } catch ( NullPointerException|NumberFormatException e) {
                content.put("PERC_DESCONTO_ADIC", webPedidoItem.getPerc_desconto_adic());
            } catch ( Exception e) {
                content.put("PERC_DESCONTO_ADIC", webPedidoItem.getPerc_desconto_adic());
            }
            if (db.updateDados("TBL_WEB_PEDIDO_ITENS", content, "ID_WEB_ITEM = " + webPedidoItem.getId_web_item()) >= 1)
                return true;
            return false;
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<WebPedidoItens> getLista(String SQL) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        List<WebPedidoItens> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        ProdutoDAO produtoDAO = new ProdutoDAO(db);
        do {
            WebPedidoItens webPedidoItens = new WebPedidoItens();
            try {
                webPedidoItens.setId_web_item(cursor.getString(cursor.getColumnIndex("ID_WEB_ITEM")));
                webPedidoItens.setId_web_item_servidor(cursor.getString(cursor.getColumnIndex("ID_WEB_ITEM_SERVIDOR")));
                webPedidoItens.setValor_preco_pago(cursor.getString(cursor.getColumnIndex("VALOR_PRECO_PAGO")));
                webPedidoItens.setId_pedido(cursor.getString(cursor.getColumnIndex("ID_PEDIDO")));
                webPedidoItens.setId_produto(cursor.getString(cursor.getColumnIndex("ID_PRODUTO")));
                webPedidoItens.setId_empresa(cursor.getString(cursor.getColumnIndex("ID_EMPRESA")));
                webPedidoItens.setQuantidade(cursor.getString(cursor.getColumnIndex("QUANTIDADE")));
                webPedidoItens.setValor_unitario(cursor.getFloat(cursor.getColumnIndex("VALOR_UNITARIO")));
                webPedidoItens.setValor_bruto(cursor.getString(cursor.getColumnIndex("VALOR_BRUTO")));
                webPedidoItens.setValor_desconto_per(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER")));
                webPedidoItens.setValor_desconto_real(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL")));
                webPedidoItens.setValor_desconto_per_add(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER_ADD")));
                webPedidoItens.setValor_desconto_real_add(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL_ADD")));
                webPedidoItens.setValor_total(cursor.getString(cursor.getColumnIndex("VALOR_TOTAL")));
                webPedidoItens.setData_movimentacao(cursor.getString(cursor.getColumnIndex("DATA_MOVIMENTACAO")));
                webPedidoItens.setUsuario_lancamento_id(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_ID")));
                webPedidoItens.setUsuario_lancamento_data(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_DATA")));
                webPedidoItens.setId_item_desconto(cursor.getString(cursor.getColumnIndex("ID_ITEM_DESCONTO")));
                webPedidoItens.setPontos_unitario(cursor.getString(cursor.getColumnIndex("PONTOS_UNITARIO")));
                webPedidoItens.setPontos_total(cursor.getString(cursor.getColumnIndex("PONTOS_TOTAL")));
                webPedidoItens.setPontos_coeficiente(cursor.getString(cursor.getColumnIndex("PONTOS_COEFICIENTE")));
                webPedidoItens.setPontos_cor(cursor.getString(cursor.getColumnIndex("PONTOS_COR")));
                webPedidoItens.setComissao_percentual(cursor.getString(cursor.getColumnIndex("COMISSAO_PERCENTUAL")));
                webPedidoItens.setComissao_valor(cursor.getString(cursor.getColumnIndex("COMISSAO_VALOR")));
                webPedidoItens.setValor_bonus_credor(cursor.getString(cursor.getColumnIndex("VALOR_BONUS_CREDOR")));
                webPedidoItens.setPerc_bonus_credor(cursor.getString(cursor.getColumnIndex("PERC_BONUS_CREDOR")));
                webPedidoItens.setId_tabela_preco(cursor.getString(cursor.getColumnIndex("ID_TABELA_PRECO")));
                webPedidoItens.setValor_desconto_per_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER_ORIG")));
                webPedidoItens.setValor_desconto_real_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL_ORIG")));
                webPedidoItens.setValor_desconto_per_add_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER_ADD_ORIG")));
                webPedidoItens.setValor_desconto_real_add_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL_ADD_ORIG")));
                webPedidoItens.setId_tabela_preco_faixa_orig(cursor.getString(cursor.getColumnIndex("ID_TABELA_PRECO_FAIXA_ORIG")));
                webPedidoItens.setValor_total_orig(cursor.getString(cursor.getColumnIndex("VALOR_TOTAL_ORIG")));
                webPedidoItens.setPontos_unitario_orig(cursor.getString(cursor.getColumnIndex("PONTOS_UNITARIO_ORIG")));
                webPedidoItens.setPontos_coeficiente_orig(cursor.getString(cursor.getColumnIndex("PONTOS_COEFICIENTE_ORIG")));
                webPedidoItens.setComissao_percentual_orig(cursor.getString(cursor.getColumnIndex("COMISSAO_PERCENTUAL_ORIG")));
                webPedidoItens.setValor_bonus_credor_orig(cursor.getString(cursor.getColumnIndex("VALOR_BONUS_CREDOR_ORIG")));
                webPedidoItens.setPerc_bonus_credor_orig(cursor.getString(cursor.getColumnIndex("PERC_BONUS_CREDOR_ORIG")));
                webPedidoItens.setComissao_valor_orig(cursor.getString(cursor.getColumnIndex("COMISSAO_VALOR_ORIG")));
                webPedidoItens.setPontos_total_orig(cursor.getString(cursor.getColumnIndex("PONTOS_TOTAL_ORIG")));
                webPedidoItens.setPontos_cor_orig(cursor.getString(cursor.getColumnIndex("PONTOS_COR_ORIG")));
                webPedidoItens.setTipoDesconto(cursor.getString(cursor.getColumnIndex("TIPO_DESCONTO")));
                webPedidoItens.setProduto_materia_prima(cursor.getString(cursor.getColumnIndex("PRODUTO_MATERIA_PRIMA")));
                webPedidoItens.setProduto_tercerizacao(cursor.getString(cursor.getColumnIndex("PRODUTO_TERCERIZACAO")));
                //webPedidoItens.setIdLinhaColecao( cursor.getInt(cursor.getColumnIndex("ID_LINHA_COLECAO")) );
                webPedidoItens.setIdCampanha(cursor.getInt(cursor.getColumnIndex("ID_CAMPANHA")));
                webPedidoItens.setProduto_edita_valor("N");
                webPedidoItens.setIcms_st_valor(cursor.getString(cursor.getColumnIndex("ICMS_ST_VALOR")));
                webPedidoItens.setIcms_st_calcula(cursor.getString(cursor.getColumnIndex("ICMS_ST_CALCULA")));
                webPedidoItens.setIcms_st_percentual(cursor.getFloat(cursor.getColumnIndex("ICMS_ST_PERCENTUAL")));
                try {
                    webPedidoItens.setProduto_preco_editado(cursor.getFloat(cursor.getColumnIndex("PRODUTO_PRECO_EDITADO")));
                } catch ( NumberFormatException|NullPointerException e) {
                    webPedidoItens.setProduto_preco_editado(0.00f);
                } catch (Exception e) {
                    webPedidoItens.setProduto_preco_editado(0.00f);
                }

                webPedidoItens.setProdutoAtivo(true);
                try {
                    webPedidoItens.setId_sku(cursor.getInt(cursor.getColumnIndex("ID_SKU")));
                    webPedidoItens.setNome_sku(cursor.getString(cursor.getColumnIndex("NOME_SKU")));
                } catch ( NullPointerException e) {
                    webPedidoItens.setId_sku(0);
                    webPedidoItens.setNome_sku("");
                } catch ( Exception e) {
                    webPedidoItens.setId_sku(0);
                    webPedidoItens.setNome_sku("");
                }
                try {  //Caso nao exista no SQL
                    webPedidoItens.setNomeCampanha(cursor.getString(cursor.getColumnIndex("NOME_CAMPANHA")));
                } catch (NullPointerException e) {
                    webPedidoItens.setNomeCampanha("");
                } catch (Exception e) {
                    webPedidoItens.setNomeCampanha("");
                }

                try {
                    webPedidoItens.setValor_desconto_adic(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_ADIC")));
                    webPedidoItens.setPerc_desconto_adic(cursor.getString(cursor.getColumnIndex("PERC_DESCONTO_ADIC")));
                } catch ( NullPointerException e) {
                    webPedidoItens.setValor_desconto_adic("0.00");
                    webPedidoItens.setPerc_desconto_adic("0.00");
                }catch ( Exception e) {
                    webPedidoItens.setValor_desconto_adic("0.00");
                    webPedidoItens.setPerc_desconto_adic("0.00");
                }

                try {
                    try {
                        String SQLP = "SELECT P.*,  TI.VALOR_UNITARIO AS PRECO_TABELA_CLIENTE, TC.NOME_TABELA AS NOME_TABELA FROM TBL_PRODUTO AS P " +
                                " LEFT JOIN  TBL_TABELA_PRECO_ITENS AS TI ON TI.ID_PRODUTO = P.ID_PRODUTO  AND TI.ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco() +
                                " LEFT JOIN TBL_TABELA_PRECO_CAB   AS TC ON TC.ID_TABELA = TI.ID_TABELA " +
                                " WHERE P.ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'";
                        Produto produto = produtoDAO.getLista(SQLP,"").get(0);
                        webPedidoItens.setProduto(produto);
                        webPedidoItens.setProdutoBase(true);
                        if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                            webPedidoItens.setProduto_edita_valor("S");
                    } catch ( CursorIndexOutOfBoundsException e) {
                        try {
                            Produto produto = produtoDAO.getLista("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'","").get(0);
                            webPedidoItens.setProduto(produto);
                            if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                                webPedidoItens.setProduto_edita_valor("S");
                        } catch (CursorIndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        } catch (IndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        }
                        webPedidoItens.setProdutoBase(true);
                    }  catch ( IndexOutOfBoundsException e){
                        try {
                            Produto produto = produtoDAO.getLista("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'","").get(0);
                            webPedidoItens.setProduto(produto);
                            if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                                webPedidoItens.setProduto_edita_valor("S");
                        } catch (CursorIndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        } catch (IndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        }
                        webPedidoItens.setProdutoBase(true);
                    } catch (NullPointerException e) {
                        try {
                            Produto produto = produtoDAO.getLista("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'","").get(0);
                            webPedidoItens.setProduto(produto);
                            if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                                webPedidoItens.setProduto_edita_valor("S");
                        } catch ( CursorIndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        } catch ( IndexOutOfBoundsException ex){
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        }
                        webPedidoItens.setProdutoBase(true);
                    }
                } catch (CursorIndexOutOfBoundsException e) {
                    webPedidoItens.setProdutoBase(false);
                    webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                    webPedidoItens.setProdutoAtivo(false);
                    e.printStackTrace();
                }
                lista.add(webPedidoItens);
            }catch ( CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            System.gc();
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public boolean delete(Context context, List<WebPedidoItens> webPedidoItens) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        db = new DBHelper(context);
        for (WebPedidoItens webPedidoIten : webPedidoItens) {
            try {
                if( db.deleteDados( "TBL_WEB_PEDIDO_ITENS", "ID_PEDIDO =? AND ID_WEB_ITEM=?", new String[]{webPedidoIten.getId_pedido(), webPedidoIten.getId_web_item()} ) <= 0) {
                    return false;
                }
            } catch ( Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        return true;
    }

    public boolean delete(Context context, WebPedidoItens webPedidoIten) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        db = new DBHelper(context);
        try {
            if( db.deleteDados( "TBL_WEB_PEDIDO_ITENS", "ID_PEDIDO =? AND ID_WEB_ITEM=?", new String[]{webPedidoIten.getId_pedido(), webPedidoIten.getId_web_item().trim()} ) >= 1)
                return true;
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<WebPedidoItens> getListaDuplicacao(String SQL, Configuracao configuracao, EmpresaParametro empresaParametro, Float saldoVerba,
                                                   Cliente cliente, CampanhaComercialCabDAO campanhaComercialCabDAO) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        List<WebPedidoItens> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        ProdutoDAO produtoDAO = new ProdutoDAO(db);
        Produto produto = new Produto();
        do {
            WebPedidoItens webPedidoItens = new WebPedidoItens();

            try {
                webPedidoItens.setId_web_item(cursor.getString(cursor.getColumnIndex("ID_WEB_ITEM")));
                webPedidoItens.setId_web_item_servidor(cursor.getString(cursor.getColumnIndex("ID_WEB_ITEM_SERVIDOR")));
                webPedidoItens.setValor_preco_pago(cursor.getString(cursor.getColumnIndex("VALOR_PRECO_PAGO")));
                webPedidoItens.setId_pedido(cursor.getString(cursor.getColumnIndex("ID_PEDIDO")));
                webPedidoItens.setId_produto(cursor.getString(cursor.getColumnIndex("ID_PRODUTO")));
                webPedidoItens.setId_empresa(cursor.getString(cursor.getColumnIndex("ID_EMPRESA")));
                webPedidoItens.setQuantidade(cursor.getString(cursor.getColumnIndex("QUANTIDADE")));
                webPedidoItens.setValor_unitario(cursor.getFloat(cursor.getColumnIndex("VALOR_UNITARIO")));
                webPedidoItens.setValor_bruto(cursor.getString(cursor.getColumnIndex("VALOR_BRUTO")));
                webPedidoItens.setValor_desconto_per(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER")));
                webPedidoItens.setValor_desconto_real(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL")));
                webPedidoItens.setValor_desconto_per_add(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER_ADD")));
                webPedidoItens.setValor_desconto_real_add(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL_ADD")));
                webPedidoItens.setValor_total(cursor.getString(cursor.getColumnIndex("VALOR_TOTAL")));
                webPedidoItens.setData_movimentacao(cursor.getString(cursor.getColumnIndex("DATA_MOVIMENTACAO")));
                webPedidoItens.setUsuario_lancamento_id(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_ID")));
                webPedidoItens.setUsuario_lancamento_data(cursor.getString(cursor.getColumnIndex("USUARIO_LANCAMENTO_DATA")));
                webPedidoItens.setId_item_desconto(cursor.getString(cursor.getColumnIndex("ID_ITEM_DESCONTO")));
                webPedidoItens.setPontos_unitario(cursor.getString(cursor.getColumnIndex("PONTOS_UNITARIO")));
                webPedidoItens.setPontos_total(cursor.getString(cursor.getColumnIndex("PONTOS_TOTAL")));
                webPedidoItens.setPontos_coeficiente(cursor.getString(cursor.getColumnIndex("PONTOS_COEFICIENTE")));
                webPedidoItens.setPontos_cor(cursor.getString(cursor.getColumnIndex("PONTOS_COR")));
                webPedidoItens.setComissao_percentual(cursor.getString(cursor.getColumnIndex("COMISSAO_PERCENTUAL")));
                webPedidoItens.setComissao_valor(cursor.getString(cursor.getColumnIndex("COMISSAO_VALOR")));
                webPedidoItens.setValor_bonus_credor(cursor.getString(cursor.getColumnIndex("VALOR_BONUS_CREDOR")));
                webPedidoItens.setPerc_bonus_credor(cursor.getString(cursor.getColumnIndex("PERC_BONUS_CREDOR")));
                webPedidoItens.setId_tabela_preco(cursor.getString(cursor.getColumnIndex("ID_TABELA_PRECO")));
                webPedidoItens.setValor_desconto_per_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER_ORIG")));
                webPedidoItens.setValor_desconto_real_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL_ORIG")));
                webPedidoItens.setValor_desconto_per_add_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_PER_ADD_ORIG")));
                webPedidoItens.setValor_desconto_real_add_orig(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_REAL_ADD_ORIG")));
                webPedidoItens.setId_tabela_preco_faixa_orig(cursor.getString(cursor.getColumnIndex("ID_TABELA_PRECO_FAIXA_ORIG")));
                webPedidoItens.setValor_total_orig(cursor.getString(cursor.getColumnIndex("VALOR_TOTAL_ORIG")));
                webPedidoItens.setPontos_unitario_orig(cursor.getString(cursor.getColumnIndex("PONTOS_UNITARIO_ORIG")));
                webPedidoItens.setPontos_coeficiente_orig(cursor.getString(cursor.getColumnIndex("PONTOS_COEFICIENTE_ORIG")));
                webPedidoItens.setComissao_percentual_orig(cursor.getString(cursor.getColumnIndex("COMISSAO_PERCENTUAL_ORIG")));
                webPedidoItens.setValor_bonus_credor_orig(cursor.getString(cursor.getColumnIndex("VALOR_BONUS_CREDOR_ORIG")));
                webPedidoItens.setPerc_bonus_credor_orig(cursor.getString(cursor.getColumnIndex("PERC_BONUS_CREDOR_ORIG")));
                webPedidoItens.setComissao_valor_orig(cursor.getString(cursor.getColumnIndex("COMISSAO_VALOR_ORIG")));
                webPedidoItens.setPontos_total_orig(cursor.getString(cursor.getColumnIndex("PONTOS_TOTAL_ORIG")));
                webPedidoItens.setPontos_cor_orig(cursor.getString(cursor.getColumnIndex("PONTOS_COR_ORIG")));
                webPedidoItens.setTipoDesconto(cursor.getString(cursor.getColumnIndex("TIPO_DESCONTO")));
                webPedidoItens.setProduto_materia_prima(cursor.getString(cursor.getColumnIndex("PRODUTO_MATERIA_PRIMA")));
                webPedidoItens.setProduto_tercerizacao(cursor.getString(cursor.getColumnIndex("PRODUTO_TERCERIZACAO")));
                //webPedidoItens.setIdLinhaColecao( cursor.getInt(cursor.getColumnIndex("ID_LINHA_COLECAO")) );
                webPedidoItens.setIdCampanha(cursor.getInt(cursor.getColumnIndex("ID_CAMPANHA")));
                //webPedidoItens.setNomeCampanha(cursor.getString(cursor.getColumnIndex("NOME_CAMPANHA")));
                webPedidoItens.setProduto_edita_valor("N");
                webPedidoItens.setIcms_st_valor(cursor.getString(cursor.getColumnIndex("ICMS_ST_VALOR")));
                webPedidoItens.setIcms_st_calcula(cursor.getString(cursor.getColumnIndex("ICMS_ST_CALCULA")));
                webPedidoItens.setIcms_st_percentual(cursor.getFloat(cursor.getColumnIndex("ICMS_ST_PERCENTUAL")));
                try {
                    webPedidoItens.setProduto_preco_editado(cursor.getFloat(cursor.getColumnIndex("PRODUTO_PRECO_EDITADO")));
                } catch ( NumberFormatException|NullPointerException e) {
                    webPedidoItens.setProduto_preco_editado(0.00f);
                } catch (Exception e) {
                    webPedidoItens.setProduto_preco_editado(0.00f);
                }

                webPedidoItens.setProdutoAtivo(true);
                try {
                    webPedidoItens.setId_sku(cursor.getInt(cursor.getColumnIndex("ID_SKU")));
                    webPedidoItens.setNome_sku(cursor.getString(cursor.getColumnIndex("NOME_SKU")));
                } catch ( NullPointerException e) {
                    webPedidoItens.setId_sku(0);
                    webPedidoItens.setNome_sku("");
                } catch ( Exception e) {
                    webPedidoItens.setId_sku(0);
                    webPedidoItens.setNome_sku("");
                }


                try {
                    webPedidoItens.setValor_desconto_adic(cursor.getString(cursor.getColumnIndex("VALOR_DESCONTO_ADIC")));
                    webPedidoItens.setPerc_desconto_adic(cursor.getString(cursor.getColumnIndex("PERC_DESCONTO_ADIC")));
                } catch ( NullPointerException e) {
                    webPedidoItens.setValor_desconto_adic("0.00");
                    webPedidoItens.setPerc_desconto_adic("0.00");
                }catch ( Exception e) {
                    webPedidoItens.setValor_desconto_adic("0.00");
                    webPedidoItens.setPerc_desconto_adic("0.00");
                }

                try {
                    try {
                        String SQLP = "SELECT P.*,  TI.VALOR_UNITARIO AS PRECO_TABELA_CLIENTE, TC.NOME_TABELA AS NOME_TABELA FROM TBL_PRODUTO AS P " +
                                " LEFT JOIN  TBL_TABELA_PRECO_ITENS AS TI ON TI.ID_PRODUTO = P.ID_PRODUTO  AND TI.ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco() +
                                " LEFT JOIN TBL_TABELA_PRECO_CAB   AS TC ON TC.ID_TABELA = TI.ID_TABELA " +
                                " WHERE P.ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'";
                        produto = produtoDAO.getLista(SQLP,"").get(0);
                        webPedidoItens.setProduto(produto);
                        webPedidoItens.setProdutoBase(true);

                        if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                            webPedidoItens.setProduto_edita_valor("S");
                        //if ( webPedidoItens.getValor_unitario() != Float.parseFloat(produto.getVenda_preco())){
                            webPedidoItens = calculaValorItem(produto, webPedidoItens, configuracao, empresaParametro, saldoVerba);
                        //}
                    } catch ( CursorIndexOutOfBoundsException e) {
                        try {
                            produto = produtoDAO.getLista("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'","").get(0);
                            webPedidoItens.setProduto(produto);
                            if ( webPedidoItens.getValor_unitario() != Float.parseFloat(produto.getVenda_preco())){
                                webPedidoItens = calculaValorItem(produto, webPedidoItens, configuracao, empresaParametro, saldoVerba);
                            }
                            if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                                webPedidoItens.setProduto_edita_valor("S");
                        } catch (CursorIndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        } catch (IndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        }
                        webPedidoItens.setProdutoBase(true);
                    }  catch ( IndexOutOfBoundsException e){
                        try {
                            produto = produtoDAO.getLista("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'","").get(0);
                            webPedidoItens.setProduto(produto);

                            webPedidoItens = calculaValorItem(produto, webPedidoItens, configuracao, empresaParametro, saldoVerba);

                            if ( produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                                webPedidoItens.setProduto_edita_valor("S");
                        } catch (CursorIndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        } catch (IndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        }
                        webPedidoItens.setProdutoBase(true);
                    } catch (NullPointerException e) {
                        try {
                            produto = produtoDAO.getLista("SELECT * FROM TBL_PRODUTO WHERE ID_PRODUTO = '" + webPedidoItens.getId_produto() + "'", "").get(0);
                            webPedidoItens.setProduto(produto);

                            webPedidoItens = calculaValorItem(produto, webPedidoItens, configuracao, empresaParametro, saldoVerba);

                            if (produto.getProduto_edita_valor().equalsIgnoreCase("S"))
                                webPedidoItens.setProduto_edita_valor("S");
                        } catch (CursorIndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        } catch (IndexOutOfBoundsException ex) {
                            webPedidoItens.setProdutoAtivo(false);
                            webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                        }
                        webPedidoItens.setProdutoBase(true);
                    }
                } catch (CursorIndexOutOfBoundsException e) {
                    webPedidoItens.setProdutoBase(false);
                    webPedidoItens.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                    webPedidoItens.setProdutoAtivo(false);
                    e.printStackTrace();
                }

                try {
                    if (configuracao.getTrabalha_com_campanhas().equalsIgnoreCase("S")) {
                        try {
                            List<CampanhaComercialCab> listaCampanha = campanhaComercialCabDAO.getLista( String.valueOf(cliente.getId_cadastro_servidor()), produto.getId_produto(), String.valueOf(produto.getIdLinhaColecao()), webPedidoItens.getIdCampanha());
                            if (listaCampanha.size() > 0) {
                                webPedidoItens.setIdCampanha(listaCampanha.get(0).getIdCampanha());
                                webPedidoItens.setNomeCampanha(listaCampanha.get(0).getNomeCampanha());
                            } else {
                                webPedidoItens.setNomeCampanha("");
                                webPedidoItens.setIdCampanha(0);
                            }
                        } catch (CursorIndexOutOfBoundsException|NullPointerException e) {
                            webPedidoItens.setNomeCampanha("");
                            webPedidoItens.setIdCampanha(0);
                        } catch ( Exception e) {
                           e.printStackTrace();
                        }
                    } else {
                        webPedidoItens.setNomeCampanha("");
                        webPedidoItens.setIdCampanha(0);
                    }
                } catch ( NullPointerException ex) {
                } catch ( Exception ex) {
                }



                lista.add(webPedidoItens);
            }catch ( CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            System.gc();
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    private WebPedidoItens calculaValorItem(Produto produto, WebPedidoItens webPedidoItens, Configuracao configuracao, EmpresaParametro empresaParametro, Float saldoVerba) {

        Float totalProdutoBruto = 0.0f;
        Float desconto = 0.0f;
        Float descontoVerba = 0.0f;
        Float valorIcmsST = 0.0f;
        BigDecimal bdesconto = null;
        BigDecimal bDescontoVerba = null;
        BigDecimal btotalBruto = null;
        BigDecimal bValorIcmsST = null;
        btotalBruto = new BigDecimal( Float.parseFloat(webPedidoItens.getQuantidade()) * Float.parseFloat(produto.getVenda_preco())).setScale(2, RoundingMode.HALF_EVEN);
        totalProdutoBruto = Float.parseFloat(String.valueOf(btotalBruto));
        if ( !webPedidoItens.getValor_desconto_per().equalsIgnoreCase("0")) {
            bdesconto = new BigDecimal((Float.parseFloat(webPedidoItens.getValor_desconto_per()) * (totalProdutoBruto)) / 100).setScale(3, RoundingMode.HALF_EVEN);
            int tamanho = String.valueOf(bdesconto).length();

            if (String.valueOf(bdesconto).substring(tamanho - 1).equalsIgnoreCase("5")) {
                if (((Integer.parseInt(String.valueOf(bdesconto).substring(tamanho - 2, tamanho - 1))) % 2) == 0) {
                    bdesconto = new BigDecimal((Float.parseFloat(webPedidoItens.getValor_desconto_per()) * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.DOWN);
                } else {
                    bdesconto = new BigDecimal((Float.parseFloat(webPedidoItens.getValor_desconto_per()) * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.UP);
                }
            } else {
                bdesconto = new BigDecimal((Float.parseFloat(webPedidoItens.getValor_desconto_per()) * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.HALF_EVEN);
            }

            //bdesconto = new BigDecimal((Float.parseFloat(edtDesconto.getText().toString()) * (totalProdutoBruto)) / 100).setScale(3, RoundingMode.DOWN);
            try {
                desconto = Float.parseFloat(String.valueOf(bdesconto));
            } catch (NullPointerException | NumberFormatException e) {

            } catch (Exception e) {

            }
            //edtDescontoReais.setText(MascaraUtil.duasCasaDecimal(desconto));
            if ( saldoVerba > 0.00f) {
                if (!webPedidoItens.getPerc_desconto_adic().equalsIgnoreCase("0")) {
                    bDescontoVerba = new BigDecimal((Float.parseFloat(webPedidoItens.getPerc_desconto_adic()) * (totalProdutoBruto - desconto)) / 100).setScale(3, RoundingMode.HALF_EVEN);
                    tamanho = String.valueOf(bDescontoVerba).length();
                    if (String.valueOf(bDescontoVerba).substring(tamanho - 1).equalsIgnoreCase("5")) {
                        if (((Integer.parseInt(String.valueOf(bDescontoVerba).substring(tamanho - 2, tamanho - 1))) % 2) == 0) {
                            bDescontoVerba = new BigDecimal((Float.parseFloat(webPedidoItens.getPerc_desconto_adic()) * (totalProdutoBruto - desconto)) / 100).setScale(2, RoundingMode.DOWN);
                        } else {
                            bDescontoVerba = new BigDecimal((Float.parseFloat(webPedidoItens.getPerc_desconto_adic()) * (totalProdutoBruto - desconto)) / 100).setScale(2, RoundingMode.UP);
                        }
                    } else {
                        bDescontoVerba = new BigDecimal((Float.parseFloat(webPedidoItens.getPerc_desconto_adic()) * (totalProdutoBruto - desconto)) / 100).setScale(2, RoundingMode.HALF_EVEN);
                    }
                    //bDescontoVerba = new BigDecimal((Float.parseFloat(edtDescontoVerba.getText().toString()) * (totalProdutoBruto - desconto)) / 100).setScale(3, RoundingMode.DOWN);
                }
                try {
                    descontoVerba = Float.parseFloat(String.valueOf(bDescontoVerba));
                } catch (NullPointerException | NumberFormatException e) {

                } catch (Exception e) {

                }
            } else {
                webPedidoItens.setPerc_desconto_adic("0");
            }
            try {
                if (configuracao.getTrabalha_com_icms_st().equalsIgnoreCase("S") && empresaParametro.getEndereco_uf().equalsIgnoreCase(ClienteHelper.getCliente().getEndereco_uf())) {
                    if (webPedidoItens.getIcms_st_calcula().equalsIgnoreCase("S")) {
                        if (webPedidoItens.getIcms_st_percentual() > 0.00f) {
                            bValorIcmsST = new BigDecimal((webPedidoItens.getIcms_st_percentual() * (totalProdutoBruto - (desconto + descontoVerba))) / 100).setScale(4, RoundingMode.HALF_EVEN);
                            tamanho = String.valueOf(bValorIcmsST).length();
                            if ((String.valueOf(bValorIcmsST).substring(tamanho - 2, tamanho - 1)).trim().equalsIgnoreCase("5")) {
                                if (((Integer.parseInt(String.valueOf(bValorIcmsST).substring(tamanho - 2, tamanho - 1))) % 2) == 0) {
                                    bValorIcmsST = new BigDecimal((webPedidoItens.getIcms_st_percentual() * (totalProdutoBruto - (desconto + descontoVerba))) / 100).setScale(2, RoundingMode.UP);
                                } else {
                                    bValorIcmsST = new BigDecimal((webPedidoItens.getIcms_st_percentual() * (totalProdutoBruto - (desconto + descontoVerba))) / 100).setScale(2, RoundingMode.DOWN /*RoundingMode.HALF_EVEN*/);
                                }
                            } else {
                                bValorIcmsST = new BigDecimal((webPedidoItens.getIcms_st_percentual() * (totalProdutoBruto - (desconto + descontoVerba))) / 100).setScale(2, RoundingMode.HALF_EVEN);
                            }
                            //bValorIcmsST = new BigDecimal((webPedidoItem.getIcms_st_percentual() * (totalProdutoBruto - (desconto + descontoVerba))) / 100).setScale(3, RoundingMode.DOWN);
                        }
                    }
                }
            } catch (NullPointerException | NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                valorIcmsST = Float.parseFloat(String.valueOf(bValorIcmsST));
                //edtvalorIcmsST.setText(String.valueOf(valorIcmsST));
                webPedidoItens.setIcms_st_valor(String.valueOf(valorIcmsST));
            } catch (NullPointerException | NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*
            if ( !edtDescontoVerba.getText().toString().isEmpty()) {
                edtTotal.setText(MascaraUtil.mascaraVirgula((totalProdutoBruto + valorIcmsST) - (desconto + descontoVerba)));
            } else {
                edtTotal.setText(MascaraUtil.mascaraVirgula((totalProdutoBruto + valorIcmsST) - desconto));
            }

             */
        }
        webPedidoItens.setValor_unitario(Float.parseFloat(produto.getVenda_preco()));
        webPedidoItens.setIcms_st_percentual(produto.getProduto_st_aliquota());
        webPedidoItens.setIcms_st_valor(String.valueOf(valorIcmsST));
        webPedidoItens.setIcms_st_calcula(produto.getProduto_suj_icms_st());
        webPedidoItens.setValor_total(String.valueOf((totalProdutoBruto + valorIcmsST) - (desconto + descontoVerba)));
        webPedidoItens.setValor_bruto(String.valueOf((totalProdutoBruto + valorIcmsST)));
        webPedidoItens.setValor_desconto_real(String.valueOf(desconto));
        webPedidoItens.setValor_desconto_adic(String.valueOf(descontoVerba));
        return  webPedidoItens;
    }


}
