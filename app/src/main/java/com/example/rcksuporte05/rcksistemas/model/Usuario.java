package com.example.rcksuporte05.rcksistemas.model;

public class Usuario {

    private String id_usuario;
    private String ativo;
    private String nome_usuario;
    private String login;
    private String senha;
    private String senha_confirma;
    private String data_cadastro;
    private String usuario_cadatro;
    private String data_alterado;
    private String usuario_alterou;
    private String aparece_cad_usuario;
    private String cliente_lista_todos;
    private String cliente_lista_setor;
    private String cliente_lista_representante;
    private String pedido_lista_todos;
    private String pedido_lista_setor;
    private String pedido_lista_representante;
    private String mensagem_lista_financeiro;
    private String mensagem_lista_todos;
    private String mensagem_lista_setor;
    private String mensagem_lista_representante;
    private String orcamento_lista_todos;
    private String orcamento_lista_setor;
    private String orcamento_lista_representante;
    private String usuario_lista_todos;
    private String usuario_lista_setor;
    private String usuario_lista_representante;
    private String excluido;
    private String id_setor;
    private String id_quando_vendedor;
    private String aparelho_id;
    private String token;
    private String idEmpresaMultiDevice;
    private String logado;
    private String email_vendedor;
    public String  aparelho_key_firebase;
    private String aparelho_versao_app;
    private String aparelho_notifica_update_app;
    private String aparelho_notifica_status_ped;
    private String aparelho_notifica_vencto_fin;
    private String aparelho_link_app;
    private boolean aparelho_notificado;
    private String usuario_tipo;
    private String usuario_permite_n_logins;
    private String usuario_permite_biometria;
    private int usuario_avaliacao_app;
    private String usuario_edita_preco_app;

    private String data_sincronia_cliente;
    private String data_sincronia_produto;
    private String data_sincronia_imagem;
    private String data_sincronia_cobranca;
    private String saldo_verba;
    private String data_atualizao_verba;


    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getNome_usuario() {
        return nome_usuario;
    }

    public void setNome_usuario(String nome_usuario) {
        this.nome_usuario = nome_usuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenha_confirma() {
        return senha_confirma;
    }

    public void setSenha_confirma(String senha_confirma) {
        this.senha_confirma = senha_confirma;
    }

    public String getData_cadastro() {
        return data_cadastro;
    }

    public void setData_cadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getUsuario_cadatro() {
        return usuario_cadatro;
    }

    public void setUsuario_cadatro(String usuario_cadatro) {
        this.usuario_cadatro = usuario_cadatro;
    }

    public String getData_alterado() {
        return data_alterado;
    }

    public void setData_alterado(String data_alterado) {
        this.data_alterado = data_alterado;
    }

    public String getUsuario_alterou() {
        return usuario_alterou;
    }

    public void setUsuario_alterou(String usuario_alterou) {
        this.usuario_alterou = usuario_alterou;
    }

    public String getAparece_cad_usuario() {
        return aparece_cad_usuario;
    }

    public void setAparece_cad_usuario(String aparece_cad_usuario) {
        this.aparece_cad_usuario = aparece_cad_usuario;
    }

    public String getCliente_lista_todos() {
        return cliente_lista_todos;
    }

    public void setCliente_lista_todos(String cliente_lista_todos) {
        this.cliente_lista_todos = cliente_lista_todos;
    }

    public String getCliente_lista_setor() {
        return cliente_lista_setor;
    }

    public void setCliente_lista_setor(String cliente_lista_setor) {
        this.cliente_lista_setor = cliente_lista_setor;
    }

    public String getCliente_lista_representante() {
        return cliente_lista_representante;
    }

    public void setCliente_lista_representante(String cliente_lista_representante) {
        this.cliente_lista_representante = cliente_lista_representante;
    }

    public String getPedido_lista_todos() {
        return pedido_lista_todos;
    }

    public void setPedido_lista_todos(String pedido_lista_todos) {
        this.pedido_lista_todos = pedido_lista_todos;
    }

    public String getPedido_lista_setor() {
        return pedido_lista_setor;
    }

    public void setPedido_lista_setor(String pedido_lista_setor) {
        this.pedido_lista_setor = pedido_lista_setor;
    }

    public String getPedido_lista_representante() {
        return pedido_lista_representante;
    }

    public void setPedido_lista_representante(String pedido_lista_representante) {
        this.pedido_lista_representante = pedido_lista_representante;
    }

    public String getMensagem_lista_financeiro() {
        return mensagem_lista_financeiro;
    }

    public void setMensagem_lista_financeiro(String mensagem_lista_financeiro) {
        this.mensagem_lista_financeiro = mensagem_lista_financeiro;
    }

    public String getMensagem_lista_todos() {
        return mensagem_lista_todos;
    }

    public void setMensagem_lista_todos(String mensagem_lista_todos) {
        this.mensagem_lista_todos = mensagem_lista_todos;
    }

    public String getMensagem_lista_setor() {
        return mensagem_lista_setor;
    }

    public void setMensagem_lista_setor(String mensagem_lista_setor) {
        this.mensagem_lista_setor = mensagem_lista_setor;
    }

    public String getMensagem_lista_representante() {
        return mensagem_lista_representante;
    }

    public void setMensagem_lista_representante(String mensagem_lista_representante) {
        this.mensagem_lista_representante = mensagem_lista_representante;
    }

    public String getOrcamento_lista_todos() {
        return orcamento_lista_todos;
    }

    public void setOrcamento_lista_todos(String orcamento_lista_todos) {
        this.orcamento_lista_todos = orcamento_lista_todos;
    }

    public String getOrcamento_lista_setor() {
        return orcamento_lista_setor;
    }

    public void setOrcamento_lista_setor(String orcamento_lista_setor) {
        this.orcamento_lista_setor = orcamento_lista_setor;
    }

    public String getOrcamento_lista_representante() {
        return orcamento_lista_representante;
    }

    public void setOrcamento_lista_representante(String orcamento_lista_representante) {
        this.orcamento_lista_representante = orcamento_lista_representante;
    }

    public String getUsuario_lista_todos() {
        return usuario_lista_todos;
    }

    public void setUsuario_lista_todos(String usuario_lista_todos) {
        this.usuario_lista_todos = usuario_lista_todos;
    }

    public String getUsuario_lista_setor() {
        return usuario_lista_setor;
    }

    public void setUsuario_lista_setor(String usuario_lista_setor) {
        this.usuario_lista_setor = usuario_lista_setor;
    }

    public String getUsuario_lista_representante() {
        return usuario_lista_representante;
    }

    public void setUsuario_lista_representante(String usuario_lista_representante) {
        this.usuario_lista_representante = usuario_lista_representante;
    }

    public String getExcluido() {
        return excluido;
    }

    public void setExcluido(String excluido) {
        this.excluido = excluido;
    }

    public String getId_setor() {
        return id_setor;
    }

    public void setId_setor(String id_setor) {
        this.id_setor = id_setor;
    }

    public String getId_quando_vendedor() {
        return id_quando_vendedor;
    }

    public void setId_quando_vendedor(String id_quando_vendedor) {
        this.id_quando_vendedor = id_quando_vendedor;
    }

    public String getAparelho_id() {
        return aparelho_id;
    }

    public void setAparelho_id(String aparelho_id) {
        this.aparelho_id = aparelho_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdEmpresaMultiDevice() {
        return idEmpresaMultiDevice;
    }

    public void setIdEmpresaMultiDevice(String idEmpresaMultiDevice) {
        this.idEmpresaMultiDevice = idEmpresaMultiDevice;
    }

    public String getLogado() {
        return logado;
    }

    public void setLogado(String logado) {
        this.logado = logado;
    }

    public String getEmail_vendedor() {
        return email_vendedor;
    }

    public void setEmail_vendedor(String email_vendedor) {
        this.email_vendedor = email_vendedor;
    }

    public String getAparelho_key_firebase() {
        return aparelho_key_firebase;
    }

    public void setAparelho_key_firebase(String aparelho_key_firebase) {
        this.aparelho_key_firebase = aparelho_key_firebase;
     }

    public String getAparelho_versao_app() {
        return aparelho_versao_app;
    }

    public void setAparelho_versao_app(String aparelho_versao_app) {
        this.aparelho_versao_app = aparelho_versao_app;
    }

    public String getAparelho_notifica_update_app() {
        return aparelho_notifica_update_app;
    }

    public void setAparelho_notifica_update_app(String aparelho_notifica_update_app) {
        this.aparelho_notifica_update_app = aparelho_notifica_update_app;
    }

    public String getAparelho_notifica_status_ped() {
        return aparelho_notifica_status_ped;
    }

    public void setAparelho_notifica_status_ped(String aparelho_notifica_status_ped) {
        this.aparelho_notifica_status_ped = aparelho_notifica_status_ped;
    }

    public String getAparelho_notifica_vencto_fin() {
        return aparelho_notifica_vencto_fin;
    }

    public void setAparelho_notifica_vencto_fin(String aparelho_notifica_vencto_fin) {
        this.aparelho_notifica_vencto_fin = aparelho_notifica_vencto_fin;
    }

    public String getAparelho_link_app() {
        return aparelho_link_app;
    }

    public void setAparelho_link_app(String aparelho_link_app) {
        this.aparelho_link_app = aparelho_link_app;
    }

    public boolean isAparelho_notificado() {
        return aparelho_notificado;
    }

    public void setAparelho_notificado(boolean aparelho_notificado) {
        this.aparelho_notificado = aparelho_notificado;
    }

    public String getUsuario_tipo() {
        return usuario_tipo;
    }

    public void setUsuario_tipo(String usuario_tipo) {
        this.usuario_tipo = usuario_tipo;
    }

    public String getUsuario_permite_n_logins() {
        return usuario_permite_n_logins;
    }

    public void setUsuario_permite_n_logins(String usuario_permite_n_logins) {
        this.usuario_permite_n_logins = usuario_permite_n_logins;
    }

    public String getUsuario_permite_biometria() {
        return usuario_permite_biometria;
    }

    public void setUsuario_permite_biometria(String usuario_permite_biometria) {
        this.usuario_permite_biometria = usuario_permite_biometria;
    }

    public int getUsuario_avaliacao_app() {
        return usuario_avaliacao_app;
    }

    public void setUsuario_avaliacao_app(int usuario_avaliacao_app) {
        this.usuario_avaliacao_app = usuario_avaliacao_app;
    }

    public String getData_sincronia_cliente() {
        return data_sincronia_cliente;
    }

    public void setData_sincronia_cliente(String data_sincronia_cliente) {
        this.data_sincronia_cliente = data_sincronia_cliente;
    }

    public String getData_sincronia_produto() {
        return data_sincronia_produto;
    }

    public void setData_sincronia_produto(String data_sincronia_produto) {
        this.data_sincronia_produto = data_sincronia_produto;
    }

    public String getData_sincronia_imagem() {
        return data_sincronia_imagem;
    }

    public void setData_sincronia_imagem(String data_sincronia_imagem) {
        this.data_sincronia_imagem = data_sincronia_imagem;
    }

    public String getUsuario_edita_preco_app() {
        return usuario_edita_preco_app;
    }

    public void setUsuario_edita_preco_app(String usuario_edita_preco_app) {
        this.usuario_edita_preco_app = usuario_edita_preco_app;
    }

    public String getSaldo_verba() {
        return saldo_verba;
    }

    public void setSaldo_verba(String saldo_verba) {
        this.saldo_verba = saldo_verba;
    }

    public String getData_atualizao_verba() {
        return data_atualizao_verba;
    }

    public void setData_atualizao_verba(String data_atualizao_verba) {
        this.data_atualizao_verba = data_atualizao_verba;
    }

    public String getData_sincronia_cobranca() {
        return data_sincronia_cobranca;
    }

    public void setData_sincronia_cobranca(String data_sincronia_cobranca) {
        this.data_sincronia_cobranca = data_sincronia_cobranca;
    }
}
