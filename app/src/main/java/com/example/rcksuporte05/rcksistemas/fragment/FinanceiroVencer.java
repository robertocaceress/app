package com.example.rcksuporte05.rcksistemas.fragment;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.PreferenciaDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.ListaHistoricoFinanceiroAdapter;
import com.example.rcksuporte05.rcksistemas.api.Api;
import com.example.rcksuporte05.rcksistemas.api.Rotas;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.Financeiro;
import com.example.rcksuporte05.rcksistemas.model.Preferencia;
import com.example.rcksuporte05.rcksistemas.util.DividerItemDecoration;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FinanceiroVencer extends Fragment implements ListaHistoricoFinanceiroAdapter.FinanceiroAdapterListener{
    @BindView(R.id.listaFinanceiro)
    RecyclerView recyclerView;

    @BindView(R.id.edtTotalTitulos)
    TextView edtTotalTitulos;
    @BindView(R.id.edtTotalJuros)
    TextView edtTotalJuros;
    @BindView(R.id.edtTotalPago)
    TextView edtTotalPago;
    @BindView(R.id.edtTotalSaldo)
    TextView edtTotalSaldo;

    @BindView(R.id.layout_progress)
    LinearLayout layout_progress;

    @BindView(R.id.progress_ajuda)
    ProgressBar progress_ajuda;

    @BindView(R.id.progress_title)
    TextView progress_title;

    private View view;
    Context context;
    private static ListaHistoricoFinanceiroAdapter adapterListaFinanceiro;
    private static List<Financeiro> listaFinanceiro = new ArrayList<>();
    PackageInfo pInfo = null;
    private DBHelper db;
    private ConfiguracaoDAO configuracaoDAO;
    private PreferenciaDAO preferenciaDAO;

    private LinearLayout.LayoutParams params;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_financeiro, container, false);
        ButterKnife.bind(this, view);
        context = this.getContext();
        try {
            pInfo = context.getPackageManager().getPackageInfo( context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        try {
            pesquisaTitulo("vn", ClienteHelper.getCliente().getId_cadastro_servidor());
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }

        return (view);
    }
    private void pesquisaTitulo(String tipo, int idCliente) {  //Tipo vc = vencido , vn = a vencer  , qt = quitado
         showProgress();

        String version = pInfo.versionName.substring(0,5);
        int dias_historico_financeiro = 0;
        db = new DBHelper(context);
        preferenciaDAO = new PreferenciaDAO(db);
        try {
            Preferencia preferencia = preferenciaDAO.getPreferencia("SELECT * FROM TBL_PREFERENCIA");
            dias_historico_financeiro = preferencia.getDias_hist_financeiro();
        } catch ( NullPointerException | NumberFormatException e ) {
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        configuracaoDAO = new ConfiguracaoDAO(db);
        final Configuracao configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        if ( !configuracao.getId().isEmpty() ) {
            // progress.show();
            //if (configuracao.getConexao_segura().isEmpty() || configuracao.getConexao_segura().equalsIgnoreCase("N")) {
                //Api.url = "http://"  + configuracao.getUrl_ip_1().trim().toString() + ":" + Integer.toString(configuracao.getPorta_1()).trim() + "/rckwhalleAPI" + version + "/ws/";
            //} else {
                //Api.url = "https://" + configuracao.getUrl_ip_1().trim().toString() + ":" + Integer.toString(configuracao.getPorta_1()).trim() + "/rckwhalleAPI" + version + "/ws/";
           // }

            final Rotas apiRotas = Api.buildRetrofit(false);
            Map<String, String> cabecalho = new HashMap<>();
            cabecalho.put("AUTHORIZATION", UsuarioHelper.getUsuario().getToken());
            Call<List<Financeiro>> call = apiRotas.getFinanceiro(tipo, idCliente, dias_historico_financeiro, cabecalho);

            call.enqueue(new Callback<List<Financeiro>>() {
                @Override
                public void onResponse(Call<List<Financeiro>> call, Response<List<Financeiro>> response) {
                    if (response.code() == 200) {
                        listaFinanceiro = response.body();
                        setRecyclerView(listaFinanceiro);
                    } else {
                        hideProgress();
                        Toast.makeText(context, "Não foi possivel sincronizar com o servidor! Por favor verifique sua conexão e tente novamente!.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<Financeiro>> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(context, "Não foi possivel sincronizar com o servidor! Por favor verifique sua conexão e tente novamente!!", Toast.LENGTH_LONG).show();
                    //t.printStackTrace();

                }
            });
        } else {
            hideProgress();
            Toast.makeText( context, "Parametros para conexão não cadastrados, por gentileza clique no botão ajuda antes de continuar",Toast.LENGTH_LONG).show();
        }

    }
    private void setRecyclerView(List<Financeiro> lista) {

        adapterListaFinanceiro = new ListaHistoricoFinanceiroAdapter(lista);//, listenerFinanceiro);
        recyclerView.setAdapter(adapterListaFinanceiro);
        adapterListaFinanceiro.notifyDataSetChanged();
        float totalTitulos = 0.0f;
        float totalJuros   = 0.0f;
        float totalPago    = 0.0f;
        float totalSaldo   = 0.0f;
        try {
            if (lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    totalTitulos += Float.parseFloat(lista.get(i).getValor_documento());
                    try {
                        totalJuros += Float.parseFloat(lista.get(i).getValor_juros());
                    } catch (Exception e) {

                    }
                    try {
                        totalPago += Float.parseFloat(lista.get(i).getValor_pago());
                    } catch (Exception e) {

                    }
                    try {
                        totalSaldo += Float.parseFloat(lista.get(i).getValor_saldo());
                    } catch (Exception e) {

                    }
                }
                edtTotalTitulos.setText(MascaraUtil.duasCasaDecimal(totalTitulos));
                edtTotalJuros.setText(MascaraUtil.duasCasaDecimal(totalJuros));
                edtTotalPago.setText(MascaraUtil.duasCasaDecimal(totalPago));
                edtTotalSaldo.setText(MascaraUtil.duasCasaDecimal(totalSaldo));
            } else
                Toast.makeText(context, "Não foram encontrados registros no histórico financeiro!", Toast.LENGTH_SHORT).show();

        }catch ( NullPointerException e ) {
            edtTotalTitulos.setText(MascaraUtil.duasCasaDecimal( totalTitulos ));
            edtTotalJuros.setText(MascaraUtil.duasCasaDecimal( totalJuros ));
            edtTotalPago.setText(MascaraUtil.duasCasaDecimal( totalPago ));
            edtTotalSaldo.setText(MascaraUtil.duasCasaDecimal( totalSaldo ));
        } catch ( Exception e) {
            edtTotalTitulos.setText(MascaraUtil.duasCasaDecimal( totalTitulos ));
            edtTotalJuros.setText(MascaraUtil.duasCasaDecimal( totalJuros ));
            edtTotalPago.setText(MascaraUtil.duasCasaDecimal( totalPago ));
            edtTotalSaldo.setText(MascaraUtil.duasCasaDecimal( totalSaldo ));
        }
        hideProgress();
    }

    private void hideProgress() {
        try {
            progress_ajuda.setVisibility(View.INVISIBLE);
            progress_title.setVisibility(View.INVISIBLE);
            params.weight = 0.0f;
            params.height = 0;
            layout_progress.setLayoutParams(params);
        } catch ( NullPointerException e) {
        } catch ( Exception e){
        }
    }
    private void showProgress() {
        params = (LinearLayout.LayoutParams)
                layout_progress.getLayoutParams();
        params.weight = 0.2f;
        params.height = 40;
        layout_progress.setLayoutParams(params);

        progress_ajuda.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        progress_ajuda.setVisibility(View.VISIBLE);
        progress_title.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickListener(int position) {
    }

    @Override
    public void onLongClickListener(int position) {
    }
}
