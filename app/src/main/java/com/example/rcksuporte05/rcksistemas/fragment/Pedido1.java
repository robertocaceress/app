package com.example.rcksuporte05.rcksistemas.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.BO.PedidoBO;
import com.example.rcksuporte05.rcksistemas.DAO.CadastroFinanceiroResumoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.CampanhaComercialCabDAO;
import com.example.rcksuporte05.rcksistemas.DAO.CondicoesPagamentoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.ProdutoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoItensDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.HistoricoFinanceiroHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.activity.ActivityPedidoMain;
import com.example.rcksuporte05.rcksistemas.activity.ActivityProduto;
import com.example.rcksuporte05.rcksistemas.activity.ActivityProdutoDrawer;
import com.example.rcksuporte05.rcksistemas.activity.ActivityProdutoPedido;
import com.example.rcksuporte05.rcksistemas.adapters.ListaAdapterProdutoPedido;
import com.example.rcksuporte05.rcksistemas.model.CampanhaComercialCab;
import com.example.rcksuporte05.rcksistemas.model.CondicoesPagamento;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.Produto;
import com.example.rcksuporte05.rcksistemas.model.PromocaoRetorno;
import com.example.rcksuporte05.rcksistemas.model.TabelaPrecoItem;
import com.example.rcksuporte05.rcksistemas.model.WebPedidoItens;
import com.example.rcksuporte05.rcksistemas.util.DividerItemDecoration;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Pedido1 extends Fragment implements ListaAdapterProdutoPedido.ProdutoPedidoAdapterListener {

    public static List<WebPedidoItens> listaProdutoRemovido = new ArrayList<>();
    private static ListaAdapterProdutoPedido listaAdapterProdutoPedido;
    private static List<WebPedidoItens> listaProdutoPedido = new ArrayList<>();
    private static RecyclerView lstProdutoPedido;
    private DBHelper db = new DBHelper(PedidoHelper.getActivityPedidoMain());
    private WebPedidoItensDAO webPedidoItensDAO = new WebPedidoItensDAO(db);
    private TabelaPrecoItem tabelaPrecoItem;
    private Button btnInserirProduto;
    private Bundle bundle;
    private int idCliente;
    private CadastroFinanceiroResumoDAO cadastroFinanceiroResumoDAO;
    private ProdutoDAO produtoDAO = new ProdutoDAO(db);
    private PromocaoRetorno promocaoRetorno;
    public static ListaAdapterProdutoPedido getListaAdapterProdutoPedido() {
        return listaAdapterProdutoPedido;
    }

    public static boolean removeProdutos(final List<WebPedidoItens> webPedidoItens) {
        DBHelper db1 = new DBHelper(PedidoHelper.getActivityPedidoMain());
        WebPedidoItensDAO webPedidoItensDAO1 = new WebPedidoItensDAO(db1);
        for ( int i = 0 ; i < webPedidoItens.size(); i++) {
            if ( webPedidoItensDAO1.delete(PedidoHelper.getActivityPedidoMain(),  webPedidoItens.get(i))) {
                if ( !listaProdutoPedido.remove(webPedidoItens.get(i)))
                    showMsgAlerta("Falha ao remover o produto excluido da lista de produtos! Favor entrar em contato com o suporte técnico" + webPedidoItens.get(i).getNome_produto());
            } else {
                listaAdapterProdutoPedido.getList(listaProdutoPedido);
                lstProdutoPedido.setAdapter(listaAdapterProdutoPedido);
                PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
                listaAdapterProdutoPedido.notifyDataSetChanged();
                showMsgAlerta("Falha ao excluir o produto" + webPedidoItens.get(i).getNome_produto());
                return false;
            }
        }
        listaAdapterProdutoPedido.getList(listaProdutoPedido);
        lstProdutoPedido.setAdapter(listaAdapterProdutoPedido);
        PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        listaAdapterProdutoPedido.notifyDataSetChanged();
        return  true;
    }

    public static void zerarDesconto() {
        if (listaProdutoPedido.size() > 0)
            for (WebPedidoItens webPedidoItem : listaProdutoPedido)
                if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                    webPedidoItem.setValor_desconto_per("0");
                    webPedidoItem.setDescontoIndevido(false);
                    webPedidoItem.setValor_desconto_real(String.valueOf(0));
                    webPedidoItem.setValor_total(String.valueOf(Float.parseFloat(webPedidoItem.getValor_bruto())));
                }
        if (listaProdutoPedido.size() > 0) {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        }
    }

    public static Double calcularDesconto() {
        Double valorDesconto = 0.00;
        if (listaProdutoPedido.size() > 0)
            for (WebPedidoItens webPedidoItem : listaProdutoPedido)
                valorDesconto += Double.parseDouble(webPedidoItem.getValor_desconto_real());
        if (valorDesconto <= 0.00)
            valorDesconto = 0.00;
        return valorDesconto;
    }

    public static  int totalProdutos() {
        if (listaProdutoPedido !=  null)
            return listaProdutoPedido.size();
        return 0;
    }

    public static void colocarDesconto() {
        if (listaProdutoPedido.size() > 0)
            for (WebPedidoItens webPedidoItem : listaProdutoPedido)
                if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                    webPedidoItem.setValor_desconto_per("5");
                    webPedidoItem.setDescontoIndevido(false);
                    Float descontoReal = 0.00f;
                    descontoReal = (Float.parseFloat(webPedidoItem.getValor_total()) * 0.05f);
                    webPedidoItem.setValor_desconto_real(String.valueOf(descontoReal));
                    webPedidoItem.setValor_total(String.valueOf(Float.parseFloat(webPedidoItem.getValor_bruto()) - descontoReal));
                }
        if ( listaProdutoPedido.size() > 0) {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        }
    }

    public static void colocarDescontoCategoria(Float descontoCategoria) {
        Float totalProdutoBruto = 0.0f;
        Float desconto = 0.0f;
        BigDecimal bdesconto = null;
        BigDecimal btotalBruto = null;//Tabela e desconto por categoria
        Float descontoReal = 0.00f;
        if (listaProdutoPedido.size() > 0)

            for (WebPedidoItens webPedidoItem : listaProdutoPedido)
                if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                    webPedidoItem.setValor_desconto_per(String.valueOf(descontoCategoria));
                    webPedidoItem.setDescontoIndevido(false);

                    btotalBruto = new BigDecimal( Float.parseFloat(webPedidoItem.getQuantidade()) *  webPedidoItem.getValor_unitario() ).setScale(2, RoundingMode.HALF_EVEN);
                    totalProdutoBruto = Float.parseFloat(String.valueOf(btotalBruto));
                    bdesconto = new BigDecimal((descontoCategoria *  totalProdutoBruto ) / 100).setScale(3, RoundingMode.HALF_EVEN);
                    int tamanho = String.valueOf(bdesconto).length();
                    if ( String.valueOf(bdesconto).substring( tamanho-1 ).equalsIgnoreCase("5")) {
                        if ( ((Integer.parseInt(String.valueOf(bdesconto).substring(tamanho-2,tamanho-1))) % 2) == 0){
                            bdesconto = new BigDecimal(( descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.DOWN);
                        } else{
                            bdesconto = new BigDecimal(( descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.UP);
                        }
                    } else {
                        bdesconto = new BigDecimal(( descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.HALF_EVEN);
                    }
                    descontoReal = Float.parseFloat(String.valueOf(bdesconto));

                    //descontoReal = (Float.parseFloat(webPedidoItem.getValor_total()) * descontoCategoria);
                    webPedidoItem.setValor_desconto_real(String.valueOf(descontoReal));
                    webPedidoItem.setValor_total(String.valueOf(Float.parseFloat(webPedidoItem.getValor_bruto()) - descontoReal));
                }
        if ( listaProdutoPedido.size() > 0) {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        }
    }

    public static void colocarDescontoPrazo( Float desconto, DBHelper db) {
        if ( listaProdutoPedido.size() > 0) {
            String tipo_tabela = "";
            try {
                if( ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                    if ( ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("6"))
                        tipo_tabela = "6";
                }
            } catch ( Exception e) {
                e.printStackTrace();
            }

            for (WebPedidoItens webPedidoItem : listaProdutoPedido) {
                if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                    if ( tipo_tabela.equalsIgnoreCase("6")) {
                        if (  db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_PRODUTO = " + webPedidoItem.getId_produto() + " AND ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco()).size() <= 0){
                            Float quantidade = Float.parseFloat(webPedidoItem.getQuantidade());
                            webPedidoItem.setValor_desconto_per(String.valueOf(desconto));
                            webPedidoItem.setDescontoIndevido(false);
                            webPedidoItem.setValor_total( String.valueOf((Float.parseFloat( webPedidoItem.getVenda_preco()) * quantidade) - ((desconto) / 100) * (Float.parseFloat( webPedidoItem.getVenda_preco()) * quantidade)));
                            webPedidoItem.setValor_desconto_real(String.valueOf( (desconto / 100) * (Float.parseFloat( webPedidoItem.getVenda_preco()) * quantidade)));
                        }
                    } else {
                        Float quantidade = Float.parseFloat(webPedidoItem.getQuantidade());
                        webPedidoItem.setValor_desconto_per(String.valueOf(desconto));
                        webPedidoItem.setDescontoIndevido(false);
                        webPedidoItem.setValor_total( String.valueOf((Float.parseFloat( webPedidoItem.getVenda_preco()) * quantidade) - ((desconto) / 100) * (Float.parseFloat( webPedidoItem.getVenda_preco()) * quantidade)));
                        webPedidoItem.setValor_desconto_real(String.valueOf( (desconto / 100) * (Float.parseFloat( webPedidoItem.getVenda_preco()) * quantidade)));
                    }
                }
            }
        }
        if ( listaProdutoPedido.size() > 0) {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        }
    }

    public static void verificaDescontoPrazo(Float desconto_prazo) {
        if (listaProdutoPedido.size() > 0)
            for (WebPedidoItens webPedidoItem : listaProdutoPedido)
                if (Float.parseFloat(webPedidoItem.getValor_desconto_per()) > desconto_prazo)
                    webPedidoItem.setDescontoIndevido(true);
                else
                    webPedidoItem.setDescontoIndevido(false);

        if ( listaProdutoPedido.size() > 0) {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pedido1, container, false);
        webPedidoItensDAO = new WebPedidoItensDAO(db);
        listaProdutoPedido = new ArrayList<>();
        final PedidoHelper pedidoHelper = new PedidoHelper(this);
        bundle = getArguments();
        lstProdutoPedido = (RecyclerView) view.findViewById(R.id.lstProdutosPedido);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        lstProdutoPedido.setLayoutManager(layoutManager);
        //lstProdutoPedido.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        btnInserirProduto = (Button) view.findViewById(R.id.btnInserirProduto);
        btnInserirProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pedidoHelper.verificaCliente()) {
                    Intent intent = new Intent(getContext(), ActivityProdutoDrawer.class);
                    intent.putExtra("acao", 1);
                    intent.putExtra("idPedido", PedidoHelper.getIdPedido());
                    intent.putExtra("idCliente", ClienteHelper.getCliente().getId_cadastro());
                    PedidoHelper.setListaWebPedidoItens(listaProdutoPedido);
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), "Selecione um cliente antes de prosseguir!", Toast.LENGTH_SHORT).show();
                    PedidoHelper.pintaTxtNomeCliente();
                    pedidoHelper.moveTela(0);
                }
                //zerarDesconto();
            }
        });


        if (PedidoHelper.getIdPedido() > 0) {
            //final int pedido = PedidoHelper.getIdPedido();
            if (bundle.getInt("vizualizacao") == 1) {
                btnInserirProduto.setVisibility(View.INVISIBLE);
                Configuracao configuracao = new Configuracao();
                ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db );

                configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
                PedidoHelper.getWebPedido().setTrabalha_com_campanha(configuracao.getTrabalha_com_campanhas());
            }
            try {
                if ( PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S")) {
                    listaProdutoPedido = webPedidoItensDAO.getLista("SELECT WI.*, CP.NOME_CAMPANHA AS NOME_CAMPANHA FROM TBL_WEB_PEDIDO_ITENS AS WI " +
                            "LEFT JOIN TBL_CAMPANHA_COMERCIAL_CAB AS CP ON CP.ID_CAMPANHA = WI.ID_CAMPANHA " +
                            "WHERE WI.ID_PEDIDO = " + PedidoHelper.getIdPedido() + " GROUP BY WI.ID_PRODUTO ORDER BY WI.ID_WEB_ITEM");
                } else {
                    listaProdutoPedido = webPedidoItensDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + PedidoHelper.getIdPedido() + " ORDER BY ID_WEB_ITEM");
                }
                preencheLista(listaProdutoPedido);
            } catch (CursorIndexOutOfBoundsException e) {
            } catch ( NullPointerException e) {
                //listaProdutoPedido = webPedidoItensDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO_ITENS WHERE ID_PEDIDO = " + pedido);
                //preencheLista(listaProdutoPedido);
                showMsgAlerta("O pedido encontra-se sem produto!!");
            }
        } else if (PedidoHelper.getWebPedido() != null) {
            listaProdutoPedido = PedidoHelper.getListaWebPedidoItens();
            preencheLista(listaProdutoPedido);
        }

        if (bundle.getInt("vizualizacao") == 1)
            btnInserirProduto.setVisibility(View.INVISIBLE);

        System.gc();
        return (view);
    }

    public boolean inserirProduto(WebPedidoItens webPedidoItem) {
        if ( PedidoHelper.getIdPedido() > 0) {
            if (db.contagem("SELECT COUNT(*) FROM TBL_WEB_PEDIDO_ITENS WHERE (ID_PEDIDO = " + PedidoHelper.getIdPedido() + " AND ID_PRODUTO = '" + webPedidoItem.getId_produto().trim() + "') " +
                    " OR (ID_PEDIDO = " + PedidoHelper.getIdPedido() + " AND ID_PRODUTO = '" + webPedidoItem.getId_produto() + "');") <= 0) {
                webPedidoItem.setId_pedido(String.valueOf(PedidoHelper.getIdPedido()));
                webPedidoItem.setId_empresa(UsuarioHelper.getUsuario().getIdEmpresaMultiDevice());
                webPedidoItem.setUsuario_lancamento_id(String.valueOf(bundle.getInt("idUsuario")));
                if (listaProdutoPedido == null)
                    listaProdutoPedido = new ArrayList<>();
                int id_web_item = (int) webPedidoItensDAO.add(webPedidoItem); //listaWebPedidoItens.get(i));
                if (id_web_item > 0) {
                    webPedidoItem.setId_web_item(Integer.toString(id_web_item));
                    listaProdutoPedido.add(webPedidoItem);
                    preencheLista(listaProdutoPedido);
                    return true;
                } else {
                    showMsgAlerta("Houve um erro ao obter os dados do produto! Pedimos por gentileza que informe o suporte técnico sobre o mesmo!!!! \n");
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;

    }

    public boolean alterarProduto(WebPedidoItens webPedidoItem, int position) {
        webPedidoItem.setId_empresa(UsuarioHelper.getUsuario().getIdEmpresaMultiDevice());
        webPedidoItem.setUsuario_lancamento_id(UsuarioHelper.getUsuario().getId_usuario());
        if ( webPedidoItensDAO.update( webPedidoItem) ) {
            listaProdutoPedido.set(position, webPedidoItem);
            preencheLista(listaProdutoPedido);
            return true;
        }
        return false;
    }

    public List<WebPedidoItens> salvaPedidos() {
        for (WebPedidoItens webPedidoItens : listaProdutoPedido) {
            if ((PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("N") || webPedidoItens.getProduto_materia_prima().equals("S") || webPedidoItens.getProduto_tercerizacao().equals("S"))
                    && Float.parseFloat(webPedidoItens.getValor_desconto_real()) > 0.f) {
                if ( ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S") && ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")){
                    webPedidoItens.setDescontoIndevido(false);
                } else {
                    webPedidoItens.setDescontoIndevido(true);
                }
            }
            if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("N") && PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("S"))
                webPedidoItens.setDescontoIndevido(false);
        }
        try {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
            PedidoHelper.setListaWebPedidoItens(listaProdutoPedido);
        } catch ( NullPointerException e) {
           e.printStackTrace();
        }
        return listaProdutoPedido;
    }

    public List<WebPedidoItens> salvaPedidos(Float desconto) {
        for (WebPedidoItens webPedidoItens : listaProdutoPedido)
            if ((PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("N") || webPedidoItens.getProduto_materia_prima().equals("S") || webPedidoItens.getProduto_tercerizacao().equals("S"))
                    && Float.parseFloat(webPedidoItens.getValor_desconto_real()) > 0.f)
                webPedidoItens.setDescontoIndevido(true);
        listaAdapterProdutoPedido.notifyDataSetChanged();
        PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        PedidoHelper.setListaWebPedidoItens(listaProdutoPedido);
        return listaProdutoPedido;
    }

    public void preencheLista(List<WebPedidoItens> webPedidoItens) {
        try {
            if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                listaAdapterProdutoPedido = new ListaAdapterProdutoPedido(webPedidoItens, this, true);
            else
                listaAdapterProdutoPedido = new ListaAdapterProdutoPedido(webPedidoItens, this, false);
        } catch ( NullPointerException e) {
            listaAdapterProdutoPedido = new ListaAdapterProdutoPedido(webPedidoItens, this, false);
        }
        lstProdutoPedido.setAdapter(listaAdapterProdutoPedido);
        listaAdapterProdutoPedido.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        if (listaProdutoPedido != null)
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());

        if (ClienteHelper.getCliente() != null)
            verificaFinanceiro(); //Verifica situação financeira atras de pendencia

        if (listaProdutoPedido != null && listaProdutoPedido.size() > 0) {
            if ( (ClienteHelper.getCliente().getId_cadastro() != idCliente && idCliente > 0) && ClienteHelper.getCliente().getId_cadastro_servidor() > 0) {
                //Enviar o cadastro da empresa para verificar os parametros
                try {
                    if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                        if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")) {
                            if (ClienteHelper.getCliente().getIdCategoria() > 0) {
                                ViewGroup viewGroup = PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
                                View dialogView = LayoutInflater.from(PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
                                AlertDialog.Builder builder = new AlertDialog.Builder(PedidoHelper.getActivityPedidoMain());
                                builder.setView(dialogView);
                                AlertDialog alertDialog = builder.create();
                                alertDialog.setCancelable(false);

                                TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
                                if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                                    textView.setText("Você alterou o cliente. Deseja alterar o desconto do(s) produto(s) para a categoria do cliente " + ClienteHelper.getCliente().getNome_cadastro() + "?\nTodas as campanhas lançadas serão removidas, caso não sejam aplicaveis ao cliente alterado!");
                                else
                                    textView.setText("Você alterou o cliente. Deseja alterar o desconto do(s) produto(s) para a categoria do cliente " + ClienteHelper.getCliente().getNome_cadastro() + "?");
                                Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
                                btnNao.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                                        verificaDescontoTabelaCampanha(true, true);
                                        //else
                                        //    verificaDescontoTabelaCampanha(true, false);
                                        alertDialog.dismiss();
                                    }
                                });
                                Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
                                btnSim.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                                            verificaDescontoTabelaCampanha(true, true);
                                        else
                                            verificaDescontoTabelaCampanha(true, false);
                                        alertDialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                            } else {
                                showMsgAlerta("O cliente " + ClienteHelper.getCliente().getNome_cadastro() + " não tem uma categoria definida!");
                            }


                        } else if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("4")) {
                            if (ClienteHelper.getCliente().getIdCategoria() > 0) {
                                ViewGroup viewGroup =  PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
                                View dialogView = LayoutInflater.from( PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
                                AlertDialog.Builder builder = new AlertDialog.Builder( PedidoHelper.getActivityPedidoMain());
                                builder.setView(dialogView);
                                AlertDialog alertDialog = builder.create();
                                alertDialog.setCancelable(false);

                                TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
                                if ( PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                                    textView.setText("Você alterou o cliente. Deseja alterar o desconto do(s) produto(s) para a categoria do cliente " + ClienteHelper.getCliente().getNome_cadastro() + "?\nTodas as campanhas lançadas serão removidas, caso não sejam aplicaveis ao cliente alterado!");
                                else
                                    textView.setText("Você alterou o cliente. Deseja alterar o desconto do(s) produto(s) para a categoria do cliente " + ClienteHelper.getCliente().getNome_cadastro() + "?");
                                Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
                                btnNao.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                                            verificaDescontoTabelaCampanha(false, true);
                                        else
                                            verificaDescontoTabelaCampanha(true, false);
                                        alertDialog.dismiss();
                                    }
                                });
                                Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
                                btnSim.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S"))
                                            verificaDescontoTabelaCampanha(true, true);
                                        else
                                            verificaDescontoTabelaCampanha(true, false);
                                        alertDialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                            } else {
                                showMsgAlerta("O cliente " + ClienteHelper.getCliente().getNome_cadastro() + " não tem uma categoria definida!");
                            }
                        } else if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("6")) {
                            if (ClienteHelper.getCliente().getId_tabela_preco() > 0) {
                                ViewGroup viewGroup =  PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
                                View dialogView = LayoutInflater.from( PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
                                AlertDialog.Builder builder = new AlertDialog.Builder( PedidoHelper.getActivityPedidoMain());
                                builder.setView(dialogView);
                                AlertDialog alertDialog = builder.create();
                                alertDialog.setCancelable(false);

                                TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
                                textView.setText("Você alterou o cliente. Deseja alterar o preço do(s) produto(s) para a tabela do cliente " + ClienteHelper.getCliente().getNome_cadastro() + "?");

                                Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
                                btnNao.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });
                                Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
                                btnSim.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        verificaDescontoTabela();
                                        alertDialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                            } else {
                                verificaDescontoSemTabela();
                                showMsgSucesso("O cliente " + ClienteHelper.getCliente().getNome_cadastro() + " não tem uma tabela de preço definida! O preço dos produtos foram redefinidos para a tabela padrão!");
                            }
                        }
                    }
                }catch ( NullPointerException e ) {
                }catch ( Exception e ) {
                }
            }
            idCliente = ClienteHelper.getCliente().getId_cadastro();

            setTabelaPrecoItem();

            PedidoBO pedidoBO = new PedidoBO();
            if (bundle.getInt("vizualizacao") != 1)
                verificaDescontoIndevido(pedidoBO);
            try {
                listaAdapterProdutoPedido.notifyDataSetChanged();
            } catch ( NullPointerException e) {
                e.printStackTrace();
            }
        }

        super.onResume();
    }

    @Override
    public void onDestroy() {
        try {
            listaProdutoRemovido.clear();
            listaAdapterProdutoPedido = null;
            listaProdutoPedido.clear();
            lstProdutoPedido = null;
        } catch (Exception e) {
        }
        System.gc();
        super.onDestroy();
    }

    @Override
    public void onRowClicked(int position) {
        if (listaAdapterProdutoPedido.getSelectedItemCount() > 0) {
            toggleSelection(position);
        } else {
            if (listaProdutoPedido.get(position).getProdutoBase())
                showProdutoPedido(position);
            else
                showMsgAlerta("O produto " + listaProdutoPedido.get(position).getNome_produto() + " não esta mais disponivel para venda externa!");
        }
    }

    @Override
    public void onLongRowClicked(int position) {
        if (bundle.getInt("vizualizacao") != 1)
            PedidoHelper.getActivityPedidoMain().enableActionMode(position);
    }

    @Override
    public void onButtonClicked(int position) {
        if (bundle.getInt("vizualizacao") != 1) {
            ViewGroup viewGroup = PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setView(dialogView);
            AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(false);
            TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
            textView.setText("Tem certeza que deseja excluir o produto selecionado");
            Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
            btnNao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
            btnSim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    List<WebPedidoItens> listaExclusao = new ArrayList<>();
                    listaExclusao.add(listaAdapterProdutoPedido.getItem(position));
                    PedidoHelper.getActivityPedidoMain().excluiProdutos(null, listaExclusao);
                }
            });
            alertDialog.show();
        }
    }

    public void toggleSelection(int position) {
        listaAdapterProdutoPedido.toggleSelection(position);
        int count = listaAdapterProdutoPedido.getSelectedItemCount();

        if (count == 0) {
            PedidoHelper.getActivityPedidoMain().actionMode.finish();
        } else {
            PedidoHelper.getActivityPedidoMain().actionMode.setTitle(String.valueOf(count));
            PedidoHelper.getActivityPedidoMain().actionMode.invalidate();
        }
    }

    public void showProdutoPedido( int position) {
        if (bundle.getInt("vizualizacao") != 1) {
            Intent intent = new Intent(getContext(), ActivityProdutoPedido.class);
            intent.putExtra("pedido", 1);
            intent.putExtra("position", position);
            PedidoHelper.setWebPedidoItem(listaProdutoPedido.get(position));
            PedidoHelper.setListaWebPedidoItens(listaProdutoPedido);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), ActivityProdutoPedido.class);
            intent.putExtra("pedido", 1);
            intent.putExtra("vizualizacao", 1);
            intent.putExtra("position", position);
            PedidoHelper.setWebPedidoItem(listaProdutoPedido.get(position));
            startActivity(intent);
        }
    }

    public void showMsgSucesso(String mensagem) {
        ViewGroup viewGroup = PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from( PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_sucesso_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(PedidoHelper.getActivityPedidoMain());
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    public static void showMsgAlerta(String mensagem) {
        ViewGroup viewGroup = PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from( PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_alerta_mensagem, viewGroup, false);
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PedidoHelper.getActivityPedidoMain());
        builder.setView(dialogView);
        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private boolean verificaCampanha(int idCliente, int idProduto, int idLinhaColecao, int idCampanha) {
        CampanhaComercialCabDAO campanhaComercialCabDAO = new CampanhaComercialCabDAO(db);
        try {
            List<CampanhaComercialCab> listaCampanha = campanhaComercialCabDAO.getLista(idCliente, idProduto, idLinhaColecao);
            if (listaCampanha.size() > 0)
                for (CampanhaComercialCab campanha : listaCampanha)
                    if (campanha.getIdCampanha() == idCampanha)
                        return true;
        } catch (CursorIndexOutOfBoundsException | NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void verificaFinanceiro() {
        try {
            cadastroFinanceiroResumoDAO = new CadastroFinanceiroResumoDAO(db);
            if (ClienteHelper.getCliente().getId_cadastro_servidor() > 0){
                HistoricoFinanceiroHelper.setCadastroFinanceiroResumo(cadastroFinanceiroResumoDAO.getCadastro(ClienteHelper.getCliente().getId_cadastro_servidor()));
                if (HistoricoFinanceiroHelper.getCadastroFinanceiroResumo().getFinanceiroVencido() > 0) {
                    PedidoHelper.pintaTxtNomeCliente();
                    Toast.makeText(getActivity(), "Este cliente possui pendências financeiras!", Toast.LENGTH_LONG).show();
                }
            }
        } catch (NullPointerException | CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }


    private void verificaDescontoTabelaCampanha( boolean desconto, boolean campanha) {
        //String descontoCategoria = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0).getPerc_desc_final();
        Configuracao configuracao = new ConfiguracaoDAO(db).getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        Float descontoCategoria = 0.0f;
        try {
            if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("4")) {
                    if (ClienteHelper.getCliente().getIdCategoria() >= 1) {
                        TabelaPrecoItem tabelaCateoriaItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                        if (PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("S")) {
                            descontoCategoria = Float.parseFloat(tabelaCateoriaItem.getPerc_desc_final());
                        } else {
                            descontoCategoria = 0.0f;
                        }
                    }
                } else if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")) {
                    if (ClienteHelper.getCliente().getIdCategoria() >= 1) {
                        TabelaPrecoItem tabelaCateoriaItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                        descontoCategoria = Float.parseFloat(tabelaCateoriaItem.getPerc_desc_final());
                    }
                }
            }

        } catch ( NullPointerException|CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        PedidoBO pedidoBO1 = new PedidoBO();
        Float totalProdutoBruto = 0.0f;

        BigDecimal bdesconto = null;
        BigDecimal btotalBruto = null;//Tabela e desconto por categoria
        Float descontoReal = 0.00f;
        for (WebPedidoItens webPedidoItem : listaProdutoPedido) {
            if (desconto) {
                if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                    if ( !webPedidoItem.getValor_desconto_per().isEmpty()) {
                        promocaoRetorno = pedidoBO1.calculaDesconto(ClienteHelper.getCliente().getId_cadastro_servidor(), webPedidoItem.getId_produto(), getContext());
                        if (promocaoRetorno != null && promocaoRetorno.getValorDesconto() > 0 && promocaoRetorno.getValorDesconto() > descontoCategoria) {
                            if ( !( promocaoRetorno.getValorDesconto() == Float.parseFloat(webPedidoItem.getValor_desconto_per()) )) {
                                if ( Float.parseFloat( webPedidoItem.getValor_desconto_per()) > promocaoRetorno.getValorDesconto()) {
                                    if ( configuracao.getTrabalha_com_desc_auto_app().equalsIgnoreCase("S")) {
                                        webPedidoItem.setValor_desconto_per(String.valueOf(descontoCategoria));
                                        webPedidoItem.setDescontoIndevido(false);

                                        btotalBruto = new BigDecimal(Float.parseFloat(webPedidoItem.getQuantidade()) * webPedidoItem.getValor_unitario()).setScale(2, RoundingMode.HALF_EVEN);
                                        totalProdutoBruto = Float.parseFloat(String.valueOf(btotalBruto));
                                        bdesconto = new BigDecimal((descontoCategoria * totalProdutoBruto) / 100).setScale(3, RoundingMode.HALF_EVEN);
                                        int tamanho = String.valueOf(bdesconto).length();
                                        if (String.valueOf(bdesconto).substring(tamanho - 1).equalsIgnoreCase("5")) {
                                            if (((Integer.parseInt(String.valueOf(bdesconto).substring(tamanho - 2, tamanho - 1))) % 2) == 0) {
                                                bdesconto = new BigDecimal((descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.DOWN);
                                            } else {
                                                bdesconto = new BigDecimal((descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.UP);
                                            }
                                        } else {
                                            bdesconto = new BigDecimal((descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.HALF_EVEN);
                                        }

                                        descontoReal = Float.parseFloat(String.valueOf(bdesconto));

                                        //Float descontoReais = (Float.parseFloat(descontoCategoria) * Float.parseFloat(webPedidoItem.getValor_bruto())) / 100;
                                        webPedidoItem.setValor_desconto_real(String.valueOf(descontoReal));
                                        webPedidoItem.setValor_total(String.valueOf(Float.parseFloat(webPedidoItem.getValor_bruto()) - descontoReal));
                                    }
                                }
                            }
                        } else {
                            if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                                if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("6")) {
                                    if (ClienteHelper.getCliente().getId_tabela_preco() >= 1) {
                                        try {
                                            TabelaPrecoItem tabelaItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_PRODUTO = " + webPedidoItem.getId_produto() + " AND ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco()).get(0);
                                            if (!tabelaItem.getId_item().isEmpty()) {
                                                tabelaPrecoItem.setPerc_desc_final("0");
                                                descontoCategoria = 0.0f;
                                            }
                                        } catch ( NullPointerException|CursorIndexOutOfBoundsException e) {
                                            e.printStackTrace();
                                        } catch ( Exception e ) {
                                            e.printStackTrace();

                                        }
                                    }
                                }
                                /*
                                if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")) {
                                    if (ClienteHelper.getCliente().getId_tabela_preco() >= 1) {
                                        try {
                                            //TabelaPrecoItem tabelaItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_PRODUTO = " + webPedidoItem.getId_produto() + " AND ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco()).get(0);
                                            TabelaPrecoItem tabelaItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                                            if (!tabelaItem.getId_item().isEmpty()) {
                                                tabelaPrecoItem.setPerc_desc_final("0");
                                                descontoCategoria = 0.0f;
                                            }
                                        } catch ( NullPointerException|CursorIndexOutOfBoundsException e) {
                                            e.printStackTrace();
                                        } catch ( Exception e ) {
                                            e.printStackTrace();

                                        }
                                    }
                                }

                                 */
                            }
                            if ( !(Float.parseFloat( webPedidoItem.getValor_desconto_per()) == descontoCategoria) ){
                                if ( configuracao.getTrabalha_com_desc_auto_app().equalsIgnoreCase("S")) {
                                    webPedidoItem.setValor_desconto_per(String.valueOf(descontoCategoria));
                                    webPedidoItem.setDescontoIndevido(false);

                                    btotalBruto = new BigDecimal(Float.parseFloat(webPedidoItem.getQuantidade()) * webPedidoItem.getValor_unitario()).setScale(2, RoundingMode.HALF_EVEN);
                                    totalProdutoBruto = Float.parseFloat(String.valueOf(btotalBruto));
                                    bdesconto = new BigDecimal((descontoCategoria * totalProdutoBruto) / 100).setScale(3, RoundingMode.HALF_EVEN);
                                    int tamanho = String.valueOf(bdesconto).length();
                                    if (String.valueOf(bdesconto).substring(tamanho - 1).equalsIgnoreCase("5")) {
                                        if (((Integer.parseInt(String.valueOf(bdesconto).substring(tamanho - 2, tamanho - 1))) % 2) == 0) {
                                            bdesconto = new BigDecimal((descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.DOWN);
                                        } else {
                                            bdesconto = new BigDecimal((descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.UP);
                                        }
                                    } else {
                                        bdesconto = new BigDecimal((descontoCategoria * (totalProdutoBruto)) / 100).setScale(2, RoundingMode.HALF_EVEN);
                                    }

                                    descontoReal = Float.parseFloat(String.valueOf(bdesconto));

                                    //Float descontoReais = (Float.parseFloat(descontoCategoria) * Float.parseFloat(webPedidoItem.getValor_bruto())) / 100;
                                    webPedidoItem.setValor_desconto_real(String.valueOf(descontoReal));
                                    webPedidoItem.setValor_total(String.valueOf(Float.parseFloat(webPedidoItem.getValor_bruto()) - descontoReal));
                                }
                            }
                        }
                    }

                }
            }
            if ( campanha) {
                if (PedidoHelper.getWebPedido().getTrabalha_com_campanha().equalsIgnoreCase("S") && webPedidoItem.getIdCampanha() > 0) {
                    if (!verificaCampanha(ClienteHelper.getCliente().getId_cadastro_servidor(), Integer.parseInt(webPedidoItem.getId_produto()), webPedidoItem.getIdLinhaColecao(), webPedidoItem.getIdCampanha())) {
                        webPedidoItem.setNomeCampanha("");
                        webPedidoItem.setIdCampanha(0);
                    }
                }
            }
        }
        listaAdapterProdutoPedido.notifyDataSetChanged();
        for (int i = 0; listaProdutoPedido.size() > i; i++)
            webPedidoItensDAO.update(listaProdutoPedido.get(i));
        if ( desconto && campanha) {
            listaAdapterProdutoPedido.notifyDataSetChanged();
            PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
            PedidoHelper.getActivityPedidoMain().updatePedido();
        } else {
            if ( desconto && !campanha) {
                listaAdapterProdutoPedido.notifyDataSetChanged();
                PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
                PedidoHelper.getActivityPedidoMain().updatePedido();
            } else {
                listaAdapterProdutoPedido.notifyDataSetChanged();
            }
        }
    }
    private void verificaDescontoTabela() {
        Float totalProdutoBruto = 0.0f;
        Float quantidade = 0.0f;
        Float descontoReais = 0.0f;
        List<Produto> lista = new ArrayList<>();
        String SQL;

        for (WebPedidoItens webPedidoItem : listaProdutoPedido) {
            if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                List<TabelaPrecoItem> listaTabelaPrecoItem = db.listaTabelaPrecoItem("SELECT VALOR_UNITARIO, PERC_DESC_FINAL FROM TBL_TABELA_PRECO_ITENS WHERE ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco());
                quantidade = Float.parseFloat(webPedidoItem.getQuantidade());
                //webPedidoItem.setValor_desconto_per(descontoCategoria);
                if (listaTabelaPrecoItem.size() > 0) {
                    totalProdutoBruto = quantidade * Float.parseFloat(listaTabelaPrecoItem.get(0).getValor_unitario());
                    webPedidoItem.setValor_unitario(Float.parseFloat(listaTabelaPrecoItem.get(0).getValor_unitario()));
                    webPedidoItem.setVenda_preco(listaTabelaPrecoItem.get(0).getValor_unitario() );
                    descontoReais = (Float.parseFloat( webPedidoItem.getValor_desconto_per()) * (totalProdutoBruto)) / 100;
                    webPedidoItem.setValor_total(String.valueOf(totalProdutoBruto - descontoReais ));
                    webPedidoItem.setValor_bruto(String.valueOf(totalProdutoBruto));
                    webPedidoItem.setValor_desconto_real(MascaraUtil.duasCasaDecimal(descontoReais));
                } else {
                    SQL = "SELECT P.*,  TI.VALOR_UNITARIO AS PRECO_TABELA_CLIENTE, TC.NOME_TABELA AS NOME_TABELA  FROM TBL_PRODUTO AS P " +
                            " LEFT JOIN TBL_TABELA_PRECO_ITENS AS TI ON TI.ID_PRODUTO = P.ID_PRODUTO  AND TI.ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco() +
                            " LEFT JOIN TBL_TABELA_PRECO_CAB   AS TC ON TC.ID_TABELA = TI.ID_TABELA " +
                            " WHERE P.ID_PRODUTO = '" + webPedidoItem.getId_produto() + "' ORDER BY P.ATIVO DESC, P.NOME_PRODUTO;";


                    lista = produtoDAO.getLista(SQL,"");
                    if ( lista.size() > 0) {
                        totalProdutoBruto = quantidade * Float.parseFloat( lista.get(0).getVenda_preco()) ;
                        webPedidoItem.setValor_unitario(Float.parseFloat( lista.get(0).getVenda_preco() ));
                        webPedidoItem.setVenda_preco(lista.get(0).getVenda_preco() );
                        descontoReais = (Float.parseFloat(webPedidoItem.getValor_desconto_per()) * (totalProdutoBruto)) / 100;
                        webPedidoItem.setValor_total(String.valueOf(totalProdutoBruto - descontoReais));
                        webPedidoItem.setValor_bruto(String.valueOf(totalProdutoBruto));
                        webPedidoItem.setValor_desconto_real(MascaraUtil.duasCasaDecimal(descontoReais));
                    }
                }
            }
        }

        listaAdapterProdutoPedido.notifyDataSetChanged();
        for (int i = 0; listaProdutoPedido.size() > i; i++)
            webPedidoItensDAO.update(listaProdutoPedido.get(i));
        listaAdapterProdutoPedido.notifyDataSetChanged();
        PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        PedidoHelper.getActivityPedidoMain().updatePedido();

    }

    private void verificaDescontoSemTabela() {
        List<Produto> lista = new ArrayList<>();
        Float totalProdutoBruto = 0.0f;
        Float quantidade = 0.0f;
        Float descontoReais = 0.0f;
        String SQL;
        for (WebPedidoItens webPedidoItem : listaProdutoPedido) {
            quantidade = Float.parseFloat(webPedidoItem.getQuantidade());
            if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                SQL = "SELECT P.*,  TI.VALOR_UNITARIO AS PRECO_TABELA_CLIENTE, TC.NOME_TABELA AS NOME_TABELA  FROM TBL_PRODUTO AS P " +
                        " LEFT JOIN TBL_TABELA_PRECO_ITENS AS TI ON TI.ID_PRODUTO = P.ID_PRODUTO  AND TI.ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco() +
                        " LEFT JOIN TBL_TABELA_PRECO_CAB   AS TC ON TC.ID_TABELA = TI.ID_TABELA " +
                        " WHERE P.ID_PRODUTO = '" + webPedidoItem.getId_produto() + "' ORDER BY P.ATIVO DESC, P.NOME_PRODUTO;";


                lista = produtoDAO.getLista(SQL,"");
                if (lista.size() > 0) {
                    totalProdutoBruto = quantidade * Float.parseFloat(lista.get(0).getVenda_preco());
                    webPedidoItem.setValor_unitario(Float.parseFloat(lista.get(0).getVenda_preco()));
                    webPedidoItem.setVenda_preco(lista.get(0).getVenda_preco() );

                    //produtoSelecionado.setValor_total(String.valueOf((Float.parseFloat(produto.getVenda_preco()) * quantidade) - ((Float.parseFloat(edtDesconto.getText().toString()) / 100) * (Float.parseFloat(produto.getVenda_preco()) * quantidade))));

                    descontoReais = (Float.parseFloat(webPedidoItem.getValor_desconto_per()) * (totalProdutoBruto)) / 100;
                    webPedidoItem.setValor_total(String.valueOf(totalProdutoBruto - descontoReais));
                    webPedidoItem.setValor_bruto(String.valueOf(totalProdutoBruto));
                    webPedidoItem.setValor_desconto_real(MascaraUtil.duasCasaDecimal(descontoReais));

                }
            }
        }
        listaAdapterProdutoPedido.notifyDataSetChanged();
        for (int i = 0; listaProdutoPedido.size() > i; i++)
            webPedidoItensDAO.update(listaProdutoPedido.get(i));

        listaAdapterProdutoPedido.notifyDataSetChanged();
        PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
        PedidoHelper.getActivityPedidoMain().updatePedido();

    }

    private void verificaDescontoIndevido( PedidoBO pedidoBO) {
        boolean descontoIndevido = false;
        Float descontoPromocao = 0.00f;
        Float descontoTabela   = 0.00f;
        Float descontoPrazo    = 0.00f;
        Float descontoMaximo   = 0.00f;
        for (WebPedidoItens webPedidoItens : listaProdutoPedido) {
            //Float descontoMaximoPermitido = pedidoBO.calculaDesconto(ClienteHelper.getCliente().getId_cadastro_servidor(), webPedidoItens.getId_produto(), getActivity()).getValorDesconto();
            try {
                descontoPromocao = pedidoBO.calculaDesconto(ClienteHelper.getCliente().getId_cadastro_servidor(), webPedidoItens.getId_produto(), getActivity()).getValorDesconto();
            } catch ( NullPointerException|NumberFormatException e) {

            } catch ( Exception e) {

            }
            try {
                tabelaPrecoItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                descontoTabela = Float.parseFloat(tabelaPrecoItem.getPerc_desc_final());
            } catch ( NullPointerException|NumberFormatException e) {
                 e.printStackTrace();
            } catch ( Exception e) {
                   e.printStackTrace();
            }

            try {
                Cursor cursor = db.listaDados("SELECT DESCONTO_PERC FROM TBL_CONDICOES_PAG_CAB WHERE ID_CONDICAO = '" + PedidoHelper.getWebPedido().getId_condicao_pagamento() + "' AND ACEITA_DESCONTO = 'S'");
                if (cursor.moveToNext())
                    descontoPrazo = Float.parseFloat(cursor.getString( cursor.getColumnIndex("DESCONTO_PERC")));
            } catch (SQLException|CursorIndexOutOfBoundsException|NumberFormatException|NullPointerException e) {
                e.printStackTrace();
            } catch ( Exception e ) {
                e.printStackTrace();
            }

            if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S") &&
                    ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")) {
                descontoMaximo = descontoTabela;

            } else {
                descontoMaximo = descontoTabela;
            }
            if ( descontoMaximo <= 0) {
                descontoMaximo = descontoPrazo;
            }

            if ( descontoPromocao > descontoMaximo) {
                descontoMaximo = descontoPromocao;
            }

            /*
            try {
                if (descontoMaximoPermitido <= 0 || Float.parseFloat(tabelaPrecoItem.getPerc_desc_final()) > descontoMaximoPermitido) {
                    if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S") &&
                            ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2") &&
                            tabelaPrecoItem == null) {
                        if (ClienteHelper.getCliente().getIdCategoria() >= 1) {
                            tabelaPrecoItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                            try {
                                descontoMaximoPermitido = Float.parseFloat(tabelaPrecoItem.getPerc_desc_final());
                            } catch (NullPointerException | NumberFormatException | IndexOutOfBoundsException e) {
                                descontoMaximoPermitido = 0.0f;
                            } catch (Exception e) {
                                descontoMaximoPermitido = 0.0f;
                            }
                        }
                    }    else{
                        descontoMaximoPermitido = Float.parseFloat(tabelaPrecoItem.getPerc_desc_final());
                    }
                }
            } catch ( NumberFormatException|NullPointerException e) {
                if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S") &&
                        ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2") &&
                        tabelaPrecoItem == null) {
                    if (ClienteHelper.getCliente().getIdCategoria() >= 1) {
                        tabelaPrecoItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                        try {
                            if ( descontoMaximoPermitido <= Float.parseFloat(tabelaPrecoItem.getPerc_desc_final()))
                                     descontoMaximoPermitido = Float.parseFloat(tabelaPrecoItem.getPerc_desc_final());
                        } catch (NullPointerException | NumberFormatException | IndexOutOfBoundsException ex) {
                            descontoMaximoPermitido = 0.0f;
                        } catch (Exception ex) {
                            descontoMaximoPermitido = 0.0f;
                        }
                    }
                }
            } catch ( Exception e ) {
                descontoMaximoPermitido = 0.0f;
            }
            if ( descontoMaximoPermitido <= 0.0f ) {
                try {
                    Cursor cursor = db.listaDados("SELECT DESCONTO_PERC FROM TBL_CONDICOES_PAG_CAB WHERE ID_CONDICAO = '" + PedidoHelper.getWebPedido().getId_condicao_pagamento() + "' AND ACEITA_DESCONTO = 'S'");
                    if (cursor.moveToNext())
                        descontoMaximoPermitido = Float.parseFloat(cursor.getString( cursor.getColumnIndex("DESCONTO_PERC")));
                } catch (SQLException |NumberFormatException|NullPointerException e) {
                    e.printStackTrace();
                } catch ( Exception e ) {
                    e.printStackTrace();
                }

            }
            try {
                if (ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S") && ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")
                        && ClienteHelper.getCliente().getIdCategoria() > 0 ) {
                    CondicoesPagamentoDAO condicoesPagamentoDAO = new CondicoesPagamentoDAO(db);
                    CondicoesPagamento condicoesPagamento = condicoesPagamentoDAO.getCondicao("SELECT * FROM TBL_CONDICOES_PAG_CAB WHERE ID_CONDICAO = '" + PedidoHelper.getWebPedido().getId_condicao_pagamento() + "'");
                    if ( condicoesPagamento != null ) {
                        if ( condicoesPagamento.getAceita_desconto().equalsIgnoreCase("S")) {
                            if ( ClienteHelper.getCliente().getId_cadastro_servidor() > 0) {
                                descontoMaximoPermitido = pedidoBO.calculaDesconto(ClienteHelper.getCliente());
                            }
                        }
                    }
                }
            } catch ( NumberFormatException|NullPointerException e) {
                e.printStackTrace();
            } catch ( Exception e) {
                e.printStackTrace();
            }
        */


            if (Float.parseFloat(webPedidoItens.getValor_desconto_per()) > descontoMaximo) {
                webPedidoItens.setDescontoIndevido(true);
                descontoIndevido = true;
            }else {
                webPedidoItens.setDescontoIndevido(false);
            }
            if ( descontoIndevido ){
                PedidoHelper.getWebPedido().setDescontoIndevido("S");
                PedidoHelper.getActivityPedidoMain().updatePedido();
            }
        }
    }

    private void setTabelaPrecoItem() {
        try {
            if ( ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("4"))
                    tabelaPrecoItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_CATEGORIA = " + ClienteHelper.getCliente().getIdCategoria()).get(0);
                else if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("6"))
                    tabelaPrecoItem = db.listaTabelaPrecoItem("SELECT * FROM TBL_TABELA_PRECO_ITENS WHERE ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco()).get(0);
            }
        } catch ( IndexOutOfBoundsException | NullPointerException e ) {
            e.printStackTrace();
        } catch ( Exception e) {
            e.printStackTrace();
        }
    }
}
/*
                                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                alert.setTitle("Atenção");
                                alert.setMessage("Você alterou o cliente. Deseja alterar o preço do(s) produto(s) para a tabela do cliente " + ClienteHelper.getCliente().getNome_cadastro() + "?");
                                alert.setNegativeButton("NÃO", null);
                                alert.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Float totalProdutoBruto = 0.0f;
                                        Float quantidade = 0.0f;
                                        Float descontoReais = 0.0f;
                                        List<Produto> lista = new ArrayList<>();
                                        String SQL;

                                        for (WebPedidoItens webPedidoItem : listaProdutoPedido) {
                                            if (!webPedidoItem.getProduto_tercerizacao().equals("S") && !webPedidoItem.getProduto_materia_prima().equals("S")) {
                                                List<TabelaPrecoItem> listaTabelaPrecoItem = db.listaTabelaPrecoItem("SELECT VALOR_UNITARIO, PERC_DESC_FINAL FROM TBL_TABELA_PRECO_ITENS WHERE ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco());
                                                quantidade = Float.parseFloat(webPedidoItem.getQuantidade());
                                                //webPedidoItem.setValor_desconto_per(descontoCategoria);
                                                if (listaTabelaPrecoItem.size() > 0) {
                                                    totalProdutoBruto = quantidade * Float.parseFloat(listaTabelaPrecoItem.get(0).getValor_unitario());
                                                    webPedidoItem.setValor_unitario(Float.parseFloat(listaTabelaPrecoItem.get(0).getValor_unitario()));
                                                    webPedidoItem.setVenda_preco(listaTabelaPrecoItem.get(0).getValor_unitario() );
                                                    descontoReais = (Float.parseFloat( webPedidoItem.getValor_desconto_per()) * (totalProdutoBruto)) / 100;
                                                    webPedidoItem.setValor_total(String.valueOf(totalProdutoBruto - descontoReais ));
                                                    webPedidoItem.setValor_bruto(String.valueOf(totalProdutoBruto));
                                                    webPedidoItem.setValor_desconto_real(MascaraUtil.duasCasaDecimal(descontoReais));


                                                    //webPedidoItem.setDescontoIndevido(false);
                                                    // Float descontoReais = (Float.parseFloat(descontoCategoria) * Float.parseFloat(webPedidoItem.getValor_bruto())) / 100;
                                                    //webPedidoItem.setValor_desconto_real(String.valueOf(descontoReais));
                                                    //webPedidoItem.setValor_total(String.valueOf(Float.parseFloat(webPedidoItem.getValor_bruto()) - descontoReais));
                                                } else {
                                                   SQL = "SELECT P.*,  TI.VALOR_UNITARIO AS PRECO_TABELA_CLIENTE, TC.NOME_TABELA AS NOME_TABELA  FROM TBL_PRODUTO AS P " +
                                                            " LEFT JOIN TBL_TABELA_PRECO_ITENS AS TI ON TI.ID_PRODUTO = P.ID_PRODUTO  AND TI.ID_TABELA = " + ClienteHelper.getCliente().getId_tabela_preco() +
                                                            " LEFT JOIN TBL_TABELA_PRECO_CAB   AS TC ON TC.ID_TABELA = TI.ID_TABELA " +
                                                            " WHERE P.ID_PRODUTO = '" + webPedidoItem.getId_produto() + "' ORDER BY P.ATIVO DESC, P.NOME_PRODUTO;";


                                                    lista = db.listaProduto(SQL);
                                                    if ( lista.size() > 0) {
                                                        totalProdutoBruto = quantidade * Float.parseFloat( lista.get(0).getVenda_preco()) ;
                                                        webPedidoItem.setValor_unitario(Float.parseFloat( lista.get(0).getVenda_preco() ));
                                                        webPedidoItem.setVenda_preco(lista.get(0).getVenda_preco() );
                                                        descontoReais = (Float.parseFloat(webPedidoItem.getValor_desconto_per()) * (totalProdutoBruto)) / 100;
                                                        webPedidoItem.setValor_total(String.valueOf(totalProdutoBruto - descontoReais));
                                                        webPedidoItem.setValor_bruto(String.valueOf(totalProdutoBruto));
                                                        webPedidoItem.setValor_desconto_real(MascaraUtil.duasCasaDecimal(descontoReais));
                                                    }
                                                }
                                            }
                                        }
                                        listaAdapterProdutoPedido.notifyDataSetChanged();
                                        for (int i = 0; listaProdutoPedido.size() > i; i++) {
                                            webPedidoItensDAO.atualizarTBL_WEB_PEDIDO_ITENS(listaProdutoPedido.get(i));
                                        }
                                        listaAdapterProdutoPedido.notifyDataSetChanged();
                                        PedidoHelper.calculaValorPedido(listaProdutoPedido, PedidoHelper.getActivityPedidoMain());
                                        PedidoHelper.getActivityPedidoMain().updatePedido();
                                    }
                                });
                                alert.show();

                                 */

