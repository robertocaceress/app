package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;

import com.example.rcksuporte05.rcksistemas.model.Municipio;
import com.example.rcksuporte05.rcksistemas.model.Pais;

import java.util.ArrayList;
import java.util.List;

public class MunicipioDAO {

    private DBHelper db;
    public MunicipioDAO(DBHelper db) {
        this.db = db;
    }

    public long add(Municipio municipio) {
        ContentValues content = new ContentValues();
        try {
            content.put("ID", municipio.getId());
            content.put("NOME_MUNICIPIO", municipio.getNome_municipio());
            content.put("UF", municipio.getUf());
            System.gc();
            return db.addDados("TBL_CADASTRO_MUNICIPIO", content);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Municipio> getLista(String SQL)  {
        List<Municipio> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        do {
            Municipio municipio = new Municipio();
            try {
                municipio.setId(cursor.getString(cursor.getColumnIndex("ID")));
                municipio.setNome_municipio(cursor.getString(cursor.getColumnIndex("NOME_MUNICIPIO")));
                municipio.setUf(cursor.getString(cursor.getColumnIndex("UF")));
                lista.add(municipio);
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }
}
