package com.example.rcksuporte05.rcksistemas.model;

public class Configuracao {
    private String id;

    private String usuario;
    private String senha;
    private String url_ip_1;
    private int porta_1;
    private String url_ip_desc_1;
    private String url_ip_2;
    private int porta_2;
    private String url_ip_desc_2;
    private String conexao_segura;
    private String trabalha_com_campanhas;
    private String trabalha_com_dscto_reais;
    private String trabalha_com_limite_cred;
    private String trabalha_com_pre_pedido;
    private String pesquisa_produto_por_linha_grupo; //G Grupo L Linha
    private String trabalha_com_confirmacao_prazo;
    private String trabalha_com_unidade_manut_estoque;
    private String trabalha_com_paginacao_dados;
    private String layout_venda_erp;
    private String id_con_pgto_default_app;
    private String trabalha_com_cobranca;
    private String trabalha_com_icms_st;
    private String trabalha_com_desc_auto_app; //Desconto automatico ao incluir item no app
    private String sincronia_automatica_app;
    private String trabalha_com_verba_pedido;
    private String lista_todos_pedidos;

    public Configuracao() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUrl_ip_1() {
        return url_ip_1;
    }

    public void setUrl_ip_1(String url_ip_1) {
        this.url_ip_1 = url_ip_1;
    }

    public String getUrl_ip_desc_1() {
        return url_ip_desc_1;
    }

    public void setUrl_ip_desc_1(String url_ip_desc_1) {
        this.url_ip_desc_1 = url_ip_desc_1;
    }

    public String getUrl_ip_2() {
        return url_ip_2;
    }

    public void setUrl_ip_2(String url_ip_2) {
        this.url_ip_2 = url_ip_2;
    }

    public String getUrl_ip_desc_2() {
        return url_ip_desc_2;
    }

    public void setUrl_ip_desc_2(String url_ip_desc_2) {
        this.url_ip_desc_2 = url_ip_desc_2;
    }

    public int getPorta_1() {
        return porta_1;
    }

    public void setPorta_1(int porta_1) {
        this.porta_1 = porta_1;
    }

    public int getPorta_2() {
        return porta_2;
    }

    public void setPorta_2(int porta_2) {
        this.porta_2 = porta_2;
    }

    public String getConexao_segura() {
        return conexao_segura;
    }

    public void setConexao_segura(String conexao_segura) {
        this.conexao_segura = conexao_segura;
    }

    public String getTrabalha_com_campanhas() {
        return trabalha_com_campanhas;
    }

    public void setTrabalha_com_campanhas(String trabalha_com_campanhas) {
        this.trabalha_com_campanhas = trabalha_com_campanhas;
    }

    public String getTrabalha_com_dscto_reais() {
        return trabalha_com_dscto_reais;
    }

    public void setTrabalha_com_dscto_reais(String trabalha_com_dscto_reais) {
        this.trabalha_com_dscto_reais = trabalha_com_dscto_reais;
    }

    public String getTrabalha_com_limite_cred() {
        return trabalha_com_limite_cred;
    }

    public void setTrabalha_com_limite_cred(String trabalha_com_limite_cred) {
        this.trabalha_com_limite_cred = trabalha_com_limite_cred;
    }

    public String getTrabalha_com_pre_pedido() {
        return trabalha_com_pre_pedido;
    }

    public void setTrabalha_com_pre_pedido(String trabalha_com_pre_pedido) {
        this.trabalha_com_pre_pedido = trabalha_com_pre_pedido;
    }

    public String getPesquisa_produto_por_linha_grupo() {
        return pesquisa_produto_por_linha_grupo;
    }

    public void setPesquisa_produto_por_linha_grupo(String pesquisa_produto_por_linha_grupo) {
        this.pesquisa_produto_por_linha_grupo = pesquisa_produto_por_linha_grupo;
    }

    public String getTrabalha_com_confirmacao_prazo() {
        return trabalha_com_confirmacao_prazo;
    }

    public void setTrabalha_com_confirmacao_prazo(String trabalha_com_confirmacao_prazo) {
        this.trabalha_com_confirmacao_prazo = trabalha_com_confirmacao_prazo;
    }

    public String getTrabalha_com_unidade_manut_estoque() {
        return trabalha_com_unidade_manut_estoque;
    }

    public void setTrabalha_com_unidade_manut_estoque(String trabalha_com_unidade_manut_estoque) {
        this.trabalha_com_unidade_manut_estoque = trabalha_com_unidade_manut_estoque;
    }

    public String getTrabalha_com_paginacao_dados() {
        return trabalha_com_paginacao_dados;
    }

    public void setTrabalha_com_paginacao_dados(String trabalha_com_paginacao_dados) {
        this.trabalha_com_paginacao_dados = trabalha_com_paginacao_dados;
    }

    public String getLayout_venda_erp() {
        return layout_venda_erp;
    }

    public void setLayout_venda_erp(String layout_venda_erp) {
        this.layout_venda_erp = layout_venda_erp;
    }

    public String getId_con_pgto_default_app() {
        return id_con_pgto_default_app;
    }

    public void setId_con_pgto_default_app(String id_con_pgto_default_app) {
        this.id_con_pgto_default_app = id_con_pgto_default_app;
    }

    public String getTrabalha_com_cobranca() {
        return trabalha_com_cobranca;
    }

    public void setTrabalha_com_cobranca(String trabalha_com_cobranca) {
        this.trabalha_com_cobranca = trabalha_com_cobranca;
    }

    public String getTrabalha_com_icms_st() {
        return trabalha_com_icms_st;
    }

    public void setTrabalha_com_icms_st(String trabalha_com_icms_st) {
        this.trabalha_com_icms_st = trabalha_com_icms_st;
    }

    public String getTrabalha_com_desc_auto_app() {
        return trabalha_com_desc_auto_app;
    }

    public void setTrabalha_com_desc_auto_app(String trabalha_com_desc_auto_app) {
        this.trabalha_com_desc_auto_app = trabalha_com_desc_auto_app;
    }

    public String getSincronia_automatica_app() {
        return sincronia_automatica_app;
    }

    public void setSincronia_automatica_app(String sincronia_automatica_app) {
        this.sincronia_automatica_app = sincronia_automatica_app;
    }

    public String getTrabalha_com_verba_pedido() {
        return trabalha_com_verba_pedido;
    }

    public void setTrabalha_com_verba_pedido(String trabalha_com_verba_pedido) {
        this.trabalha_com_verba_pedido = trabalha_com_verba_pedido;
    }

    public String getLista_todos_pedidos() {
        return lista_todos_pedidos;
    }

    public void setLista_todos_pedidos(String lista_todos_pedidos) {
        this.lista_todos_pedidos = lista_todos_pedidos;
    }
}
