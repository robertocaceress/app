package com.example.rcksuporte05.rcksistemas.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.CursorIndexOutOfBoundsException;
import android.icu.math.BigDecimal;
import android.icu.text.NumberFormat;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rcksuporte05.rcksistemas.DAO.CondicoesPagamentoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.ConfiguracaoDAO;
import com.example.rcksuporte05.rcksistemas.DAO.DBHelper;
import com.example.rcksuporte05.rcksistemas.DAO.EmpresaParametroDAO;
import com.example.rcksuporte05.rcksistemas.DAO.MoedaDAO;
import com.example.rcksuporte05.rcksistemas.DAO.OperacaoEstoqueDAO;
import com.example.rcksuporte05.rcksistemas.DAO.WebPedidoDAO;
import com.example.rcksuporte05.rcksistemas.Helper.ClienteHelper;
import com.example.rcksuporte05.rcksistemas.Helper.PedidoHelper;
import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.activity.ActivityCobrancaRecebimento;
import com.example.rcksuporte05.rcksistemas.activity.ActivityPedidoMain;
import com.example.rcksuporte05.rcksistemas.model.Cliente;
import com.example.rcksuporte05.rcksistemas.model.CondicoesPagamento;
import com.example.rcksuporte05.rcksistemas.model.Configuracao;
import com.example.rcksuporte05.rcksistemas.model.Moeda;
import com.example.rcksuporte05.rcksistemas.model.Operacao;
import com.example.rcksuporte05.rcksistemas.model.WebPedido;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Pedido2 extends Fragment {

    private static Cliente objetoCliente = null;
    @BindView(R.id.spPagamento)
    Spinner spPagamento;

    @BindView(R.id.spOperacao)
    Spinner spOperacao;

    @BindView(R.id.lytMoeda)
    LinearLayout lytMoeda;
    @BindView(R.id.spMoeda)
    Spinner spMoeda;

    @BindView(R.id.txtDataEmissao)
    TextView edtDataEmissao;

    @BindView(R.id.txtDaDataDeEmissao)
    TextView txtDataEmissao;

    @BindView(R.id.txtDataEntrega)
    TextView txtDataEntrega;

    @BindView(R.id.edtDataEntrega)
    EditText edtDataEntrega;

    @BindView(R.id.edtValorRecibo)
    EditText edtValorRecibo;

    @BindView(R.id.edtSaldoDevedor)
    EditText edtSaldoDevedor;

    @BindView(R.id.edtObservacao)
    EditText edtObservacao;

    @BindView(R.id.btnSalvarPedido)
    Button btnSalvarPedido;
    public Boolean control = false;
    private ArrayAdapter<Operacao> adapterOperacao;
    private ArrayAdapter<CondicoesPagamento> adapterPagamento;
    private ArrayAdapter<Moeda> adapterMoeda;
    private DBHelper db = new DBHelper( PedidoHelper.getActivityPedidoMain());
    private WebPedidoDAO webPedidoDAO;
    private WebPedido webPedido = new WebPedido();
    private EmpresaParametroDAO empresaParametroDAO  = new EmpresaParametroDAO(db);
    private ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
    private Configuracao configuracao;
    private Bundle bundle;
    private PedidoHelper pedidoHelper;
    private Context context;
    private String idCondPag = "";
    //String position;
    Locale mLocale;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pedido2, container, false);
        ButterKnife.bind(this, view);
        context = this.getContext();
        webPedidoDAO = new WebPedidoDAO(db);
        try {
            edtDataEmissao.setText(db.pegaDataAtual());
        } catch ( NullPointerException e) {
            e.printStackTrace();
        }
        pedidoHelper = new PedidoHelper(this);
        configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
        edtSaldoDevedor.setFocusable(false);

        bundle = getArguments();
        try {
            CondicoesPagamentoDAO condicoesPagamentoCabDAO = new CondicoesPagamentoDAO(db);
            OperacaoEstoqueDAO operacaoEstoqueDAO = new OperacaoEstoqueDAO(db);
            List<CondicoesPagamento> listaCondicoesPagamentos = new ArrayList<>();
            String sql;
            try {
                if ( PedidoHelper.getWebPedido().getCadastro() != null) {
                    sql = "SELECT CP.* FROM TBL_CONDICOES_PAG_CAB AS CP INNER JOIN TBL_CADASTRO_CONDICOES_PAG AS CCP ON ( CCP.ID_CONDICAO = CP.ID_CONDICAO AND CCP.ID_CADASTRO = ";
                    sql += PedidoHelper.getWebPedido().getCadastro().getId_cadastro_servidor() + ") WHERE CP.ATIVO = 'S' AND  ( CP.NOME_CONDICAO != '' AND CP.NOME_CONDICAO IS NOT NULL) ORDER BY CP.ID_CONDICAO";
                    if ( configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                        float limiteCredito = Float.parseFloat(ClienteHelper.getCliente().getLimite_credito());
                        float limitUtilizado = Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencido()) + Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencendo());
                        if ( (limiteCredito - limitUtilizado) < 0.00f) {
                            sql = "SELECT CP.* FROM TBL_CONDICOES_PAG_CAB AS CP INNER JOIN TBL_CADASTRO_CONDICOES_PAG AS CCP ON ( CCP.ID_CONDICAO = CP.ID_CONDICAO AND CCP.ID_CADASTRO = ";
                            sql += PedidoHelper.getWebPedido().getCadastro().getId_cadastro_servidor() + ") WHERE CP.ATIVO = 'S' AND  ( CP.NOME_CONDICAO != '' AND CP.NOME_CONDICAO IS NOT NULL) AND CP.TIPO_CONDICAO = '1' ORDER BY CP.ID_CONDICAO";
                        }
                    }
                    listaCondicoesPagamentos = condicoesPagamentoCabDAO.getLista(sql);
                }
            } catch ( NullPointerException|CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            try {
                if (listaCondicoesPagamentos.size() <= 0) {
                    sql = "SELECT * FROM TBL_CONDICOES_PAG_CAB  WHERE ATIVO = 'S' AND  (NOME_CONDICAO != '' AND NOME_CONDICAO IS NOT NULL) ORDER BY ID_CONDICAO;";
                    if (configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                        float limiteCredito = Float.parseFloat(ClienteHelper.getCliente().getLimite_credito());
                        float limitUtilizado = Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencido()) + Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencendo());
                        if ((limiteCredito - limitUtilizado) < 0.00f) {
                            sql = "SELECT * FROM TBL_CONDICOES_PAG_CAB  WHERE ATIVO = 'S' AND  (NOME_CONDICAO != '' AND NOME_CONDICAO IS NOT NULL) AND TIPO_CONDICAO = '1' ORDER BY ID_CONDICAO;";
                        }
                    }
                    listaCondicoesPagamentos = condicoesPagamentoCabDAO.getLista(sql);
                }
            } catch ( NullPointerException e) {
                e.printStackTrace();
            }
            adapterPagamento = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, listaCondicoesPagamentos);

            spPagamento.setAdapter(adapterPagamento);
            try {
                if (!configuracao.getId_con_pgto_default_app().equalsIgnoreCase("0")) {
                    for (int i = 0; i < listaCondicoesPagamentos.size(); i++)
                        if (listaCondicoesPagamentos.get(i).getId_condicao().equalsIgnoreCase(configuracao.getId_con_pgto_default_app()))
                            spPagamento.setSelection(i);
                } else
                    spPagamento.setSelection(0);
            } catch ( NullPointerException e) {
                try {
                    if ( PedidoHelper.getWebPedido().getId_condicao_pagamento() !=null || !PedidoHelper.getWebPedido().getId_condicao_pagamento().isEmpty()) {
                        for ( int i = 0; i < listaCondicoesPagamentos.size(); i++) {
                            if ( listaCondicoesPagamentos.get(i).getId_condicao().equalsIgnoreCase(PedidoHelper.getWebPedido().getId_condicao_pagamento())) {
                                spPagamento.setSelection(i);
                                break;
                            }

                        }
                    }
                } catch ( NullPointerException ex) {
                    spPagamento.setSelection(0);
                }

            }
            //idCondPag = adapterPagamento.getItem(0).getId_condicao();

            adapterOperacao = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, operacaoEstoqueDAO.getLista("SELECT * FROM TBL_OPERACAO_ESTOQUE;"));
            spOperacao.setAdapter(adapterOperacao);

            try {
                MoedaDAO moedaDAO = new MoedaDAO(db);
                List<Moeda> lista = moedaDAO.getLista("SELECT * FROM TBL_MOEDA;");
                adapterMoeda = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, lista);
                spMoeda.setAdapter(adapterMoeda);
                if (lista.size() <= 0) {
                    lytMoeda.setVisibility(View.INVISIBLE);
                    spMoeda.setVisibility(View.INVISIBLE);
                }
            } catch ( CursorIndexOutOfBoundsException | NullPointerException ex) {
                lytMoeda.setVisibility(View.INVISIBLE);
                spMoeda.setVisibility(View.INVISIBLE);
            } catch ( Exception ex) {
                lytMoeda.setVisibility(View.INVISIBLE);
                spMoeda.setVisibility(View.INVISIBLE);
            }

        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        if (PedidoHelper.getIdPedido() > 0) {
            webPedido = webPedidoDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO WHERE ID_WEB_PEDIDO = " + PedidoHelper.getIdPedido()).get(0);
            objetoCliente = webPedido.getCadastro();

            //Seleciona Condição de pagamento correta dentro do Spinner spPagamento
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_condicao_pagamento().equals(adapterPagamento.getItem(i).getId_condicao()));
                spPagamento.setSelection(i);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Seleciona operação correta dentro do Spinner spOperacao
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_operacao().equals(adapterOperacao.getItem(i).getId_operacao()));
                spOperacao.setSelection(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Seleciona operação correta dentro do Spinner spMoeda
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_moeda_padrao().equals(adapterMoeda.getItem(i).getId_moeda()));
                spMoeda.setSelection(i);
            } catch ( NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            edtObservacao.setText(webPedido.getObservacoes());
            try {
                edtValorRecibo.setText( MascaraUtil.duasCasaDecimal(webPedido.getValor_recibo()));
            } catch ( NullPointerException|NumberFormatException e) {
                edtValorRecibo.setText(MascaraUtil.mascaraReal("0.00"));
            }
            try {
                    edtSaldoDevedor.setText( MascaraUtil.mascaraReal(Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencido())
                            + Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencendo()) )) ;
                } catch ( NullPointerException|NumberFormatException e) {
                    edtSaldoDevedor.setText(MascaraUtil.mascaraReal("0.00"));
            }
            try {
                edtDataEmissao.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedido.getData_emissao())));
                edtDataEntrega.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedido.getData_prev_entrega())));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (!configuracao.getLayout_venda_erp().equalsIgnoreCase("25")) {
                    txtDataEmissao.setText("DATA DE EMISSÃO");
                    txtDataEntrega.setText("DATA DE FATURAMENTO");
                    hideEditValorRecibo();
                    showEditDataEntrega();
                } else {
                    txtDataEmissao.setText("SALDO DEVEDOR");
                    txtDataEntrega.setText("VALOR RECIBO");
                    hideEditDataEntrega();
                    showEditValorRecibo();
                }
            } catch (NullPointerException e) {
                txtDataEmissao.setText("DATA DE EMISSÃO");
                txtDataEntrega.setText("DATA DE FATURAMENTO");
                hideEditValorRecibo();
                showEditDataEntrega();
            }
        } else if (PedidoHelper.getWebPedido() != null) {
            webPedido = PedidoHelper.getWebPedido();
            objetoCliente = webPedido.getCadastro();
            //Seleciona Condição de pagamento correta dentro do Spinner spPagamento
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_condicao_pagamento().equals(adapterPagamento.getItem(i).getId_condicao()));
                spPagamento.setSelection(i);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Seleciona operação correta dentro do Spinner spOperacao
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_operacao().equals(adapterOperacao.getItem(i).getId_operacao()));
                spOperacao.setSelection(i);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Seleciona operação correta dentro do Spinner spMoeda
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_moeda_padrao().equals(adapterMoeda.getItem(i).getId_moeda()));
                spMoeda.setSelection(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
            edtObservacao.setText(webPedido.getObservacoes());
            try {
                edtValorRecibo.setText( MascaraUtil.duasCasaDecimal(webPedido.getValor_recibo()));
            } catch ( NullPointerException|NumberFormatException e) {
                edtValorRecibo.setText(MascaraUtil.duasCasaDecimal("0.00"));
            }
            try {
                edtSaldoDevedor.setText(MascaraUtil.mascaraReal(Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencido())
                        + Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencendo()) ));
            } catch ( NullPointerException|NumberFormatException e) {
                edtSaldoDevedor.setText(MascaraUtil.mascaraReal("0.00"));
            }
            try {
                edtDataEmissao.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedido.getData_emissao())));
                if ( !configuracao.getLayout_venda_erp().equalsIgnoreCase("25") ) {
                    txtDataEmissao.setText("DATA DE EMISSÃO");
                    txtDataEntrega.setText("DATA DE FATURAMENTO");
                    edtDataEntrega.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(webPedido.getData_prev_entrega())));
                    hideEditValorRecibo();
                    showEditDataEntrega();
                } else {
                    txtDataEmissao.setText("SALDO DEVEDOR");
                    txtDataEntrega.setText("VALOR RECIBO ");
                    edtDataEntrega.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(db.pegaDataAtual())));
                    hideEditDataEntrega();
                    showEditValorRecibo();
                }
            } catch (Exception e) {
                if ( !configuracao.getLayout_venda_erp().equalsIgnoreCase("25") ) {
                    txtDataEmissao.setText("DATA DE EMISSÃO");
                    txtDataEntrega.setText("DATA DE FATURAMENTO");
                    hideEditValorRecibo();
                    showEditDataEntrega();
                } else{
                    txtDataEmissao.setText("SALDO DEVEDOR");
                    txtDataEntrega.setText("VALOR RECIBO ");
                    hideEditDataEntrega();
                    showEditValorRecibo();
                    try {
                        edtDataEntrega.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(db.pegaDataAtual())));
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                }
                try {
                    edtDataEmissao.setText(new SimpleDateFormat("dd/MM/yyyy")
                            .format(new SimpleDateFormat("yyyy-MM-dd")
                                    .parse(db.pegaDataAtual())));
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            try {
                edtValorRecibo.setText( MascaraUtil.duasCasaDecimal(webPedido.getValor_recibo()));
            } catch ( NullPointerException|NumberFormatException e) {
                edtValorRecibo.setText(MascaraUtil.duasCasaDecimal("0.00"));
            }
            try {
                edtSaldoDevedor.setText(MascaraUtil.mascaraReal(Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencido())
                        + Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencendo()) ));
            } catch ( NullPointerException|NumberFormatException e) {
                edtSaldoDevedor.setText(MascaraUtil.mascaraReal("0.00"));
            }
            try {
                if (!configuracao.getLayout_venda_erp().equalsIgnoreCase("25")) {
                    txtDataEmissao.setText("DATA DE EMISSÃO");
                    txtDataEntrega.setText("DATA DE FATURAMENTO");
                    hideEditValorRecibo();
                    showEditDataEntrega();
                } else {
                    txtDataEmissao.setText("SALDO DEVEDOR");
                    txtDataEntrega.setText("VALOR RECIBO ");
                    hideEditDataEntrega();
                    showEditValorRecibo();
                    try {
                        edtDataEntrega.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(db.pegaDataAtual())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            } catch ( NullPointerException e) {
                txtDataEmissao.setText("DATA DE EMISSÃO");
                txtDataEntrega.setText("DATA DE FATURAMENTO");
                hideEditValorRecibo();
                showEditDataEntrega();
            }
            try {
                edtDataEmissao.setText(new SimpleDateFormat("dd/MM/yyyy")
                        .format(new SimpleDateFormat("yyyy-MM-dd")
                                .parse(db.pegaDataAtual())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        btnSalvarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfiguracaoDAO configuracaoDAO = new ConfiguracaoDAO(db);
                Configuracao configuracao = configuracaoDAO.getConfiguracao("SELECT * FROM TBL_CONFIGURACAO");
                try {
                    if (configuracao.getTrabalha_com_confirmacao_prazo().equalsIgnoreCase("S"))
                        if ( PedidoHelper.getWebPedido().getPrazo_confirmado().equalsIgnoreCase("N") || PedidoHelper.getWebPedido().getPrazo_confirmado().isEmpty() ) {
                            Toast.makeText(context, "Obrigátoria a confirmação do prazo/condição de pagamento, favor clicar na lista de prazo(s)/condição de pagamento e efetivar a confirmação!!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                } catch ( NullPointerException e) {
                    e.printStackTrace();
                }
                try {
                    if (configuracao.getLayout_venda_erp().equalsIgnoreCase("25")) {
                        webPedido.setData_prev_entrega(db.pegaDataAtual());
                    }
                } catch ( NullPointerException e){
                        webPedido.setData_prev_entrega(db.pegaDataAtual());
                }
                ProgressDialog dialog = new ProgressDialog(PedidoHelper.getActivityPedidoMain());
                dialog.setTitle("Atenção!");
                dialog.setMessage("Salvando o Pedido");
                dialog.setCancelable(false);
                dialog.show();
                if (pedidoHelper.salvaPedido()) {
                    dialog.dismiss();
                    showMsgSucesso("Pedido salvo com sucesso!");
                } else
                    dialog.dismiss();
            }
        });

        if (bundle.getInt("vizualizacao") == 1) {
            btnSalvarPedido.setVisibility(View.INVISIBLE);
            edtObservacao.setFocusable(false);
            spPagamento.setEnabled(false);
            edtDataEntrega.setFocusable(false);
            edtValorRecibo.setFocusable(false);
            spOperacao.setEnabled(false);
            spMoeda.setEnabled(false);

        } else {
            edtDataEntrega.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mostraDatePickerDialog(PedidoHelper.getActivityPedidoMain(), edtDataEntrega);
                }
            });
        }

        spOperacao.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    pedidoHelper.salvaPedidoParcial();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spMoeda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if ( !(bundle.getInt("vizualizacao") == 1))
                    pedidoHelper.salvaPedidoParcial();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        /*
        try {
            spPagamento.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                    PedidoHelper.getWebPedido().getWebPedidoItens();

                }
            });
        } catch ( RuntimeException e) {
            e.printStackTrace();
        }
        */

        spPagamento.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                webPedido.setPrazo_confirmado("S");
                PedidoHelper.getWebPedido().setPrazo_confirmado("S");

                return false;
            }
        });
        spPagamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( adapterPagamento.getItem(position).getTipo_condicao().equalsIgnoreCase("1")) {
                    edtValorRecibo.setEnabled(false);
                    edtValorRecibo.setText("0.00");
                }else
                    edtValorRecibo.setEnabled(true);

                if ( idCondPag.equalsIgnoreCase(adapterPagamento.getItem(position).getId_condicao())) {
                    PedidoHelper.setCondicoesPagamento(adapterPagamento.getItem(position));
                    return;
                }
                idCondPag = adapterPagamento.getItem(position).getId_condicao();
                spPagamento.setSelection(position);
                PedidoHelper.setCondicoesPagamento(adapterPagamento.getItem(position));
                pedidoHelper.salvaPedidoParcial();
                if ( !(bundle.getInt("vizualizacao") == 1)) {
                    //pedidoHelper.salvaPedidoParcial();
                    //Alteração feita em 18/04/2019   RRC
                    try {
                        if ( ClienteHelper.getCliente().getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                            if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("6")) {
                                if (!PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("S")) {
                                    if (PedidoHelper.verificaDesconto()) {
                                        if (Pedido1.calcularDesconto() > 0.00)
                                            showMsgSimNao(0.0f, 0.0f, "A condição de pagamento informada não permite desconto(s), e foi detectado desconto(s) no(s) produto(s). Deseja limpar todo(s) o(s) desconto(s) permitido(s)?\n" +
                                                    "(O total da venda será de " + MascaraUtil.mascaraReal(PedidoHelper.calculaValorSemDesconto()) + ")");
                                        else
                                            Toast.makeText(getActivity(), "A condição de pagamento informada não permite desconto(s)! Nenhum produto/desconto cadastrado/informado!", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getActivity(), "A condição de pagamento informada não permite desconto(s)! Nenhum produto/desconto cadastrado/informado!", Toast.LENGTH_LONG).show();
                                    }

                                } else /*if (position == 1 || position ==2 )*/ {
                                    if (PedidoHelper.getIdPedido() > 0) {
                                        if (Pedido1.totalProdutos() > 0) {
                                            Float desconto_perc = 0.0f;
                                            try {
                                                String desconto = adapterPagamento.getItem(position).getDesconto_perc();
                                                desconto_perc = Float.parseFloat(adapterPagamento.getItem(position).getDesconto_perc());
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            if (desconto_perc > 0.00) {
                                                Float valorDesconto = PedidoHelper.calculaValorComDescontoPrazo(desconto_perc);
                                                Float valorTotal = 0.0f;
                                                try {
                                                    valorTotal = Float.parseFloat(PedidoHelper.getWebPedido().getValor_total());
                                                } catch (NullPointerException e) {

                                                } catch (Exception e) {

                                                }
                                                DecimalFormat decimal = new DecimalFormat("0.00");
                                                //Double vl_conv = Double.parseDouble(resultado);
                                                String total = (decimal.format(valorTotal));
                                                total = total.replaceAll(",", ".");
                                                final Float desconto_prazo = desconto_perc;
                                                String total_com_desconto = (decimal.format(PedidoHelper.calculaValorComDescontoPrazo(desconto_perc)));
                                                total_com_desconto = total_com_desconto.replaceAll(",", ".");
                                                if (Float.parseFloat(total) != Float.parseFloat(total_com_desconto)) {
                                                    showMsgSimNao(desconto_prazo, desconto_prazo, "A condição de pagamento informada permite desconto(s) de " + desconto_perc + "%\n" +
                                                            "(O total da venda com desconto será de " + MascaraUtil.mascaraReal(PedidoHelper.calculaValorComDescontoPrazo(desconto_perc)) + ")\n Deseja reprocessar os dados com o desconto de " + desconto_perc + "% para o(s) produto(s) permitido(s)?");

                                                } else {
                                                    Pedido1.verificaDescontoPrazo(desconto_perc);
                                                }
                                            } else {
                                                float valor_desconto = 0.0f;
                                                try {
                                                    valor_desconto = Float.parseFloat(PedidoHelper.getWebPedido().getValor_desconto());
                                                } catch (NullPointerException | NumberFormatException e) {
                                                    e.printStackTrace();
                                                    valor_desconto = 0.1f;
                                                } catch (Exception e) {
                                                    valor_desconto = 0.1f;
                                                    e.printStackTrace();
                                                }

                                                if (valor_desconto > 0.00) {
                                                    showMsgSimNao(0.0f, 0.0f, "Desconto não autorizado ou percentual de desconto não informado !\nDeseja reprocessar os dados?\n Atenção!! Todos os descontos aplicados serão removidos!");
                                                }
                                            }
                                        } else {
                                            Toast.makeText(getActivity(), "A condição de pagamento informada permite desconto(s)! Nenhum produto cadastrado!", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                                return;
                            }
                            if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("4")) {
                                if (!PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("S")) {
                                    if (PedidoHelper.verificaDesconto()) {
                                        if (Pedido1.calcularDesconto() > 0.00) {
                                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                            alert.setTitle("Atenção");
                                            alert.setMessage("A condição de pagamento/categoria informada não permite desconto(s), e foi detectado desconto(s) no(s) produto(s). Deseja limpar todo(s) o(s) desconto(s)?\n" +
                                                    "(O total da venda será de " + MascaraUtil.mascaraReal(PedidoHelper.calculaValorSemDesconto()) + ")");
                                            alert.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    webPedidoDAO.updateDescontoIndevido(PedidoHelper.getWebPedido(), "S");
                                                }
                                            });
                                            alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Pedido1.zerarDesconto();
                                                }
                                            });
                                            alert.show();
                                        } else {
                                            Toast.makeText(getActivity(), "A condição de pagamento/categoria informada não permite desconto(s)! Nenhum produto/desconto cadastrado/informado!", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "A condição de pagamento/categoria informada não permite desconto(s)! Nenhum produto/desconto cadastrado/informado!", Toast.LENGTH_LONG).show();
                                    }
                                }
                                return;
                            }
                            if (ClienteHelper.getCliente().getTipo_tabela_preco().equalsIgnoreCase("2")) {
                                return;
                            }
                        } else {
                            if (!PedidoHelper.getCondicoesPagamento().getAceita_desconto().equals("S")) {
                                if (PedidoHelper.verificaDesconto()) {
                                    if (Pedido1.calcularDesconto() > 0.00) {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Atenção");
                                        alert.setMessage("A condição de pagamento/categoria informada não permite desconto(s), e foi detectado desconto(s) no(s) produto(s). Deseja limpar todo(s) o(s) desconto(s)?\n" +
                                                "(O total da venda será de " + MascaraUtil.mascaraReal(PedidoHelper.calculaValorSemDesconto()) + ")");
                                        alert.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                webPedidoDAO.updateDescontoIndevido(PedidoHelper.getWebPedido(), "S");
                                            }
                                        });
                                        alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Pedido1.zerarDesconto();
                                            }
                                        });
                                        alert.show();
                                    } else {
                                        Toast.makeText(getActivity(), "A condição de pagamento/categoria informada não permite desconto(s)! Nenhum produto/desconto cadastrado/informado!", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "A condição de pagamento/categoria informada não permite desconto(s)! Nenhum produto/desconto cadastrado/informado!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                if (PedidoHelper.getIdPedido() > 0) {
                                    if (Pedido1.totalProdutos() > 0) {
                                        Float desconto_perc = 0.0f;
                                        try {
                                            String desconto = adapterPagamento.getItem(position).getDesconto_perc();
                                            desconto_perc = Float.parseFloat(adapterPagamento.getItem(position).getDesconto_perc());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (desconto_perc > 0.00) {
                                            Float valorDesconto = PedidoHelper.calculaValorComDescontoPrazo(desconto_perc);
                                            Float valorTotal = 0.0f;
                                            try {
                                                valorTotal = Float.parseFloat(PedidoHelper.getWebPedido().getValor_total());
                                            } catch (NullPointerException e) {

                                            } catch (Exception e) {

                                            }
                                            DecimalFormat decimal = new DecimalFormat("0.00");
                                            //Double vl_conv = Double.parseDouble(resultado);
                                            String total = (decimal.format(valorTotal));
                                            total = total.replaceAll(",", ".");
                                            final Float desconto_prazo = desconto_perc;
                                            String total_com_desconto = (decimal.format(PedidoHelper.calculaValorComDescontoPrazo(desconto_perc)));
                                            total_com_desconto = total_com_desconto.replaceAll(",", ".");
                                            if (Float.parseFloat(total) != Float.parseFloat(total_com_desconto)) {
                                                showMsgSimNao(desconto_prazo, desconto_prazo, "A condição de pagamento informada permite desconto(s) de " + desconto_perc + "%\n" +
                                                        "(O total da venda com desconto será de " + MascaraUtil.mascaraReal(PedidoHelper.calculaValorComDescontoPrazo(desconto_perc)) + ")\n Deseja reprocessar os dados com o desconto de " + desconto_perc + "% para o(s) produto(s) permitido(s)?");

                                            } else {
                                                Pedido1.verificaDescontoPrazo(desconto_perc);
                                            }
                                        } else {
                                            float valor_desconto = 0.0f;
                                            try {
                                                valor_desconto = Float.parseFloat(PedidoHelper.getWebPedido().getValor_desconto());
                                            } catch (NullPointerException | NumberFormatException e) {
                                                e.printStackTrace();
                                                valor_desconto = 0.1f;
                                            } catch (Exception e) {
                                                valor_desconto = 0.1f;
                                                e.printStackTrace();
                                            }
                                            if (valor_desconto > 0.00) {
                                                showMsgSimNao(0.0f, 0.0f, "Desconto não autorizado ou percentual de desconto não informado !\nDeseja reprocessar os dados?\n Atenção!! Todos os descontos aplicados serão removidos!");
                                            }
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "A condição de pagamento informada permite desconto(s)! Nenhum produto cadastrado!", Toast.LENGTH_LONG).show();
                                    }
                                }
                                //webPedidoDAO.updateDescontoIndevido(PedidoHelper.getWebPedido(), "N");
                            }
                        }


                    } catch ( NullPointerException e) {
                    } catch ( Exception e) {
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /*
        edtValorRecibo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (!edtValorRecibo.getText().toString().trim().isEmpty()) {
                        webPedido.setValor_recibo(edtValorRecibo.getText().toString().toString().replaceAll("\\s+", "").replace(",", "."));
                    } else {
                        webPedido.setValor_recibo("0.00");
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        */
        System.gc();
        mLocale = new Locale("pt", "BR");
        edtValorRecibo.addTextChangedListener(new MoneyTextWatcher(edtValorRecibo, mLocale));
        return (view);
    }

    public void mostraDatePickerDialog(Context context, final EditText campoTexto) {
        final Calendar calendar;
        //Prepara data anterior caso ja tenha sido selecionada
        calendar = campoTexto.getTag() != null ? ((Calendar) campoTexto.getTag()) : Calendar.getInstance();
        //if (campoTexto.getTag() != null)
            //calendar = ((Calendar) campoTexto.getTag());
        //else
           // calendar = Calendar.getInstance();

        new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                campoTexto.setText(new SimpleDateFormat("dd/MM/yyyy").format(newDate.getTime()));
                campoTexto.setTag(newDate);
                PedidoHelper.editTextDataEntrega().setBackgroundResource(R.drawable.borda_edittext);
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void pegaCliente(Cliente cliente) {
        objetoCliente = cliente;

    }

    public WebPedido salvaPedido() {
        webPedido.setId_condicao_pagamento(adapterPagamento.getItem(spPagamento.getSelectedItemPosition()).getId_condicao());
        webPedido.setCadastro(objetoCliente);
        webPedido.setObservacoes(edtObservacao.getText().toString());
        try {
            if (configuracao.getLayout_venda_erp().equalsIgnoreCase("25"))
                webPedido.setData_prev_entrega(db.pegaDataAtual());
            else
                webPedido.setData_prev_entrega(edtDataEntrega.getText().toString().trim());
        } catch ( NullPointerException e) {
            webPedido.setData_prev_entrega(db.pegaDataAtual());
        }
        webPedido.setPedido_enviado("N");
        webPedido.setId_operacao(adapterOperacao.getItem(spOperacao.getSelectedItemPosition()).getId_operacao());
        try {
            webPedido.setId_moeda_padrao(adapterMoeda.getItem(spMoeda.getSelectedItemPosition()).getId_moeda());
        } catch ( NullPointerException e) {
        } catch ( Exception e) {
        }
        try {
            webPedido.setValor_recibo( ( (edtValorRecibo.getText().toString().substring(2)
                    .replaceAll("\\s+", "")).replace(".","") ).replace(",", ".") );
        } catch ( NullPointerException|NumberFormatException|IndexOutOfBoundsException e) {
            webPedido.setValor_recibo("0.00");
        } catch ( Exception e) {
            webPedido.setValor_recibo("0.00");
        }
        try {
            if ( !edtValorRecibo.getText().toString().isEmpty()) {
                webPedido.setValor_recibo( ( (edtValorRecibo.getText().toString().substring(2)
                        .replaceAll("\\s+", "")).replace(".","") ).replace(",", ".") );
            }
        } catch ( NullPointerException|NumberFormatException|IndexOutOfBoundsException e) {
            webPedido.setValor_recibo("0.00");
        } catch ( Exception e) {
            webPedido.setValor_recibo("0.00");
            e.printStackTrace();
        }

        return webPedido;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        CondicoesPagamentoDAO condicoesPagamentoCabDAO = new CondicoesPagamentoDAO(db);
        List<CondicoesPagamento> listaCondicoesPagamentos = new ArrayList<>();
        String sql;
        try {

            try {
                if ( PedidoHelper.getWebPedido().getCadastro() != null) {
                    sql = "SELECT CP.* FROM TBL_CONDICOES_PAG_CAB AS CP INNER JOIN TBL_CADASTRO_CONDICOES_PAG AS CCP ON ( CCP.ID_CONDICAO = CP.ID_CONDICAO AND CCP.ID_CADASTRO = ";
                    sql += PedidoHelper.getWebPedido().getCadastro().getId_cadastro_servidor() + ") WHERE CP.ATIVO = 'S' AND  ( CP.NOME_CONDICAO != '' AND CP.NOME_CONDICAO IS NOT NULL) ORDER BY CP.ID_CONDICAO";
                    if ( configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                        float limiteCredito = Float.parseFloat(ClienteHelper.getCliente().getLimite_credito());
                        float limitUtilizado = Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencido()) + Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencendo());
                        if ( (limiteCredito - limitUtilizado) < 0.00f) {
                            sql = "SELECT CP.* FROM TBL_CONDICOES_PAG_CAB AS CP INNER JOIN TBL_CADASTRO_CONDICOES_PAG AS CCP ON ( CCP.ID_CONDICAO = CP.ID_CONDICAO AND CCP.ID_CADASTRO = ";
                            sql += PedidoHelper.getWebPedido().getCadastro().getId_cadastro_servidor() + ") WHERE CP.ATIVO = 'S' AND  ( CP.NOME_CONDICAO != '' AND CP.NOME_CONDICAO IS NOT NULL) AND CP.TIPO_CONDICAO = '1' ORDER BY CP.ID_CONDICAO";
                        }
                    }
                    listaCondicoesPagamentos = condicoesPagamentoCabDAO.getLista(sql);
                }
            } catch ( NullPointerException e) {
                e.printStackTrace();
            }
            try {
                if (listaCondicoesPagamentos.size() <= 0) {
                    sql = "SELECT * FROM TBL_CONDICOES_PAG_CAB  WHERE ATIVO = 'S' AND  (NOME_CONDICAO != '' AND NOME_CONDICAO IS NOT NULL) ORDER BY ID_CONDICAO;";
                    if (configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                        float limiteCredito = Float.parseFloat(ClienteHelper.getCliente().getLimite_credito());
                        float limitUtilizado = Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencido()) + Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencendo());
                        if ((limiteCredito - limitUtilizado) < 0.00f) {
                            sql = "SELECT * FROM TBL_CONDICOES_PAG_CAB  WHERE ATIVO = 'S' AND  (NOME_CONDICAO != '' AND NOME_CONDICAO IS NOT NULL) AND TIPO_CONDICAO = '1' ORDER BY ID_CONDICAO;";
                        }
                    }
                    listaCondicoesPagamentos = condicoesPagamentoCabDAO.getLista(sql);
                }
                adapterPagamento = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, listaCondicoesPagamentos);
                spPagamento.setAdapter(adapterPagamento);
                if (!configuracao.getId_con_pgto_default_app().equalsIgnoreCase("0")) {
                    for (int i = 0; i < listaCondicoesPagamentos.size(); i++)
                        if (listaCondicoesPagamentos.get(i).getId_condicao().equalsIgnoreCase(configuracao.getId_con_pgto_default_app())) {
                            spPagamento.setSelection(i);
                            webPedido.setId_condicao_pagamento( listaCondicoesPagamentos.get(i).getId_condicao());
                        }
                } else {
                    spPagamento.setSelection(0);
                }
            } catch ( NullPointerException e) {
                e.printStackTrace();
            }
        } catch (CursorIndexOutOfBoundsException e) {
            sql =  "SELECT * FROM TBL_CONDICOES_PAG_CAB  WHERE ATIVO = 'S' AND  (NOME_CONDICAO != '' AND NOME_CONDICAO IS NOT NULL)  ORDER BY ID_CONDICAO;";
            if ( configuracao.getTrabalha_com_limite_cred().equalsIgnoreCase("S")) {
                float limiteCredito = Float.parseFloat(ClienteHelper.getCliente().getLimite_credito());
                float limitUtilizado = Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencido()) + Float.parseFloat(ClienteHelper.getCliente().getFinanceiro_vencendo());
                if ( (limiteCredito - limitUtilizado) < 0.00f) {
                    sql =  "SELECT * FROM TBL_CONDICOES_PAG_CAB  WHERE ATIVO = 'S' AND  (NOME_CONDICAO != '' AND NOME_CONDICAO IS NOT NULL) AND TIPO_CONDICAO = '1' ORDER BY ID_CONDICAO;";
                }
            }
            listaCondicoesPagamentos = condicoesPagamentoCabDAO.getLista(sql);
            adapterPagamento = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, listaCondicoesPagamentos);
            spPagamento.setAdapter(adapterPagamento);
            if (!configuracao.getId_con_pgto_default_app().equalsIgnoreCase("0")) {
                for (int i = 0; i < listaCondicoesPagamentos.size(); i++)
                    if (listaCondicoesPagamentos.get(i).getId_condicao().equalsIgnoreCase(configuracao.getId_con_pgto_default_app())) {
                        spPagamento.setSelection(i);
                        webPedido.setId_condicao_pagamento( listaCondicoesPagamentos.get(i).getId_condicao());
                    }
            } else {
                spPagamento.setSelection(0);
            }
        }
        if (PedidoHelper.getIdPedido() > 0) {
            webPedido = webPedidoDAO.getLista("SELECT * FROM TBL_WEB_PEDIDO WHERE ID_WEB_PEDIDO = " + PedidoHelper.getIdPedido()).get(0);
            objetoCliente = webPedido.getCadastro();
            //Seleciona Condição de pagamento correta dentro do Spinner spPagamento
            try {
                int i = -1;
                do {
                    i++;
                }
                while (!webPedido.getId_condicao_pagamento().equals(adapterPagamento.getItem(i).getId_condicao()));
                spPagamento.setSelection(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            edtValorRecibo.setText( MascaraUtil.duasCasaDecimal(webPedido.getValor_recibo()));
        } catch ( NullPointerException|NumberFormatException e) {
            edtValorRecibo.setText(MascaraUtil.duasCasaDecimal("0.00"));
        } catch (Exception e) {
            edtValorRecibo.setText(MascaraUtil.duasCasaDecimal("0.00"));
        }
        try {
            edtSaldoDevedor.setText( MascaraUtil.mascaraReal(Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencido())
                    + Float.parseFloat(webPedido.getCadastro().getFinanceiro_vencendo()) ));
        } catch ( NullPointerException|NumberFormatException e) {
            edtSaldoDevedor.setText(MascaraUtil.mascaraReal("0.00"));
        } catch ( Exception e) {
            edtSaldoDevedor.setText(MascaraUtil.mascaraReal("0.00"));
        }
        super.onResume();
    }


    @Override
    public void onDestroy() {
        spPagamento.setSelection(spPagamento.getSelectedItemPosition());
        objetoCliente = null;
        System.gc();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        int x = spPagamento.getSelectedItemPosition();
        spPagamento.setSelection(spPagamento.getSelectedItemPosition());
        super.onPause();
    }
    @Override
    public void onStop() {
        int x = spPagamento.getSelectedItemPosition();
        spPagamento.setSelection(spPagamento.getSelectedItemPosition());
        super.onStop();
    }


    public void showMsgSucesso(String mensagem) {
        ViewGroup viewGroup = PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from( PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_sucesso_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(PedidoHelper.getActivityPedidoMain());
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button button = (Button) dialogView.findViewById(R.id.btnMensagemOK);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PedidoHelper.getActivityPedidoMain().finish();
            }
        });
        if ( alertDialog != null) {
            alertDialog.show();
        }
    }

    public void showMsgSimNao( float verificaDescontoPrazo , float colocaDescontoPrazo, String mensagem) {
        ViewGroup viewGroup = PedidoHelper.getActivityPedidoMain().findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(PedidoHelper.getActivityPedidoMain()).inflate(R.layout.dialog_sim_nao_mensagem, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(PedidoHelper.getActivityPedidoMain());
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView textView = (TextView) dialogView.findViewById(R.id.txvMensagemOk);
        textView.setText(mensagem);
        Button btnNao = (Button) dialogView.findViewById(R.id.btnMensagemNao);
        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pedido1.verificaDescontoPrazo(verificaDescontoPrazo);
                alertDialog.dismiss();

            }
        });
        Button btnSim = (Button) dialogView.findViewById(R.id.btnMensagemSim);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pedido1.zerarDesconto();
                Pedido1.colocarDescontoPrazo(colocaDescontoPrazo, db);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void hideEditDataEntrega() {
        ViewGroup.LayoutParams params = edtDataEntrega.getLayoutParams();
        params.height = 0;
        edtDataEntrega.setLayoutParams(params);
        edtDataEmissao.setLayoutParams(params);
    }
    private void showEditDataEntrega() {
        ViewGroup.LayoutParams params = edtDataEntrega.getLayoutParams();
        params.height =  ViewGroup.LayoutParams.WRAP_CONTENT;
        edtDataEntrega.setLayoutParams(params);
        edtDataEmissao.setLayoutParams(params);
    }

    private void hideEditValorRecibo() {
        ViewGroup.LayoutParams params = edtValorRecibo.getLayoutParams();
        params.height = 0;
        edtValorRecibo.setLayoutParams(params);
        edtSaldoDevedor.setLayoutParams(params);
    }
    private void showEditValorRecibo() {
        ViewGroup.LayoutParams params = edtValorRecibo.getLayoutParams();
        params.height =  ViewGroup.LayoutParams.WRAP_CONTENT;
        edtValorRecibo.setLayoutParams(params);
        edtSaldoDevedor.setLayoutParams(params);

        //edtDataEmissao
        //edtSaldoDevedor
    }
    public class MoneyTextWatcher implements TextWatcher {
        private final WeakReference<EditText> editTextWeakReference;
        private final Locale locale;

        public MoneyTextWatcher(EditText editText, Locale locale) {
            this.editTextWeakReference = new WeakReference<EditText>(editText);
            this.locale = locale != null ? locale : Locale.getDefault();
        }

        public MoneyTextWatcher(EditText editText) {
            this.editTextWeakReference = new WeakReference<EditText>(editText);
            this.locale = Locale.getDefault();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void afterTextChanged(Editable editable) {
            EditText editText = editTextWeakReference.get();
            if (editText == null) return;
            editText.removeTextChangedListener(this);

            BigDecimal parsed = parseToBigDecimal(editable.toString(), locale);
            String formatted = NumberFormat.getCurrencyInstance(locale).format(parsed);
            // NumberFormat.getNumberInstance(locale).format(parsed); // sem o simbolo de moeda

            editText.setText(formatted);
            editText.setSelection(formatted.length());
            editText.addTextChangedListener(this);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        private BigDecimal parseToBigDecimal(String value, Locale locale) {
            String replaceable = String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance(locale).getCurrency().getSymbol());

            String cleanString = value.replaceAll(replaceable, "");
            try {
                webPedido.setValor_recibo(String.valueOf(new BigDecimal(cleanString).setScale(
                        2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR
                )));
                return new BigDecimal(cleanString).setScale(
                        2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR
                );
            } catch ( NullPointerException|NumberFormatException e) {
                webPedido.setValor_recibo("0.00");
                return  new BigDecimal(0.00);
            }
        }
    }

}


/*
 try {
                    if (!edtValorRecibo.getText().toString().trim().isEmpty()) {
                        webPedido.setValor_recibo(edtValorRecibo.getText().toString().toString().replaceAll("\\s+", "").replace(",", "."));
                    } else {
                        webPedido.setValor_recibo("0.00");
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
 */