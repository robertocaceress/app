package com.example.rcksuporte05.rcksistemas.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.util.Log;

import com.example.rcksuporte05.rcksistemas.Helper.UsuarioHelper;
import com.example.rcksuporte05.rcksistemas.model.Deposito;
import com.example.rcksuporte05.rcksistemas.model.Produto;
import com.example.rcksuporte05.rcksistemas.model.ProdutoFoto;
import com.example.rcksuporte05.rcksistemas.model.ProdutoGrupo;
import com.example.rcksuporte05.rcksistemas.model.ProdutoLinhaColecao;
import com.example.rcksuporte05.rcksistemas.model.ProdutoSubGrupo;

import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO {
    private DBHelper db;

    public ProdutoDAO(DBHelper db) {
        this.db = db;
    }

    public long add(Produto produto) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ATIVO", produto.getAtivo());
            content.put("ID_PRODUTO", produto.getId_produto());
            content.put("ID_LINHA_COLECAO", produto.getIdLinhaColecao());
            content.put("NOME_PRODUTO", produto.getNome_produto());
            content.put("UNIDADE", produto.getUnidade());
            content.put("TIPO_CADASTRO", produto.getTipo_cadastro());
            content.put("ID_ENTIDADE", produto.getId_entidade());
            content.put("NCM", produto.getNcm());
            content.put("ID_GRUPO", produto.getId_grupo());
            content.put("ID_SUB_GRUPO", produto.getId_sub_grupo());
            content.put("PESO_BRUTO", produto.getPeso_bruto());
            content.put("PESO_LIQUIDO", produto.getPeso_liquido());
            content.put("CODIGO_EM_BARRAS", produto.getCodigo_em_barras());
            content.put("MOVIMENTA_ESTOQUE", produto.getMovimenta_estoque());
            content.put("NOME_DA_MARCA", produto.getNome_da_marca());
            content.put("ID_EMPRESA", produto.getId_empresa());
            content.put("ID_ORIGEM", produto.getId_origem());
            content.put("CUSTO_PRODUTO", produto.getCusto_produto());
            content.put("CUSTO_PER_IPI", produto.getCusto_per_ipi());
            content.put("CUSTO_IPI", produto.getCusto_ipi());
            content.put("CUSTO_PER_FRETE", produto.getCusto_per_frete());
            content.put("CUSTO_FRETE", produto.getCusto_frete());
            content.put("CUSTO_PER_ICMS", produto.getCusto_per_icms());
            content.put("CUSTO_ICMS", produto.getCusto_icms());
            content.put("CUSTO_PER_FIN", produto.getCusto_per_fin());
            content.put("CUSTO_FIN", produto.getCusto_fin());
            content.put("CUSTO_PER_SUBST", produto.getCusto_per_subst());
            content.put("CUSTO_SUBT", produto.getCusto_subt());
            content.put("CUSTO_PER_OUTROS", produto.getCusto_per_outros());
            content.put("CUSTO_OUTROS", produto.getCusto_outros());
            content.put("VALOR_CUSTO", produto.getValor_custo());
            content.put("EXCLUIDO", produto.getExcluido());
            content.put("EXCLUIDO_POR", produto.getExcluido_por());
            content.put("EXCLUIDO_POR_DATA", produto.getExcluido_por_data());
            content.put("EXCLUIDO_CODIGO_NOVO", produto.getExcluido_codigo_novo());
            content.put("AJUSTE_PRECO_DATA", produto.getAjuste_preco_data());
            content.put("AJUSTE_PRECO_NFE", produto.getAjuste_preco_nfe());
            content.put("AJUSTE_PRECO_USUARIO", produto.getAjuste_preco_usuario());
            content.put("TOTAL_CUSTO", produto.getTotal_custo());
            content.put("TOTAL_CREDITO", produto.getTotal_credito());
            content.put("VALOR_CUSTO_ESTOQUE", produto.getValor_custo_estoque());
            content.put("CUSTO_DATA_INICIAL", produto.getCusto_data_inicial());
            content.put("CUSTO_VALOR_INICIAL", produto.getCusto_valor_inicial());
            content.put("PRODUTO_VENDA", produto.getProduto_venda());
            content.put("PRODUTO_INSUMO", produto.getProduto_insumo());
            content.put("PRODUTO_CONSUMO", produto.getProduto_consumo());
            content.put("PRODUTO_PRODUCAO", produto.getProduto_producao());
            content.put("VENDA_PERC_COMISSAO", produto.getVenda_perc_comissao());
            content.put("VENDA_PRECO", produto.getVenda_preco());
            content.put("VENDA_PERC_COMISSAO_DOIS", produto.getVenda_perc_comissao_dois());
            content.put("DESCRICAO", produto.getDescricao());
            content.put("NOME_SUB_GRUPO", produto.getNome_sub_grupo());
            content.put("NOME_GRUPO", produto.getNome_grupo());
            content.put("PRODUTO_MATERIA_PRIMA", produto.getProduto_materia_prima());
            content.put("PRODUTO_TERCERIZACAO", produto.getProduto_tercerizacao());
            content.put("SALDO_ESTOQUE", produto.getSaldo_estoque());
            content.put("TRAVAR_FAT_SEM_ESTOQUE", produto.getTravar_fat_sem_estoque());
            content.put("UTILIZA_TABELA_PRECO", produto.getUtiliza_tabela_preco());
            content.put("PRODUTO_EDITA_PRECO_APP", produto.getProduto_edita_valor());
            content.put("PRODUTO_ST_ALIQUOTA", produto.getProduto_st_aliquota());
            content.put("PRODUTO_SUJ_ICMS_ST", produto.getProduto_suj_icms_st());
            System.gc();
            return db.addDados("TBL_PRODUTO", content);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long updateSaldoEstoque(Produto produto) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("SALDO_ESTOQUE", produto.getSaldo_estoque());
            System.gc();
            return db.updateDados("TBL_PRODUTO", content, "ID_PRODUTO = '" + produto.getId_produto().trim() + "'" );
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public List<Produto> getLista(String SQL, String idCliente) {
        List<Produto> lista = new ArrayList<>();
        Cursor cursorAnx = null;
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        Produto produto;
        do {
            produto = new Produto();
            try {
                produto.setId_produto(cursor.getString(cursor.getColumnIndex("ID_PRODUTO")));
                produto.setIdLinhaColecao(cursor.getInt(cursor.getColumnIndex("ID_LINHA_COLECAO")));
                produto.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")));
                produto.setDescricao(cursor.getString(cursor.getColumnIndex("DESCRICAO")));
                produto.setUnidade(cursor.getString(cursor.getColumnIndex("UNIDADE")));
                produto.setVenda_preco(cursor.getString(cursor.getColumnIndex("VENDA_PRECO")));
                produto.setAtivo(cursor.getString(cursor.getColumnIndex("ATIVO")));
                produto.setCodigo_em_barras(cursor.getString(cursor.getColumnIndex("CODIGO_EM_BARRAS")));
                produto.setNome_sub_grupo(cursor.getString(cursor.getColumnIndex("NOME_SUB_GRUPO")));
                produto.setNome_grupo(cursor.getString(cursor.getColumnIndex("NOME_GRUPO")));
                produto.setProduto_tercerizacao(cursor.getString(cursor.getColumnIndex("PRODUTO_TERCERIZACAO")));
                produto.setProduto_materia_prima(cursor.getString(cursor.getColumnIndex("PRODUTO_MATERIA_PRIMA")));
                produto.setSaldo_estoque(cursor.getFloat(cursor.getColumnIndex("SALDO_ESTOQUE")));
                produto.setTravar_fat_sem_estoque(cursor.getString(cursor.getColumnIndex("TRAVAR_FAT_SEM_ESTOQUE")));
                produto.setUtiliza_tabela_preco(cursor.getString(cursor.getColumnIndex("UTILIZA_TABELA_PRECO")));
                produto.setProduto_edita_valor(cursor.getString(cursor.getColumnIndex("PRODUTO_EDITA_PRECO_APP")));
                produto.setProduto_st_aliquota(cursor.getFloat(cursor.getColumnIndex("PRODUTO_ST_ALIQUOTA")));
                produto.setProduto_suj_icms_st(cursor.getString(cursor.getColumnIndex("PRODUTO_SUJ_ICMS_ST")));
                try {
                    produto.setW_id_produto( cursor.getString(cursor.getColumnIndex("W_ID_PRODUTO")) );
                    produto.setId_campanha_produto(cursor.getString(cursor.getColumnIndex("ID_CAMPANHA")));
                    produto.setId_campanha_cliente(cursor.getString(cursor.getColumnIndex("ID_CAMPANHA_C")));
                } catch ( NullPointerException e) {
                    produto.setW_id_produto("-");
                } catch ( Exception e) {
                    produto.setW_id_produto("-");
                }
                if (produto.getUtiliza_tabela_preco().equalsIgnoreCase("S")) {
                    try {
                        if (Float.parseFloat(cursor.getString(cursor.getColumnIndex("PRECO_TABELA_CLIENTE"))) > 0.0f) {
                            produto.setVenda_preco(cursor.getString(cursor.getColumnIndex("PRECO_TABELA_CLIENTE")));
                            produto.setNome_tabela_preco(cursor.getString(cursor.getColumnIndex("NOME_TABELA")));
                        }
                    } catch (NullPointerException e) {
                    } catch (Exception e) {
                    }
                }
                try {
                    SQL = "SELECT FOTO_MINIATURA, FOTO_ARQUIVO FROM TBL_PRODUTO_FOTO WHERE ID_PRODUTO = '" +
                            cursor.getString(cursor.getColumnIndex("ID_PRODUTO")).trim()
                            + "' AND ATIVO = 'S' AND FOTO_PRINCIPAL = 'S'";
                    cursorAnx = db.listaDados(SQL);
                    if (cursor.getCount() > 0) {
                        if (cursorAnx.moveToNext()) {
                            produto.setAnexo_1(cursorAnx.getString(cursorAnx.getColumnIndex("FOTO_ARQUIVO")));

                            //if (cursorAnx.getString(cursorAnx.getColumnIndex("FOTO_ARQUIVO")).length() >= 1000000) {
                                //produto.setAnexo_1(cursorAnx.getString(cursorAnx.getColumnIndex("FOTO_MINIATURA")));
                            //} else {
                            //    produto.setAnexo_1(cursorAnx.getString(cursorAnx.getColumnIndex("FOTO_MINIATURA")));
                            //}
                        }else
                            produto.setAnexo_1("");
                    } else
                        produto.setAnexo_1("");
                } catch (Exception e) {
                    produto.setAnexo_1("");
                    //e.printStackTrace();
                }

                if (cursorAnx != null)
                    cursorAnx.close();
                try{
                    boolean produtoPromocao = false;
                    produto.setId_promocao("0");
                    int id_promocao = 0;
                    while ( produtoPromocao == false ) {
                        id_promocao = getPromocao1(produto, idCliente);
                        if (id_promocao > 0) {
                            produtoPromocao = true;
                            produto.setId_promocao(String.valueOf(id_promocao));
                            break;
                        }
                        id_promocao = getPromocao2(produto);
                        if (id_promocao > 0) {
                            produtoPromocao = true;
                            produto.setId_promocao(String.valueOf(id_promocao));
                            break;
                        }
                        id_promocao = getPromocao3(produto);
                        if (id_promocao > 0) {
                            produtoPromocao = true;
                            produto.setId_promocao(String.valueOf(id_promocao));
                            break;
                        }
                        id_promocao = getPromocao4(produto);
                        if (id_promocao > 0) {
                            produtoPromocao = true;
                            produto.setId_promocao(String.valueOf(id_promocao));
                            break;
                        }
                        produtoPromocao = true;
                    }
                } catch ( Exception e) {
                    e.printStackTrace();
                }
                lista.add(produto);
            } catch (CursorIndexOutOfBoundsException e) {
                //e.printStackTrace();
            }
            //System.gc();
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    private int getPromocao1(Produto produto, String idCliente) {
        String SQL = "SELECT PC.*, PP.* FROM TBL_PROMOCAO_CAB AS PC " +
                "INNER JOIN TBL_PROMOCAO_PRODUTO AS PP  ON ( PP.ID_PROMOCAO = PC.ID_PROMOCAO AND PP.ID_PRODUTO = '" + produto.getId_produto() + "') " +
                "INNER JOIN TBL_PROMOCAO_CLIENTE AS PCL ON ( PCL.ID_PROMOCAO = PC.ID_PROMOCAO AND PCL.ID_CADASTRO = '" + idCliente + "')" +
                "WHERE PC.APLICACAO_CLIENTE = 1 AND PC.APLICACAO_PRODUTO = 1 AND PC.ATIVO = 'S' AND PC.ID_EMPRESA = " + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        try {
            cursor.moveToFirst();
            return  cursor.getInt(cursor.getColumnIndex("ID_PROMOCAO"));
        } catch (IndexOutOfBoundsException | NullPointerException e) {
            e.printStackTrace();
        }
        return 0;
    }
    private int getPromocao2(Produto produto){
        String SQL = "SELECT PC.*, PP.* FROM TBL_PROMOCAO_CAB AS PC INNER JOIN TBL_PROMOCAO_PRODUTO AS PP " +
                "ON ( PP.ID_PROMOCAO = PC.ID_PROMOCAO AND PP.ID_PRODUTO = '" + produto.getId_produto() + "') " +
                "WHERE PC.APLICACAO_CLIENTE = 0 AND PC.APLICACAO_PRODUTO = 1 AND PC.ATIVO = 'S' AND PC.ID_EMPRESA = " + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        try {
            cursor.moveToFirst();
            return  cursor.getInt(cursor.getColumnIndex("ID_PROMOCAO"));
        } catch (IndexOutOfBoundsException | NullPointerException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int getPromocao3(Produto produto){
        String SQL = "SELECT PC.*, PP.* FROM TBL_PROMOCAO_CAB AS PC INNER JOIN TBL_PROMOCAO_PRODUTO AS PP " +
                "ON ( PP.ID_PROMOCAO = PC.ID_PROMOCAO AND PP.ID_PRODUTO = '" + produto.getId_produto() + "') " +
                "WHERE PC.APLICACAO_CLIENTE = 1 AND PC.APLICACAO_PRODUTO = 0 AND PC.ATIVO = 'S' AND PC.ID_EMPRESA = " + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        try {
            cursor.moveToFirst();
            return  cursor.getInt(cursor.getColumnIndex("ID_PROMOCAO"));
        } catch (IndexOutOfBoundsException | NullPointerException e) {
            e.printStackTrace();
        }
        return  0;
    }

    private int getPromocao4(Produto produto){
        String SQL = "SELECT PC.*, PP.* FROM TBL_PROMOCAO_CAB AS PC INNER JOIN TBL_PROMOCAO_PRODUTO AS PP " +
                "ON ( PP.ID_PROMOCAO = PC.ID_PROMOCAO AND PP.ID_PRODUTO = '" + produto.getId_produto() + "') " +
                "WHERE PC.APLICACAO_CLIENTE = 0 AND PC.APLICACAO_PRODUTO = 0 AND PC.ATIVO = 'S' AND PC.ID_EMPRESA = " + UsuarioHelper.getUsuario().getIdEmpresaMultiDevice();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        try {
            cursor.moveToFirst();
            return cursor.getInt(cursor.getColumnIndex("ID_PROMOCAO"));
        } catch (IndexOutOfBoundsException | NullPointerException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalReg(String SQL) {
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return 0;
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("TOTALREG"));
    }

    public long add(ProdutoFoto produtoFoto) {
        //Alteração no codigo tratando excessao e retorno
        //Roberto caceres
        ContentValues content = new ContentValues();
        try {
            content.put("ATIVO", produtoFoto.getAtivo());
            content.put("ID_PRODUTO", produtoFoto.getId_produto());
            content.put("ID_FOTO", produtoFoto.getId_foto());
            content.put("USUARIO_DATA", produtoFoto.getUsuario_data());
            content.put("FOTO_ARQUIVO", produtoFoto.getFoto_arquivo());
            content.put("FOTO_MINIATURA", produtoFoto.getFoto_miniatura());
            content.put("FOTO_PRINCIPAL", produtoFoto.getFoto_principal());
            System.gc();
            return db.addDados("TBL_PRODUTO_FOTO", content);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<ProdutoFoto> getListaFotos(String SQL) {
        List<ProdutoFoto> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        do {
            ProdutoFoto produtoFoto = new ProdutoFoto();
            try {
                produtoFoto.setImages(cursor.getString(cursor.getColumnIndex("FOTO_ARQUIVO")));
                produtoFoto.setId_foto(cursor.getString(cursor.getColumnIndex("ID_FOTO")));
                lista.add(produtoFoto);
            } catch (CursorIndexOutOfBoundsException|OutOfMemoryError e) {
                e.printStackTrace();
                produtoFoto.setImages("");
                produtoFoto.setId_foto("");
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public List<ProdutoFoto> getListaAnexos(String SQL) {
        List<ProdutoFoto> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if (cursor.getCount() <= 0)
            return lista;
        cursor.moveToFirst();
        do {
            ProdutoFoto produtoFoto = new ProdutoFoto();
            try {
                produtoFoto.setAtivo("S");
                produtoFoto.setId_produto( cursor.getString(cursor.getColumnIndex("ID_PRODUTO")));
                produtoFoto.setFoto_arquivo(cursor.getString(cursor.getColumnIndex("FOTO_ARQUIVO")));
                produtoFoto.setFoto_miniatura(cursor.getString(cursor.getColumnIndex("FOTO_MINIATURA")));
                produtoFoto.setFoto_principal(cursor.getString(cursor.getColumnIndex("FOTO_PRINCIPAL")));
                produtoFoto.setFbase64("");
                produtoFoto.setNome_produto(cursor.getString(cursor.getColumnIndex("NOME_PRODUTO")).replace("'\'"," "));
                lista.add(produtoFoto);
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public Cursor getListaFotoID(String SQL) {
        return db.listaDados(SQL);
    }

    public boolean add(ProdutoSubGrupo produtoSubGrupo) {
        ContentValues content = new ContentValues();
        content.put("ID", produtoSubGrupo.getId_grupo());
        content.put("NOME_SUB_GRUPO", produtoSubGrupo.getNome_sub_grupo());
        content.put("ATIVO", produtoSubGrupo.getAtivo());
        try {
            if ( db.addDados("TBL_PRODUTO_SUB_GRUPO",  content) > 0)
                return true;
            return  false;
        } catch (SQLException e) {
        }
        return false;
    }

    public boolean add(ProdutoGrupo produtoGrupo) {
        ContentValues content = new ContentValues();
        content.put("ID", produtoGrupo.getId_grupo());
        content.put("NOME_GRUPO", produtoGrupo.getNome_grupo());
        content.put("ATIVO", produtoGrupo.getAtivo());
        try {
            if ( db.addDados("TBL_PRODUTO_GRUPO",  content) > 0)
                return true;
            return  false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean add(Deposito deposito) {
        ContentValues content = new ContentValues();
        content.put("ID_DEPOSITO", deposito.getId_deposito());
        content.put("NOME_DEPOSITO", deposito.getNome_deposito());
        content.put("ID_EMPRESA", deposito.getId_empresa());
        try {
            if ( db.addDados("TBL_DEPOSITO",  content) > 0)
                return true;
            return  false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public List<ProdutoSubGrupo> getListaSubGrupo(String SQL) {
        List<ProdutoSubGrupo> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return  lista;
        cursor.moveToFirst();
        do {
            ProdutoSubGrupo produtoSubGrupo = new ProdutoSubGrupo();
            try {
                produtoSubGrupo.setId_grupo(cursor.getString(cursor.getColumnIndex("ID")));
                produtoSubGrupo.setNome_sub_grupo(cursor.getString(cursor.getColumnIndex("NOME_SUB_GRUPO")));
                produtoSubGrupo.setTotal_produto(cursor.getString(cursor.getColumnIndex("TOTAL_PRODUTO_SUB_GRUPO")));
                lista.add(produtoSubGrupo);
            } catch ( ArrayIndexOutOfBoundsException e) {
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public List<ProdutoSubGrupo> getListaGrupos(String SQL) {
        List<ProdutoSubGrupo> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return  lista;
        cursor.moveToFirst();
        do {
            ProdutoSubGrupo produtoGrupo = new ProdutoSubGrupo();
            try {
                produtoGrupo.setId_grupo(cursor.getString(cursor.getColumnIndex("ID")));
                produtoGrupo.setNome_sub_grupo(cursor.getString(cursor.getColumnIndex("NOME_GRUPO")));
                produtoGrupo.setAtivo(cursor.getString(cursor.getColumnIndex("ATIVO")));
                lista.add(produtoGrupo);
            } catch ( ArrayIndexOutOfBoundsException e) {
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public List<ProdutoGrupo> getListaGrupo(String SQL) {
        List<ProdutoGrupo> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return  lista;
        cursor.moveToFirst();
        do {
            ProdutoGrupo produtoGrupo = new ProdutoGrupo();
            try {
                produtoGrupo.setId_grupo(cursor.getString(cursor.getColumnIndex("ID")));
                produtoGrupo.setNome_grupo(cursor.getString(cursor.getColumnIndex("NOME_GRUPO")));
                produtoGrupo.setAtivo(cursor.getString(cursor.getColumnIndex("ATIVO")));
                lista.add(produtoGrupo);
            } catch ( ArrayIndexOutOfBoundsException e) {
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public List<ProdutoLinhaColecao> getListaLinha(String SQL) {
        List<ProdutoLinhaColecao> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return  lista;
        cursor.moveToFirst();
        do {
            ProdutoLinhaColecao produtoLinha = new ProdutoLinhaColecao();
            try {
                produtoLinha.setIdLinhaColecao(cursor.getInt(cursor.getColumnIndex("ID_LINHA_COLECAO")));
                produtoLinha.setNomeDescricaoLinha(cursor.getString(cursor.getColumnIndex("NOME_DESCRICAO_LINHA")));
                produtoLinha.setTotal_produto(cursor.getString(cursor.getColumnIndex("TOTAL_PRODUTO_LINHA")));
                lista.add(produtoLinha);
            } catch ( ArrayIndexOutOfBoundsException e) {
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }

    public List<Deposito> getListaDeposito(String SQL) {
        List<Deposito> lista = new ArrayList<>();
        Cursor cursor = db.listaDados(SQL);
        if ( cursor.getCount() <= 0)
            return  lista;
        cursor.moveToFirst();
        do {
            Deposito deposito = new Deposito();
            try {
                deposito.setId_deposito(cursor.getString(cursor.getColumnIndex("ID_DEPOSITO")));
                deposito.setNome_deposito(cursor.getString(cursor.getColumnIndex("NOME_DEPOSITO")));
                deposito.setId_empresa(cursor.getString(cursor.getColumnIndex("ID_EMPRESA")));
                lista.add(deposito);
            } catch ( ArrayIndexOutOfBoundsException e) {
            }
        } while (cursor.moveToNext());
        cursor.close();
        System.gc();
        return lista;
    }


    public Cursor getCursor(String SQL) {
        Cursor cursor = db.listaDados(SQL);
        return  cursor;
    }
}

