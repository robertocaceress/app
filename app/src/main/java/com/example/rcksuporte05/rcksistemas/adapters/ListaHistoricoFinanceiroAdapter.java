package com.example.rcksuporte05.rcksistemas.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rcksuporte05.rcksistemas.R;
import com.example.rcksuporte05.rcksistemas.adapters.viewHolder.FinanceiroViewHolder;
import com.example.rcksuporte05.rcksistemas.model.Financeiro;
import com.example.rcksuporte05.rcksistemas.util.MascaraUtil;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.zip.DataFormatException;

public class ListaHistoricoFinanceiroAdapter extends RecyclerView.Adapter<FinanceiroViewHolder> {

    private Activity context;
    private List<Financeiro> listaFinanceiros;
    //private FinanceiroAdapterListener listener;
    private FinanceiroViewHolder holder;
    private SparseBooleanArray selectedItems;

    public ListaHistoricoFinanceiroAdapter(List<Financeiro> listaFinanceiros){//, FinanceiroAdapterListener listener/*) {
        this.listaFinanceiros = listaFinanceiros;
        this.context = context;
        this.selectedItems = new SparseBooleanArray();
    }

    @NonNull
    @Override
    public FinanceiroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_financeiro, parent, false);
        return new FinanceiroViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FinanceiroViewHolder holder, int position) {
        holder.finDocumento.setText( listaFinanceiros.get(position).getDocumento() );
        holder.finParcela.setText( listaFinanceiros.get(position).getParcela() );
          try {
            holder.finEmissao.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(listaFinanceiros.get(position).getData_emissao())));
            holder.finVencimento.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(listaFinanceiros.get(position).getData_vencimento())));
            holder.finPagamento.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(listaFinanceiros.get(position).getData_pagamento())));
        } catch ( NullPointerException e) {
        } catch (Exception e) {
        }

        holder.finEspecie.setText( listaFinanceiros.get(position).getEspecie() );
        holder.finValorDoc.setText( MascaraUtil.duasCasaDecimal(listaFinanceiros.get(position).getValor_documento()) );
        if ( !listaFinanceiros.get(position).getValor_juros().isEmpty() )
            holder.finJuroDesconto.setText( MascaraUtil.duasCasaDecimal(listaFinanceiros.get(position).getValor_juros()) );
        else
            holder.finJuroDesconto.setText("0.00");
        if ( !listaFinanceiros.get(position).getValor_pago().isEmpty() ) {
            holder.finValorPago.setText(MascaraUtil.duasCasaDecimal(listaFinanceiros.get(position).getValor_pago()));
            try {
                if (listaFinanceiros.get(position).getData_pagamento().isEmpty())
                    holder.txvPagamento.setText("");
                else
                    holder.txvPagamento.setText("Pagamento:");
            } catch ( NullPointerException e) {
                holder.txvPagamento.setText("");
            }

        }else {
            holder.finValorPago.setText("0.00");
            holder.txvPagamento.setText("");
        }
        if ( !listaFinanceiros.get(position).getValor_saldo().isEmpty() )
            holder.finValorSaldo.setText( MascaraUtil.duasCasaDecimal(listaFinanceiros.get(position).getValor_saldo()) );
        else
            holder.finValorSaldo.setText("0.00");
        holder.itemView
                .setBackgroundColor(selectedItems.get(position) ? Color.parseColor("#dfdfdf")
                        : Color.TRANSPARENT);
        applyCLickEnvents(holder, position);
        System.gc();
    }

    @Override
    public int getItemCount() {
        if (listaFinanceiros != null)
            return listaFinanceiros.size();
        return 0;
    }

    public interface FinanceiroAdapterListener {
        void onClickListener(int position);
        void onLongClickListener(int position);
    }

    private void applyCLickEnvents(final FinanceiroViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }
}
