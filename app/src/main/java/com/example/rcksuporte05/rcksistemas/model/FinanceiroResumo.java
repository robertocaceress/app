package com.example.rcksuporte05.rcksistemas.model;

public class FinanceiroResumo {
    private String data_emissao;
    private String data_baixa;
    private String data_vencimento;
    private String data_competencia;
    private String valor_bruto;
    private String valor_total;
    private String valor_credito;
    private String valor_debito;
    private String valor_juros;
    private String valor_descontos;
    private String data;
    private int id_conta;
    private String nome_conta;
    private String debito_credito;
    private int total_docs;
    private String documento;
    private String parcela;
    private String nome_favorecido;
    private String id_bairro;
    private String id_cidade;
    private String id_regiao;
    private String id_favorecido;
    private String valor_recebimento;
    private String ativo;
    private String cpf_cnpj;
    private String docs;

    private String nome_fantasia;
    private String endereco;
    private String endereco_bairro;
    private String endereco_numero;
    private String endereco_complemento;
    private String endereco_uf;
    private String nome_municipio;
    private String endereco_cep;
    private String telefone_principal;
    private String telefone_dois;
    private String status;
    private String id_android;
    private String data_reagendamento;
    private String ordem;
    private String devedor;

    public String getData_emissao() {
        return data_emissao;
    }

    public void setData_emissao(String data_emissao) {
        this.data_emissao = data_emissao;
    }

    public String getData_baixa() {
        return data_baixa;
    }

    public void setData_baixa(String data_baixa) {
        this.data_baixa = data_baixa;
    }

    public String getValor_bruto() {
        return valor_bruto;
    }

    public void setValor_bruto(String valor_bruto) {
        this.valor_bruto = valor_bruto;
    }

    public String getValor_total() {
        return valor_total;
    }

    public void setValor_total(String valor_total) {
        this.valor_total = valor_total;
    }

    public String getValor_credito() {
        return valor_credito;
    }

    public void setValor_credito(String valor_credito) {
        this.valor_credito = valor_credito;
    }

    public String getValor_debito() {
        return valor_debito;
    }

    public void setValor_debito(String valor_debito) {
        this.valor_debito = valor_debito;
    }

    public String getValor_juros() {
        return valor_juros;
    }

    public void setValor_juros(String valor_juros) {
        this.valor_juros = valor_juros;
    }

    public String getValor_descontos() {
        return valor_descontos;
    }

    public void setValor_descontos(String valor_descontos) {
        this.valor_descontos = valor_descontos;
    }

    public int getId_conta() {
        return id_conta;
    }

    public void setId_conta(int id_conta) {
        this.id_conta = id_conta;
    }

    public String getNome_conta() {
        return nome_conta;
    }

    public void setNome_conta(String nome_conta) {
        this.nome_conta = nome_conta;
    }

    public String getDebito_credito() {
        return debito_credito;
    }

    public void setDebito_credito(String debito_credito) {
        this.debito_credito = debito_credito;
    }

    public int getTotal_docs() {
        return total_docs;
    }

    public void setTotal_docs(int total_docs) {
        this.total_docs = total_docs;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getParcela() {
        return parcela;
    }

    public void setParcela(String parcela) {
        this.parcela = parcela;
    }

    public String getNome_favorecido() {
        return nome_favorecido;
    }

    public void setNome_favorecido(String nome_favorecido) {
        this.nome_favorecido = nome_favorecido;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData_vencimento() {
        return data_vencimento;
    }

    public void setData_vencimento(String data_vencimento) {
        this.data_vencimento = data_vencimento;
    }

    public String getData_competencia() {
        return data_competencia;
    }

    public void setData_competencia(String data_competencia) {
        this.data_competencia = data_competencia;
    }

    public String getId_bairro() {
        return id_bairro;
    }

    public void setId_bairro(String id_bairro) {
        this.id_bairro = id_bairro;
    }

    public String getId_cidade() {
        return id_cidade;
    }

    public void setId_cidade(String id_cidade) {
        this.id_cidade = id_cidade;
    }

    public String getId_regiao() {
        return id_regiao;
    }

    public void setId_regiao(String id_regiao) {
        this.id_regiao = id_regiao;
    }

    public String getId_favorecido() {
        return id_favorecido;
    }

    public void setId_favorecido(String id_favorecido) {
        this.id_favorecido = id_favorecido;
    }

    public String getValor_recebimento() {
        return valor_recebimento;
    }

    public void setValor_recebimento(String valor_recebimento) {
        this.valor_recebimento = valor_recebimento;
    }

    public String getCpf_cnpj() {
        return cpf_cnpj;
    }

    public void setCpf_cnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    public String getDocs() {
        return docs;
    }

    public void setDocs(String docs) {
        this.docs = docs;
    }

    public String getNome_fantasia() {
        return nome_fantasia;
    }

    public void setNome_fantasia(String nome_fantasia) {
        this.nome_fantasia = nome_fantasia;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEndereco_bairro() {
        return endereco_bairro;
    }

    public void setEndereco_bairro(String endereco_bairro) {
        this.endereco_bairro = endereco_bairro;
    }

    public String getEndereco_numero() {
        return endereco_numero;
    }

    public void setEndereco_numero(String endereco_numero) {
        this.endereco_numero = endereco_numero;
    }

    public String getEndereco_complemento() {
        return endereco_complemento;
    }

    public void setEndereco_complemento(String endereco_complemento) {
        this.endereco_complemento = endereco_complemento;
    }

    public String getEndereco_uf() {
        return endereco_uf;
    }

    public void setEndereco_uf(String endereco_uf) {
        this.endereco_uf = endereco_uf;
    }

    public String getNome_municipio() {
        return nome_municipio;
    }

    public void setNome_municipio(String nome_municipio) {
        this.nome_municipio = nome_municipio;
    }

    public String getEndereco_cep() {
        return endereco_cep;
    }

    public void setEndereco_cep(String endereco_cep) {
        this.endereco_cep = endereco_cep;
    }

    public String getTelefone_principal() {
        return telefone_principal;
    }

    public void setTelefone_principal(String telefone_principal) {
        this.telefone_principal = telefone_principal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_android() {
        return id_android;
    }

    public void setId_android(String id_android) {
        this.id_android = id_android;
    }

    public String getTelefone_dois() {
        return telefone_dois;
    }

    public void setTelefone_dois(String telefone_dois) {
        this.telefone_dois = telefone_dois;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getData_reagendamento() {
        return data_reagendamento;
    }

    public void setData_reagendamento(String data_reagendamento) {
        this.data_reagendamento = data_reagendamento;
    }

    public String getOrdem() {
        return ordem;
    }

    public void setOrdem(String ordem) {
        this.ordem = ordem;
    }

    public String getDevedor() {
        return devedor;
    }

    public void setDevedor(String devedor) {
        this.devedor = devedor;
    }
}
