/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.rcksuporte05.rcksistemas;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.rckwhalle.rcksuporte05.rcksistemas";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "2.6.2-A DB V27";
}
