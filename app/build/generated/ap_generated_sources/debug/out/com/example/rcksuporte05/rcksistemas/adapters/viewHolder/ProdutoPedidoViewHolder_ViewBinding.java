// Generated code from Butter Knife. Do not modify!
package com.example.rcksuporte05.rcksistemas.adapters.viewHolder;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.rcksuporte05.rcksistemas.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProdutoPedidoViewHolder_ViewBinding implements Unbinder {
  private ProdutoPedidoViewHolder target;

  @UiThread
  public ProdutoPedidoViewHolder_ViewBinding(ProdutoPedidoViewHolder target, View source) {
    this.target = target;

    target.idPosition = Utils.findRequiredViewAsType(source, R.id.idPosition, "field 'idPosition'", TextView.class);
    target.nomeListaProduto = Utils.findRequiredViewAsType(source, R.id.nomeListaProduto, "field 'nomeListaProduto'", TextView.class);
    target.precoProduto = Utils.findRequiredViewAsType(source, R.id.precoProduto, "field 'precoProduto'", TextView.class);
    target.textViewUnidadeMedida = Utils.findRequiredViewAsType(source, R.id.textViewUnidadeMedida, "field 'textViewUnidadeMedida'", TextView.class);
    target.textViewCampanha = Utils.findRequiredViewAsType(source, R.id.textViewCampanha, "field 'textViewCampanha'", TextView.class);
    target.nomeCampanha = Utils.findRequiredViewAsType(source, R.id.nomeCampanha, "field 'nomeCampanha'", TextView.class);
    target.txtDesconto = Utils.findRequiredViewAsType(source, R.id.txtDesconto, "field 'txtDesconto'", TextView.class);
    target.itemView = Utils.findRequiredViewAsType(source, R.id.rlProdutoPedido, "field 'itemView'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProdutoPedidoViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.idPosition = null;
    target.nomeListaProduto = null;
    target.precoProduto = null;
    target.textViewUnidadeMedida = null;
    target.textViewCampanha = null;
    target.nomeCampanha = null;
    target.txtDesconto = null;
    target.itemView = null;
  }
}
