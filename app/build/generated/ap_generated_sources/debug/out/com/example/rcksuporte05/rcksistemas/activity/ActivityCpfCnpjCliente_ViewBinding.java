// Generated code from Butter Knife. Do not modify!
package com.example.rcksuporte05.rcksistemas.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.rcksuporte05.rcksistemas.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivityCpfCnpjCliente_ViewBinding implements Unbinder {
  private ActivityCpfCnpjCliente target;

  @UiThread
  public ActivityCpfCnpjCliente_ViewBinding(ActivityCpfCnpjCliente target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivityCpfCnpjCliente_ViewBinding(ActivityCpfCnpjCliente target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtCpfCnpj = Utils.findRequiredViewAsType(source, R.id.txtCpfCnpj, "field 'txtCpfCnpj'", TextView.class);
    target.edtCpfCnpj = Utils.findRequiredViewAsType(source, R.id.edtCpfCnpj, "field 'edtCpfCnpj'", EditText.class);
    target.btnVerificar = Utils.findRequiredViewAsType(source, R.id.btnVerificar, "field 'btnVerificar'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivityCpfCnpjCliente target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtCpfCnpj = null;
    target.edtCpfCnpj = null;
    target.btnVerificar = null;
  }
}
