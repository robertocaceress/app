package com.example.modelapi.Model;

public class Cliente {

    private String ativo;
    private String id_empresa;
    private String id_cadastro;
    private String pessoa_f_j;
    private String data_aniversario;
    private String nome_cadastro;
    private String nome_fantasia;
    private String cpf_cnpj;
    private String inscri_estadual;
    private String inscri_municipal;
    private String endereco;
    private String endereco_bairro;
    private String endereco_numero;
    private String endereco_complemento;
    private String endereco_uf;
    private String endereco_id_municipio;
    private String endereco_cep;
    private String usuario_id;
    private String usuario_nome;
    private String usuario_data;
    private String f_cliente;
    private String f_fornecedor;
    private String f_funcionario;
    private String f_vendedor;
    private String f_transportador;
    private String data_ultima_compra;
    private String nome_vendedor;
    private String f_id_cliente;
    private String id_entidade;
    private String f_id_fornecedor;
    private String f_id_vendedor;
    private String f_id_transportador;
    private String telefone_principal;
    private String email_principal;
    private String nome_pais;
    private String f_id_funcionario;
    private String avisar_com_dias;
    private String observacoes;
    private String padrao_id_c_custo;
    private String padrao_id_c_gerenciadora;
    private String padrao_id_c_analitica;
    private String cob_endereco;
    private String cob_endereco_bairro;
    private String cob_endereco_numero;
    private String cob_endereco_complemento;
    private String cob_endereco_uf;
    private String cob_endereco_id_municipio;
    private String cob_endereco_cep;
    private String nome_pais_cob;
    private String limite_credito;
    private String limite_disponivel;
    private String pessoa_contato_financeiro;
    private String email_financeiro;
    private String observacoes_faturamento;
    private String observacoes_financeiro;
    private String telefone_dois;
    private String telefone_tres;
    private String pessoa_contato_principal;
    private String ind_da_ie_destinatario;
    private String comissao_percentual;
    private String id_setor;
    private String nfe_email_enviar;
    private String nfe_email_um;
    private String nfe_email_dois;
    private String nfe_email_tres;
    private String nfe_email_quatro;
    private String nfe_email_cinco;
    private String id_grupo_vendedor;
    private String vendedor_usa_portal;
    private String vendedor_id_user_portal;
    private String f_tarifa;
    private String f_id_tarifa;
    private String f_produtor;
    private String rg_numero;
    private String rg_ssp;
    private String conta_contabil;
    private String motorista;
    private String f_id_motorista;
    private String habilitacao_numero;
    private String habilitacao_categoria;
    private String habilitacao_vencimento;
    private String mot_id_transportadora;
    private String local_cadastro;

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(String id_empresa) {
        this.id_empresa = id_empresa;
    }

    public String getId_cadastro() {
        return id_cadastro;
    }

    public void setId_cadastro(String id_cadastro) {
        this.id_cadastro = id_cadastro;
    }

    public String getPessoa_f_j() {
        return pessoa_f_j;
    }

    public void setPessoa_f_j(String pessoa_f_j) {
        this.pessoa_f_j = pessoa_f_j;
    }

    public String getData_aniversario() {
        return data_aniversario;
    }

    public void setData_aniversario(String data_aniversario) {
        this.data_aniversario = data_aniversario;
    }

    public String getNome_cadastro() {
        return nome_cadastro;
    }

    public void setNome_cadastro(String nome_cadastro) {
        this.nome_cadastro = nome_cadastro;
    }

    public String getNome_fantasia() {
        return nome_fantasia;
    }

    public void setNome_fantasia(String nome_fantasia) {
        this.nome_fantasia = nome_fantasia;
    }

    public String getCpf_cnpj() {
        return cpf_cnpj;
    }

    public void setCpf_cnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    public String getInscri_estadual() {
        return inscri_estadual;
    }

    public void setInscri_estadual(String inscri_estadual) {
        this.inscri_estadual = inscri_estadual;
    }

    public String getInscri_municipal() {
        return inscri_municipal;
    }

    public void setInscri_municipal(String inscri_municipal) {
        this.inscri_municipal = inscri_municipal;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEndereco_bairro() {
        return endereco_bairro;
    }

    public void setEndereco_bairro(String endereco_bairro) {
        this.endereco_bairro = endereco_bairro;
    }

    public String getEndereco_numero() {
        return endereco_numero;
    }

    public void setEndereco_numero(String endereco_numero) {
        this.endereco_numero = endereco_numero;
    }

    public String getEndereco_complemento() {
        return endereco_complemento;
    }

    public void setEndereco_complemento(String endereco_complemento) {
        this.endereco_complemento = endereco_complemento;
    }

    public String getEndereco_uf() {
        return endereco_uf;
    }

    public void setEndereco_uf(String endereco_uf) {
        this.endereco_uf = endereco_uf;
    }

    public String getEndereco_id_municipio() {
        return endereco_id_municipio;
    }

    public void setEndereco_id_municipio(String endereco_id_municipio) {
        this.endereco_id_municipio = endereco_id_municipio;
    }

    public String getEndereco_cep() {
        return endereco_cep;
    }

    public void setEndereco_cep(String endereco_cep) {
        this.endereco_cep = endereco_cep;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getUsuario_nome() {
        return usuario_nome;
    }

    public void setUsuario_nome(String usuario_nome) {
        this.usuario_nome = usuario_nome;
    }

    public String getUsuario_data() {
        return usuario_data;
    }

    public void setUsuario_data(String usuario_data) {
        this.usuario_data = usuario_data;
    }

    public String getF_cliente() {
        return f_cliente;
    }

    public void setF_cliente(String f_cliente) {
        this.f_cliente = f_cliente;
    }

    public String getF_fornecedor() {
        return f_fornecedor;
    }

    public void setF_fornecedor(String f_fornecedor) {
        this.f_fornecedor = f_fornecedor;
    }

    public String getF_funcionario() {
        return f_funcionario;
    }

    public void setF_funcionario(String f_funcionario) {
        this.f_funcionario = f_funcionario;
    }

    public String getF_vendedor() {
        return f_vendedor;
    }

    public void setF_vendedor(String f_vendedor) {
        this.f_vendedor = f_vendedor;
    }

    public String getF_transportador() {
        return f_transportador;
    }

    public void setF_transportador(String f_transportador) {
        this.f_transportador = f_transportador;
    }

    public String getData_ultima_compra() {
        return data_ultima_compra;
    }

    public void setData_ultima_compra(String data_ultima_compra) {
        this.data_ultima_compra = data_ultima_compra;
    }

    public String getF_id_cliente() {
        return f_id_cliente;
    }

    public void setF_id_cliente(String f_id_cliente) {
        this.f_id_cliente = f_id_cliente;
    }

    public String getId_entidade() {
        return id_entidade;
    }

    public void setId_entidade(String id_entidade) {
        this.id_entidade = id_entidade;
    }

    public String getF_id_fornecedor() {
        return f_id_fornecedor;
    }

    public void setF_id_fornecedor(String f_id_fornecedor) {
        this.f_id_fornecedor = f_id_fornecedor;
    }

    public String getF_id_vendedor() {
        return f_id_vendedor;
    }

    public void setF_id_vendedor(String f_id_vendedor) {
        this.f_id_vendedor = f_id_vendedor;
    }

    public String getF_id_transportador() {
        return f_id_transportador;
    }

    public void setF_id_transportador(String f_id_transportador) {
        this.f_id_transportador = f_id_transportador;
    }

    public String getTelefone_principal() {
        return telefone_principal;
    }

    public void setTelefone_principal(String telefone_principal) {
        this.telefone_principal = telefone_principal;
    }

    public String getEmail_principal() {
        return email_principal;
    }

    public void setEmail_principal(String email_principal) {
        this.email_principal = email_principal;
    }

    public String getF_id_funcionario() {
        return f_id_funcionario;
    }

    public void setF_id_funcionario(String f_id_funcionario) {
        this.f_id_funcionario = f_id_funcionario;
    }

    public String getAvisar_com_dias() {
        return avisar_com_dias;
    }

    public void setAvisar_com_dias(String avisar_com_dias) {
        this.avisar_com_dias = avisar_com_dias;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getPadrao_id_c_custo() {
        return padrao_id_c_custo;
    }

    public void setPadrao_id_c_custo(String padrao_id_c_custo) {
        this.padrao_id_c_custo = padrao_id_c_custo;
    }

    public String getPadrao_id_c_gerenciadora() {
        return padrao_id_c_gerenciadora;
    }

    public void setPadrao_id_c_gerenciadora(String padrao_id_c_gerenciadora) {
        this.padrao_id_c_gerenciadora = padrao_id_c_gerenciadora;
    }

    public String getPadrao_id_c_analitica() {
        return padrao_id_c_analitica;
    }

    public void setPadrao_id_c_analitica(String padrao_id_c_analitica) {
        this.padrao_id_c_analitica = padrao_id_c_analitica;
    }

    public String getCob_endereco() {
        return cob_endereco;
    }

    public void setCob_endereco(String cob_endereco) {
        this.cob_endereco = cob_endereco;
    }

    public String getCob_endereco_bairro() {
        return cob_endereco_bairro;
    }

    public void setCob_endereco_bairro(String cob_endereco_bairro) {
        this.cob_endereco_bairro = cob_endereco_bairro;
    }

    public String getCob_endereco_numero() {
        return cob_endereco_numero;
    }

    public void setCob_endereco_numero(String cob_endereco_numero) {
        this.cob_endereco_numero = cob_endereco_numero;
    }

    public String getCob_endereco_complemento() {
        return cob_endereco_complemento;
    }

    public void setCob_endereco_complemento(String cob_endereco_complemento) {
        this.cob_endereco_complemento = cob_endereco_complemento;
    }

    public String getCob_endereco_uf() {
        return cob_endereco_uf;
    }

    public void setCob_endereco_uf(String cob_endereco_uf) {
        this.cob_endereco_uf = cob_endereco_uf;
    }

    public String getCob_endereco_id_municipio() {
        return cob_endereco_id_municipio;
    }

    public void setCob_endereco_id_municipio(String cob_endereco_id_municipio) {
        this.cob_endereco_id_municipio = cob_endereco_id_municipio;
    }

    public String getCob_endereco_cep() {
        return cob_endereco_cep;
    }

    public void setCob_endereco_cep(String cob_endereco_cep) {
        this.cob_endereco_cep = cob_endereco_cep;
    }

    public String getNome_pais_cob() {
        return nome_pais_cob;
    }

    public void setNome_pais_cob(String nome_pais_cob) {
        this.nome_pais_cob = nome_pais_cob;
    }

    public String getLimite_credito() {
        return limite_credito;
    }

    public void setLimite_credito(String limite_credito) {
        this.limite_credito = limite_credito;
    }

    public String getLimite_disponivel() {
        return limite_disponivel;
    }

    public void setLimite_disponivel(String limite_disponivel) {
        this.limite_disponivel = limite_disponivel;
    }

    public String getPessoa_contato_financeiro() {
        return pessoa_contato_financeiro;
    }

    public void setPessoa_contato_financeiro(String pessoa_contato_financeiro) {
        this.pessoa_contato_financeiro = pessoa_contato_financeiro;
    }

    public String getEmail_financeiro() {
        return email_financeiro;
    }

    public void setEmail_financeiro(String email_financeiro) {
        this.email_financeiro = email_financeiro;
    }

    public String getObservacoes_faturamento() {
        return observacoes_faturamento;
    }

    public void setObservacoes_faturamento(String observacoes_faturamento) {
        this.observacoes_faturamento = observacoes_faturamento;
    }

    public String getObservacoes_financeiro() {
        return observacoes_financeiro;
    }

    public void setObservacoes_financeiro(String observacoes_financeiro) {
        this.observacoes_financeiro = observacoes_financeiro;
    }

    public String getTelefone_dois() {
        return telefone_dois;
    }

    public void setTelefone_dois(String telefone_dois) {
        this.telefone_dois = telefone_dois;
    }

    public String getTelefone_tres() {
        return telefone_tres;
    }

    public void setTelefone_tres(String telefone_tres) {
        this.telefone_tres = telefone_tres;
    }

    public String getPessoa_contato_principal() {
        return pessoa_contato_principal;
    }

    public void setPessoa_contato_principal(String pessoa_contato_principal) {
        this.pessoa_contato_principal = pessoa_contato_principal;
    }

    public String getInd_da_ie_destinatario() {
        return ind_da_ie_destinatario;
    }

    public void setInd_da_ie_destinatario(String ind_da_ie_destinatario) {
        this.ind_da_ie_destinatario = ind_da_ie_destinatario;
    }

    public String getComissao_percentual() {
        return comissao_percentual;
    }

    public void setComissao_percentual(String comissao_percentual) {
        this.comissao_percentual = comissao_percentual;
    }

    public String getId_setor() {
        return id_setor;
    }

    public void setId_setor(String id_setor) {
        this.id_setor = id_setor;
    }

    public String getNfe_email_enviar() {
        return nfe_email_enviar;
    }

    public void setNfe_email_enviar(String nfe_email_enviar) {
        this.nfe_email_enviar = nfe_email_enviar;
    }

    public String getNfe_email_um() {
        return nfe_email_um;
    }

    public void setNfe_email_um(String nfe_email_um) {
        this.nfe_email_um = nfe_email_um;
    }

    public String getNfe_email_dois() {
        return nfe_email_dois;
    }

    public void setNfe_email_dois(String nfe_email_dois) {
        this.nfe_email_dois = nfe_email_dois;
    }

    public String getNfe_email_tres() {
        return nfe_email_tres;
    }

    public void setNfe_email_tres(String nfe_email_tres) {
        this.nfe_email_tres = nfe_email_tres;
    }

    public String getNfe_email_quatro() {
        return nfe_email_quatro;
    }

    public void setNfe_email_quatro(String nfe_email_quatro) {
        this.nfe_email_quatro = nfe_email_quatro;
    }

    public String getNfe_email_cinco() {
        return nfe_email_cinco;
    }

    public void setNfe_email_cinco(String nfe_email_cinco) {
        this.nfe_email_cinco = nfe_email_cinco;
    }

    public String getId_grupo_vendedor() {
        return id_grupo_vendedor;
    }

    public void setId_grupo_vendedor(String id_grupo_vendedor) {
        this.id_grupo_vendedor = id_grupo_vendedor;
    }

    public String getVendedor_usa_portal() {
        return vendedor_usa_portal;
    }

    public void setVendedor_usa_portal(String vendedor_usa_portal) {
        this.vendedor_usa_portal = vendedor_usa_portal;
    }

    public String getVendedor_id_user_portal() {
        return vendedor_id_user_portal;
    }

    public void setVendedor_id_user_portal(String vendedor_id_user_portal) {
        this.vendedor_id_user_portal = vendedor_id_user_portal;
    }

    public String getF_tarifa() {
        return f_tarifa;
    }

    public void setF_tarifa(String f_tarifa) {
        this.f_tarifa = f_tarifa;
    }

    public String getF_id_tarifa() {
        return f_id_tarifa;
    }

    public void setF_id_tarifa(String f_id_tarifa) {
        this.f_id_tarifa = f_id_tarifa;
    }

    public String getF_produtor() {
        return f_produtor;
    }

    public void setF_produtor(String f_produtor) {
        this.f_produtor = f_produtor;
    }

    public String getRg_numero() {
        return rg_numero;
    }

    public void setRg_numero(String rg_numero) {
        this.rg_numero = rg_numero;
    }

    public String getRg_ssp() {
        return rg_ssp;
    }

    public void setRg_ssp(String rg_ssp) {
        this.rg_ssp = rg_ssp;
    }

    public String getConta_contabil() {
        return conta_contabil;
    }

    public void setConta_contabil(String conta_contabil) {
        this.conta_contabil = conta_contabil;
    }

    public String getMotorista() {
        return motorista;
    }

    public void setMotorista(String motorista) {
        this.motorista = motorista;
    }

    public String getF_id_motorista() {
        return f_id_motorista;
    }

    public void setF_id_motorista(String f_id_motorista) {
        this.f_id_motorista = f_id_motorista;
    }

    public String getHabilitacao_numero() {
        return habilitacao_numero;
    }

    public void setHabilitacao_numero(String habilitacao_numero) {
        this.habilitacao_numero = habilitacao_numero;
    }

    public String getHabilitacao_categoria() {
        return habilitacao_categoria;
    }

    public void setHabilitacao_categoria(String habilitacao_categoria) {
        this.habilitacao_categoria = habilitacao_categoria;
    }

    public String getHabilitacao_vencimento() {
        return habilitacao_vencimento;
    }

    public void setHabilitacao_vencimento(String habilitacao_vencimento) {
        this.habilitacao_vencimento = habilitacao_vencimento;
    }

    public String getMot_id_transportadora() {
        return mot_id_transportadora;
    }

    public void setMot_id_transportadora(String mot_id_transportadora) {
        this.mot_id_transportadora = mot_id_transportadora;
    }

    public String getLocal_cadastro() {
        return local_cadastro;
    }

    public void setLocal_cadastro(String local_cadastro) {
        this.local_cadastro = local_cadastro;
    }

    public String getNome_vendedor() {
        return nome_vendedor;
    }

    public void setNome_vendedor(String nome_vendedor) {
        this.nome_vendedor = nome_vendedor;
    }

    public String getNome_pais() {
        return nome_pais;
    }

    public void setNome_pais(String nome_pais) {
        this.nome_pais = nome_pais;
    }
}
